--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activity_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE activity_log (
    id bigint NOT NULL,
    log_name character varying(255),
    description text NOT NULL,
    subject_type character varying(255),
    subject_id bigint,
    causer_type character varying(255),
    causer_id bigint,
    properties json,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE activity_log OWNER TO postgres;

--
-- Name: activity_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE activity_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE activity_log_id_seq OWNER TO postgres;

--
-- Name: activity_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE activity_log_id_seq OWNED BY activity_log.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE failed_jobs OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE failed_jobs_id_seq OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE failed_jobs_id_seq OWNED BY failed_jobs.id;


--
-- Name: his_getbphtb_bpn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE his_getbphtb_bpn (
    t_idhis_getbpn integer NOT NULL,
    t_username character varying(255),
    t_password character varying(255),
    t_nop character varying(255),
    t_ntpd character varying(255),
    t_nik character varying(255),
    t_nama character varying(255),
    t_alamat text,
    t_kelurahan_op character varying(255),
    t_kecamatan_op character varying(255),
    t_kota_kab_op character varying(255),
    t_luastanah double precision,
    t_luasbangunan double precision,
    t_pembayaran character varying(255),
    t_status character varying(255),
    t_tanggal_pembayaran timestamp(0) without time zone,
    t_jenisbayar character varying(255),
    respon_code character varying(255),
    t_iduser integer,
    t_tglby_system timestamp(0) without time zone DEFAULT now() NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE his_getbphtb_bpn OWNER TO postgres;

--
-- Name: his_getbphtb_bpn_t_idhis_getbpn_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE his_getbphtb_bpn_t_idhis_getbpn_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE his_getbphtb_bpn_t_idhis_getbpn_seq OWNER TO postgres;

--
-- Name: his_getbphtb_bpn_t_idhis_getbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE his_getbphtb_bpn_t_idhis_getbpn_seq OWNED BY his_getbphtb_bpn.t_idhis_getbpn;


--
-- Name: his_getpbb_bpn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE his_getpbb_bpn (
    t_idhis_pbbbpn integer NOT NULL,
    t_username character varying(255) NOT NULL,
    t_password character varying(255) NOT NULL,
    t_nop character varying(255) NOT NULL,
    t_nik character varying(255) NOT NULL,
    t_nama_wp character varying(255) NOT NULL,
    t_alamat_op text NOT NULL,
    t_kecamatan_op character varying(255) NOT NULL,
    t_kelurahan_op character varying(255) NOT NULL,
    t_kota_kab_op character varying(255) NOT NULL,
    t_luas_tanah_op double precision,
    t_luas_bangunan_op double precision,
    t_njop_tanah_op double precision,
    t_njop_bangunan_op double precision,
    t_status_tunggakan character varying(255) NOT NULL,
    t_iduser integer,
    t_tglby_system timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE his_getpbb_bpn OWNER TO postgres;

--
-- Name: his_getpbb_bpn_t_idhis_pbbbpn_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE his_getpbb_bpn_t_idhis_pbbbpn_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE his_getpbb_bpn_t_idhis_pbbbpn_seq OWNER TO postgres;

--
-- Name: his_getpbb_bpn_t_idhis_pbbbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE his_getpbb_bpn_t_idhis_pbbbpn_seq OWNED BY his_getpbb_bpn.t_idhis_pbbbpn;


--
-- Name: his_postdatabpn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE his_postdatabpn (
    t_idhis_posbpn integer NOT NULL,
    t_username character varying(255) NOT NULL,
    t_password character varying(255) NOT NULL,
    t_aktaid character varying(255) NOT NULL,
    t_tgl_akta timestamp(0) without time zone NOT NULL,
    t_nop character varying(255) NOT NULL,
    t_nik character varying(255) NOT NULL,
    t_nib character varying(255) NOT NULL,
    t_npwp character varying(255) NOT NULL,
    t_nama_wp character varying(255) NOT NULL,
    t_alamat_op text NOT NULL,
    t_kecamatan_op character varying(255) NOT NULL,
    t_kelurahan_op character varying(255) NOT NULL,
    t_kota_kab_op character varying(255) NOT NULL,
    t_luastanah_op double precision,
    t_luasbangunan_op double precision,
    t_no_sertipikat character varying(255) NOT NULL,
    t_no_akta character varying(255) NOT NULL,
    t_ppat character varying(255) NOT NULL,
    t_status character varying(255) NOT NULL,
    t_iduser integer,
    t_tglby_system timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE his_postdatabpn OWNER TO postgres;

--
-- Name: his_postdatabpn_t_idhis_posbpn_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE his_postdatabpn_t_idhis_posbpn_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE his_postdatabpn_t_idhis_posbpn_seq OWNER TO postgres;

--
-- Name: his_postdatabpn_t_idhis_posbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE his_postdatabpn_t_idhis_posbpn_seq OWNED BY his_postdatabpn.t_idhis_posbpn;


--
-- Name: integrasi_sertifikat_bpn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE integrasi_sertifikat_bpn (
    t_idsertifikat integer NOT NULL,
    aktaid character varying(255) NOT NULL,
    tgl_akta timestamp(0) without time zone NOT NULL,
    nop character varying(255) NOT NULL,
    nib character varying(255) NOT NULL,
    nik character varying(255) NOT NULL,
    npwp character varying(255) NOT NULL,
    nama_wp character varying(255) NOT NULL,
    alamat_op character varying(255) NOT NULL,
    kelurahan_op character varying(255) NOT NULL,
    kecamatan_op character varying(255) NOT NULL,
    kota_op character varying(255) NOT NULL,
    luastanah_op double precision,
    luasbangunan_op double precision,
    ppat character varying(255) NOT NULL,
    no_sertipikat character varying(255) NOT NULL,
    no_akta character varying(255) NOT NULL,
    t_iduser integer,
    t_tgltransaksi timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE integrasi_sertifikat_bpn OWNER TO postgres;

--
-- Name: integrasi_sertifikat_bpn_t_idsertifikat_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE integrasi_sertifikat_bpn_t_idsertifikat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE integrasi_sertifikat_bpn_t_idsertifikat_seq OWNER TO postgres;

--
-- Name: integrasi_sertifikat_bpn_t_idsertifikat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE integrasi_sertifikat_bpn_t_idsertifikat_seq OWNED BY integrasi_sertifikat_bpn.t_idsertifikat;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- Name: model_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE model_has_permissions OWNER TO postgres;

--
-- Name: model_has_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE model_has_roles OWNER TO postgres;

--
-- Name: notifications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE notifications (
    id uuid NOT NULL,
    type character varying(255) NOT NULL,
    notifiable_type character varying(255) NOT NULL,
    notifiable_id bigint NOT NULL,
    data text NOT NULL,
    read_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE notifications OWNER TO postgres;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE password_resets OWNER TO postgres;

--
-- Name: permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE permissions OWNER TO postgres;

--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE permissions_id_seq OWNER TO postgres;

--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE permissions_id_seq OWNED BY permissions.id;


--
-- Name: role_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE role_has_permissions OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: s_acuan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_acuan (
    s_idacuan integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_kd_blok character varying(3),
    s_permetertanah double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_acuan OWNER TO postgres;

--
-- Name: s_acuan_jenistanah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_acuan_jenistanah (
    s_idacuan_jenis integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_kd_blok character varying(3),
    id_jenistanah integer,
    s_permetertanah double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_acuan_jenistanah OWNER TO postgres;

--
-- Name: s_acuan_jenistanah_s_idacuan_jenis_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_acuan_jenistanah_s_idacuan_jenis_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_acuan_jenistanah_s_idacuan_jenis_seq OWNER TO postgres;

--
-- Name: s_acuan_jenistanah_s_idacuan_jenis_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_acuan_jenistanah_s_idacuan_jenis_seq OWNED BY s_acuan_jenistanah.s_idacuan_jenis;


--
-- Name: s_acuan_s_idacuan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_acuan_s_idacuan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_acuan_s_idacuan_seq OWNER TO postgres;

--
-- Name: s_acuan_s_idacuan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_acuan_s_idacuan_seq OWNED BY s_acuan.s_idacuan;


--
-- Name: s_background; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_background (
    id bigint NOT NULL,
    s_thumbnail text,
    s_id_status integer NOT NULL,
    s_iduser integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_background OWNER TO postgres;

--
-- Name: s_background_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_background_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_background_id_seq OWNER TO postgres;

--
-- Name: s_background_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_background_id_seq OWNED BY s_background.id;


--
-- Name: s_hak_akses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_hak_akses (
    s_id_hakakses integer NOT NULL,
    s_nama_hakakses character varying(255)
);


ALTER TABLE s_hak_akses OWNER TO postgres;

--
-- Name: s_hak_akses_s_id_hakakses_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_hak_akses_s_id_hakakses_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_hak_akses_s_id_hakakses_seq OWNER TO postgres;

--
-- Name: s_hak_akses_s_id_hakakses_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_hak_akses_s_id_hakakses_seq OWNED BY s_hak_akses.s_id_hakakses;


--
-- Name: s_harga_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_harga_history (
    s_idhistory integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_kd_blok character varying(3),
    s_kd_urut character varying(4),
    s_kd_jenis integer,
    s_tahun_sppt integer,
    s_permetertanah double precision,
    id_jenistanah integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_harga_history OWNER TO postgres;

--
-- Name: s_harga_history_s_idhistory_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_harga_history_s_idhistory_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_harga_history_s_idhistory_seq OWNER TO postgres;

--
-- Name: s_harga_history_s_idhistory_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_harga_history_s_idhistory_seq OWNED BY s_harga_history.s_idhistory;


--
-- Name: s_jenis_bidangusaha; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_jenis_bidangusaha (
    s_idbidang_usaha integer NOT NULL,
    s_nama_bidangusaha character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_jenis_bidangusaha OWNER TO postgres;

--
-- Name: s_jenis_bidangusaha_s_idbidang_usaha_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_jenis_bidangusaha_s_idbidang_usaha_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_jenis_bidangusaha_s_idbidang_usaha_seq OWNER TO postgres;

--
-- Name: s_jenis_bidangusaha_s_idbidang_usaha_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_jenis_bidangusaha_s_idbidang_usaha_seq OWNED BY s_jenis_bidangusaha.s_idbidang_usaha;


--
-- Name: s_jenisdoktanah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_jenisdoktanah (
    s_iddoktanah integer NOT NULL,
    s_namadoktanah text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_jenisdoktanah OWNER TO postgres;

--
-- Name: s_jenisdoktanah_s_iddoktanah_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_jenisdoktanah_s_iddoktanah_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_jenisdoktanah_s_iddoktanah_seq OWNER TO postgres;

--
-- Name: s_jenisdoktanah_s_iddoktanah_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_jenisdoktanah_s_iddoktanah_seq OWNED BY s_jenisdoktanah.s_iddoktanah;


--
-- Name: s_jenisfasilitas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_jenisfasilitas (
    s_idjenisfasilitas integer NOT NULL,
    s_kodejenisfasilitas character varying(5),
    s_namajenisfasilitas character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_jenisfasilitas OWNER TO postgres;

--
-- Name: s_jenisfasilitas_s_idjenisfasilitas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_jenisfasilitas_s_idjenisfasilitas_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_jenisfasilitas_s_idjenisfasilitas_seq OWNER TO postgres;

--
-- Name: s_jenisfasilitas_s_idjenisfasilitas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_jenisfasilitas_s_idjenisfasilitas_seq OWNED BY s_jenisfasilitas.s_idjenisfasilitas;


--
-- Name: s_jenishaktanah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_jenishaktanah (
    s_idhaktanah integer NOT NULL,
    s_kodehaktanah character varying(10),
    s_namahaktanah text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_jenishaktanah OWNER TO postgres;

--
-- Name: s_jenishaktanah_s_idhaktanah_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_jenishaktanah_s_idhaktanah_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_jenishaktanah_s_idhaktanah_seq OWNER TO postgres;

--
-- Name: s_jenishaktanah_s_idhaktanah_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_jenishaktanah_s_idhaktanah_seq OWNED BY s_jenishaktanah.s_idhaktanah;


--
-- Name: s_jenisketetapan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_jenisketetapan (
    s_idjenisketetapan integer NOT NULL,
    s_namajenisketetapan character varying(255) NOT NULL,
    s_namasingkatjenisketetapan character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_jenisketetapan OWNER TO postgres;

--
-- Name: s_jenisketetapan_s_idjenisketetapan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_jenisketetapan_s_idjenisketetapan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_jenisketetapan_s_idjenisketetapan_seq OWNER TO postgres;

--
-- Name: s_jenisketetapan_s_idjenisketetapan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_jenisketetapan_s_idjenisketetapan_seq OWNED BY s_jenisketetapan.s_idjenisketetapan;


--
-- Name: s_jenispengurangan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_jenispengurangan (
    s_idpengurangan integer NOT NULL,
    s_persentase integer,
    s_namapengurangan text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_jenispengurangan OWNER TO postgres;

--
-- Name: s_jenispengurangan_s_idpengurangan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_jenispengurangan_s_idpengurangan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_jenispengurangan_s_idpengurangan_seq OWNER TO postgres;

--
-- Name: s_jenispengurangan_s_idpengurangan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_jenispengurangan_s_idpengurangan_seq OWNED BY s_jenispengurangan.s_idpengurangan;


--
-- Name: s_jenistanah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_jenistanah (
    id_jenistanah integer NOT NULL,
    nama_jenistanah character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_jenistanah OWNER TO postgres;

--
-- Name: s_jenistanah_id_jenistanah_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_jenistanah_id_jenistanah_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_jenistanah_id_jenistanah_seq OWNER TO postgres;

--
-- Name: s_jenistanah_id_jenistanah_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_jenistanah_id_jenistanah_seq OWNED BY s_jenistanah.id_jenistanah;


--
-- Name: s_jenistransaksi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_jenistransaksi (
    s_idjenistransaksi integer NOT NULL,
    s_kodejenistransaksi character varying(5),
    s_namajenistransaksi character varying(255),
    s_idstatus_pht integer,
    s_id_dptnpoptkp integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_jenistransaksi OWNER TO postgres;

--
-- Name: s_jenistransaksi_s_idjenistransaksi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_jenistransaksi_s_idjenistransaksi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_jenistransaksi_s_idjenistransaksi_seq OWNER TO postgres;

--
-- Name: s_jenistransaksi_s_idjenistransaksi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_jenistransaksi_s_idjenistransaksi_seq OWNED BY s_jenistransaksi.s_idjenistransaksi;


--
-- Name: s_kecamatan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_kecamatan (
    s_idkecamatan integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_namakecamatan text NOT NULL,
    s_latitude character varying(255),
    s_longitude character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_kecamatan OWNER TO postgres;

--
-- Name: s_kecamatan_s_idkecamatan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_kecamatan_s_idkecamatan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_kecamatan_s_idkecamatan_seq OWNER TO postgres;

--
-- Name: s_kecamatan_s_idkecamatan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_kecamatan_s_idkecamatan_seq OWNED BY s_kecamatan.s_idkecamatan;


--
-- Name: s_kelurahan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_kelurahan (
    s_idkelurahan integer NOT NULL,
    s_idkecamatan integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_namakelurahan text NOT NULL,
    s_latitude character varying(255),
    s_longitude character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_kelurahan OWNER TO postgres;

--
-- Name: s_kelurahan_s_idkelurahan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_kelurahan_s_idkelurahan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_kelurahan_s_idkelurahan_seq OWNER TO postgres;

--
-- Name: s_kelurahan_s_idkelurahan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_kelurahan_s_idkelurahan_seq OWNED BY s_kelurahan.s_idkelurahan;


--
-- Name: s_koderekening; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_koderekening (
    s_korekid integer NOT NULL,
    s_korektipe character varying(2) NOT NULL,
    s_korekkelompok character varying(2) NOT NULL,
    s_korekjenis character varying(2) NOT NULL,
    s_korekobjek character varying(4) NOT NULL,
    s_korekrincian character varying(4) NOT NULL,
    s_korekrinciansub character varying(10) NOT NULL,
    s_koreknama character varying(255) NOT NULL,
    s_korekketerangan text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_koderekening OWNER TO postgres;

--
-- Name: s_koderekening_s_korekid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_koderekening_s_korekid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_koderekening_s_korekid_seq OWNER TO postgres;

--
-- Name: s_koderekening_s_korekid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_koderekening_s_korekid_seq OWNED BY s_koderekening.s_korekid;


--
-- Name: s_notaris; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_notaris (
    s_idnotaris integer NOT NULL,
    s_namanotaris character varying(255),
    s_alamatnotaris text,
    s_kodenotaris character varying(255),
    s_sknotaris character varying(255),
    s_noid_bpn character varying(255),
    s_tgl1notaris timestamp(0) without time zone,
    s_tgl2notaris timestamp(0) without time zone,
    s_statusnotaris integer,
    s_npwpd_notaris character varying(255),
    s_daerahkerja text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_notaris OWNER TO postgres;

--
-- Name: s_notaris_s_idnotaris_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_notaris_s_idnotaris_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_notaris_s_idnotaris_seq OWNER TO postgres;

--
-- Name: s_notaris_s_idnotaris_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_notaris_s_idnotaris_seq OWNED BY s_notaris.s_idnotaris;


--
-- Name: s_pejabat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_pejabat (
    s_idpejabat integer NOT NULL,
    s_namapejabat text NOT NULL,
    s_jabatanpejabat text,
    s_nippejabat character varying(255),
    s_golonganpejabat text,
    s_alamat text,
    s_filettd text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_pejabat OWNER TO postgres;

--
-- Name: s_pejabat_s_idpejabat_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_pejabat_s_idpejabat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_pejabat_s_idpejabat_seq OWNER TO postgres;

--
-- Name: s_pejabat_s_idpejabat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_pejabat_s_idpejabat_seq OWNED BY s_pejabat.s_idpejabat;


--
-- Name: s_pemda; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_pemda (
    s_idpemda integer NOT NULL,
    s_namaprov character varying(255),
    s_namakabkot character varying(255),
    s_namaibukotakabkot character varying(255),
    s_kodeprovinsi character varying(3),
    s_kodekabkot character varying(4),
    s_namainstansi character varying(255),
    s_namasingkatinstansi character varying(255),
    s_alamatinstansi text,
    s_notelinstansi character varying(15),
    s_namabank character varying(255),
    s_norekbank character varying(255),
    s_logo text,
    s_namacabang character varying(255),
    s_kodecabang character varying(255),
    s_kodepos character varying(5),
    s_email text,
    s_urlbphtb character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_pemda OWNER TO postgres;

--
-- Name: s_pemda_s_idpemda_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_pemda_s_idpemda_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_pemda_s_idpemda_seq OWNER TO postgres;

--
-- Name: s_pemda_s_idpemda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_pemda_s_idpemda_seq OWNED BY s_pemda.s_idpemda;


--
-- Name: s_persyaratan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_persyaratan (
    s_idpersyaratan integer NOT NULL,
    s_idjenistransaksi integer NOT NULL,
    s_namapersyaratan character varying(255),
    s_idwajib_up integer,
    s_lokasimenu integer,
    "order" integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_persyaratan OWNER TO postgres;

--
-- Name: s_persyaratan_s_idpersyaratan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_persyaratan_s_idpersyaratan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_persyaratan_s_idpersyaratan_seq OWNER TO postgres;

--
-- Name: s_persyaratan_s_idpersyaratan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_persyaratan_s_idpersyaratan_seq OWNED BY s_persyaratan.s_idpersyaratan;


--
-- Name: s_presentase_wajar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_presentase_wajar (
    s_idpresentase_wajar integer NOT NULL,
    s_nilaimin double precision NOT NULL,
    s_nilaimax double precision NOT NULL,
    s_ketpresentase_wajar character varying(255),
    s_css_warna character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_presentase_wajar OWNER TO postgres;

--
-- Name: s_presentase_wajar_s_idpresentase_wajar_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_presentase_wajar_s_idpresentase_wajar_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_presentase_wajar_s_idpresentase_wajar_seq OWNER TO postgres;

--
-- Name: s_presentase_wajar_s_idpresentase_wajar_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_presentase_wajar_s_idpresentase_wajar_seq OWNED BY s_presentase_wajar.s_idpresentase_wajar;


--
-- Name: s_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_status (
    s_id_status integer NOT NULL,
    s_nama_status character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_status OWNER TO postgres;

--
-- Name: s_status_bayar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_status_bayar (
    s_id_status_bayar integer NOT NULL,
    s_nama_status_bayar character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_status_bayar OWNER TO postgres;

--
-- Name: s_status_bayar_s_id_status_bayar_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_status_bayar_s_id_status_bayar_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_status_bayar_s_id_status_bayar_seq OWNER TO postgres;

--
-- Name: s_status_bayar_s_id_status_bayar_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_status_bayar_s_id_status_bayar_seq OWNED BY s_status_bayar.s_id_status_bayar;


--
-- Name: s_status_berkas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_status_berkas (
    s_id_status_berkas integer NOT NULL,
    s_nama_status_berkas character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_status_berkas OWNER TO postgres;

--
-- Name: s_status_berkas_s_id_status_berkas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_status_berkas_s_id_status_berkas_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_status_berkas_s_id_status_berkas_seq OWNER TO postgres;

--
-- Name: s_status_berkas_s_id_status_berkas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_status_berkas_s_id_status_berkas_seq OWNED BY s_status_berkas.s_id_status_berkas;


--
-- Name: s_status_dptnpoptkp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_status_dptnpoptkp (
    s_id_dptnpoptkp integer NOT NULL,
    s_nama_dptnpoptkp character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_status_dptnpoptkp OWNER TO postgres;

--
-- Name: s_status_dptnpoptkp_s_id_dptnpoptkp_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_status_dptnpoptkp_s_id_dptnpoptkp_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_status_dptnpoptkp_s_id_dptnpoptkp_seq OWNER TO postgres;

--
-- Name: s_status_dptnpoptkp_s_id_dptnpoptkp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_status_dptnpoptkp_s_id_dptnpoptkp_seq OWNED BY s_status_dptnpoptkp.s_id_dptnpoptkp;


--
-- Name: s_status_kabid; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_status_kabid (
    s_id_status_kabid integer NOT NULL,
    s_nama_status_kabid character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_status_kabid OWNER TO postgres;

--
-- Name: s_status_kabid_s_id_status_kabid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_status_kabid_s_id_status_kabid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_status_kabid_s_id_status_kabid_seq OWNER TO postgres;

--
-- Name: s_status_kabid_s_id_status_kabid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_status_kabid_s_id_status_kabid_seq OWNED BY s_status_kabid.s_id_status_kabid;


--
-- Name: s_status_lihat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_status_lihat (
    s_id_status_lihat integer NOT NULL,
    s_nama_status_lihat character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_status_lihat OWNER TO postgres;

--
-- Name: s_status_lihat_s_id_status_lihat_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_status_lihat_s_id_status_lihat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_status_lihat_s_id_status_lihat_seq OWNER TO postgres;

--
-- Name: s_status_lihat_s_id_status_lihat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_status_lihat_s_id_status_lihat_seq OWNED BY s_status_lihat.s_id_status_lihat;


--
-- Name: s_status_npwpd; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_status_npwpd (
    s_idstatus_npwpd integer NOT NULL,
    s_nama_status character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_status_npwpd OWNER TO postgres;

--
-- Name: s_status_npwpd_s_idstatus_npwpd_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_status_npwpd_s_idstatus_npwpd_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_status_npwpd_s_idstatus_npwpd_seq OWNER TO postgres;

--
-- Name: s_status_npwpd_s_idstatus_npwpd_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_status_npwpd_s_idstatus_npwpd_seq OWNED BY s_status_npwpd.s_idstatus_npwpd;


--
-- Name: s_status_pelayanan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_status_pelayanan (
    s_id_status_layanan integer NOT NULL,
    s_nama_status_layanan character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_status_pelayanan OWNER TO postgres;

--
-- Name: s_status_pelayanan_s_id_status_layanan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_status_pelayanan_s_id_status_layanan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_status_pelayanan_s_id_status_layanan_seq OWNER TO postgres;

--
-- Name: s_status_pelayanan_s_id_status_layanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_status_pelayanan_s_id_status_layanan_seq OWNED BY s_status_pelayanan.s_id_status_layanan;


--
-- Name: s_status_perhitungan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_status_perhitungan (
    s_idstatus_pht integer NOT NULL,
    s_nama character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_status_perhitungan OWNER TO postgres;

--
-- Name: s_status_perhitungan_s_idstatus_pht_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_status_perhitungan_s_idstatus_pht_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_status_perhitungan_s_idstatus_pht_seq OWNER TO postgres;

--
-- Name: s_status_perhitungan_s_idstatus_pht_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_status_perhitungan_s_idstatus_pht_seq OWNED BY s_status_perhitungan.s_idstatus_pht;


--
-- Name: s_status_s_id_status_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_status_s_id_status_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_status_s_id_status_seq OWNER TO postgres;

--
-- Name: s_status_s_id_status_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_status_s_id_status_seq OWNED BY s_status.s_id_status;


--
-- Name: s_target_bphtb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_target_bphtb (
    s_id_target_bphtb integer NOT NULL,
    s_id_target_status integer,
    s_tahun_target integer,
    s_nilai_target double precision,
    s_keterangan character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_target_bphtb OWNER TO postgres;

--
-- Name: s_target_bphtb_s_id_target_bphtb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_target_bphtb_s_id_target_bphtb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_target_bphtb_s_id_target_bphtb_seq OWNER TO postgres;

--
-- Name: s_target_bphtb_s_id_target_bphtb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_target_bphtb_s_id_target_bphtb_seq OWNED BY s_target_bphtb.s_id_target_bphtb;


--
-- Name: s_target_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_target_status (
    s_id_target_status integer NOT NULL,
    s_nama_status character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_target_status OWNER TO postgres;

--
-- Name: s_target_status_s_id_target_status_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_target_status_s_id_target_status_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_target_status_s_id_target_status_seq OWNER TO postgres;

--
-- Name: s_target_status_s_id_target_status_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_target_status_s_id_target_status_seq OWNED BY s_target_status.s_id_target_status;


--
-- Name: s_tarif_bphtb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_tarif_bphtb (
    s_idtarifbphtb integer NOT NULL,
    s_tarif_bphtb double precision,
    s_tgl_awal date,
    s_tgl_akhir date,
    s_dasar_hukum text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_tarif_bphtb OWNER TO postgres;

--
-- Name: s_tarif_bphtb_s_idtarifbphtb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_tarif_bphtb_s_idtarifbphtb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_tarif_bphtb_s_idtarifbphtb_seq OWNER TO postgres;

--
-- Name: s_tarif_bphtb_s_idtarifbphtb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_tarif_bphtb_s_idtarifbphtb_seq OWNED BY s_tarif_bphtb.s_idtarifbphtb;


--
-- Name: s_tarifnpoptkp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_tarifnpoptkp (
    s_idtarifnpoptkp integer NOT NULL,
    s_idjenistransaksinpoptkp integer,
    s_tarifnpoptkp double precision,
    s_tarifnpoptkptambahan double precision,
    s_dasarhukumnpoptkp text,
    s_statusnpoptkp integer,
    s_tglberlaku_awal date,
    s_tglberlaku_akhir date,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_tarifnpoptkp OWNER TO postgres;

--
-- Name: s_tarifnpoptkp_s_idtarifnpoptkp_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_tarifnpoptkp_s_idtarifnpoptkp_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_tarifnpoptkp_s_idtarifnpoptkp_seq OWNER TO postgres;

--
-- Name: s_tarifnpoptkp_s_idtarifnpoptkp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_tarifnpoptkp_s_idtarifnpoptkp_seq OWNED BY s_tarifnpoptkp.s_idtarifnpoptkp;


--
-- Name: s_tempo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_tempo (
    s_idtempo integer NOT NULL,
    s_haritempo integer NOT NULL,
    s_tglberlaku_awal timestamp(0) without time zone,
    s_tglberlaku_akhir timestamp(0) without time zone,
    s_id_status integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_tempo OWNER TO postgres;

--
-- Name: s_tempo_s_idtempo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_tempo_s_idtempo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_tempo_s_idtempo_seq OWNER TO postgres;

--
-- Name: s_tempo_s_idtempo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_tempo_s_idtempo_seq OWNED BY s_tempo.s_idtempo;


--
-- Name: s_users_api; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_users_api (
    s_idusers_api integer NOT NULL,
    s_username character varying(255),
    s_password character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_users_api OWNER TO postgres;

--
-- Name: s_users_api_s_idusers_api_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_users_api_s_idusers_api_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_users_api_s_idusers_api_seq OWNER TO postgres;

--
-- Name: s_users_api_s_idusers_api_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_users_api_s_idusers_api_seq OWNED BY s_users_api.s_idusers_api;


--
-- Name: s_wajib_up; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE s_wajib_up (
    s_idwajib_up integer NOT NULL,
    s_ket_wjb character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE s_wajib_up OWNER TO postgres;

--
-- Name: s_wajib_up_s_idwajib_up_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_wajib_up_s_idwajib_up_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_wajib_up_s_idwajib_up_seq OWNER TO postgres;

--
-- Name: s_wajib_up_s_idwajib_up_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s_wajib_up_s_idwajib_up_seq OWNED BY s_wajib_up.s_idwajib_up;


--
-- Name: t_bpn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_bpn (
    t_idbpn integer NOT NULL,
    t_noakta character varying(100),
    t_tglakta timestamp(0) without time zone,
    t_namappat character varying(255),
    t_nop character varying(255),
    t_nib character varying(255),
    t_ntpd character varying(255),
    t_nik character varying(255),
    t_npwp character varying(50),
    t_namawp character varying(255),
    t_kelurahanop character varying(255),
    t_kecamatanop character varying(255),
    t_kotaop character varying(255),
    t_luastanahop double precision,
    t_jenishak character varying(255),
    t_koordinat_x character varying(255),
    t_koordinat_y character varying(255),
    t_tgltransaksi timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_bpn OWNER TO postgres;

--
-- Name: t_bpn_t_idbpn_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_bpn_t_idbpn_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_bpn_t_idbpn_seq OWNER TO postgres;

--
-- Name: t_bpn_t_idbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_bpn_t_idbpn_seq OWNED BY t_bpn.t_idbpn;


--
-- Name: t_file_keringanan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_file_keringanan (
    t_idfile_keringanan integer NOT NULL,
    t_idspt integer,
    t_iduser_upload integer,
    letak_file text NOT NULL,
    nama_file text NOT NULL,
    t_tgl_upload timestamp(0) without time zone,
    t_keterangan_file text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_file_keringanan OWNER TO postgres;

--
-- Name: t_file_keringanan_t_idfile_keringanan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_file_keringanan_t_idfile_keringanan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_file_keringanan_t_idfile_keringanan_seq OWNER TO postgres;

--
-- Name: t_file_keringanan_t_idfile_keringanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_file_keringanan_t_idfile_keringanan_seq OWNED BY t_file_keringanan.t_idfile_keringanan;


--
-- Name: t_file_objek; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_file_objek (
    t_idfile_objek integer NOT NULL,
    t_idspt integer,
    t_iduser_upload integer,
    letak_file text,
    nama_file text,
    t_tgl_upload timestamp(0) without time zone,
    t_keterangan_file text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_file_objek OWNER TO postgres;

--
-- Name: t_file_objek_t_idfile_objek_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_file_objek_t_idfile_objek_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_file_objek_t_idfile_objek_seq OWNER TO postgres;

--
-- Name: t_file_objek_t_idfile_objek_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_file_objek_t_idfile_objek_seq OWNED BY t_file_objek.t_idfile_objek;


--
-- Name: t_filesyarat_bphtb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_filesyarat_bphtb (
    t_id_filesyarat integer NOT NULL,
    t_idspt integer,
    s_idpersyaratan integer,
    s_idjenistransaksi integer,
    t_iduser_upload integer,
    letak_file text,
    nama_file text,
    t_tgl_upload timestamp(0) without time zone,
    t_keterangan_file text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_filesyarat_bphtb OWNER TO postgres;

--
-- Name: t_filesyarat_bphtb_t_id_filesyarat_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_filesyarat_bphtb_t_id_filesyarat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_filesyarat_bphtb_t_id_filesyarat_seq OWNER TO postgres;

--
-- Name: t_filesyarat_bphtb_t_id_filesyarat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_filesyarat_bphtb_t_id_filesyarat_seq OWNED BY t_filesyarat_bphtb.t_id_filesyarat;


--
-- Name: t_inputajb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_inputajb (
    t_idajb integer NOT NULL,
    t_idspt integer,
    t_tgl_ajb timestamp(0) without time zone,
    t_no_ajb character varying(255) NOT NULL,
    t_iduser_input integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_inputajb OWNER TO postgres;

--
-- Name: t_inputajb_t_idajb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_inputajb_t_idajb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_inputajb_t_idajb_seq OWNER TO postgres;

--
-- Name: t_inputajb_t_idajb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_inputajb_t_idajb_seq OWNED BY t_inputajb.t_idajb;


--
-- Name: t_laporbulanan_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_laporbulanan_detail (
    t_idlaporbulanan integer,
    t_idajb integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_laporbulanan_detail OWNER TO postgres;

--
-- Name: t_laporbulanan_head; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_laporbulanan_head (
    t_idlaporbulanan integer NOT NULL,
    t_no_lapor integer,
    t_tgl_lapor timestamp(0) without time zone,
    t_untuk_bulan integer,
    t_untuk_tahun integer,
    t_keterangan text NOT NULL,
    t_iduser_input integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_laporbulanan_head OWNER TO postgres;

--
-- Name: t_laporbulanan_head_t_idlaporbulanan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_laporbulanan_head_t_idlaporbulanan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_laporbulanan_head_t_idlaporbulanan_seq OWNER TO postgres;

--
-- Name: t_laporbulanan_head_t_idlaporbulanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_laporbulanan_head_t_idlaporbulanan_seq OWNED BY t_laporbulanan_head.t_idlaporbulanan;


--
-- Name: t_nik_bersama; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_nik_bersama (
    t_id_nik_bersama integer NOT NULL,
    t_idspt integer,
    t_nik_bersama character varying(255),
    t_nama_bersama character varying(255),
    t_nama_jalan text,
    t_rt_bersama character varying(255),
    t_rw_bersama character varying(255),
    t_kecamatan_bersama character varying(255),
    t_kelurahan_bersama character varying(255),
    t_kabkota_bersama character varying(255),
    t_nohp_bersama character varying(255),
    t_kodepos_bersama character varying(255),
    t_npwp_bersama character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_nik_bersama OWNER TO postgres;

--
-- Name: t_nik_bersama_t_id_nik_bersama_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_nik_bersama_t_id_nik_bersama_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_nik_bersama_t_id_nik_bersama_seq OWNER TO postgres;

--
-- Name: t_nik_bersama_t_id_nik_bersama_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_nik_bersama_t_id_nik_bersama_seq OWNED BY t_nik_bersama.t_id_nik_bersama;


--
-- Name: t_notif_validasi_berkas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_notif_validasi_berkas (
    t_id_notif_berkas integer NOT NULL,
    t_idspt integer,
    t_isipesan text,
    t_dari integer,
    t_tglkirim timestamp(0) without time zone,
    t_untuk integer,
    s_id_status_lihat integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_notif_validasi_berkas OWNER TO postgres;

--
-- Name: t_notif_validasi_berkas_t_id_notif_berkas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_notif_validasi_berkas_t_id_notif_berkas_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_notif_validasi_berkas_t_id_notif_berkas_seq OWNER TO postgres;

--
-- Name: t_notif_validasi_berkas_t_id_notif_berkas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_notif_validasi_berkas_t_id_notif_berkas_seq OWNED BY t_notif_validasi_berkas.t_id_notif_berkas;


--
-- Name: t_notif_validasi_kabid; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_notif_validasi_kabid (
    t_id_notif_kabid integer NOT NULL,
    t_idspt integer,
    t_isipesan text,
    t_dari integer,
    t_tglkirim timestamp(0) without time zone,
    t_untuk integer,
    s_id_status_lihat integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_notif_validasi_kabid OWNER TO postgres;

--
-- Name: t_notif_validasi_kabid_t_id_notif_kabid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_notif_validasi_kabid_t_id_notif_kabid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_notif_validasi_kabid_t_id_notif_kabid_seq OWNER TO postgres;

--
-- Name: t_notif_validasi_kabid_t_id_notif_kabid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_notif_validasi_kabid_t_id_notif_kabid_seq OWNED BY t_notif_validasi_kabid.t_id_notif_kabid;


--
-- Name: t_pelayanan_angsuran; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pelayanan_angsuran (
    t_idangsuran integer NOT NULL,
    t_idspt integer,
    t_noangsuran integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_angsuran character varying(255),
    t_jmlhangsuran double precision,
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pelayanan_angsuran OWNER TO postgres;

--
-- Name: t_pelayanan_angsuran_t_idangsuran_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pelayanan_angsuran_t_idangsuran_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pelayanan_angsuran_t_idangsuran_seq OWNER TO postgres;

--
-- Name: t_pelayanan_angsuran_t_idangsuran_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pelayanan_angsuran_t_idangsuran_seq OWNED BY t_pelayanan_angsuran.t_idangsuran;


--
-- Name: t_pelayanan_keberatan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pelayanan_keberatan (
    t_idkeberatan integer NOT NULL,
    t_idspt integer,
    t_nokeberatan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_keberatan character varying(255),
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    t_jmlhpotongan_disetujui double precision,
    t_persentase_disetujui double precision,
    t_jmlh_spt_sebenarnya double precision,
    t_jmlh_spt_hasilpot double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pelayanan_keberatan OWNER TO postgres;

--
-- Name: t_pelayanan_keberatan_t_idkeberatan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pelayanan_keberatan_t_idkeberatan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pelayanan_keberatan_t_idkeberatan_seq OWNER TO postgres;

--
-- Name: t_pelayanan_keberatan_t_idkeberatan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pelayanan_keberatan_t_idkeberatan_seq OWNED BY t_pelayanan_keberatan.t_idkeberatan;


--
-- Name: t_pelayanan_keringanan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pelayanan_keringanan (
    t_idkeringanan integer NOT NULL,
    t_idspt integer,
    t_nokeringanan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_keringanan character varying(255),
    t_jmlhpotongan double precision,
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    t_jmlhpotongan_disetujui double precision,
    t_persentase_disetujui double precision,
    t_jmlh_spt_sebenarnya double precision,
    t_jmlh_spt_hasilpot double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pelayanan_keringanan OWNER TO postgres;

--
-- Name: t_pelayanan_keringanan_t_idkeringanan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pelayanan_keringanan_t_idkeringanan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pelayanan_keringanan_t_idkeringanan_seq OWNER TO postgres;

--
-- Name: t_pelayanan_keringanan_t_idkeringanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pelayanan_keringanan_t_idkeringanan_seq OWNED BY t_pelayanan_keringanan.t_idkeringanan;


--
-- Name: t_pelayanan_mutasi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pelayanan_mutasi (
    t_idmutasi integer NOT NULL,
    t_idspt integer,
    t_nomutasi integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_mutasi character varying(255),
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pelayanan_mutasi OWNER TO postgres;

--
-- Name: t_pelayanan_mutasi_t_idmutasi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pelayanan_mutasi_t_idmutasi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pelayanan_mutasi_t_idmutasi_seq OWNER TO postgres;

--
-- Name: t_pelayanan_mutasi_t_idmutasi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pelayanan_mutasi_t_idmutasi_seq OWNED BY t_pelayanan_mutasi.t_idmutasi;


--
-- Name: t_pelayanan_pembatalan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pelayanan_pembatalan (
    t_idpembatalan integer NOT NULL,
    t_idspt integer,
    t_nopembatalan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_keterangan_pembatalan text,
    t_iduser_pengajuan integer,
    t_nosk_pembatalan character varying(255) NOT NULL,
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pelayanan_pembatalan OWNER TO postgres;

--
-- Name: t_pelayanan_pembatalan_t_idpembatalan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pelayanan_pembatalan_t_idpembatalan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pelayanan_pembatalan_t_idpembatalan_seq OWNER TO postgres;

--
-- Name: t_pelayanan_pembatalan_t_idpembatalan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pelayanan_pembatalan_t_idpembatalan_seq OWNED BY t_pelayanan_pembatalan.t_idpembatalan;


--
-- Name: t_pelayanan_penundaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pelayanan_penundaan (
    t_idpenundaan integer NOT NULL,
    t_idspt integer,
    t_nopenundaan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_penundaan character varying(255),
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pelayanan_penundaan OWNER TO postgres;

--
-- Name: t_pelayanan_penundaan_t_idpenundaan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pelayanan_penundaan_t_idpenundaan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pelayanan_penundaan_t_idpenundaan_seq OWNER TO postgres;

--
-- Name: t_pelayanan_penundaan_t_idpenundaan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pelayanan_penundaan_t_idpenundaan_seq OWNED BY t_pelayanan_penundaan.t_idpenundaan;


--
-- Name: t_pembayaran_bphtb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pembayaran_bphtb (
    t_id_pembayaran integer NOT NULL,
    t_idspt integer,
    t_nourut_bayar integer,
    s_id_status_bayar integer,
    t_iduser_bayar integer,
    t_tglpembayaran_pokok timestamp(0) without time zone,
    t_jmlhbayar_pokok double precision,
    t_tglpembayaran_denda timestamp(0) without time zone,
    t_jmlhbayar_denda double precision,
    t_jmlhbayar_total double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pembayaran_bphtb OWNER TO postgres;

--
-- Name: t_pembayaran_bphtb_t_id_pembayaran_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pembayaran_bphtb_t_id_pembayaran_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pembayaran_bphtb_t_id_pembayaran_seq OWNER TO postgres;

--
-- Name: t_pembayaran_bphtb_t_id_pembayaran_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pembayaran_bphtb_t_id_pembayaran_seq OWNED BY t_pembayaran_bphtb.t_id_pembayaran;


--
-- Name: t_pembayaran_skpdkb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pembayaran_skpdkb (
    t_id_bayar_skpdkb integer NOT NULL,
    t_idspt integer,
    t_idskpdkb integer,
    t_nourut_bayar integer,
    s_id_status_bayar integer,
    t_iduser_bayar integer,
    t_tglpembayaran_skpdkb timestamp(0) without time zone,
    t_jmlhbayar_skpdkb double precision,
    t_tglpembayaran_denda timestamp(0) without time zone,
    t_jmlhbayar_denda double precision,
    t_jmlhbayar_total double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pembayaran_skpdkb OWNER TO postgres;

--
-- Name: t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq OWNER TO postgres;

--
-- Name: t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq OWNED BY t_pembayaran_skpdkb.t_id_bayar_skpdkb;


--
-- Name: t_pembayaran_skpdkbt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pembayaran_skpdkbt (
    t_id_bayar_skpdkb integer NOT NULL,
    t_idspt integer,
    t_idskpdkbt integer,
    t_nourut_bayar integer,
    s_id_status_bayar integer,
    t_iduser_bayar integer,
    t_tglpembayaran_skpdkbt timestamp(0) without time zone,
    t_jmlhbayar_skpdkbt double precision,
    t_tglpembayaran_denda timestamp(0) without time zone,
    t_jmlhbayar_denda double precision,
    t_jmlhbayar_total double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pembayaran_skpdkbt OWNER TO postgres;

--
-- Name: t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq OWNER TO postgres;

--
-- Name: t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq OWNED BY t_pembayaran_skpdkbt.t_id_bayar_skpdkb;


--
-- Name: t_pemeriksaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pemeriksaan (
    t_idpemeriksa integer NOT NULL,
    t_idspt integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT now() NOT NULL,
    t_iduser_buat integer,
    t_idpejabat1 integer,
    t_idpejabat2 integer,
    t_idpejabat3 integer,
    t_idpejabat4 integer,
    t_noperiksa character varying(255),
    t_keterangan text,
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_luastanah_pemeriksa double precision,
    t_njoptanah_pemeriksa double precision,
    t_totalnjoptanah_pemeriksa double precision,
    t_luasbangunan_pemeriksa double precision,
    t_njopbangunan_pemeriksa double precision,
    t_totalnjopbangunan_pemeriksa double precision,
    t_grandtotalnjop_pemeriksa double precision,
    t_nilaitransaksispt_pemeriksa double precision,
    t_trf_aphb_kali_pemeriksa double precision,
    t_trf_aphb_bagi_pemeriksa double precision,
    t_permeter_tanah_pemeriksa double precision,
    t_idtarifbphtb_pemeriksa integer,
    t_persenbphtb_pemeriksa double precision,
    t_npop_bphtb_pemeriksa double precision,
    t_npoptkp_bphtb_pemeriksa double precision,
    t_npopkp_bphtb_pemeriksa double precision,
    t_nilaibphtb_pemeriksa double precision,
    t_idpersetujuan_pemeriksa integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pemeriksaan OWNER TO postgres;

--
-- Name: t_pemeriksaan_t_idpemeriksa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pemeriksaan_t_idpemeriksa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pemeriksaan_t_idpemeriksa_seq OWNER TO postgres;

--
-- Name: t_pemeriksaan_t_idpemeriksa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pemeriksaan_t_idpemeriksa_seq OWNED BY t_pemeriksaan.t_idpemeriksa;


--
-- Name: t_pen_denda_ajb_notaris; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pen_denda_ajb_notaris (
    t_id_ajbdenda integer NOT NULL,
    t_idspt integer,
    t_idnotaris integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_nourut_ajbdenda integer,
    t_nilai_ajbdenda double precision,
    t_iduser_buat integer,
    s_id_status_bayar integer,
    t_tglbayar_ajbdenda timestamp(0) without time zone,
    t_jmlhbayar_ajbdenda double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pen_denda_ajb_notaris OWNER TO postgres;

--
-- Name: t_pen_denda_ajb_notaris_t_id_ajbdenda_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pen_denda_ajb_notaris_t_id_ajbdenda_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pen_denda_ajb_notaris_t_id_ajbdenda_seq OWNER TO postgres;

--
-- Name: t_pen_denda_ajb_notaris_t_id_ajbdenda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pen_denda_ajb_notaris_t_id_ajbdenda_seq OWNED BY t_pen_denda_ajb_notaris.t_id_ajbdenda;


--
-- Name: t_pen_denda_lpor_notaris; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_pen_denda_lpor_notaris (
    t_id_pendenda integer NOT NULL,
    t_idlaporbulanan integer,
    t_idnotaris integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_nourut_pendenda integer,
    t_nilai_pendenda double precision,
    t_iduser_buat integer,
    s_id_status_bayar integer,
    t_tglbayar_pendenda timestamp(0) without time zone,
    t_jmlhbayar_pendenda double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_pen_denda_lpor_notaris OWNER TO postgres;

--
-- Name: t_pen_denda_lpor_notaris_t_id_pendenda_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_pen_denda_lpor_notaris_t_id_pendenda_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_pen_denda_lpor_notaris_t_id_pendenda_seq OWNER TO postgres;

--
-- Name: t_pen_denda_lpor_notaris_t_id_pendenda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_pen_denda_lpor_notaris_t_id_pendenda_seq OWNED BY t_pen_denda_lpor_notaris.t_id_pendenda;


--
-- Name: t_skpdkb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_skpdkb (
    t_idskpdkb integer NOT NULL,
    t_idspt integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT now() NOT NULL,
    t_iduser_buat integer,
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_luastanah_skpdkb double precision,
    t_njoptanah_skpdkb double precision,
    t_totalnjoptanah_skpdkb double precision,
    t_luasbangunan_skpdkb double precision,
    t_njopbangunan_skpdkb double precision,
    t_totalnjopbangunan_skpdkb double precision,
    t_grandtotalnjop_skpdkb double precision,
    t_nilaitransaksispt_skpdkb double precision,
    t_trf_aphb_kali_skpdkb double precision,
    t_trf_aphb_bagi_skpdkb double precision,
    t_permeter_tanah_skpdkb double precision,
    t_idtarifbphtb_skpdkb integer,
    t_persenbphtb_skpdkb double precision,
    t_npop_bphtb_skpdkb double precision,
    t_npoptkp_bphtb_skpdkb double precision,
    t_npopkp_bphtb_skpdkb double precision,
    t_nilai_bayar_sblumnya double precision,
    t_nilai_pokok_skpdkb double precision,
    t_jmlh_blndenda double precision,
    t_jmlh_dendaskpdkb double precision,
    t_jmlh_totalskpdkb double precision,
    t_tglbuat_kodebyar timestamp(0) without time zone,
    t_nourut_kodebayar integer,
    t_kodebayar_skpdkb character varying(255),
    t_tgljatuhtempo_skpdkb timestamp(0) without time zone,
    t_idjenisketetapan integer,
    t_idpersetujuan_skpdkb integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_skpdkb OWNER TO postgres;

--
-- Name: t_skpdkb_t_idskpdkb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_skpdkb_t_idskpdkb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_skpdkb_t_idskpdkb_seq OWNER TO postgres;

--
-- Name: t_skpdkb_t_idskpdkb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_skpdkb_t_idskpdkb_seq OWNED BY t_skpdkb.t_idskpdkb;


--
-- Name: t_skpdkbt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_skpdkbt (
    t_idskpdkbt integer NOT NULL,
    t_idspt integer,
    t_idskpdkb integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT now() NOT NULL,
    t_iduser_buat integer,
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_luastanah_skpdkbt double precision,
    t_njoptanah_skpdkbt double precision,
    t_totalnjoptanah_skpdkbt double precision,
    t_luasbangunan_skpdkbt double precision,
    t_njopbangunan_skpdkbt double precision,
    t_totalnjopbangunan_skpdkbt double precision,
    t_grandtotalnjop_skpdkbt double precision,
    t_nilaitransaksispt_skpdkbt double precision,
    t_trf_aphb_kali_skpdkbt double precision,
    t_trf_aphb_bagi_skpdkbt double precision,
    t_permeter_tanah_skpdkbt double precision,
    t_idtarifbphtb_skpdkbt integer,
    t_persenbphtb_skpdkbt double precision,
    t_npop_bphtb_skpdkbt double precision,
    t_npoptkp_bphtb_skpdkbt double precision,
    t_npopkp_bphtb_skpdkbt double precision,
    t_nilai_bayar_sblumnya double precision,
    t_nilai_pokok_skpdkbt double precision,
    t_jmlh_blndenda double precision,
    t_jmlh_dendaskpdkbt double precision,
    t_jmlh_totalskpdkbt double precision,
    t_tglbuat_kodebyar timestamp(0) without time zone,
    t_nourut_kodebayar integer,
    t_kodebayar_skpdkbt character varying(255),
    t_tgljatuhtempo_skpdkbt timestamp(0) without time zone,
    t_idjenisketetapan integer,
    t_idpersetujuan_skpdkb integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_skpdkbt OWNER TO postgres;

--
-- Name: t_skpdkbt_t_idskpdkbt_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_skpdkbt_t_idskpdkbt_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_skpdkbt_t_idskpdkbt_seq OWNER TO postgres;

--
-- Name: t_skpdkbt_t_idskpdkbt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_skpdkbt_t_idskpdkbt_seq OWNED BY t_skpdkbt.t_idskpdkbt;


--
-- Name: t_spt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_spt (
    t_idspt integer NOT NULL,
    t_kohirspt integer,
    t_tgldaftar_spt timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT now() NOT NULL,
    t_periodespt integer,
    t_iduser_buat integer,
    t_idjenistransaksi integer,
    t_idnotaris_spt integer,
    t_npwpd character varying(255),
    t_idstatus_npwpd integer,
    t_idbidang_usaha integer,
    t_nama_pembeli character varying(255),
    t_nik_pembeli character varying(255),
    t_npwp_pembeli character varying(255),
    t_jalan_pembeli text,
    t_kabkota_pembeli character varying(255),
    t_idkec_pembeli integer,
    t_namakecamatan_pembeli character varying(255),
    t_idkel_pembeli integer,
    t_namakelurahan_pembeli character varying(255),
    t_rt_pembeli character varying(255),
    t_rw_pembeli character varying(255),
    t_nohp_pembeli character varying(255),
    t_notelp_pembeli character varying(255),
    t_email_pembeli character varying(255),
    t_kodepos_pembeli character varying(255),
    t_siup_pembeli character varying(255),
    t_nib_pembeli character varying(255),
    t_ketdomisili_pembeli character varying(255),
    t_b_nama_pngjwb_pembeli character varying(255),
    t_b_nik_pngjwb_pembeli character varying(255),
    t_b_npwp_pngjwb_pembeli character varying(255),
    t_b_statusjab_pngjwb_pembeli character varying(255),
    t_b_jalan_pngjwb_pembeli text,
    t_b_kabkota_pngjwb_pembeli character varying(255),
    t_b_idkec_pngjwb_pembeli integer,
    t_b_namakec_pngjwb_pembeli character varying(255),
    t_b_idkel_pngjwb_pembeli integer,
    t_b_namakel_pngjwb_pembeli character varying(255),
    t_b_rt_pngjwb_pembeli character varying(255),
    t_b_rw_pngjwb_pembeli character varying(255),
    t_b_nohp_pngjwb_pembeli character varying(255),
    t_b_notelp_pngjwb_pembeli character varying(255),
    t_b_email_pngjwb_pembeli character varying(255),
    t_b_kodepos_pngjwb_pembeli character varying(255),
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_nama_sppt character varying(255),
    t_jalan_sppt character varying(255),
    t_kabkota_sppt character varying(255),
    t_kecamatan_sppt character varying(255),
    t_kelurahan_sppt character varying(255),
    t_rt_sppt character varying(255),
    t_rw_sppt character varying(255),
    t_luastanah_sismiop double precision,
    t_luasbangunan_sismiop double precision,
    t_njoptanah_sismiop double precision,
    t_njopbangunan_sismiop double precision,
    t_luastanah double precision,
    t_njoptanah double precision,
    t_totalnjoptanah double precision,
    t_luasbangunan double precision,
    t_njopbangunan double precision,
    t_totalnjopbangunan double precision,
    t_grandtotalnjop double precision,
    t_nilaitransaksispt double precision,
    t_tarif_pembagian_aphb_kali double precision,
    t_tarif_pembagian_aphb_bagi double precision,
    t_permeter_tanah double precision,
    t_idjenisfasilitas integer,
    t_idjenishaktanah integer,
    t_idjenisdoktanah integer,
    t_idpengurangan integer,
    t_id_jenistanah integer,
    t_nosertifikathaktanah character varying(255),
    t_tgldok_tanah timestamp(0) without time zone,
    s_latitude character varying(255),
    s_longitude character varying(255),
    t_idtarifbphtb integer,
    t_persenbphtb double precision,
    t_npop_bphtb double precision,
    t_npoptkp_bphtb double precision,
    t_npopkp_bphtb double precision,
    t_nilai_bphtb_fix double precision,
    t_nilai_bphtb_real double precision,
    t_idbidang_penjual integer,
    t_nama_penjual character varying(255),
    t_nik_penjual character varying(255),
    t_npwp_penjual character varying(255),
    t_jalan_penjual text,
    t_kabkota_penjual character varying(255),
    t_idkec_penjual integer,
    t_namakec_penjual character varying(255),
    t_idkel_penjual integer,
    t_namakel_penjual character varying(255),
    t_rt_penjual character varying(255),
    t_rw_penjual character varying(255),
    t_nohp_penjual character varying(255),
    t_notelp_penjual character varying(255),
    t_email_penjual character varying(255),
    t_kodepos_penjual character varying(255),
    t_siup_penjual character varying(255),
    t_nib_penjual character varying(255),
    t_ketdomisili_penjual character varying(255),
    t_b_nama_pngjwb_penjual character varying(255),
    t_b_nik_pngjwb_penjual character varying(255),
    t_b_npwp_pngjwb_penjual character varying(255),
    t_b_statusjab_pngjwb_penjual character varying(255),
    t_b_jalan_pngjwb_penjual text,
    t_b_kabkota_pngjwb_penjual character varying(255),
    t_b_idkec_pngjwb_penjual integer,
    t_b_namakec_pngjwb_penjual character varying(255),
    t_b_idkel_pngjwb_penjual integer,
    t_b_namakel_pngjwb_penjual character varying(255),
    t_b_rt_pngjwb_penjual character varying(255),
    t_b_rw_pngjwb_penjual character varying(255),
    t_b_nohp_pngjwb_penjual character varying(255),
    t_b_notelp_pngjwb_penjual character varying(255),
    t_b_email_pngjwb_penjual character varying(255),
    t_b_kodepos_pngjwb_penjual character varying(255),
    t_tglbuat_kodebyar timestamp(0) without time zone,
    t_nourut_kodebayar integer,
    t_kodebayar_bphtb character varying(255),
    t_tglketetapan_spt timestamp(0) without time zone,
    t_tgljatuhtempo_spt timestamp(0) without time zone,
    t_idjenisketetapan integer,
    t_idpersetujuan_bphtb integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    t_ntpd character varying(255)
);


ALTER TABLE t_spt OWNER TO postgres;

--
-- Name: t_spt_t_idspt_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_spt_t_idspt_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_spt_t_idspt_seq OWNER TO postgres;

--
-- Name: t_spt_t_idspt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_spt_t_idspt_seq OWNED BY t_spt.t_idspt;


--
-- Name: t_validasi_berkas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_validasi_berkas (
    t_id_validasi_berkas integer NOT NULL,
    t_idspt integer,
    s_id_status_berkas integer,
    t_tglvalidasi timestamp(0) without time zone,
    t_iduser_validasi integer,
    t_keterangan_berkas text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    t_persyaratan_validasi_berkas character varying(255)
);


ALTER TABLE t_validasi_berkas OWNER TO postgres;

--
-- Name: t_validasi_berkas_t_id_validasi_berkas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_validasi_berkas_t_id_validasi_berkas_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_validasi_berkas_t_id_validasi_berkas_seq OWNER TO postgres;

--
-- Name: t_validasi_berkas_t_id_validasi_berkas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_validasi_berkas_t_id_validasi_berkas_seq OWNED BY t_validasi_berkas.t_id_validasi_berkas;


--
-- Name: t_validasi_kabid; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_validasi_kabid (
    t_id_validasi_kabid integer NOT NULL,
    t_idspt integer,
    s_id_status_kabid integer,
    t_tglvalidasi timestamp(0) without time zone,
    t_iduser_validasi integer,
    t_keterangan_kabid text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE t_validasi_kabid OWNER TO postgres;

--
-- Name: t_validasi_kabid_t_id_validasi_kabid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE t_validasi_kabid_t_id_validasi_kabid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE t_validasi_kabid_t_id_validasi_kabid_seq OWNER TO postgres;

--
-- Name: t_validasi_kabid_t_id_validasi_kabid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE t_validasi_kabid_t_id_validasi_kabid_seq OWNED BY t_validasi_kabid.t_id_validasi_kabid;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    username character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    s_tipe_pejabat integer,
    s_idpejabat integer,
    s_idnotaris integer,
    s_id_hakakses integer,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY activity_log ALTER COLUMN id SET DEFAULT nextval('activity_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY failed_jobs ALTER COLUMN id SET DEFAULT nextval('failed_jobs_id_seq'::regclass);


--
-- Name: t_idhis_getbpn; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY his_getbphtb_bpn ALTER COLUMN t_idhis_getbpn SET DEFAULT nextval('his_getbphtb_bpn_t_idhis_getbpn_seq'::regclass);


--
-- Name: t_idhis_pbbbpn; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY his_getpbb_bpn ALTER COLUMN t_idhis_pbbbpn SET DEFAULT nextval('his_getpbb_bpn_t_idhis_pbbbpn_seq'::regclass);


--
-- Name: t_idhis_posbpn; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY his_postdatabpn ALTER COLUMN t_idhis_posbpn SET DEFAULT nextval('his_postdatabpn_t_idhis_posbpn_seq'::regclass);


--
-- Name: t_idsertifikat; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY integrasi_sertifikat_bpn ALTER COLUMN t_idsertifikat SET DEFAULT nextval('integrasi_sertifikat_bpn_t_idsertifikat_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY permissions ALTER COLUMN id SET DEFAULT nextval('permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: s_idacuan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_acuan ALTER COLUMN s_idacuan SET DEFAULT nextval('s_acuan_s_idacuan_seq'::regclass);


--
-- Name: s_idacuan_jenis; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_acuan_jenistanah ALTER COLUMN s_idacuan_jenis SET DEFAULT nextval('s_acuan_jenistanah_s_idacuan_jenis_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_background ALTER COLUMN id SET DEFAULT nextval('s_background_id_seq'::regclass);


--
-- Name: s_id_hakakses; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_hak_akses ALTER COLUMN s_id_hakakses SET DEFAULT nextval('s_hak_akses_s_id_hakakses_seq'::regclass);


--
-- Name: s_idhistory; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_harga_history ALTER COLUMN s_idhistory SET DEFAULT nextval('s_harga_history_s_idhistory_seq'::regclass);


--
-- Name: s_idbidang_usaha; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenis_bidangusaha ALTER COLUMN s_idbidang_usaha SET DEFAULT nextval('s_jenis_bidangusaha_s_idbidang_usaha_seq'::regclass);


--
-- Name: s_iddoktanah; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenisdoktanah ALTER COLUMN s_iddoktanah SET DEFAULT nextval('s_jenisdoktanah_s_iddoktanah_seq'::regclass);


--
-- Name: s_idjenisfasilitas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenisfasilitas ALTER COLUMN s_idjenisfasilitas SET DEFAULT nextval('s_jenisfasilitas_s_idjenisfasilitas_seq'::regclass);


--
-- Name: s_idhaktanah; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenishaktanah ALTER COLUMN s_idhaktanah SET DEFAULT nextval('s_jenishaktanah_s_idhaktanah_seq'::regclass);


--
-- Name: s_idjenisketetapan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenisketetapan ALTER COLUMN s_idjenisketetapan SET DEFAULT nextval('s_jenisketetapan_s_idjenisketetapan_seq'::regclass);


--
-- Name: s_idpengurangan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenispengurangan ALTER COLUMN s_idpengurangan SET DEFAULT nextval('s_jenispengurangan_s_idpengurangan_seq'::regclass);


--
-- Name: id_jenistanah; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenistanah ALTER COLUMN id_jenistanah SET DEFAULT nextval('s_jenistanah_id_jenistanah_seq'::regclass);


--
-- Name: s_idjenistransaksi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenistransaksi ALTER COLUMN s_idjenistransaksi SET DEFAULT nextval('s_jenistransaksi_s_idjenistransaksi_seq'::regclass);


--
-- Name: s_idkecamatan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_kecamatan ALTER COLUMN s_idkecamatan SET DEFAULT nextval('s_kecamatan_s_idkecamatan_seq'::regclass);


--
-- Name: s_idkelurahan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_kelurahan ALTER COLUMN s_idkelurahan SET DEFAULT nextval('s_kelurahan_s_idkelurahan_seq'::regclass);


--
-- Name: s_korekid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_koderekening ALTER COLUMN s_korekid SET DEFAULT nextval('s_koderekening_s_korekid_seq'::regclass);


--
-- Name: s_idnotaris; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_notaris ALTER COLUMN s_idnotaris SET DEFAULT nextval('s_notaris_s_idnotaris_seq'::regclass);


--
-- Name: s_idpejabat; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_pejabat ALTER COLUMN s_idpejabat SET DEFAULT nextval('s_pejabat_s_idpejabat_seq'::regclass);


--
-- Name: s_idpemda; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_pemda ALTER COLUMN s_idpemda SET DEFAULT nextval('s_pemda_s_idpemda_seq'::regclass);


--
-- Name: s_idpersyaratan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_persyaratan ALTER COLUMN s_idpersyaratan SET DEFAULT nextval('s_persyaratan_s_idpersyaratan_seq'::regclass);


--
-- Name: s_idpresentase_wajar; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_presentase_wajar ALTER COLUMN s_idpresentase_wajar SET DEFAULT nextval('s_presentase_wajar_s_idpresentase_wajar_seq'::regclass);


--
-- Name: s_id_status; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status ALTER COLUMN s_id_status SET DEFAULT nextval('s_status_s_id_status_seq'::regclass);


--
-- Name: s_id_status_bayar; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_bayar ALTER COLUMN s_id_status_bayar SET DEFAULT nextval('s_status_bayar_s_id_status_bayar_seq'::regclass);


--
-- Name: s_id_status_berkas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_berkas ALTER COLUMN s_id_status_berkas SET DEFAULT nextval('s_status_berkas_s_id_status_berkas_seq'::regclass);


--
-- Name: s_id_dptnpoptkp; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_dptnpoptkp ALTER COLUMN s_id_dptnpoptkp SET DEFAULT nextval('s_status_dptnpoptkp_s_id_dptnpoptkp_seq'::regclass);


--
-- Name: s_id_status_kabid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_kabid ALTER COLUMN s_id_status_kabid SET DEFAULT nextval('s_status_kabid_s_id_status_kabid_seq'::regclass);


--
-- Name: s_id_status_lihat; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_lihat ALTER COLUMN s_id_status_lihat SET DEFAULT nextval('s_status_lihat_s_id_status_lihat_seq'::regclass);


--
-- Name: s_idstatus_npwpd; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_npwpd ALTER COLUMN s_idstatus_npwpd SET DEFAULT nextval('s_status_npwpd_s_idstatus_npwpd_seq'::regclass);


--
-- Name: s_id_status_layanan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_pelayanan ALTER COLUMN s_id_status_layanan SET DEFAULT nextval('s_status_pelayanan_s_id_status_layanan_seq'::regclass);


--
-- Name: s_idstatus_pht; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_perhitungan ALTER COLUMN s_idstatus_pht SET DEFAULT nextval('s_status_perhitungan_s_idstatus_pht_seq'::regclass);


--
-- Name: s_id_target_bphtb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_target_bphtb ALTER COLUMN s_id_target_bphtb SET DEFAULT nextval('s_target_bphtb_s_id_target_bphtb_seq'::regclass);


--
-- Name: s_id_target_status; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_target_status ALTER COLUMN s_id_target_status SET DEFAULT nextval('s_target_status_s_id_target_status_seq'::regclass);


--
-- Name: s_idtarifbphtb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_tarif_bphtb ALTER COLUMN s_idtarifbphtb SET DEFAULT nextval('s_tarif_bphtb_s_idtarifbphtb_seq'::regclass);


--
-- Name: s_idtarifnpoptkp; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_tarifnpoptkp ALTER COLUMN s_idtarifnpoptkp SET DEFAULT nextval('s_tarifnpoptkp_s_idtarifnpoptkp_seq'::regclass);


--
-- Name: s_idtempo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_tempo ALTER COLUMN s_idtempo SET DEFAULT nextval('s_tempo_s_idtempo_seq'::regclass);


--
-- Name: s_idusers_api; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_users_api ALTER COLUMN s_idusers_api SET DEFAULT nextval('s_users_api_s_idusers_api_seq'::regclass);


--
-- Name: s_idwajib_up; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_wajib_up ALTER COLUMN s_idwajib_up SET DEFAULT nextval('s_wajib_up_s_idwajib_up_seq'::regclass);


--
-- Name: t_idbpn; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_bpn ALTER COLUMN t_idbpn SET DEFAULT nextval('t_bpn_t_idbpn_seq'::regclass);


--
-- Name: t_idfile_keringanan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_file_keringanan ALTER COLUMN t_idfile_keringanan SET DEFAULT nextval('t_file_keringanan_t_idfile_keringanan_seq'::regclass);


--
-- Name: t_idfile_objek; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_file_objek ALTER COLUMN t_idfile_objek SET DEFAULT nextval('t_file_objek_t_idfile_objek_seq'::regclass);


--
-- Name: t_id_filesyarat; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_filesyarat_bphtb ALTER COLUMN t_id_filesyarat SET DEFAULT nextval('t_filesyarat_bphtb_t_id_filesyarat_seq'::regclass);


--
-- Name: t_idajb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_inputajb ALTER COLUMN t_idajb SET DEFAULT nextval('t_inputajb_t_idajb_seq'::regclass);


--
-- Name: t_idlaporbulanan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_laporbulanan_head ALTER COLUMN t_idlaporbulanan SET DEFAULT nextval('t_laporbulanan_head_t_idlaporbulanan_seq'::regclass);


--
-- Name: t_id_nik_bersama; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_nik_bersama ALTER COLUMN t_id_nik_bersama SET DEFAULT nextval('t_nik_bersama_t_id_nik_bersama_seq'::regclass);


--
-- Name: t_id_notif_berkas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_berkas ALTER COLUMN t_id_notif_berkas SET DEFAULT nextval('t_notif_validasi_berkas_t_id_notif_berkas_seq'::regclass);


--
-- Name: t_id_notif_kabid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_kabid ALTER COLUMN t_id_notif_kabid SET DEFAULT nextval('t_notif_validasi_kabid_t_id_notif_kabid_seq'::regclass);


--
-- Name: t_idangsuran; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_angsuran ALTER COLUMN t_idangsuran SET DEFAULT nextval('t_pelayanan_angsuran_t_idangsuran_seq'::regclass);


--
-- Name: t_idkeberatan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keberatan ALTER COLUMN t_idkeberatan SET DEFAULT nextval('t_pelayanan_keberatan_t_idkeberatan_seq'::regclass);


--
-- Name: t_idkeringanan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keringanan ALTER COLUMN t_idkeringanan SET DEFAULT nextval('t_pelayanan_keringanan_t_idkeringanan_seq'::regclass);


--
-- Name: t_idmutasi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_mutasi ALTER COLUMN t_idmutasi SET DEFAULT nextval('t_pelayanan_mutasi_t_idmutasi_seq'::regclass);


--
-- Name: t_idpembatalan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_pembatalan ALTER COLUMN t_idpembatalan SET DEFAULT nextval('t_pelayanan_pembatalan_t_idpembatalan_seq'::regclass);


--
-- Name: t_idpenundaan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_penundaan ALTER COLUMN t_idpenundaan SET DEFAULT nextval('t_pelayanan_penundaan_t_idpenundaan_seq'::regclass);


--
-- Name: t_id_pembayaran; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_bphtb ALTER COLUMN t_id_pembayaran SET DEFAULT nextval('t_pembayaran_bphtb_t_id_pembayaran_seq'::regclass);


--
-- Name: t_id_bayar_skpdkb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkb ALTER COLUMN t_id_bayar_skpdkb SET DEFAULT nextval('t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq'::regclass);


--
-- Name: t_id_bayar_skpdkb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkbt ALTER COLUMN t_id_bayar_skpdkb SET DEFAULT nextval('t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq'::regclass);


--
-- Name: t_idpemeriksa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pemeriksaan ALTER COLUMN t_idpemeriksa SET DEFAULT nextval('t_pemeriksaan_t_idpemeriksa_seq'::regclass);


--
-- Name: t_id_ajbdenda; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_ajb_notaris ALTER COLUMN t_id_ajbdenda SET DEFAULT nextval('t_pen_denda_ajb_notaris_t_id_ajbdenda_seq'::regclass);


--
-- Name: t_id_pendenda; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_lpor_notaris ALTER COLUMN t_id_pendenda SET DEFAULT nextval('t_pen_denda_lpor_notaris_t_id_pendenda_seq'::regclass);


--
-- Name: t_idskpdkb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkb ALTER COLUMN t_idskpdkb SET DEFAULT nextval('t_skpdkb_t_idskpdkb_seq'::regclass);


--
-- Name: t_idskpdkbt; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkbt ALTER COLUMN t_idskpdkbt SET DEFAULT nextval('t_skpdkbt_t_idskpdkbt_seq'::regclass);


--
-- Name: t_idspt; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt ALTER COLUMN t_idspt SET DEFAULT nextval('t_spt_t_idspt_seq'::regclass);


--
-- Name: t_id_validasi_berkas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_validasi_berkas ALTER COLUMN t_id_validasi_berkas SET DEFAULT nextval('t_validasi_berkas_t_id_validasi_berkas_seq'::regclass);


--
-- Name: t_id_validasi_kabid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_validasi_kabid ALTER COLUMN t_id_validasi_kabid SET DEFAULT nextval('t_validasi_kabid_t_id_validasi_kabid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: activity_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY activity_log (id, log_name, description, subject_type, subject_id, causer_type, causer_id, properties, created_at, updated_at) FROM stdin;
1	default	updated	App\\Models\\User	1	App\\Models\\User	1	{"attributes":{"name":"admin","username":"admin","email":"admin@mail.com"},"old":{"name":"admin","username":"admin","email":"admin@mail.com"}}	2021-03-18 13:38:44	2021-03-18 13:38:44
2	default	updated	App\\Models\\User	4	App\\Models\\User	4	{"attributes":{"name":"notaris","username":"notaris","email":"notaris@mail.com"},"old":{"name":"notaris","username":"notaris","email":"notaris@mail.com"}}	2021-03-18 15:21:08	2021-03-18 15:21:08
3	default	updated	App\\Models\\User	4	App\\Models\\User	4	{"attributes":{"name":"notaris","username":"notaris","email":"notaris@mail.com"},"old":{"name":"notaris","username":"notaris","email":"notaris@mail.com"}}	2021-03-18 15:24:04	2021-03-18 15:24:04
4	default	updated	App\\Models\\User	2	App\\Models\\User	1	{"attributes":{"name":"validasi-berkas","username":"validasiberkas","email":"validasi-berkas@mail.com"},"old":{"name":"validasi-berkas","username":"validasiberkas","email":"validasi-berkas@mail.com"}}	2021-03-18 15:26:12	2021-03-18 15:26:12
5	default	updated	App\\Models\\User	4	App\\Models\\User	1	{"attributes":{"name":"notaris","username":"notariss","email":"notaris@mail.com"},"old":{"name":"notaris","username":"notaris","email":"notaris@mail.com"}}	2021-03-18 15:27:10	2021-03-18 15:27:10
6	default	updated	App\\Models\\User	4	App\\Models\\User	1	{"attributes":{"name":"notaris","username":"notaris","email":"notaris@mail.com"},"old":{"name":"notaris","username":"notariss","email":"notaris@mail.com"}}	2021-03-19 13:14:46	2021-03-19 13:14:46
7	default	updated	App\\Models\\User	4	App\\Models\\User	1	{"attributes":{"name":"notaris","username":"notaris","email":"notaris@mail.com"},"old":{"name":"notaris","username":"notaris","email":"notaris@mail.com"}}	2021-03-19 13:15:26	2021-03-19 13:15:26
8	default	created	App\\Models\\Setting\\LoginBackground	3	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/190321135136.jpg","s_id_status":1}}	2021-03-19 13:51:36	2021-03-19 13:51:36
9	default	updated	App\\Models\\User	4	App\\Models\\User	1	{"attributes":{"name":"notaris","username":"notaris","email":"notarisade@mail.com"},"old":{"name":"notaris","username":"notaris","email":"notaris@mail.com"}}	2021-03-19 13:56:58	2021-03-19 13:56:58
10	default	created	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-19 14:08:14	2021-03-19 14:08:14
11	default	created	App\\Models\\Spt\\Filesyarat	1	App\\Models\\User	1	{"attributes":{"t_idspt":20,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/20\\/","nama_file":"images.jpg","t_tgl_upload":"2021-03-19 15:56:53","t_keterangan_file":null,"created_at":"2021-03-19T08:56:53.000000Z","updated_at":"2021-03-19T08:56:53.000000Z"}}	2021-03-19 15:56:54	2021-03-19 15:56:54
12	default	created	App\\Models\\Spt\\Filesyarat	2	App\\Models\\User	1	{"attributes":{"t_idspt":20,"s_idpersyaratan":2,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/20\\/","nama_file":"images2.jpg","t_tgl_upload":"2021-03-19 15:56:57","t_keterangan_file":null,"created_at":"2021-03-19T08:56:57.000000Z","updated_at":"2021-03-19T08:56:57.000000Z"}}	2021-03-19 15:56:57	2021-03-19 15:56:57
13	default	created	App\\Models\\Spt\\Filesyarat	3	App\\Models\\User	1	{"attributes":{"t_idspt":20,"s_idpersyaratan":3,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/20\\/","nama_file":"images-135045376.jpg","t_tgl_upload":"2021-03-19 15:57:01","t_keterangan_file":null,"created_at":"2021-03-19T08:57:01.000000Z","updated_at":"2021-03-19T08:57:01.000000Z"}}	2021-03-19 15:57:01	2021-03-19 15:57:01
14	default	created	App\\Models\\Spt\\Filesyarat	4	App\\Models\\User	1	{"attributes":{"t_idspt":20,"s_idpersyaratan":4,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/20\\/","nama_file":"images2-910855821.jpg","t_tgl_upload":"2021-03-19 15:57:05","t_keterangan_file":null,"created_at":"2021-03-19T08:57:05.000000Z","updated_at":"2021-03-19T08:57:05.000000Z"}}	2021-03-19 15:57:05	2021-03-19 15:57:05
15	default	created	App\\Models\\Spt\\Filesyarat	5	App\\Models\\User	1	{"attributes":{"t_idspt":20,"s_idpersyaratan":5,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/20\\/","nama_file":"images-1115919712.jpg","t_tgl_upload":"2021-03-19 15:57:08","t_keterangan_file":null,"created_at":"2021-03-19T08:57:08.000000Z","updated_at":"2021-03-19T08:57:08.000000Z"}}	2021-03-19 15:57:08	2021-03-19 15:57:08
16	default	created	App\\Models\\Spt\\Filesyarat	6	App\\Models\\User	1	{"attributes":{"t_idspt":20,"s_idpersyaratan":6,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/20\\/","nama_file":"images2-1932719026.jpg","t_tgl_upload":"2021-03-19 15:57:12","t_keterangan_file":null,"created_at":"2021-03-19T08:57:12.000000Z","updated_at":"2021-03-19T08:57:12.000000Z"}}	2021-03-19 15:57:12	2021-03-19 15:57:12
17	default	created	App\\Models\\Spt\\Filesyarat	7	App\\Models\\User	1	{"attributes":{"t_idspt":20,"s_idpersyaratan":7,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/20\\/","nama_file":"images-76595725.jpg","t_tgl_upload":"2021-03-19 15:57:15","t_keterangan_file":null,"created_at":"2021-03-19T08:57:15.000000Z","updated_at":"2021-03-19T08:57:15.000000Z"}}	2021-03-19 15:57:15	2021-03-19 15:57:15
18	default	created	App\\Models\\Spt\\Filefotoobjek	1	App\\Models\\User	1	{"attributes":{"t_idspt":20,"t_iduser_upload":1,"letak_file":"upload\\/file_fotoobjek\\/2021\\/jualbeli\\/20\\/","nama_file":"images.jpg","t_tgl_upload":"2021-03-19 15:57:39","t_keterangan_file":null,"created_at":"2021-03-19T08:57:39.000000Z","updated_at":"2021-03-19T08:57:39.000000Z"}}	2021-03-19 15:57:39	2021-03-19 15:57:39
19	default	updated	App\\Models\\User	4	App\\Models\\User	1	{"attributes":{"name":"notaris","username":"notaris","email":"notarisade@mail.com"},"old":{"name":"notaris","username":"notaris","email":"notarisade@mail.com"}}	2021-03-19 16:14:29	2021-03-19 16:14:29
20	default	updated	App\\Models\\User	4	App\\Models\\User	1	{"attributes":{"name":"notaris","username":"notaris","email":"notarisade@mail.com"},"old":{"name":"notaris","username":"notaris","email":"notarisade@mail.com"}}	2021-03-19 16:50:41	2021-03-19 16:50:41
21	default	updated	App\\Models\\User	4	App\\Models\\User	1	{"attributes":{"name":"notaris","username":"notaris","email":"notarisade@mail.com"},"old":{"name":"notaris","username":"notaris","email":"notarisade@mail.com"}}	2021-03-19 16:51:39	2021-03-19 16:51:39
22	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 09:42:26	2021-03-22 09:42:26
23	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 09:53:01	2021-03-22 09:53:01
24	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 09:53:37	2021-03-22 09:53:37
25	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 09:55:54	2021-03-22 09:55:54
26	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 09:58:48	2021-03-22 09:58:48
27	default	created	App\\Models\\Spt\\Filesyarat	8	App\\Models\\User	1	{"attributes":{"t_idspt":21,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/21\\/","nama_file":"images-231527731.jpg","t_tgl_upload":"2021-03-22 10:01:02","t_keterangan_file":null,"created_at":"2021-03-22T03:01:02.000000Z","updated_at":"2021-03-22T03:01:02.000000Z"}}	2021-03-22 10:01:02	2021-03-22 10:01:02
28	default	created	App\\Models\\Spt\\Filesyarat	9	App\\Models\\User	1	{"attributes":{"t_idspt":21,"s_idpersyaratan":2,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/21\\/","nama_file":"images2-209503751.jpg","t_tgl_upload":"2021-03-22 10:01:07","t_keterangan_file":null,"created_at":"2021-03-22T03:01:07.000000Z","updated_at":"2021-03-22T03:01:07.000000Z"}}	2021-03-22 10:01:07	2021-03-22 10:01:07
29	default	created	App\\Models\\Spt\\Filesyarat	10	App\\Models\\User	1	{"attributes":{"t_idspt":21,"s_idpersyaratan":3,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/21\\/","nama_file":"images-2087958602.jpg","t_tgl_upload":"2021-03-22 10:01:10","t_keterangan_file":null,"created_at":"2021-03-22T03:01:10.000000Z","updated_at":"2021-03-22T03:01:10.000000Z"}}	2021-03-22 10:01:10	2021-03-22 10:01:10
30	default	created	App\\Models\\Spt\\Filesyarat	11	App\\Models\\User	1	{"attributes":{"t_idspt":21,"s_idpersyaratan":4,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/21\\/","nama_file":"images2-1786663174.jpg","t_tgl_upload":"2021-03-22 10:01:14","t_keterangan_file":null,"created_at":"2021-03-22T03:01:14.000000Z","updated_at":"2021-03-22T03:01:14.000000Z"}}	2021-03-22 10:01:14	2021-03-22 10:01:14
31	default	created	App\\Models\\Spt\\Filesyarat	12	App\\Models\\User	1	{"attributes":{"t_idspt":21,"s_idpersyaratan":5,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/21\\/","nama_file":"images-766100924.jpg","t_tgl_upload":"2021-03-22 10:01:18","t_keterangan_file":null,"created_at":"2021-03-22T03:01:18.000000Z","updated_at":"2021-03-22T03:01:18.000000Z"}}	2021-03-22 10:01:18	2021-03-22 10:01:18
32	default	created	App\\Models\\Spt\\Filesyarat	13	App\\Models\\User	1	{"attributes":{"t_idspt":21,"s_idpersyaratan":6,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/21\\/","nama_file":"images2-1888074028.jpg","t_tgl_upload":"2021-03-22 10:01:22","t_keterangan_file":null,"created_at":"2021-03-22T03:01:22.000000Z","updated_at":"2021-03-22T03:01:22.000000Z"}}	2021-03-22 10:01:22	2021-03-22 10:01:22
33	default	created	App\\Models\\Spt\\Filesyarat	14	App\\Models\\User	1	{"attributes":{"t_idspt":21,"s_idpersyaratan":7,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/21\\/","nama_file":"images-305625200.jpg","t_tgl_upload":"2021-03-22 10:01:26","t_keterangan_file":null,"created_at":"2021-03-22T03:01:26.000000Z","updated_at":"2021-03-22T03:01:26.000000Z"}}	2021-03-22 10:01:26	2021-03-22 10:01:26
34	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 10:07:58	2021-03-22 10:07:58
35	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 10:08:47	2021-03-22 10:08:47
36	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 10:10:28	2021-03-22 10:10:28
37	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 10:10:54	2021-03-22 10:10:54
38	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 10:11:47	2021-03-22 10:11:47
39	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 10:20:21	2021-03-22 10:20:21
40	default	created	App\\Models\\User	11	App\\Models\\User	1	{"attributes":{"name":"ade ilyas","username":"adeee","email":"notaris2@gmail.com"}}	2021-03-22 11:04:50	2021-03-22 11:04:50
41	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 13:47:32	2021-03-22 13:47:32
42	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 13:49:00	2021-03-22 13:49:00
43	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 13:49:18	2021-03-22 13:49:18
44	default	updated	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"},"old":{"name":"ade notaris","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 13:50:04	2021-03-22 13:50:04
45	default	created	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 13:51:51	2021-03-22 13:51:51
46	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 13:54:00	2021-03-22 13:54:00
47	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 13:56:19	2021-03-22 13:56:19
48	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 13:56:48	2021-03-22 13:56:48
49	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 13:58:04	2021-03-22 13:58:04
50	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 14:13:08	2021-03-22 14:13:08
51	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 14:18:47	2021-03-22 14:18:47
52	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 14:47:21	2021-03-22 14:47:21
53	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 14:47:53	2021-03-22 14:47:53
54	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 15:34:43	2021-03-22 15:34:43
55	default	created	App\\Models\\Pelaporan\\InputAjb	1	App\\Models\\User	12	{"attributes":{"t_idspt":4,"t_tgl_ajb":"2021-01-01 00:00:00","t_no_ajb":"123123","t_iduser_input":12}}	2021-03-22 15:37:13	2021-03-22 15:37:13
56	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 15:54:54	2021-03-22 15:54:54
57	default	updated	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"},"old":{"name":"notaris2","username":"notaris2","email":"notaris2@gmail.com"}}	2021-03-22 15:57:31	2021-03-22 15:57:31
58	default	created	App\\Models\\User	14	App\\Models\\User	1	{"attributes":{"name":"ade","username":"ade","email":"notarisade@gmail.com"}}	2021-03-22 16:31:53	2021-03-22 16:31:53
59	default	updated	App\\Models\\User	14	App\\Models\\User	1	{"attributes":{"name":"ade","username":"ade","email":"notarisade@gmail.com"},"old":{"name":"ade","username":"ade","email":"notarisade@gmail.com"}}	2021-03-22 16:33:32	2021-03-22 16:33:32
60	default	created	App\\Models\\User	15	App\\Models\\User	1	{"attributes":{"name":"ade","username":"ade","email":"frenandiade@gmail.com"}}	2021-03-22 16:43:07	2021-03-22 16:43:07
61	default	created	App\\Models\\Setting\\LoginBackground	4	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/230321085955.jpg","s_id_status":1}}	2021-03-23 08:59:56	2021-03-23 08:59:56
62	default	created	App\\Models\\Spt\\Filefotoobjek	2	App\\Models\\User	1	{"attributes":{"t_idspt":21,"t_iduser_upload":1,"letak_file":"upload\\/file_fotoobjek\\/2021\\/jualbeli\\/21\\/","nama_file":"Logo Lombok Timur.png","t_tgl_upload":"2021-03-23 09:16:02","t_keterangan_file":null,"created_at":"2021-03-23T02:16:02.000000Z","updated_at":"2021-03-23T02:16:02.000000Z"}}	2021-03-23 09:16:02	2021-03-23 09:16:02
63	default	created	App\\Models\\Setting\\LoginBackground	5	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/230321093016.png","s_id_status":1}}	2021-03-23 09:30:16	2021-03-23 09:30:16
64	default	updated	App\\Models\\Setting\\LoginBackground	5	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/gu3OUPB5tDFRB9Z5MLsaBaQyck0kQWFNT2wXcTHQ.jpg","s_id_status":1},"old":{"s_thumbnail":"images\\/login-background\\/230321093016.png","s_id_status":1}}	2021-03-23 09:30:31	2021-03-23 09:30:31
65	default	created	App\\Models\\Setting\\LoginBackground	6	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/230321093050.jpg","s_id_status":1}}	2021-03-23 09:30:50	2021-03-23 09:30:50
66	default	created	App\\Models\\Setting\\LoginBackground	7	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/230321095224.jpg","s_id_status":1}}	2021-03-23 09:52:24	2021-03-23 09:52:24
67	default	updated	App\\Models\\Setting\\LoginBackground	7	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/sv6yRwblSewi570fjexYTIdyHdgf6rPodr1gn5Vx.jpg","s_id_status":2},"old":{"s_thumbnail":"images\\/login-background\\/230321095224.jpg","s_id_status":1}}	2021-03-23 10:19:14	2021-03-23 10:19:14
68	default	updated	App\\Models\\Setting\\LoginBackground	7	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/bVh3c4gma6LDfsQPoCAezBZQsqiwfaPJr0Bo7jWe.jpg","s_id_status":2},"old":{"s_thumbnail":"images\\/login-background\\/sv6yRwblSewi570fjexYTIdyHdgf6rPodr1gn5Vx.jpg","s_id_status":2}}	2021-03-23 10:20:49	2021-03-23 10:20:49
69	default	updated	App\\Models\\Setting\\LoginBackground	7	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/M9yPgk2ypl13rvkltSwfPSZwfzVq7tlZ6ILPOE5m.jpg","s_id_status":2},"old":{"s_thumbnail":"images\\/login-background\\/bVh3c4gma6LDfsQPoCAezBZQsqiwfaPJr0Bo7jWe.jpg","s_id_status":2}}	2021-03-23 10:27:09	2021-03-23 10:27:09
70	default	updated	App\\Models\\Setting\\LoginBackground	7	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/j9f6GwdGVnXoWbuxGaRzYl43LI6tH95i68aOmSkE.jpg","s_id_status":1},"old":{"s_thumbnail":"images\\/login-background\\/M9yPgk2ypl13rvkltSwfPSZwfzVq7tlZ6ILPOE5m.jpg","s_id_status":2}}	2021-03-23 10:27:27	2021-03-23 10:27:27
71	default	created	App\\Models\\Setting\\LoginBackground	8	App\\Models\\User	1	{"attributes":{"s_thumbnail":"images\\/login-background\\/230321105341.jpg","s_id_status":1}}	2021-03-23 10:53:41	2021-03-23 10:53:41
72	default	created	App\\Models\\Spt\\Filesyarat	15	App\\Models\\User	1	{"attributes":{"t_idspt":12,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/12\\/","nama_file":"images-269065511.jpg","t_tgl_upload":"2021-03-23 14:04:43","t_keterangan_file":null,"created_at":"2021-03-23T07:04:43.000000Z","updated_at":"2021-03-23T07:04:43.000000Z"}}	2021-03-23 14:04:43	2021-03-23 14:04:43
73	default	created	App\\Models\\Spt\\Filesyarat	16	App\\Models\\User	1	{"attributes":{"t_idspt":12,"s_idpersyaratan":2,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/12\\/","nama_file":"images2-127865265.jpg","t_tgl_upload":"2021-03-23 14:04:49","t_keterangan_file":null,"created_at":"2021-03-23T07:04:49.000000Z","updated_at":"2021-03-23T07:04:49.000000Z"}}	2021-03-23 14:04:49	2021-03-23 14:04:49
74	default	created	App\\Models\\Spt\\Filesyarat	17	App\\Models\\User	1	{"attributes":{"t_idspt":12,"s_idpersyaratan":3,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/12\\/","nama_file":"images-170171692.jpg","t_tgl_upload":"2021-03-23 14:04:54","t_keterangan_file":null,"created_at":"2021-03-23T07:04:54.000000Z","updated_at":"2021-03-23T07:04:54.000000Z"}}	2021-03-23 14:04:54	2021-03-23 14:04:54
75	default	created	App\\Models\\Spt\\Filesyarat	18	App\\Models\\User	1	{"attributes":{"t_idspt":12,"s_idpersyaratan":4,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/12\\/","nama_file":"images2-518340461.jpg","t_tgl_upload":"2021-03-23 14:04:58","t_keterangan_file":null,"created_at":"2021-03-23T07:04:58.000000Z","updated_at":"2021-03-23T07:04:58.000000Z"}}	2021-03-23 14:04:58	2021-03-23 14:04:58
76	default	created	App\\Models\\Spt\\Filesyarat	19	App\\Models\\User	1	{"attributes":{"t_idspt":12,"s_idpersyaratan":5,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/12\\/","nama_file":"images-1650234786.jpg","t_tgl_upload":"2021-03-23 14:05:02","t_keterangan_file":null,"created_at":"2021-03-23T07:05:02.000000Z","updated_at":"2021-03-23T07:05:02.000000Z"}}	2021-03-23 14:05:02	2021-03-23 14:05:02
77	default	created	App\\Models\\Spt\\Filesyarat	20	App\\Models\\User	1	{"attributes":{"t_idspt":12,"s_idpersyaratan":6,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/12\\/","nama_file":"images-1761278908.jpg","t_tgl_upload":"2021-03-23 14:05:06","t_keterangan_file":null,"created_at":"2021-03-23T07:05:06.000000Z","updated_at":"2021-03-23T07:05:06.000000Z"}}	2021-03-23 14:05:06	2021-03-23 14:05:06
78	default	created	App\\Models\\Spt\\Filesyarat	21	App\\Models\\User	1	{"attributes":{"t_idspt":12,"s_idpersyaratan":7,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/12\\/","nama_file":"images2-203826003.jpg","t_tgl_upload":"2021-03-23 14:05:10","t_keterangan_file":null,"created_at":"2021-03-23T07:05:10.000000Z","updated_at":"2021-03-23T07:05:10.000000Z"}}	2021-03-23 14:05:10	2021-03-23 14:05:10
79	default	created	App\\Models\\Spt\\Filefotoobjek	3	App\\Models\\User	1	{"attributes":{"t_idspt":12,"t_iduser_upload":1,"letak_file":"upload\\/file_fotoobjek\\/2021\\/jualbeli\\/12\\/","nama_file":"images2.jpg","t_tgl_upload":"2021-03-23 14:05:25","t_keterangan_file":null,"created_at":"2021-03-23T07:05:25.000000Z","updated_at":"2021-03-23T07:05:25.000000Z"}}	2021-03-23 14:05:25	2021-03-23 14:05:25
80	default	created	App\\Models\\Spt\\Spt	22	App\\Models\\User	15	{"attributes":{"t_kohirspt":22,"t_tgldaftar_spt":"2021-03-23 14:54:56","t_tglby_system":"2021-03-23 14:54:56","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":1,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"ade ilyas","t_nik_pembeli":"1111111111111111","t_npwp_pembeli":"22.222.222.2-222.222","t_jalan_pembeli":"jalan rempoa","t_kabkota_pembeli":"Tangerang Selatan","t_idkec_pembeli":2,"t_namakecamatan_pembeli":"KUSAN HILIR","t_idkel_pembeli":2,"t_namakelurahan_pembeli":"SUNGAI LEMBU","t_rt_pembeli":"003","t_rw_pembeli":"002","t_nohp_pembeli":"098769876","t_notelp_pembeli":"09876543","t_email_pembeli":"frenan@gmail.com","t_kodepos_pembeli":"98765","t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-03-23 14:54:56	2021-03-23 14:54:56
81	default	created	App\\Models\\Spt\\Filesyarat	22	App\\Models\\User	15	{"attributes":{"t_idspt":22,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":15,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"images-811557375.jpg","t_tgl_upload":"2021-03-23 15:00:49","t_keterangan_file":null,"created_at":"2021-03-23T08:00:49.000000Z","updated_at":"2021-03-23T08:00:49.000000Z"}}	2021-03-23 15:00:49	2021-03-23 15:00:49
82	default	created	App\\Models\\Spt\\Filesyarat	23	App\\Models\\User	15	{"attributes":{"t_idspt":22,"s_idpersyaratan":2,"s_idjenistransaksi":1,"t_iduser_upload":15,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"images2-1371143477.jpg","t_tgl_upload":"2021-03-23 15:00:53","t_keterangan_file":null,"created_at":"2021-03-23T08:00:53.000000Z","updated_at":"2021-03-23T08:00:53.000000Z"}}	2021-03-23 15:00:53	2021-03-23 15:00:53
83	default	created	App\\Models\\Spt\\Filesyarat	24	App\\Models\\User	15	{"attributes":{"t_idspt":22,"s_idpersyaratan":3,"s_idjenistransaksi":1,"t_iduser_upload":15,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"images-1916385544.jpg","t_tgl_upload":"2021-03-23 15:00:57","t_keterangan_file":null,"created_at":"2021-03-23T08:00:57.000000Z","updated_at":"2021-03-23T08:00:57.000000Z"}}	2021-03-23 15:00:57	2021-03-23 15:00:57
84	default	created	App\\Models\\Spt\\Filesyarat	25	App\\Models\\User	15	{"attributes":{"t_idspt":22,"s_idpersyaratan":4,"s_idjenistransaksi":1,"t_iduser_upload":15,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"images2-1129752582.jpg","t_tgl_upload":"2021-03-23 15:01:01","t_keterangan_file":null,"created_at":"2021-03-23T08:01:01.000000Z","updated_at":"2021-03-23T08:01:01.000000Z"}}	2021-03-23 15:01:01	2021-03-23 15:01:01
85	default	created	App\\Models\\Spt\\Filesyarat	26	App\\Models\\User	15	{"attributes":{"t_idspt":22,"s_idpersyaratan":5,"s_idjenistransaksi":1,"t_iduser_upload":15,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"images-335209063.jpg","t_tgl_upload":"2021-03-23 15:01:04","t_keterangan_file":null,"created_at":"2021-03-23T08:01:04.000000Z","updated_at":"2021-03-23T08:01:04.000000Z"}}	2021-03-23 15:01:04	2021-03-23 15:01:04
86	default	created	App\\Models\\Spt\\Filesyarat	27	App\\Models\\User	15	{"attributes":{"t_idspt":22,"s_idpersyaratan":6,"s_idjenistransaksi":1,"t_iduser_upload":15,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"images2-1232128825.jpg","t_tgl_upload":"2021-03-23 15:01:08","t_keterangan_file":null,"created_at":"2021-03-23T08:01:08.000000Z","updated_at":"2021-03-23T08:01:08.000000Z"}}	2021-03-23 15:01:08	2021-03-23 15:01:08
87	default	created	App\\Models\\Spt\\Filesyarat	28	App\\Models\\User	15	{"attributes":{"t_idspt":22,"s_idpersyaratan":7,"s_idjenistransaksi":1,"t_iduser_upload":15,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"images-1945254751.jpg","t_tgl_upload":"2021-03-23 15:01:12","t_keterangan_file":null,"created_at":"2021-03-23T08:01:12.000000Z","updated_at":"2021-03-23T08:01:12.000000Z"}}	2021-03-23 15:01:12	2021-03-23 15:01:12
88	default	created	App\\Models\\Spt\\Filefotoobjek	4	App\\Models\\User	15	{"attributes":{"t_idspt":22,"t_iduser_upload":15,"letak_file":"upload\\/file_fotoobjek\\/2021\\/jualbeli\\/22\\/","nama_file":"images-1029836684.jpg","t_tgl_upload":"2021-03-23 15:01:27","t_keterangan_file":null,"created_at":"2021-03-23T08:01:27.000000Z","updated_at":"2021-03-23T08:01:27.000000Z"}}	2021-03-23 15:01:27	2021-03-23 15:01:27
\.


--
-- Name: activity_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('activity_log_id_seq', 88, true);


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('failed_jobs_id_seq', 1, false);


--
-- Data for Name: his_getbphtb_bpn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY his_getbphtb_bpn (t_idhis_getbpn, t_username, t_password, t_nop, t_ntpd, t_nik, t_nama, t_alamat, t_kelurahan_op, t_kecamatan_op, t_kota_kab_op, t_luastanah, t_luasbangunan, t_pembayaran, t_status, t_tanggal_pembayaran, t_jenisbayar, respon_code, t_iduser, t_tglby_system, created_at, updated_at) FROM stdin;
\.


--
-- Name: his_getbphtb_bpn_t_idhis_getbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('his_getbphtb_bpn_t_idhis_getbpn_seq', 1, false);


--
-- Data for Name: his_getpbb_bpn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY his_getpbb_bpn (t_idhis_pbbbpn, t_username, t_password, t_nop, t_nik, t_nama_wp, t_alamat_op, t_kecamatan_op, t_kelurahan_op, t_kota_kab_op, t_luas_tanah_op, t_luas_bangunan_op, t_njop_tanah_op, t_njop_bangunan_op, t_status_tunggakan, t_iduser, t_tglby_system, created_at, updated_at) FROM stdin;
\.


--
-- Name: his_getpbb_bpn_t_idhis_pbbbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('his_getpbb_bpn_t_idhis_pbbbpn_seq', 1, false);


--
-- Data for Name: his_postdatabpn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY his_postdatabpn (t_idhis_posbpn, t_username, t_password, t_aktaid, t_tgl_akta, t_nop, t_nik, t_nib, t_npwp, t_nama_wp, t_alamat_op, t_kecamatan_op, t_kelurahan_op, t_kota_kab_op, t_luastanah_op, t_luasbangunan_op, t_no_sertipikat, t_no_akta, t_ppat, t_status, t_iduser, t_tglby_system, created_at, updated_at) FROM stdin;
\.


--
-- Name: his_postdatabpn_t_idhis_posbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('his_postdatabpn_t_idhis_posbpn_seq', 1, false);


--
-- Data for Name: integrasi_sertifikat_bpn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY integrasi_sertifikat_bpn (t_idsertifikat, aktaid, tgl_akta, nop, nib, nik, npwp, nama_wp, alamat_op, kelurahan_op, kecamatan_op, kota_op, luastanah_op, luasbangunan_op, ppat, no_sertipikat, no_akta, t_iduser, t_tgltransaksi, created_at, updated_at) FROM stdin;
\.


--
-- Name: integrasi_sertifikat_bpn_t_idsertifikat_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('integrasi_sertifikat_bpn_t_idsertifikat_seq', 1, false);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY migrations (id, migration, batch) FROM stdin;
1	2020_11_24_000000_create_s_pejabat_table	1
2	2020_11_24_000001_create_s_notaris_table	1
3	2020_11_24_000002_create_users_table	1
4	2020_11_24_000003_create_failed_jobs_table	1
5	2020_11_24_000004_create_password_resets_table	1
6	2020_11_25_000001_create_s_status_table	1
7	2020_11_25_000002_create_s_acuan_table	1
8	2020_11_25_000003_create_s_harga_history_table	1
9	2020_11_25_000004_create_s_jenisdoktanah_table	1
10	2020_11_25_000005_create_s_jenishaktanah_table	1
11	2020_11_25_000006_create_s_jenisketetapan_table	1
12	2020_11_25_000007_create_s_jenispengurangan_table	1
13	2020_11_25_000008_create_s_jenistanah_table	1
14	2020_11_25_000009_create_s_jenistransaksi_table	1
15	2020_11_25_000010_create_s_kecamatan_table	1
16	2020_11_25_000011_create_s_kelurahan_table	1
17	2020_11_25_000012_create_s_koderekening_table	1
18	2020_11_25_000013_create_s_pemda_table	1
19	2020_11_25_000014_create_s_presentase_wajar_table	1
20	2020_11_25_000015_create_s_wajib_up_table	1
21	2020_11_25_000016_create_s_tarifbphtb_table	1
22	2020_11_25_000017_create_s_tarifnpoptkp_table	1
23	2020_11_25_000018_create_s_tempo_table	1
24	2020_11_25_000019_create_s_background_table	1
25	2020_11_25_000020_create_s_persyaratan	1
26	2020_11_25_000021_create_permission_tables	1
27	2020_11_25_000022_create_activity_log_table	1
28	2020_11_25_000023_create_s_jenisfasilitas_table	1
29	2020_11_25_000024_create_t_spt_table	1
30	2020_11_25_000025_create_pelayanan_keringanans_table	1
31	2020_11_25_000026_create_pelayanan_pembatalans_table	1
32	2020_11_26_000001_create_integrasi_sertifikat_bpn_table	1
33	2020_11_26_000002_create_his_get_bphtb_bpn_table	1
34	2020_11_26_000002_create_his_getpbb_bpn_table	1
35	2020_11_26_000003_create_his_postpbb_bpn_table	1
36	2020_11_26_000004_create_t_inputajb_table	1
37	2020_11_26_000005_create_s_target_table	1
38	2020_11_26_000006_create_s_acuan_jenistanah_table	1
39	2020_11_26_000007_create_t_validasi_berkas_table	1
40	2021_01_12_140000_create_notifications_table	1
41	2021_01_12_184458_add_column_to_validasi_berkas	1
42	2021_02_24_150805_alter_spt_table	1
\.


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('migrations_id_seq', 42, true);


--
-- Data for Name: model_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY model_has_permissions (permission_id, model_type, model_id) FROM stdin;
\.


--
-- Data for Name: model_has_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY model_has_roles (role_id, model_type, model_id) FROM stdin;
1	App\\Models\\User	1
2	App\\Models\\User	2
3	App\\Models\\User	3
4	App\\Models\\User	4
5	App\\Models\\User	5
6	App\\Models\\User	6
7	App\\Models\\User	7
8	App\\Models\\User	8
9	App\\Models\\User	9
4	App\\Models\\User	10
4	App\\Models\\User	12
4	App\\Models\\User	14
4	App\\Models\\User	15
\.


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY notifications (id, type, notifiable_type, notifiable_id, data, read_at, created_at, updated_at) FROM stdin;
d127edd0-af9d-43e4-8a30-eb3b618ba4e9	App\\Notifications\\PendaftaranNotification	App\\Models\\User	1	{"t_idspt":20,"target_notif":"verifikasi-berkas","created_at":null}	\N	2021-03-19 16:06:23	2021-03-19 16:06:23
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY permissions (id, name, guard_name, created_at, updated_at) FROM stdin;
1	MenuPendaftaran	web	2021-03-18 08:54:47	2021-03-18 08:54:47
2	SettingPemda	web	2021-03-18 08:54:47	2021-03-18 08:54:47
3	SettingKecamatan	web	2021-03-18 08:54:47	2021-03-18 08:54:47
4	SettingKelurahan	web	2021-03-18 08:54:47	2021-03-18 08:54:47
5	SettingPejabat	web	2021-03-18 08:54:47	2021-03-18 08:54:47
6	SettingNotaris	web	2021-03-18 08:54:47	2021-03-18 08:54:47
7	SettingJenisTransaksi	web	2021-03-18 08:54:47	2021-03-18 08:54:47
8	SettingPersyaratan	web	2021-03-18 08:54:47	2021-03-18 08:54:47
9	SettingHakTanah	web	2021-03-18 08:54:47	2021-03-18 08:54:47
10	SettingTarifBphtb	web	2021-03-18 08:54:47	2021-03-18 08:54:47
11	SettingTarifNpoptkp	web	2021-03-18 08:54:47	2021-03-18 08:54:47
12	SettingLoginBackground	web	2021-03-18 08:54:47	2021-03-18 08:54:47
13	SettingUser	web	2021-03-18 08:54:47	2021-03-18 08:54:47
14	SettingRole	web	2021-03-18 08:54:47	2021-03-18 08:54:47
15	SettingPermission	web	2021-03-18 08:54:47	2021-03-18 08:54:47
16	SettingAssignPermissionToRole	web	2021-03-18 08:54:47	2021-03-18 08:54:47
17	SettingAssignRoleToUser	web	2021-03-18 08:54:47	2021-03-18 08:54:47
18	SettingPersentasewajar	web	2021-03-18 08:54:47	2021-03-18 08:54:47
19	PelaporanAjb	web	2021-03-18 08:54:47	2021-03-18 08:54:47
20	Informasiop	web	2021-03-18 08:54:47	2021-03-18 08:54:47
21	PelayananKeringanan	web	2021-03-18 08:54:47	2021-03-18 08:54:47
22	PelayananKeberatan	web	2021-03-18 08:54:47	2021-03-18 08:54:47
23	PelayananAngsuran	web	2021-03-18 08:54:47	2021-03-18 08:54:47
24	PelayananPenundaan	web	2021-03-18 08:54:47	2021-03-18 08:54:47
25	PelayananPermohonanmutasi	web	2021-03-18 08:54:47	2021-03-18 08:54:47
26	PelayananPembatalan	web	2021-03-18 08:54:47	2021-03-18 08:54:47
27	Bpn	web	2021-03-18 08:54:47	2021-03-18 08:54:47
28	KppPratama	web	2021-03-18 08:54:47	2021-03-18 08:54:47
29	HistoryActivity	web	2021-03-18 08:54:47	2021-03-18 08:54:47
31	ValidasiKabid	web	2021-03-18 08:54:47	2021-03-18 08:54:47
32	Pemeriksaan	web	2021-03-18 08:54:47	2021-03-18 08:54:47
34	Pembayaran	web	2021-03-18 08:54:47	2021-03-18 08:54:47
35	Menucekniknib	web	2021-03-18 08:54:47	2021-03-18 08:54:47
33	MenuHargaWajar	web	2021-03-18 08:54:47	2021-03-18 08:54:47
36	MenuPelaporanNotaris	web	2021-03-18 15:07:48	2021-03-18 15:07:48
37	MenuHistoryTransaksi	web	2021-03-18 15:09:33	2021-03-18 15:09:33
38	SettingPass	web	2021-03-18 15:11:33	2021-03-18 15:11:33
39	DendaLaporanAjb	web	2021-03-18 15:34:16	2021-03-18 15:34:16
40	DendaAjb	web	2021-03-18 15:34:32	2021-03-18 15:34:32
41	MenuValidasiKabid	web	2021-03-18 15:37:38	2021-03-18 15:37:38
42	MenuPemeriksaan	web	2021-03-19 09:29:14	2021-03-19 09:29:14
43	Skpdkb	web	2021-03-19 09:31:45	2021-03-19 09:31:45
44	Skpdkbt	web	2021-03-19 09:32:04	2021-03-19 09:32:04
45	MenuCetakLaporan	web	2021-03-19 09:33:15	2021-03-19 09:33:15
30	MenuValidasiBerkas	web	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('permissions_id_seq', 45, true);


--
-- Data for Name: role_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY role_has_permissions (permission_id, role_id) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
13	1
14	1
15	1
16	1
17	1
18	1
19	1
20	1
21	1
22	1
23	1
24	1
25	1
26	1
27	1
28	1
29	1
30	1
31	1
32	1
33	1
34	1
35	1
1	3
19	3
20	3
29	3
30	3
31	3
32	3
35	3
33	3
36	3
37	3
38	3
39	3
40	3
27	5
28	6
34	7
26	8
32	9
41	3
42	3
43	3
44	3
45	3
1	2
19	2
32	2
35	2
33	2
36	2
37	2
1	4
19	4
20	4
35	4
36	4
37	4
38	4
38	2
39	2
40	2
42	2
30	2
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY roles (id, name, guard_name, created_at, updated_at) FROM stdin;
1	admin	web	2021-03-18 08:54:47	2021-03-18 08:54:47
2	validasiberkas	web	2021-03-18 08:54:47	2021-03-18 08:54:47
3	validasikabid	web	2021-03-18 08:54:47	2021-03-18 08:54:47
4	notaris	web	2021-03-18 08:54:47	2021-03-18 08:54:47
5	bpn	web	2021-03-18 08:54:47	2021-03-18 08:54:47
6	kpppratama	web	2021-03-18 08:54:47	2021-03-18 08:54:47
7	bendahara	web	2021-03-18 08:54:47	2021-03-18 08:54:47
8	wajibpajak	web	2021-03-18 08:54:47	2021-03-18 08:54:47
9	pemeriksa	web	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('roles_id_seq', 12, true);


--
-- Data for Name: s_acuan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_acuan (s_idacuan, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_kd_blok, s_permetertanah, created_at, updated_at) FROM stdin;
1	63	10	010	036	001	1000000	2021-03-18 08:54:47	2021-03-18 08:54:47
2	63	10	010	036	002	2000000	2021-03-18 08:54:47	2021-03-18 08:54:47
3	63	10	010	036	003	500000	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Data for Name: s_acuan_jenistanah; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_acuan_jenistanah (s_idacuan_jenis, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_kd_blok, id_jenistanah, s_permetertanah, created_at, updated_at) FROM stdin;
1	63	10	010	036	003	1	500000	2021-03-18 08:54:47	2021-03-18 08:54:47
2	63	10	010	036	003	2	1500000	2021-03-18 08:54:47	2021-03-18 08:54:47
3	63	10	010	036	003	1	300000	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_acuan_jenistanah_s_idacuan_jenis_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_acuan_jenistanah_s_idacuan_jenis_seq', 4, false);


--
-- Name: s_acuan_s_idacuan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_acuan_s_idacuan_seq', 4, false);


--
-- Data for Name: s_background; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_background (id, s_thumbnail, s_id_status, s_iduser, created_at, updated_at) FROM stdin;
6	images/login-background/230321093050.jpg	1	\N	2021-03-23 09:30:50	2021-03-23 09:30:50
8	images/login-background/230321105341.jpg	2	\N	2021-03-23 10:53:41	2021-03-23 10:53:41
\.


--
-- Name: s_background_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_background_id_seq', 8, true);


--
-- Data for Name: s_hak_akses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_hak_akses (s_id_hakakses, s_nama_hakakses) FROM stdin;
1	admin
2	operatorberkas
3	operatorkabid
4	notaris
5	bpn
6	kpppratama
7	operatorbendahara
8	wp
9	pemeriksa
\.


--
-- Name: s_hak_akses_s_id_hakakses_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_hak_akses_s_id_hakakses_seq', 10, false);


--
-- Data for Name: s_harga_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_harga_history (s_idhistory, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_kd_blok, s_kd_urut, s_kd_jenis, s_tahun_sppt, s_permetertanah, id_jenistanah, created_at, updated_at) FROM stdin;
\.


--
-- Name: s_harga_history_s_idhistory_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_harga_history_s_idhistory_seq', 1, false);


--
-- Data for Name: s_jenis_bidangusaha; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_jenis_bidangusaha (s_idbidang_usaha, s_nama_bidangusaha, created_at, updated_at) FROM stdin;
1	Pribadi	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Badan Usaha	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_jenis_bidangusaha_s_idbidang_usaha_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_jenis_bidangusaha_s_idbidang_usaha_seq', 3, false);


--
-- Data for Name: s_jenisdoktanah; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_jenisdoktanah (s_iddoktanah, s_namadoktanah, created_at, updated_at) FROM stdin;
1	Sertifikat	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Warkah	2021-03-18 08:54:47	2021-03-18 08:54:47
3	Girik	2021-03-18 08:54:47	2021-03-18 08:54:47
4	Letter C	2021-03-18 08:54:47	2021-03-18 08:54:47
5	SPT	2021-03-18 08:54:47	2021-03-18 08:54:47
6	NIB (Nomor Identifikasi Bidang Tanah)	2021-03-18 08:54:47	2021-03-18 08:54:47
7	Akta	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_jenisdoktanah_s_iddoktanah_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_jenisdoktanah_s_iddoktanah_seq', 8, false);


--
-- Data for Name: s_jenisfasilitas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_jenisfasilitas (s_idjenisfasilitas, s_kodejenisfasilitas, s_namajenisfasilitas, created_at, updated_at) FROM stdin;
1	01	Perwakilan diplomatik, konsulat berdasarkan asas perlakuan timbal balik	2021-03-18 08:54:47	2021-03-18 08:54:47
2	02	Negara untuk penyelenggaraan pemerintah dan atau untuk pelaksanaan pembangunan guna kepentingan umum	2021-03-18 08:54:47	2021-03-18 08:54:47
3	03	Badan atau perwakilan organisasi internasional yang ditetapkan oleh Menteri Keuangan	2021-03-18 08:54:47	2021-03-18 08:54:47
4	04	Orang pribadi atau badan karena konversi hak dan perbuatan hukum lain dengan tidak adanya perubahan nama	2021-03-18 08:54:47	2021-03-18 08:54:47
5	05	Karena wakaf atau warisan	2021-03-18 08:54:47	2021-03-18 08:54:47
6	06	Untuk digunakan kepentingan ibadah	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_jenisfasilitas_s_idjenisfasilitas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_jenisfasilitas_s_idjenisfasilitas_seq', 7, false);


--
-- Data for Name: s_jenishaktanah; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_jenishaktanah (s_idhaktanah, s_kodehaktanah, s_namahaktanah, created_at, updated_at) FROM stdin;
1	01	Hak Milik	2021-03-18 08:54:47	2021-03-18 08:54:47
2	02	Hak Guna Usaha	2021-03-18 08:54:47	2021-03-18 08:54:47
3	03	Hak Guna Bangunan	2021-03-18 08:54:47	2021-03-18 08:54:47
4	04	Hak Pakai	2021-03-18 08:54:47	2021-03-18 08:54:47
5	05	Hak Milik Atas Satuan Rumah	2021-03-18 08:54:47	2021-03-18 08:54:47
6	05	Hak Pengelolaan	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_jenishaktanah_s_idhaktanah_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_jenishaktanah_s_idhaktanah_seq', 7, false);


--
-- Data for Name: s_jenisketetapan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_jenisketetapan (s_idjenisketetapan, s_namajenisketetapan, s_namasingkatjenisketetapan, created_at, updated_at) FROM stdin;
1	Surat Setoran Pajak Daerah	SSPD	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Surat Ketetapan Pajak Daerah Kurang Bayar	SKPDKB	2021-03-18 08:54:47	2021-03-18 08:54:47
3	Surat Ketetapan Pajak Daerah Kurang Bayar Tambahan	SKPDKBT	2021-03-18 08:54:47	2021-03-18 08:54:47
4	Surat Ketetapan Pajak Daerah Lebih Bayar	SKPDLB	2021-03-18 08:54:47	2021-03-18 08:54:47
5	Surat Ketetapan Pajak Daerah Nihil	SKPDN	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_jenisketetapan_s_idjenisketetapan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_jenisketetapan_s_idjenisketetapan_seq', 6, false);


--
-- Data for Name: s_jenispengurangan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_jenispengurangan (s_idpengurangan, s_persentase, s_namapengurangan, created_at, updated_at) FROM stdin;
1	50	Waris / Hibah Wasiat	2021-03-18 08:54:47	2021-03-18 08:54:47
2	75	Pensiunan TNI/POLRI 2	2021-03-18 08:54:47	2021-03-18 08:54:47
3	75	Perumahan Dinas TNI/POLRI	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_jenispengurangan_s_idpengurangan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_jenispengurangan_s_idpengurangan_seq', 4, false);


--
-- Data for Name: s_jenistanah; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_jenistanah (id_jenistanah, nama_jenistanah, created_at, updated_at) FROM stdin;
1	Bukan Perumahan	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Perumahan	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_jenistanah_id_jenistanah_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_jenistanah_id_jenistanah_seq', 3, false);


--
-- Data for Name: s_jenistransaksi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_jenistransaksi (s_idjenistransaksi, s_kodejenistransaksi, s_namajenistransaksi, s_idstatus_pht, s_id_dptnpoptkp, created_at, updated_at) FROM stdin;
1	01	Jual Beli	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
2	02	Tukar Menukar	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
3	03	Hibah	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
4	04	Hibah Wasiat	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
5	05	Waris	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
6	06	Pemasukan Dalam Perseroan atau Badan Hukum Lainnya	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
7	07	Pemisahan Hak yang Mengakibatkan Peralihan	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
8	08	Penunjukan Pembeli Dalam Lelang	3	2	2021-03-18 08:54:47	2021-03-18 08:54:47
9	09	Pelaksanaan Putusan Hakim yang Mempunyai Kekuatan Hukum Tetap	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
10	10	Penggabungan Usaha	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
11	11	Peleburan Usaha	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
12	12	Pemekaran Usaha	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
13	13	Hadiah	3	2	2021-03-18 08:54:47	2021-03-18 08:54:47
14	14	Perolehan Hak Rumah Sederhana Sehat dan RSS Melalui KPR Bersubsidi	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
15	15	Pemberian Hak Baru Sebagai Kelanjutan Pelepasan Hak	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
16	16	Pemberian Hak Baru Diluar Pelepasan Hak	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_jenistransaksi_s_idjenistransaksi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_jenistransaksi_s_idjenistransaksi_seq', 17, false);


--
-- Data for Name: s_kecamatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_kecamatan (s_idkecamatan, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_namakecamatan, s_latitude, s_longitude, created_at, updated_at) FROM stdin;
1	63	10	000	LUAR DAERAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
2	63	10	010	KUSAN HILIR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
3	63	10	020	SUNGAI LOBAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
4	63	10	030	SATUI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
5	63	10	040	KUSAN HULU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
6	63	10	050	BATU LICIN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
7	63	10	060	SIMPANG EMPAT	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
8	63	10	070	KARANG BINTANG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
9	63	10	080	MANTEWE	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
10	63	10	090	ANGSANA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
11	63	10	100	KURANJI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_kecamatan_s_idkecamatan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_kecamatan_s_idkecamatan_seq', 12, false);


--
-- Data for Name: s_kelurahan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_kelurahan (s_idkelurahan, s_idkecamatan, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_namakelurahan, s_latitude, s_longitude, created_at, updated_at) FROM stdin;
1	1	63	10	000	000	LUAR DAERAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
2	2	63	10	010	001	SUNGAI LEMBU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
5	2	63	10	010	004	BATUAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
6	2	63	10	010	015	BETUNG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
7	2	63	10	010	016	PULAU SALAK	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
8	2	63	10	010	017	BERINGIN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
9	2	63	10	010	018	SUNGAI LEMBU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
10	2	63	10	010	019	GUSUNGE	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
11	2	63	10	010	020	WIRITTASI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
12	2	63	10	010	021	JUKU EJA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
13	2	63	10	010	022	PEJALA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
14	2	63	10	010	023	KOTA PAGATAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
15	2	63	10	010	024	KAMPUNG BARU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
16	2	63	10	010	025	MUARA PAGATAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
17	2	63	10	010	026	SEPUNGGUR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
18	2	63	10	010	027	API-API	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
19	2	63	10	010	028	MUARA PAGATAN TENGAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
20	2	63	10	010	029	RANTAU PANJANG HILIR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
21	2	63	10	010	030	TANETE	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
22	2	63	10	010	031	RANTAU PANJANG HULU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
23	2	63	10	010	032	PENYOLONGAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
24	2	63	10	010	033	PULAU SATU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
25	2	63	10	010	034	MUDALANG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
26	2	63	10	010	035	PASAR BARU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
27	2	63	10	010	036	BATUAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
28	2	63	10	010	037	PAGAR RUYUNG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
29	2	63	10	010	038	BARU GELANG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
30	2	63	10	010	039	MEKAR JAYA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
31	2	63	10	010	040	BETARANG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
32	2	63	10	010	041	MANURUNG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
33	2	63	10	010	042	PAKATELU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
34	2	63	10	010	043	SARING SUNGAI BUBU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
35	2	63	10	010	044	SARING SUNGAI BINJAI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
36	2	63	10	010	045	PULAU TANJUNG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
37	2	63	10	010	046	SALIMURAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
38	2	63	10	010	047	SATIUNG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
39	2	63	10	010	048	SERDANGAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
40	2	63	10	010	049	KARYA BAKTI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
41	3	63	10	020	001	SEBAMBAN BARU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
42	3	63	10	020	002	SEBAMBAN LAMA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
43	3	63	10	020	003	DWI MARGA UTAMA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
44	3	63	10	020	004	SUNGAI DUA LAUT	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
45	3	63	10	020	005	MARGA MULIA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
46	3	63	10	020	006	SARI MULYA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
47	3	63	10	020	007	TRI MULYA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
48	3	63	10	020	008	KERTA BUANA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
49	3	63	10	020	009	TRI MARTANI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
50	3	63	10	020	010	BATU MERANTI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
51	3	63	10	020	012	SUMBER MAKMUR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
52	3	63	10	020	013	SARI UTAMA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
53	3	63	10	020	014	SUNGAI LOBAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
54	3	63	10	020	015	KUD TUWUH SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
55	3	63	10	020	016	BIDURI BERSUJUD	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
56	3	63	10	020	017	DAMAR INDAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
57	3	63	10	020	018	SUMBER SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
58	3	63	10	020	019	WANASARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
59	4	63	10	030	001	SUNGAI CUKA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
60	4	63	10	030	002	SUNGAI DANAU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
61	4	63	10	030	003	SATUI TIMUR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
62	4	63	10	030	004	SATUI BARAT	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
63	4	63	10	030	005	SEKAPUK	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
64	4	63	10	030	006	SETARAP	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
65	4	63	10	030	007	TEGAL SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
66	4	63	10	030	012	SEBAMBAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
67	4	63	10	030	015	SUMBER MAKMUR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
68	4	63	10	030	016	WONO REJO	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
69	4	63	10	030	017	JOMBANG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
70	4	63	10	030	018	SEJAHTERA MULIA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
71	4	63	10	030	019	WONO REJO (UPT)	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
72	4	63	10	030	020	SUMBER ARUM	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
73	4	63	10	030	021	MAKMUR MULIA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
74	4	63	10	030	022	KUD NUSANTARA-->berubah KOPBSS	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
75	4	63	10	030	023	SINAR BULAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
76	4	63	10	030	024	AL-KAUTSAR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
77	4	63	10	030	025	BUANA SAWIT SEJAHTERA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
78	4	63	10	030	026	PANDAMARAN JAYA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
79	5	63	10	040	001	AIB--->gabung	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
80	5	63	10	040	002	MANGKALAPI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
81	5	63	10	040	003	KUNDUR-->gabung	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
82	5	63	10	040	004	TUNGKARAN LIMAU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
83	5	63	10	040	008	BAKARANGAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
84	5	63	10	040	009	LASUNG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
85	5	63	10	040	010	SUNGAI RUKAM	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
86	5	63	10	040	011	MANUNTUNG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
87	5	63	10	040	012	ANJIR BARU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
88	5	63	10	040	013	BINA WARA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
89	5	63	10	040	014	PACAKAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
90	5	63	10	040	015	BINABARU--->gabung	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
91	5	63	10	040	016	TIBARAN PANJANG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
92	5	63	10	040	017	GUNTUNG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
93	5	63	10	040	018	DARASAN BINJAI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
94	5	63	10	040	019	BATU AMPARAN--->gabung	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
95	5	63	10	040	020	TAPUS	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
96	5	63	10	040	021	HATIIF	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
97	5	63	10	040	022	TELUK KEPAYANG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
98	5	63	10	040	023	KARANG MULYA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
99	5	63	10	040	024	HARAPAN JAYA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
100	5	63	10	040	025	WONOREJO	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
101	5	63	10	040	026	KARANG SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
102	5	63	10	040	027	BATU BULAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
103	5	63	10	040	028	TAMUNIH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
104	5	63	10	040	029	KUD TUWUH SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
105	5	63	10	040	030	KSU BERKAH BERSAMA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
106	6	63	10	050	001	POLEWALI MARAJAE	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
107	6	63	10	050	002	UPT PANDAN SARI --> PINDAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
108	6	63	10	050	003	DANAU INDAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
109	6	63	10	050	004	SUKA MAJU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
110	6	63	10	050	005	MAJU BERSAMA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
111	6	63	10	050	006	MAJU MAKMUR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
112	6	63	10	050	007	KURANJI--->gabung	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
113	6	63	10	050	008	GUNUNG TINGGI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
114	6	63	10	050	009	SEGUMBANG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
115	6	63	10	050	010	KERSIK PUTIH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
116	6	63	10	050	011	PULAU SEWANGI--->gabung	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
117	6	63	10	050	012	BATU LICIN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
118	6	63	10	050	013	KAMPUNG BARU --> PINDAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
119	6	63	10	050	014	PULAU BURUNG--->gabung	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
120	6	63	10	050	015	TUNGKARAN PANGERAN --> PINDAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
121	6	63	10	050	016	TELUK BAKU--->gabung	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
122	6	63	10	050	017	SARI GADUNG --> PINDAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
123	6	63	10	050	030	BATU AMPAR--->gabung	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
124	7	63	10	060	001	KAMPUNG BARU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
125	7	63	10	060	002	TUNGKARAN PANGERAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
126	7	63	10	060	003	SARI GADUNG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
127	7	63	10	060	004	MEKARSARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
128	7	63	10	060	005	SUNGAI DUA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
129	7	63	10	060	006	GUNUNG BESAR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
130	7	63	10	060	007	BATU AMPAR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
131	7	63	10	060	008	BAROQAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
132	7	63	10	060	009	SEJAHTERA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
133	7	63	10	060	010	BERSUJUD	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
134	7	63	10	060	011	GUNUNG ANTASARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
135	7	63	10	060	012	PULAU PANJANG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
136	8	63	10	070	001	KARANG BINTANG	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
137	8	63	10	070	002	PANDAN SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
138	8	63	10	070	003	REJO WINANGUN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
139	8	63	10	070	004	SELASELILAU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
140	8	63	10	070	005	PEMATANG ULIN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
141	8	63	10	070	006	BATULICIN IRIGASI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
142	8	63	10	070	007	MANUNGGAL	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
143	8	63	10	070	008	MAJU SEJAHTERA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
144	8	63	10	070	009	KARANG REJO	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
145	8	63	10	070	010	SUMBER WANGI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
146	8	63	10	070	011	MADU RETNO	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
147	9	63	10	080	001	MANTEWE	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
148	9	63	10	080	002	DUKUH REJO	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
149	9	63	10	080	003	SUKA DAMAI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
150	9	63	10	080	004	BULU REJO	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
151	9	63	10	080	005	SIDO MULYO	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
152	9	63	10	080	006	MAJU MULYO	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
153	9	63	10	080	007	SARI MULYA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
154	9	63	10	080	008	SEPAKAT	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
155	9	63	10	080	009	EMIL BARU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
156	9	63	10	080	010	REJO SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
157	9	63	10	080	011	MANTAWAKAN MULIA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
158	10	63	10	090	001	ANGSANA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
159	10	63	10	090	002	PURWODADI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
160	10	63	10	090	003	SUMBER BARU	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
161	10	63	10	090	004	KARANG INDAH	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
162	10	63	10	090	005	BUNATI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
163	10	63	10	090	006	BANJAR SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
164	10	63	10	090	007	BAYAN SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
165	10	63	10	090	008	KUD TUWUH SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
166	10	63	10	090	009	MEKAR JAYA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
167	10	63	10	090	010	MAKMUR	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
168	11	63	10	100	001	GIRI MULYA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
169	11	63	10	100	002	KURANJI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
170	11	63	10	100	003	WARINGIN TUNGGAL	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
171	11	63	10	100	004	MUSTIKA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
172	11	63	10	100	005	INDRA LOKA JAYA	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
173	11	63	10	100	006	KARANG INTAN	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
174	11	63	10	100	007	KUD TUWUH SARI	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
175	11	63	10	100	008	RINGKIT	\N	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_kelurahan_s_idkelurahan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_kelurahan_s_idkelurahan_seq', 176, false);


--
-- Data for Name: s_koderekening; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_koderekening (s_korekid, s_korektipe, s_korekkelompok, s_korekjenis, s_korekobjek, s_korekrincian, s_korekrinciansub, s_koreknama, s_korekketerangan, created_at, updated_at) FROM stdin;
1	4	1	1	11			Bea Perolehan Hak Atas Tanah dan Bangunan (BPHTB) - LRA Bea Perolehan Hak Atas Tanah dan Bangunan (BPHTB) - LRA		2021-03-18 08:54:47	2021-03-18 08:54:47
2	4	1	1	11	01		BPHTB		2021-03-18 08:54:47	2021-03-18 08:54:47
3	4	1	7	11	01		Denda BPHTB		2021-03-18 08:54:47	2021-03-18 08:54:47
4	4	1	7	11	02		Sanksi BPHTB		2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_koderekening_s_korekid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_koderekening_s_korekid_seq', 5, false);


--
-- Data for Name: s_notaris; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_notaris (s_idnotaris, s_namanotaris, s_alamatnotaris, s_kodenotaris, s_sknotaris, s_noid_bpn, s_tgl1notaris, s_tgl2notaris, s_statusnotaris, s_npwpd_notaris, s_daerahkerja, created_at, updated_at) FROM stdin;
1	Gustimansah, SH, M.Kn	Jalan Kolonel Wahid Udin Kel. Serasan Jaya Kec. Sekayu	01	01/01/2018	1	2020-01-01 00:00:00	2020-12-31 00:00:00	1	123	Lilin	2021-03-18 08:54:46	2021-03-18 08:54:46
2	Marleni, SH, M.Kn	Jalan Mawar no 2 Kel. Serasan Jaya Kec. Sekayu	01	01/01/2018	2	2020-01-01 00:00:00	2020-12-31 00:00:00	1	123	Lilin	2021-03-18 08:54:46	2021-03-18 08:54:46
\.


--
-- Name: s_notaris_s_idnotaris_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_notaris_s_idnotaris_seq', 3, false);


--
-- Data for Name: s_pejabat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_pejabat (s_idpejabat, s_namapejabat, s_jabatanpejabat, s_nippejabat, s_golonganpejabat, s_alamat, s_filettd, created_at, updated_at) FROM stdin;
1	Rafiansyah, SE, M.Si	Kasubbid Pendataan, Pendaftaran, Penetapan dan Penilaian PBB dan BPHTB	19730709 199303 1 005	Penata Tingkat I - III D	Jalan raya no 1 Kel. Serasan Jaya Kec. Sekayu		2021-03-18 08:54:47	2021-03-18 08:54:47
2	H. RIKI JUNAIDI, AP, M. Si	Kepala Badan Pengelola Pajak dan Retribusi Daerah	19740615 199311 1 001	Pembina Utama Muda - IV C	Jalan melati no 3 Kel. Serasan Jaya Kec. Sekayu		2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_pejabat_s_idpejabat_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_pejabat_s_idpejabat_seq', 3, false);


--
-- Data for Name: s_pemda; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_pemda (s_idpemda, s_namaprov, s_namakabkot, s_namaibukotakabkot, s_kodeprovinsi, s_kodekabkot, s_namainstansi, s_namasingkatinstansi, s_alamatinstansi, s_notelinstansi, s_namabank, s_norekbank, s_logo, s_namacabang, s_kodecabang, s_kodepos, s_email, s_urlbphtb, created_at, updated_at) FROM stdin;
1	Nusa Tenggara Barat	Kabupaten Lombok Timur	Selong	35	04	Badan Pendapatan Daerah	BAPENDA	Jl. Rajak, Sandubaya	(0355) 320098	Badan Pendapatan Daerah	149.30.00001	public/dist/img/I8vv8BHpj69iOmFsFQYE4Wsdnn4Bjev1R87LtLct.png	Selong	08	83612	bapenda@lotim.co.id	\N	2021-03-18 08:54:47	2021-03-19 13:04:49
\.


--
-- Name: s_pemda_s_idpemda_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_pemda_s_idpemda_seq', 1, false);


--
-- Data for Name: s_persyaratan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_persyaratan (s_idpersyaratan, s_idjenistransaksi, s_namapersyaratan, s_idwajib_up, s_lokasimenu, "order", created_at, updated_at) FROM stdin;
1	1	Foto copy SPPT tahun bersangkutan dan Foto copy Bukti pembayaran PBB / STTS	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
2	1	Foto copy KTP yang diberi kuasa	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
3	1	Surat kuasa dari wajib pajak (apabila dikuasakan) materai 6 ribu	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
4	1	Surat Pernyataan perolehan harga transaksi Jual Beli	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
5	1	FC. KTP Penjual (Suami/Istri)	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
6	1	Foto copy KTP Pembeli (untuk Akta Jual Beli)	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
7	1	Foto copy draft akta (akta jual beli) 1 set dengan hal. lengkap dan / atau bukti hak kepemilikan tanah	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
8	2	FC. Kartu Keluarga / KK	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
9	2	FC. KTP Penerima Hak Tukar	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
10	2	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
11	2	Pernyataan Harga Transaksi	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
12	2	Surat Kuasa materai 6000	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
13	2	Foto copy KTP yang diberi kuasa	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
14	2	FC. KTP Pemberi Hak Tukar	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
15	2	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
16	2	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	9	2021-03-18 08:54:47	2021-03-18 08:54:47
17	3	Surat Kuasa materai 6000	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
18	3	Foto copy KTP Pemberi Hibah	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
19	3	FC. Kartu Identitas / KTP / Surat Domisili Dari Desa	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
20	3	Foto copy KTP yang diberi kuasa	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
21	3	Pernyataan Harga Transaksi	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
22	3	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
23	3	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
24	3	FC. Kartu Keluarga / KK	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
25	4	Surat Kuasa materai 6000	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
26	4	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
27	4	FC. Surat Kematian	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
28	4	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
29	4	Foto copy KTP yang diberi kuasa	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
30	4	FC. Kartu Identitas / KTP / Surat Domisili Dari Desa	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
31	4	Pernyataan Harga Transaksi	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
32	4	FC. Kartu Keluarga / KK	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
33	4	Surat Wasiat	1	1	9	2021-03-18 08:54:47	2021-03-18 08:54:47
34	5	KTP Penerima Kuasa	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
35	5	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
36	5	Foto Lokasi Objek Pajak 	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
37	5	FC. Kartu Keluarga / KK	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
38	5	FC. Kartu Identitas / KTP / Surat Domisili Dari Desa	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
39	5	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
40	5	FC. Surat Kematian	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
41	5	FC. Surat Keterangan Waris / SKW	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
42	5	Surat Kuasa bermaterei 6 ribu	1	1	9	2021-03-18 08:54:47	2021-03-18 08:54:47
43	5	Surat Pernyataan Nilai Pasar	1	1	10	2021-03-18 08:54:47	2021-03-18 08:54:47
44	6	Foto copy KTP yang diberi kuasa	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
45	6	FC. KTP	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
46	6	Akta Badan Hukum	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
47	6	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
48	6	FC. Kartu Keluarga / KK	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
49	6	Pernyataan Harga Transaksi	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
50	6	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
51	6	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
52	6	Surat Kuasa materai 6000	1	1	9	2021-03-18 08:54:47	2021-03-18 08:54:47
53	7	Foto copy KTP yang diberi kuasa	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
54	7	Surat Kuasa materai 6000	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
55	7	Pernyataan Harga Transaksi	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
56	7	FC. Kartu Keluarga / KK	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
57	7	FC. Bukti Kepemilikan (Sertifikat, Warkah. SPT)	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
58	7	FC. KTP	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
59	7	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
60	7	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
61	8	FC. KTP	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
62	8	FC. Bukti Kepemilikan (Sertifikat, Warkah. SPT)	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
63	8	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
64	8	Pernyataan Harga Transaksi	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
65	8	Surat Kuasa materai 6000	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
66	8	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
67	8	Risalah/Tanda Penerima Uang Hasil lelang dari Kantor Pelayanan Kakayaan Negara dan Lelang Malang	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
68	8	Foto copy KTP yang diberi kuasa	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
69	8	FC. Kartu Keluarga / KK	1	1	9	2021-03-18 08:54:47	2021-03-18 08:54:47
70	9	Surat Kuasa materai 6000	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
71	9	Fotokopi Sertifikat	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
72	9	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
73	9	FC. KTP	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
74	9	FC. Kartu Keluarga / KK	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
75	9	Putusan Pengadilan	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
76	9	Foto copy KTP yang diberi kuasa	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
77	9	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
78	9	Pernyataan Harga Transaksi	1	1	9	2021-03-18 08:54:47	2021-03-18 08:54:47
79	10	Pernyataan Harga Transaksi	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
80	10	Fotokopi Sertifikat	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
81	10	FC. KTP	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
82	10	Surat Kuasa materai 6000	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
83	10	Foto copy KTP yang diberi kuasa	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
84	10	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
85	10	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
86	10	FC. Kartu Keluarga / KK	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
87	11	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
88	11	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
89	11	FC. Kartu Keluarga / KK	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
90	11	FC. KTP	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
91	11	Foto copy KTP yang diberi kuasa	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
92	11	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
93	11	Pernyataan Harga Transaksi	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
94	11	Surat Kuasa materai 6000	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
95	12	Pernyataan Harga Transaksi	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
96	12	SPPT PBB Tahun Terakhir	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
97	12	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
98	12	Foto copy KTP yang diberi kuasa	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
99	12	Surat Kuasa materai 6000	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
100	12	FC. Kartu Keluarga / KK	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
101	12	FC. KTP	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
102	12	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
103	13	Pernyataan Harga Transaksi	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
104	13	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
105	13	Fotokopi Sertifikat	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
106	13	FC. Kartu Keluarga / KK	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
107	13	FC. KTP Penerima Hadiah	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
108	13	FC. KTP Pemberi Hadiah	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
109	13	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
110	13	Surat Kuasa materai 6000	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
111	13	Foto copy KTP yang diberi kuasa	1	1	9	2021-03-18 08:54:47	2021-03-18 08:54:47
112	14	Foto copy KTP yang diberi kuasa	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
113	14	FC. Kartu Keluarga / KK	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
114	14	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
115	14	Surat Kuasa materai 6000	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
116	14	Pernyataan Harga Transaksi	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
117	14	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
118	14	FC. KTP	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
119	14	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
120	15	FC. KTP	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
121	15	Fotokopi SPT (Surat Tanah)	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
122	15	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
123	15	 Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
124	15	Surat Kuasa materai 6000	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
125	15	Foto copy KTP yang diberi kuasa	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
126	15	FC. Kartu Keluarga / KK	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
127	15	Pernyataan Harga Transaksi	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
128	16	 Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
129	16	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
130	16	FC. Kartu Keluarga / KK	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
131	16	Pernyataan Harga Transaksi	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
132	16	Foto copy KTP yang diberi kuasa	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
133	16	FC. KTP	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
134	16	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
135	16	Surat Kuasa materai 6000	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
136	17	Foto copy KTP yang diberi kuasa	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
137	17	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
138	17	FC. Kartu Keluarga / KK	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
139	17	Surat Kuasa materai 6000	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
140	17	FC. KTP	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
141	17	Pernyataan Harga Transaksi	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
142	17	 Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
143	17	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
144	18	Akta PHB	1	1	1	2021-03-18 08:54:47	2021-03-18 08:54:47
145	18	FC.Kartu Keluarga/KK	1	1	2	2021-03-18 08:54:47	2021-03-18 08:54:47
146	18	FC. Surat Kematian	1	1	3	2021-03-18 08:54:47	2021-03-18 08:54:47
147	18	FC. Surat Keterangan Waris / SKW	1	1	4	2021-03-18 08:54:47	2021-03-18 08:54:47
148	18	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	5	2021-03-18 08:54:47	2021-03-18 08:54:47
149	18	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	6	2021-03-18 08:54:47	2021-03-18 08:54:47
150	18	Formulir Surat Setoran Pajak Daerah BPHTB/ SSPD BPHTB	1	1	7	2021-03-18 08:54:47	2021-03-18 08:54:47
151	18	FC. Kartu Identitas/ KTP/ Surat Domisili dari desa	1	1	8	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_persyaratan_s_idpersyaratan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_persyaratan_s_idpersyaratan_seq', 152, false);


--
-- Data for Name: s_presentase_wajar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_presentase_wajar (s_idpresentase_wajar, s_nilaimin, s_nilaimax, s_ketpresentase_wajar, s_css_warna, created_at, updated_at) FROM stdin;
1	0	50	Tidak Wajar	red	2021-03-18 08:54:47	2021-03-18 08:54:47
2	50.100000000000001	60	Kurang Wajar	yellow	2021-03-18 08:54:47	2021-03-18 08:54:47
3	60.100000000000001	80	Sedang	blue	2021-03-18 08:54:47	2021-03-18 08:54:47
4	80.099999999999994	100	Wajar	green	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_presentase_wajar_s_idpresentase_wajar_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_presentase_wajar_s_idpresentase_wajar_seq', 5, false);


--
-- Data for Name: s_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_status (s_id_status, s_nama_status, created_at, updated_at) FROM stdin;
1	Aktif	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Tidak	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Data for Name: s_status_bayar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_status_bayar (s_id_status_bayar, s_nama_status_bayar, created_at, updated_at) FROM stdin;
1	Sudah	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Belum	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_status_bayar_s_id_status_bayar_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_status_bayar_s_id_status_bayar_seq', 3, false);


--
-- Data for Name: s_status_berkas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_status_berkas (s_id_status_berkas, s_nama_status_berkas, created_at, updated_at) FROM stdin;
1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Tidak Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_status_berkas_s_id_status_berkas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_status_berkas_s_id_status_berkas_seq', 3, false);


--
-- Data for Name: s_status_dptnpoptkp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_status_dptnpoptkp (s_id_dptnpoptkp, s_nama_dptnpoptkp, created_at, updated_at) FROM stdin;
1	NPOPTKP Setiap Transaksi Dapat	2021-03-18 08:54:47	2021-03-18 08:54:47
2	NPOPTKP 1 Tahun Sekali dapatnya	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_status_dptnpoptkp_s_id_dptnpoptkp_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_status_dptnpoptkp_s_id_dptnpoptkp_seq', 3, false);


--
-- Data for Name: s_status_kabid; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_status_kabid (s_id_status_kabid, s_nama_status_kabid, created_at, updated_at) FROM stdin;
1	Setuju	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Tidak Setuju	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_status_kabid_s_id_status_kabid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_status_kabid_s_id_status_kabid_seq', 3, false);


--
-- Data for Name: s_status_lihat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_status_lihat (s_id_status_lihat, s_nama_status_lihat, created_at, updated_at) FROM stdin;
1	Sudah	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Belum	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_status_lihat_s_id_status_lihat_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_status_lihat_s_id_status_lihat_seq', 3, false);


--
-- Data for Name: s_status_npwpd; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_status_npwpd (s_idstatus_npwpd, s_nama_status, created_at, updated_at) FROM stdin;
1	Ada	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Tidak Ada	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_status_npwpd_s_idstatus_npwpd_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_status_npwpd_s_idstatus_npwpd_seq', 3, false);


--
-- Data for Name: s_status_pelayanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_status_pelayanan (s_id_status_layanan, s_nama_status_layanan, created_at, updated_at) FROM stdin;
\.


--
-- Name: s_status_pelayanan_s_id_status_layanan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_status_pelayanan_s_id_status_layanan_seq', 1, false);


--
-- Data for Name: s_status_perhitungan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_status_perhitungan (s_idstatus_pht, s_nama, created_at, updated_at) FROM stdin;
1	Dari Perbandingan NJOP dengan Nilai Transaksi	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Dari NJOP PBB	2021-03-18 08:54:47	2021-03-18 08:54:47
3	Dari Nilai Transaksi	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_status_perhitungan_s_idstatus_pht_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_status_perhitungan_s_idstatus_pht_seq', 4, false);


--
-- Name: s_status_s_id_status_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_status_s_id_status_seq', 3, false);


--
-- Data for Name: s_target_bphtb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_target_bphtb (s_id_target_bphtb, s_id_target_status, s_tahun_target, s_nilai_target, s_keterangan, created_at, updated_at) FROM stdin;
1	1	2020	1000000000	Target BPHTB Murni	2021-03-18 08:54:47	2021-03-18 08:54:47
2	2	2020	1600000000	Target BPHTB Perubahan	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_target_bphtb_s_id_target_bphtb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_target_bphtb_s_id_target_bphtb_seq', 3, false);


--
-- Data for Name: s_target_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_target_status (s_id_target_status, s_nama_status, created_at, updated_at) FROM stdin;
1	Murni	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Perubahan	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_target_status_s_id_target_status_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_target_status_s_id_target_status_seq', 3, false);


--
-- Data for Name: s_tarif_bphtb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_tarif_bphtb (s_idtarifbphtb, s_tarif_bphtb, s_tgl_awal, s_tgl_akhir, s_dasar_hukum, created_at, updated_at) FROM stdin;
1	5	2015-01-01	2030-12-01		2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_tarif_bphtb_s_idtarifbphtb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_tarif_bphtb_s_idtarifbphtb_seq', 2, false);


--
-- Data for Name: s_tarifnpoptkp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_tarifnpoptkp (s_idtarifnpoptkp, s_idjenistransaksinpoptkp, s_tarifnpoptkp, s_tarifnpoptkptambahan, s_dasarhukumnpoptkp, s_statusnpoptkp, s_tglberlaku_awal, s_tglberlaku_akhir, created_at, updated_at) FROM stdin;
1	1	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
2	2	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
3	3	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
4	4	300000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
5	5	300000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
6	6	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
7	7	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
8	8	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
9	9	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
10	10	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
11	11	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
12	12	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
13	13	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
14	14	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
15	15	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
16	16	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_tarifnpoptkp_s_idtarifnpoptkp_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_tarifnpoptkp_s_idtarifnpoptkp_seq', 17, false);


--
-- Data for Name: s_tempo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_tempo (s_idtempo, s_haritempo, s_tglberlaku_awal, s_tglberlaku_akhir, s_id_status, created_at, updated_at) FROM stdin;
1	5	2015-01-01 00:00:00	2030-12-01 00:00:00	1	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_tempo_s_idtempo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_tempo_s_idtempo_seq', 2, false);


--
-- Data for Name: s_users_api; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_users_api (s_idusers_api, s_username, s_password, created_at, updated_at) FROM stdin;
\.


--
-- Name: s_users_api_s_idusers_api_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_users_api_s_idusers_api_seq', 1, false);


--
-- Data for Name: s_wajib_up; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY s_wajib_up (s_idwajib_up, s_ket_wjb, created_at, updated_at) FROM stdin;
1	Wajib	2021-03-18 08:54:47	2021-03-18 08:54:47
2	Tidak	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: s_wajib_up_s_idwajib_up_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_wajib_up_s_idwajib_up_seq', 3, false);


--
-- Data for Name: t_bpn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_bpn (t_idbpn, t_noakta, t_tglakta, t_namappat, t_nop, t_nib, t_ntpd, t_nik, t_npwp, t_namawp, t_kelurahanop, t_kecamatanop, t_kotaop, t_luastanahop, t_jenishak, t_koordinat_x, t_koordinat_y, t_tgltransaksi, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_bpn_t_idbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_bpn_t_idbpn_seq', 1, false);


--
-- Data for Name: t_file_keringanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_file_keringanan (t_idfile_keringanan, t_idspt, t_iduser_upload, letak_file, nama_file, t_tgl_upload, t_keterangan_file, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_file_keringanan_t_idfile_keringanan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_file_keringanan_t_idfile_keringanan_seq', 1, false);


--
-- Data for Name: t_file_objek; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_file_objek (t_idfile_objek, t_idspt, t_iduser_upload, letak_file, nama_file, t_tgl_upload, t_keterangan_file, created_at, updated_at) FROM stdin;
1	20	1	upload/file_fotoobjek/2021/jualbeli/20/	images.jpg	2021-03-19 15:57:39	\N	2021-03-19 15:57:39	2021-03-19 15:57:39
2	21	1	upload/file_fotoobjek/2021/jualbeli/21/	Logo Lombok Timur.png	2021-03-23 09:16:02	\N	2021-03-23 09:16:02	2021-03-23 09:16:02
3	12	1	upload/file_fotoobjek/2021/jualbeli/12/	images2.jpg	2021-03-23 14:05:25	\N	2021-03-23 14:05:25	2021-03-23 14:05:25
4	22	15	upload/file_fotoobjek/2021/jualbeli/22/	images-1029836684.jpg	2021-03-23 15:01:27	\N	2021-03-23 15:01:27	2021-03-23 15:01:27
\.


--
-- Name: t_file_objek_t_idfile_objek_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_file_objek_t_idfile_objek_seq', 4, true);


--
-- Data for Name: t_filesyarat_bphtb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_filesyarat_bphtb (t_id_filesyarat, t_idspt, s_idpersyaratan, s_idjenistransaksi, t_iduser_upload, letak_file, nama_file, t_tgl_upload, t_keterangan_file, created_at, updated_at) FROM stdin;
1	20	1	1	1	upload/file_syarat/2021/jualbeli/20/	images.jpg	2021-03-19 15:56:53	\N	2021-03-19 15:56:53	2021-03-19 15:56:53
2	20	2	1	1	upload/file_syarat/2021/jualbeli/20/	images2.jpg	2021-03-19 15:56:57	\N	2021-03-19 15:56:57	2021-03-19 15:56:57
3	20	3	1	1	upload/file_syarat/2021/jualbeli/20/	images-135045376.jpg	2021-03-19 15:57:01	\N	2021-03-19 15:57:01	2021-03-19 15:57:01
4	20	4	1	1	upload/file_syarat/2021/jualbeli/20/	images2-910855821.jpg	2021-03-19 15:57:05	\N	2021-03-19 15:57:05	2021-03-19 15:57:05
5	20	5	1	1	upload/file_syarat/2021/jualbeli/20/	images-1115919712.jpg	2021-03-19 15:57:08	\N	2021-03-19 15:57:08	2021-03-19 15:57:08
6	20	6	1	1	upload/file_syarat/2021/jualbeli/20/	images2-1932719026.jpg	2021-03-19 15:57:12	\N	2021-03-19 15:57:12	2021-03-19 15:57:12
7	20	7	1	1	upload/file_syarat/2021/jualbeli/20/	images-76595725.jpg	2021-03-19 15:57:15	\N	2021-03-19 15:57:15	2021-03-19 15:57:15
8	21	1	1	1	upload/file_syarat/2021/jualbeli/21/	images-231527731.jpg	2021-03-22 10:01:02	\N	2021-03-22 10:01:02	2021-03-22 10:01:02
9	21	2	1	1	upload/file_syarat/2021/jualbeli/21/	images2-209503751.jpg	2021-03-22 10:01:07	\N	2021-03-22 10:01:07	2021-03-22 10:01:07
10	21	3	1	1	upload/file_syarat/2021/jualbeli/21/	images-2087958602.jpg	2021-03-22 10:01:10	\N	2021-03-22 10:01:10	2021-03-22 10:01:10
11	21	4	1	1	upload/file_syarat/2021/jualbeli/21/	images2-1786663174.jpg	2021-03-22 10:01:14	\N	2021-03-22 10:01:14	2021-03-22 10:01:14
12	21	5	1	1	upload/file_syarat/2021/jualbeli/21/	images-766100924.jpg	2021-03-22 10:01:18	\N	2021-03-22 10:01:18	2021-03-22 10:01:18
13	21	6	1	1	upload/file_syarat/2021/jualbeli/21/	images2-1888074028.jpg	2021-03-22 10:01:22	\N	2021-03-22 10:01:22	2021-03-22 10:01:22
14	21	7	1	1	upload/file_syarat/2021/jualbeli/21/	images-305625200.jpg	2021-03-22 10:01:26	\N	2021-03-22 10:01:26	2021-03-22 10:01:26
15	12	1	1	1	upload/file_syarat/2021/jualbeli/12/	images-269065511.jpg	2021-03-23 14:04:43	\N	2021-03-23 14:04:43	2021-03-23 14:04:43
16	12	2	1	1	upload/file_syarat/2021/jualbeli/12/	images2-127865265.jpg	2021-03-23 14:04:49	\N	2021-03-23 14:04:49	2021-03-23 14:04:49
17	12	3	1	1	upload/file_syarat/2021/jualbeli/12/	images-170171692.jpg	2021-03-23 14:04:54	\N	2021-03-23 14:04:54	2021-03-23 14:04:54
18	12	4	1	1	upload/file_syarat/2021/jualbeli/12/	images2-518340461.jpg	2021-03-23 14:04:58	\N	2021-03-23 14:04:58	2021-03-23 14:04:58
19	12	5	1	1	upload/file_syarat/2021/jualbeli/12/	images-1650234786.jpg	2021-03-23 14:05:02	\N	2021-03-23 14:05:02	2021-03-23 14:05:02
20	12	6	1	1	upload/file_syarat/2021/jualbeli/12/	images-1761278908.jpg	2021-03-23 14:05:06	\N	2021-03-23 14:05:06	2021-03-23 14:05:06
21	12	7	1	1	upload/file_syarat/2021/jualbeli/12/	images2-203826003.jpg	2021-03-23 14:05:10	\N	2021-03-23 14:05:10	2021-03-23 14:05:10
22	22	1	1	15	upload/file_syarat/2021/jualbeli/22/	images-811557375.jpg	2021-03-23 15:00:49	\N	2021-03-23 15:00:49	2021-03-23 15:00:49
23	22	2	1	15	upload/file_syarat/2021/jualbeli/22/	images2-1371143477.jpg	2021-03-23 15:00:53	\N	2021-03-23 15:00:53	2021-03-23 15:00:53
24	22	3	1	15	upload/file_syarat/2021/jualbeli/22/	images-1916385544.jpg	2021-03-23 15:00:57	\N	2021-03-23 15:00:57	2021-03-23 15:00:57
25	22	4	1	15	upload/file_syarat/2021/jualbeli/22/	images2-1129752582.jpg	2021-03-23 15:01:01	\N	2021-03-23 15:01:01	2021-03-23 15:01:01
26	22	5	1	15	upload/file_syarat/2021/jualbeli/22/	images-335209063.jpg	2021-03-23 15:01:04	\N	2021-03-23 15:01:04	2021-03-23 15:01:04
27	22	6	1	15	upload/file_syarat/2021/jualbeli/22/	images2-1232128825.jpg	2021-03-23 15:01:08	\N	2021-03-23 15:01:08	2021-03-23 15:01:08
28	22	7	1	15	upload/file_syarat/2021/jualbeli/22/	images-1945254751.jpg	2021-03-23 15:01:12	\N	2021-03-23 15:01:12	2021-03-23 15:01:12
\.


--
-- Name: t_filesyarat_bphtb_t_id_filesyarat_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_filesyarat_bphtb_t_id_filesyarat_seq', 28, true);


--
-- Data for Name: t_inputajb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_inputajb (t_idajb, t_idspt, t_tgl_ajb, t_no_ajb, t_iduser_input, created_at, updated_at) FROM stdin;
1	4	2021-01-01 00:00:00	123123	12	2021-03-22 15:37:13	2021-03-22 15:37:13
\.


--
-- Name: t_inputajb_t_idajb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_inputajb_t_idajb_seq', 1, true);


--
-- Data for Name: t_laporbulanan_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_laporbulanan_detail (t_idlaporbulanan, t_idajb, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_laporbulanan_head; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_laporbulanan_head (t_idlaporbulanan, t_no_lapor, t_tgl_lapor, t_untuk_bulan, t_untuk_tahun, t_keterangan, t_iduser_input, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_laporbulanan_head_t_idlaporbulanan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_laporbulanan_head_t_idlaporbulanan_seq', 1, false);


--
-- Data for Name: t_nik_bersama; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_nik_bersama (t_id_nik_bersama, t_idspt, t_nik_bersama, t_nama_bersama, t_nama_jalan, t_rt_bersama, t_rw_bersama, t_kecamatan_bersama, t_kelurahan_bersama, t_kabkota_bersama, t_nohp_bersama, t_kodepos_bersama, t_npwp_bersama, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_nik_bersama_t_id_nik_bersama_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_nik_bersama_t_id_nik_bersama_seq', 1, false);


--
-- Data for Name: t_notif_validasi_berkas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_notif_validasi_berkas (t_id_notif_berkas, t_idspt, t_isipesan, t_dari, t_tglkirim, t_untuk, s_id_status_lihat, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_notif_validasi_berkas_t_id_notif_berkas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_notif_validasi_berkas_t_id_notif_berkas_seq', 1, false);


--
-- Data for Name: t_notif_validasi_kabid; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_notif_validasi_kabid (t_id_notif_kabid, t_idspt, t_isipesan, t_dari, t_tglkirim, t_untuk, s_id_status_lihat, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_notif_validasi_kabid_t_id_notif_kabid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_notif_validasi_kabid_t_id_notif_kabid_seq', 1, false);


--
-- Data for Name: t_pelayanan_angsuran; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pelayanan_angsuran (t_idangsuran, t_idspt, t_noangsuran, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_angsuran, t_jmlhangsuran, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pelayanan_angsuran_t_idangsuran_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pelayanan_angsuran_t_idangsuran_seq', 1, false);


--
-- Data for Name: t_pelayanan_keberatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pelayanan_keberatan (t_idkeberatan, t_idspt, t_nokeberatan, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_keberatan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, t_jmlhpotongan_disetujui, t_persentase_disetujui, t_jmlh_spt_sebenarnya, t_jmlh_spt_hasilpot, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pelayanan_keberatan_t_idkeberatan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pelayanan_keberatan_t_idkeberatan_seq', 1, false);


--
-- Data for Name: t_pelayanan_keringanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pelayanan_keringanan (t_idkeringanan, t_idspt, t_nokeringanan, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_keringanan, t_jmlhpotongan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, t_jmlhpotongan_disetujui, t_persentase_disetujui, t_jmlh_spt_sebenarnya, t_jmlh_spt_hasilpot, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pelayanan_keringanan_t_idkeringanan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pelayanan_keringanan_t_idkeringanan_seq', 1, false);


--
-- Data for Name: t_pelayanan_mutasi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pelayanan_mutasi (t_idmutasi, t_idspt, t_nomutasi, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_mutasi, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pelayanan_mutasi_t_idmutasi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pelayanan_mutasi_t_idmutasi_seq', 1, false);


--
-- Data for Name: t_pelayanan_pembatalan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pelayanan_pembatalan (t_idpembatalan, t_idspt, t_nopembatalan, t_tglpengajuan, t_keterangan_pembatalan, t_iduser_pengajuan, t_nosk_pembatalan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pelayanan_pembatalan_t_idpembatalan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pelayanan_pembatalan_t_idpembatalan_seq', 1, false);


--
-- Data for Name: t_pelayanan_penundaan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pelayanan_penundaan (t_idpenundaan, t_idspt, t_nopenundaan, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_penundaan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pelayanan_penundaan_t_idpenundaan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pelayanan_penundaan_t_idpenundaan_seq', 1, false);


--
-- Data for Name: t_pembayaran_bphtb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pembayaran_bphtb (t_id_pembayaran, t_idspt, t_nourut_bayar, s_id_status_bayar, t_iduser_bayar, t_tglpembayaran_pokok, t_jmlhbayar_pokok, t_tglpembayaran_denda, t_jmlhbayar_denda, t_jmlhbayar_total, created_at, updated_at) FROM stdin;
1	1	1	1	1	2021-03-18 08:54:47	600000	\N	0	600000	2021-03-18 08:54:47	2021-03-18 08:54:47
2	2	2	1	1	2021-03-18 08:54:47	600000	\N	0	600000	2021-03-18 08:54:47	2021-03-18 08:54:47
3	3	3	1	1	2021-03-18 08:54:47	600000	\N	0	600000	2021-03-18 08:54:47	2021-03-18 08:54:47
4	4	4	1	1	2021-03-18 08:54:47	600000	\N	0	600000	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: t_pembayaran_bphtb_t_id_pembayaran_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pembayaran_bphtb_t_id_pembayaran_seq', 5, false);


--
-- Data for Name: t_pembayaran_skpdkb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pembayaran_skpdkb (t_id_bayar_skpdkb, t_idspt, t_idskpdkb, t_nourut_bayar, s_id_status_bayar, t_iduser_bayar, t_tglpembayaran_skpdkb, t_jmlhbayar_skpdkb, t_tglpembayaran_denda, t_jmlhbayar_denda, t_jmlhbayar_total, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq', 1, false);


--
-- Data for Name: t_pembayaran_skpdkbt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pembayaran_skpdkbt (t_id_bayar_skpdkb, t_idspt, t_idskpdkbt, t_nourut_bayar, s_id_status_bayar, t_iduser_bayar, t_tglpembayaran_skpdkbt, t_jmlhbayar_skpdkbt, t_tglpembayaran_denda, t_jmlhbayar_denda, t_jmlhbayar_total, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq', 1, false);


--
-- Data for Name: t_pemeriksaan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pemeriksaan (t_idpemeriksa, t_idspt, t_tglpenetapan, t_tglby_system, t_iduser_buat, t_idpejabat1, t_idpejabat2, t_idpejabat3, t_idpejabat4, t_noperiksa, t_keterangan, t_nop_sppt, t_tahun_sppt, t_luastanah_pemeriksa, t_njoptanah_pemeriksa, t_totalnjoptanah_pemeriksa, t_luasbangunan_pemeriksa, t_njopbangunan_pemeriksa, t_totalnjopbangunan_pemeriksa, t_grandtotalnjop_pemeriksa, t_nilaitransaksispt_pemeriksa, t_trf_aphb_kali_pemeriksa, t_trf_aphb_bagi_pemeriksa, t_permeter_tanah_pemeriksa, t_idtarifbphtb_pemeriksa, t_persenbphtb_pemeriksa, t_npop_bphtb_pemeriksa, t_npoptkp_bphtb_pemeriksa, t_npopkp_bphtb_pemeriksa, t_nilaibphtb_pemeriksa, t_idpersetujuan_pemeriksa, isdeleted, t_idoperator_deleted, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pemeriksaan_t_idpemeriksa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pemeriksaan_t_idpemeriksa_seq', 1, false);


--
-- Data for Name: t_pen_denda_ajb_notaris; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pen_denda_ajb_notaris (t_id_ajbdenda, t_idspt, t_idnotaris, t_tglpenetapan, t_nourut_ajbdenda, t_nilai_ajbdenda, t_iduser_buat, s_id_status_bayar, t_tglbayar_ajbdenda, t_jmlhbayar_ajbdenda, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pen_denda_ajb_notaris_t_id_ajbdenda_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pen_denda_ajb_notaris_t_id_ajbdenda_seq', 1, false);


--
-- Data for Name: t_pen_denda_lpor_notaris; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pen_denda_lpor_notaris (t_id_pendenda, t_idlaporbulanan, t_idnotaris, t_tglpenetapan, t_nourut_pendenda, t_nilai_pendenda, t_iduser_buat, s_id_status_bayar, t_tglbayar_pendenda, t_jmlhbayar_pendenda, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_pen_denda_lpor_notaris_t_id_pendenda_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_pen_denda_lpor_notaris_t_id_pendenda_seq', 1, false);


--
-- Data for Name: t_skpdkb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_skpdkb (t_idskpdkb, t_idspt, t_tglpenetapan, t_tglby_system, t_iduser_buat, t_nop_sppt, t_tahun_sppt, t_luastanah_skpdkb, t_njoptanah_skpdkb, t_totalnjoptanah_skpdkb, t_luasbangunan_skpdkb, t_njopbangunan_skpdkb, t_totalnjopbangunan_skpdkb, t_grandtotalnjop_skpdkb, t_nilaitransaksispt_skpdkb, t_trf_aphb_kali_skpdkb, t_trf_aphb_bagi_skpdkb, t_permeter_tanah_skpdkb, t_idtarifbphtb_skpdkb, t_persenbphtb_skpdkb, t_npop_bphtb_skpdkb, t_npoptkp_bphtb_skpdkb, t_npopkp_bphtb_skpdkb, t_nilai_bayar_sblumnya, t_nilai_pokok_skpdkb, t_jmlh_blndenda, t_jmlh_dendaskpdkb, t_jmlh_totalskpdkb, t_tglbuat_kodebyar, t_nourut_kodebayar, t_kodebayar_skpdkb, t_tgljatuhtempo_skpdkb, t_idjenisketetapan, t_idpersetujuan_skpdkb, isdeleted, t_idoperator_deleted, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_skpdkb_t_idskpdkb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_skpdkb_t_idskpdkb_seq', 1, false);


--
-- Data for Name: t_skpdkbt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_skpdkbt (t_idskpdkbt, t_idspt, t_idskpdkb, t_tglpenetapan, t_tglby_system, t_iduser_buat, t_nop_sppt, t_tahun_sppt, t_luastanah_skpdkbt, t_njoptanah_skpdkbt, t_totalnjoptanah_skpdkbt, t_luasbangunan_skpdkbt, t_njopbangunan_skpdkbt, t_totalnjopbangunan_skpdkbt, t_grandtotalnjop_skpdkbt, t_nilaitransaksispt_skpdkbt, t_trf_aphb_kali_skpdkbt, t_trf_aphb_bagi_skpdkbt, t_permeter_tanah_skpdkbt, t_idtarifbphtb_skpdkbt, t_persenbphtb_skpdkbt, t_npop_bphtb_skpdkbt, t_npoptkp_bphtb_skpdkbt, t_npopkp_bphtb_skpdkbt, t_nilai_bayar_sblumnya, t_nilai_pokok_skpdkbt, t_jmlh_blndenda, t_jmlh_dendaskpdkbt, t_jmlh_totalskpdkbt, t_tglbuat_kodebyar, t_nourut_kodebayar, t_kodebayar_skpdkbt, t_tgljatuhtempo_skpdkbt, t_idjenisketetapan, t_idpersetujuan_skpdkb, isdeleted, t_idoperator_deleted, created_at, updated_at) FROM stdin;
\.


--
-- Name: t_skpdkbt_t_idskpdkbt_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_skpdkbt_t_idskpdkbt_seq', 1, false);


--
-- Data for Name: t_spt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_spt (t_idspt, t_kohirspt, t_tgldaftar_spt, t_tglby_system, t_periodespt, t_iduser_buat, t_idjenistransaksi, t_idnotaris_spt, t_npwpd, t_idstatus_npwpd, t_idbidang_usaha, t_nama_pembeli, t_nik_pembeli, t_npwp_pembeli, t_jalan_pembeli, t_kabkota_pembeli, t_idkec_pembeli, t_namakecamatan_pembeli, t_idkel_pembeli, t_namakelurahan_pembeli, t_rt_pembeli, t_rw_pembeli, t_nohp_pembeli, t_notelp_pembeli, t_email_pembeli, t_kodepos_pembeli, t_siup_pembeli, t_nib_pembeli, t_ketdomisili_pembeli, t_b_nama_pngjwb_pembeli, t_b_nik_pngjwb_pembeli, t_b_npwp_pngjwb_pembeli, t_b_statusjab_pngjwb_pembeli, t_b_jalan_pngjwb_pembeli, t_b_kabkota_pngjwb_pembeli, t_b_idkec_pngjwb_pembeli, t_b_namakec_pngjwb_pembeli, t_b_idkel_pngjwb_pembeli, t_b_namakel_pngjwb_pembeli, t_b_rt_pngjwb_pembeli, t_b_rw_pngjwb_pembeli, t_b_nohp_pngjwb_pembeli, t_b_notelp_pngjwb_pembeli, t_b_email_pngjwb_pembeli, t_b_kodepos_pngjwb_pembeli, t_nop_sppt, t_tahun_sppt, t_nama_sppt, t_jalan_sppt, t_kabkota_sppt, t_kecamatan_sppt, t_kelurahan_sppt, t_rt_sppt, t_rw_sppt, t_luastanah_sismiop, t_luasbangunan_sismiop, t_njoptanah_sismiop, t_njopbangunan_sismiop, t_luastanah, t_njoptanah, t_totalnjoptanah, t_luasbangunan, t_njopbangunan, t_totalnjopbangunan, t_grandtotalnjop, t_nilaitransaksispt, t_tarif_pembagian_aphb_kali, t_tarif_pembagian_aphb_bagi, t_permeter_tanah, t_idjenisfasilitas, t_idjenishaktanah, t_idjenisdoktanah, t_idpengurangan, t_id_jenistanah, t_nosertifikathaktanah, t_tgldok_tanah, s_latitude, s_longitude, t_idtarifbphtb, t_persenbphtb, t_npop_bphtb, t_npoptkp_bphtb, t_npopkp_bphtb, t_nilai_bphtb_fix, t_nilai_bphtb_real, t_idbidang_penjual, t_nama_penjual, t_nik_penjual, t_npwp_penjual, t_jalan_penjual, t_kabkota_penjual, t_idkec_penjual, t_namakec_penjual, t_idkel_penjual, t_namakel_penjual, t_rt_penjual, t_rw_penjual, t_nohp_penjual, t_notelp_penjual, t_email_penjual, t_kodepos_penjual, t_siup_penjual, t_nib_penjual, t_ketdomisili_penjual, t_b_nama_pngjwb_penjual, t_b_nik_pngjwb_penjual, t_b_npwp_pngjwb_penjual, t_b_statusjab_pngjwb_penjual, t_b_jalan_pngjwb_penjual, t_b_kabkota_pngjwb_penjual, t_b_idkec_pngjwb_penjual, t_b_namakec_pngjwb_penjual, t_b_idkel_pngjwb_penjual, t_b_namakel_pngjwb_penjual, t_b_rt_pngjwb_penjual, t_b_rw_pngjwb_penjual, t_b_nohp_pngjwb_penjual, t_b_notelp_pngjwb_penjual, t_b_email_pngjwb_penjual, t_b_kodepos_pngjwb_penjual, t_tglbuat_kodebyar, t_nourut_kodebayar, t_kodebayar_bphtb, t_tglketetapan_spt, t_tgljatuhtempo_spt, t_idjenisketetapan, t_idpersetujuan_bphtb, isdeleted, t_idoperator_deleted, created_at, updated_at, t_ntpd) FROM stdin;
1	1	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 1 Pribadi	10101	202020	Jalan 1 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba1@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 1 Pribadi	2123112222		Jalan penjual 1 Pribadi	Semarang	2	Kecamatan Penjual 1	2	Namakel penjual 1	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	1	6310000012020	\N	\N	1	1	\N	\N	\N	\N	\N
2	2	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 2 Pribadi	10101	202020	Jalan 2 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba2@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 2 Pribadi	2123112222		Jalan penjual 2 Pribadi	Semarang	2	Kecamatan Penjual 2	2	Namakel penjual 2	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	2	6310000022020	\N	\N	1	1	\N	\N	\N	\N	\N
3	3	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 3 Pribadi	10101	202020	Jalan 3 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba3@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 3 Pribadi	2123112222		Jalan penjual 3 Pribadi	Semarang	2	Kecamatan Penjual 3	2	Namakel penjual 3	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	3	6310000032020	\N	\N	1	1	\N	\N	\N	\N	\N
4	4	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 4 Pribadi	10101	202020	Jalan 4 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba4@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 4 Pribadi	2123112222		Jalan penjual 4 Pribadi	Semarang	2	Kecamatan Penjual 4	2	Namakel penjual 4	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	4	6310000042020	\N	\N	1	1	\N	\N	\N	\N	\N
5	5	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 5 Pribadi	10101	202020	Jalan 5 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba5@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 5 Pribadi	2123112222		Jalan penjual 5 Pribadi	Semarang	2	Kecamatan Penjual 5	2	Namakel penjual 5	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	5	6310000052020	\N	\N	1	1	\N	\N	\N	\N	\N
6	6	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 6 Pribadi	10101	202020	Jalan 6 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba6@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 6 Pribadi	2123112222		Jalan penjual 6 Pribadi	Semarang	2	Kecamatan Penjual 6	2	Namakel penjual 6	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	6	6310000062020	\N	\N	1	1	\N	\N	\N	\N	\N
7	7	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 7 Pribadi	10101	202020	Jalan 7 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba7@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 7 Pribadi	2123112222		Jalan penjual 7 Pribadi	Semarang	2	Kecamatan Penjual 7	2	Namakel penjual 7	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	7	6310000072020	\N	\N	1	1	\N	\N	\N	\N	\N
8	8	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 8 Pribadi	10101	202020	Jalan 8 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba8@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 8 Pribadi	2123112222		Jalan penjual 8 Pribadi	Semarang	2	Kecamatan Penjual 8	2	Namakel penjual 8	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	8	6310000082020	\N	\N	1	1	\N	\N	\N	\N	\N
9	9	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 9 Pribadi	10101	202020	Jalan 9 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba9@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 9 Pribadi	2123112222		Jalan penjual 9 Pribadi	Semarang	2	Kecamatan Penjual 9	2	Namakel penjual 9	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	9	6310000092020	\N	\N	1	1	\N	\N	\N	\N	\N
10	10	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 10 Pribadi	10101	202020	Jalan 10 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba10@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 10 Pribadi	2123112222		Jalan penjual 10 Pribadi	Semarang	2	Kecamatan Penjual 10	2	Namakel penjual 10	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	10	6310000102020	\N	\N	1	1	\N	\N	\N	\N	\N
11	11	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 11 Pribadi	10101	202020	Jalan 11 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba11@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 11 Pribadi	2123112222		Jalan penjual 11 Pribadi	Semarang	2	Kecamatan Penjual 11	2	Namakel penjual 11	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	11	6310000112020	\N	\N	1	1	\N	\N	\N	\N	\N
13	13	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 13 Pribadi	10101	202020	Jalan 13 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba13@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 13 Pribadi	2123112222		Jalan penjual 13 Pribadi	Semarang	2	Kecamatan Penjual 13	2	Namakel penjual 13	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	13	6310000132020	\N	\N	1	1	\N	\N	\N	\N	\N
14	14	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 14 Pribadi	10101	202020	Jalan 14 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba14@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 14 Pribadi	2123112222		Jalan penjual 14 Pribadi	Semarang	2	Kecamatan Penjual 14	2	Namakel penjual 14	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	14	6310000142020	\N	\N	1	1	\N	\N	\N	\N	\N
15	15	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 15 Pribadi	10101	202020	Jalan 15 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba15@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 15 Pribadi	2123112222		Jalan penjual 15 Pribadi	Semarang	2	Kecamatan Penjual 15	2	Namakel penjual 15	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	15	6310000152020	\N	\N	1	1	\N	\N	\N	\N	\N
16	16	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 16 Pribadi	10101	202020	Jalan 16 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba16@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 16 Pribadi	2123112222		Jalan penjual 16 Pribadi	Semarang	2	Kecamatan Penjual 16	2	Namakel penjual 16	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	16	6310000162020	\N	\N	1	1	\N	\N	\N	\N	\N
17	17	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 17 Pribadi	10101	202020	Jalan 17 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba17@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 17 Pribadi	2123112222		Jalan penjual 17 Pribadi	Semarang	2	Kecamatan Penjual 17	2	Namakel penjual 17	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	17	6310000172020	\N	\N	1	1	\N	\N	\N	\N	\N
18	18	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 18 Pribadi	10101	202020	Jalan 18 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba18@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 18 Pribadi	2123112222		Jalan penjual 18 Pribadi	Semarang	2	Kecamatan Penjual 18	2	Namakel penjual 18	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	18	6310000182020	\N	\N	1	1	\N	\N	\N	\N	\N
19	19	2021-03-18 08:54:47	2021-03-18 08:54:47	2020	1	1	1	P.1.0000100.10.10	1	1	Pemilik 19 Pribadi	10101	202020	Jalan 19 Pribadi	Tanah Bumbu	2	BESUKI	2	SEDAYUGUNUNG	001	002	0857111	\N	coba19@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47			1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 19 Pribadi	2123112222		Jalan penjual 19 Pribadi	Semarang	2	Kecamatan Penjual 19	2	Namakel penjual 19	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	19	6310000192020	\N	\N	1	1	\N	\N	\N	\N	\N
12	12	2021-03-23 14:00:24	2021-03-18 08:54:47	2021	1	1	1	P.1.0000100.10.10	1	2	yeahh	10101	20.202.021.3-131.313	Jalan 12 Pribadi	Tanah Bumbu	2	KUSAN HILIR	2	SUNGAI LEMBU	001	002	0857111	098765	coba12@gmail.com	31110	21144444442141411414	1111111111111	jakarta	ade ilyas	2222222222222222	21.344.444.4-444.444	dirut	jalan rempoa	tangsel	4	SATUI	59	SUNGAI CUKA	002	003	09876890	0987654678	frennandiade@gmail.com	62743	63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	100	1	1	1	1	1	192 2 2020	2021-03-18 08:54:47	-8.65111430448619	116.54075028714753	1	5	180000000	60000000	120000000	600000	600000	1	Nama Penjual 12 Pribadi	2123112222		Jalan penjual 12 Pribadi	Semarang	2	Kecamatan Penjual 12	2	Namakel penjual 12	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	12	6310000122020	\N	\N	1	1	\N	\N	\N	2021-03-23 14:05:30	\N
22	22	2021-03-23 17:10:18	2021-03-23 14:54:56	2021	1	1	1	\N	\N	1	ade ilyas	1111111111111111	22.222.222.2-222.222	jalan rempoa	Tangerang Selatan	2	KUSAN HILIR	2	SUNGAI LEMBU	003	002	098769876	09876543	frenan@gmail.com	98765	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	82.12.160.008.001.0020.0	2013	AMIR WONDA	JL YOS SUDARSO	PUNCAK JAYA	KUSAN HILIR	SUNGAI LEMBU	003	004	2912	63	36000	1200000	2912	36000	104832000	63	1200000	75600000	180432000	200000000	\N	\N	42719.780219779997	\N	1	1	\N	2	219731293	2021-03-01 00:00:00	-6.140520360202533	106.88873375546875	1	5	200000000	60000000	140000000	7000000	7000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	\N	\N	2021-03-23 14:54:56	2021-03-24 10:07:18	\N
20	20	2021-03-19 16:17:28	2021-03-18 08:54:47	2021	1	1	2	P.1.0000100.10.10	1	1	Pemilik 20 Pribadi	10101	20.202.0	Jalan 20 Pribadi	Tanah Bumbu	2	KUSAN HILIR	2	SUNGAI LEMBU	001	002	0857111	09222283	coba20@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	1798000	1	1	1	1	1	192 2 2020	2021-03-01 00:00:00	-8.65111430448619	116.54075028714753	1	5	180000000	0	180000000	0	0	1	Nama Penjual 20 Pribadi	2123112222	32.111.111.1-111.111	Jalan penjual 20 Pribadi	Semarang	2	KUSAN HILIR	2	SUNGAI LEMBU	001	002	08129101	089712731	frenandiade@gmail.com	22411										\N		\N					\N			2021-03-18 08:54:47	20	6310000202020	\N	\N	1	1	\N	\N	\N	2021-03-19 16:17:36	\N
21	21	2021-03-24 10:09:33	2021-03-18 08:54:47	2021	1	1	1	P.1.0000100.10.10	1	1	Pemilik 21 Pribadi	10101	20.202.0	Jalan 21 Pribadi	Tanah Bumbu	2	KUSAN HILIR	2	SUNGAI LEMBU	001	002	0857111	09876	coba21@gmail.com	31110					\N					\N		\N					\N			63.10.010.001.001.0002.0	2020	MENDING	GANG SEBAMBAN	KOTA BARU	kecamatan	BATUAH	006	001	100	100	100000	2000	100	100000	10000000	100	2000	200000	10200000	180000000	\N	\N	1798000	1	1	1	1	1	192 2 2020	2021-03-01 00:00:00	-8.65111430448619	116.54075028714753	1	5	180000000	0	180000000	0	0	1	Nama Penjual 21 Pribadi	2123112222		Jalan penjual 21 Pribadi	Semarang	2	Kecamatan Penjual 21	2	Namakel penjual 21	001	002	08129101	\N		22411										\N		\N					\N			2021-03-18 08:54:47	21	6310000212020	\N	\N	1	1	\N	\N	\N	2021-03-24 10:09:41	\N
\.


--
-- Name: t_spt_t_idspt_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_spt_t_idspt_seq', 22, true);


--
-- Data for Name: t_validasi_berkas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_validasi_berkas (t_id_validasi_berkas, t_idspt, s_id_status_berkas, t_tglvalidasi, t_iduser_validasi, t_keterangan_berkas, created_at, updated_at, t_persyaratan_validasi_berkas) FROM stdin;
1	1	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
2	2	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
3	3	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
4	4	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
5	5	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
6	6	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
7	7	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
8	8	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
9	9	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
10	10	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
11	11	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47	\N
\.


--
-- Name: t_validasi_berkas_t_id_validasi_berkas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_validasi_berkas_t_id_validasi_berkas_seq', 12, false);


--
-- Data for Name: t_validasi_kabid; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_validasi_kabid (t_id_validasi_kabid, t_idspt, s_id_status_kabid, t_tglvalidasi, t_iduser_validasi, t_keterangan_kabid, created_at, updated_at) FROM stdin;
1	1	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47
2	2	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47
3	3	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47
4	4	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47
5	5	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47
6	6	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47
7	7	1	2021-03-18 08:54:47	1	Lengkap	2021-03-18 08:54:47	2021-03-18 08:54:47
\.


--
-- Name: t_validasi_kabid_t_id_validasi_kabid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('t_validasi_kabid_t_id_validasi_kabid_seq', 8, false);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, name, email, username, email_verified_at, password, s_tipe_pejabat, s_idpejabat, s_idnotaris, s_id_hakakses, remember_token, created_at, updated_at) FROM stdin;
1	admin	admin@mail.com	admin	\N	$2y$10$oCE6P17jKYQIOwnPeItJxewP3fkYvu/ptMRH.dn6mof1VfWd288fC	\N	\N	\N	1	\N	2021-03-18 08:54:47	2021-03-18 13:38:44
3	validasi-kabid	validasi-kabid@mail.com	validasikabid	\N	$2y$10$oCE6P17jKYQIOwnPeItJxewP3fkYvu/ptMRH.dn6mof1VfWd288fC	\N	\N	\N	3	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
5	bpn	bpn@mail.com	bpn	\N	$2y$10$oCE6P17jKYQIOwnPeItJxewP3fkYvu/ptMRH.dn6mof1VfWd288fC	\N	\N	\N	5	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
6	kpppratama	kpppratama@mail.com	kpppratama	\N	$2y$10$oCE6P17jKYQIOwnPeItJxewP3fkYvu/ptMRH.dn6mof1VfWd288fC	\N	\N	\N	6	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
7	operatorbendahara	operatorbendahara@mail.com	operatorbendahara	\N	$2y$10$oCE6P17jKYQIOwnPeItJxewP3fkYvu/ptMRH.dn6mof1VfWd288fC	\N	\N	\N	7	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
8	wajibpajak	wajibpajak@mail.com	wajibpajak	\N	$2y$10$oCE6P17jKYQIOwnPeItJxewP3fkYvu/ptMRH.dn6mof1VfWd288fC	\N	\N	\N	8	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
9	pemeriksa	pemeriksa@mail.com	pemeriksa	\N	$2y$10$oCE6P17jKYQIOwnPeItJxewP3fkYvu/ptMRH.dn6mof1VfWd288fC	\N	\N	\N	9	\N	2021-03-18 08:54:47	2021-03-18 08:54:47
2	validasi-berkas	validasi-berkas@mail.com	validasiberkas	\N	$2y$10$yLaM/khcspAwhOpgCJHmJ.p4FZ.pvIV5OXe9t43CZONmPoL3LNSRC	1	\N	\N	2	\N	2021-03-18 08:54:47	2021-03-18 15:26:12
12	notaris2	notaris2@gmail.com	notaris2	\N	$2y$10$iQbgzrOvmbSKw/za2PLmZuGmoolDI2vVzHR0/0tm5l2vvnuoG4ceu	2	\N	2	4	\N	2021-03-22 13:51:51	2021-03-22 15:57:31
15	ade	frenandiade@gmail.com	ade	\N	$2y$10$E.Kl7up.HEDi0RwbzGfHK.Xp8psGqO5VKVIu7YlBPpjzzXnDOlcQO	2	\N	1	4	\N	2021-03-22 16:43:07	2021-03-22 16:43:07
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 15, true);


--
-- Name: activity_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY activity_log
    ADD CONSTRAINT activity_log_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);


--
-- Name: his_getbphtb_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY his_getbphtb_bpn
    ADD CONSTRAINT his_getbphtb_bpn_pkey PRIMARY KEY (t_idhis_getbpn);


--
-- Name: his_getpbb_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY his_getpbb_bpn
    ADD CONSTRAINT his_getpbb_bpn_pkey PRIMARY KEY (t_idhis_pbbbpn);


--
-- Name: his_postdatabpn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY his_postdatabpn
    ADD CONSTRAINT his_postdatabpn_pkey PRIMARY KEY (t_idhis_posbpn);


--
-- Name: integrasi_sertifikat_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY integrasi_sertifikat_bpn
    ADD CONSTRAINT integrasi_sertifikat_bpn_pkey PRIMARY KEY (t_idsertifikat);


--
-- Name: migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- Name: model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- Name: notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: s_acuan_jenistanah_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_acuan_jenistanah
    ADD CONSTRAINT s_acuan_jenistanah_pkey PRIMARY KEY (s_idacuan_jenis);


--
-- Name: s_acuan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_acuan
    ADD CONSTRAINT s_acuan_pkey PRIMARY KEY (s_idacuan);


--
-- Name: s_background_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_background
    ADD CONSTRAINT s_background_pkey PRIMARY KEY (id);


--
-- Name: s_hak_akses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_hak_akses
    ADD CONSTRAINT s_hak_akses_pkey PRIMARY KEY (s_id_hakakses);


--
-- Name: s_harga_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_harga_history
    ADD CONSTRAINT s_harga_history_pkey PRIMARY KEY (s_idhistory);


--
-- Name: s_jenis_bidangusaha_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenis_bidangusaha
    ADD CONSTRAINT s_jenis_bidangusaha_pkey PRIMARY KEY (s_idbidang_usaha);


--
-- Name: s_jenisdoktanah_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenisdoktanah
    ADD CONSTRAINT s_jenisdoktanah_pkey PRIMARY KEY (s_iddoktanah);


--
-- Name: s_jenisfasilitas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenisfasilitas
    ADD CONSTRAINT s_jenisfasilitas_pkey PRIMARY KEY (s_idjenisfasilitas);


--
-- Name: s_jenishaktanah_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenishaktanah
    ADD CONSTRAINT s_jenishaktanah_pkey PRIMARY KEY (s_idhaktanah);


--
-- Name: s_jenisketetapan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenisketetapan
    ADD CONSTRAINT s_jenisketetapan_pkey PRIMARY KEY (s_idjenisketetapan);


--
-- Name: s_jenispengurangan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenispengurangan
    ADD CONSTRAINT s_jenispengurangan_pkey PRIMARY KEY (s_idpengurangan);


--
-- Name: s_jenistanah_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenistanah
    ADD CONSTRAINT s_jenistanah_pkey PRIMARY KEY (id_jenistanah);


--
-- Name: s_jenistransaksi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenistransaksi
    ADD CONSTRAINT s_jenistransaksi_pkey PRIMARY KEY (s_idjenistransaksi);


--
-- Name: s_kecamatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_kecamatan
    ADD CONSTRAINT s_kecamatan_pkey PRIMARY KEY (s_idkecamatan);


--
-- Name: s_kelurahan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_kelurahan
    ADD CONSTRAINT s_kelurahan_pkey PRIMARY KEY (s_idkelurahan);


--
-- Name: s_koderekening_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_koderekening
    ADD CONSTRAINT s_koderekening_pkey PRIMARY KEY (s_korekid);


--
-- Name: s_notaris_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_notaris
    ADD CONSTRAINT s_notaris_pkey PRIMARY KEY (s_idnotaris);


--
-- Name: s_pejabat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_pejabat
    ADD CONSTRAINT s_pejabat_pkey PRIMARY KEY (s_idpejabat);


--
-- Name: s_pemda_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_pemda
    ADD CONSTRAINT s_pemda_pkey PRIMARY KEY (s_idpemda);


--
-- Name: s_persyaratan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_persyaratan
    ADD CONSTRAINT s_persyaratan_pkey PRIMARY KEY (s_idpersyaratan);


--
-- Name: s_presentase_wajar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_presentase_wajar
    ADD CONSTRAINT s_presentase_wajar_pkey PRIMARY KEY (s_idpresentase_wajar);


--
-- Name: s_status_bayar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_bayar
    ADD CONSTRAINT s_status_bayar_pkey PRIMARY KEY (s_id_status_bayar);


--
-- Name: s_status_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_berkas
    ADD CONSTRAINT s_status_berkas_pkey PRIMARY KEY (s_id_status_berkas);


--
-- Name: s_status_dptnpoptkp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_dptnpoptkp
    ADD CONSTRAINT s_status_dptnpoptkp_pkey PRIMARY KEY (s_id_dptnpoptkp);


--
-- Name: s_status_kabid_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_kabid
    ADD CONSTRAINT s_status_kabid_pkey PRIMARY KEY (s_id_status_kabid);


--
-- Name: s_status_lihat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_lihat
    ADD CONSTRAINT s_status_lihat_pkey PRIMARY KEY (s_id_status_lihat);


--
-- Name: s_status_npwpd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_npwpd
    ADD CONSTRAINT s_status_npwpd_pkey PRIMARY KEY (s_idstatus_npwpd);


--
-- Name: s_status_pelayanan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_pelayanan
    ADD CONSTRAINT s_status_pelayanan_pkey PRIMARY KEY (s_id_status_layanan);


--
-- Name: s_status_perhitungan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status_perhitungan
    ADD CONSTRAINT s_status_perhitungan_pkey PRIMARY KEY (s_idstatus_pht);


--
-- Name: s_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_status
    ADD CONSTRAINT s_status_pkey PRIMARY KEY (s_id_status);


--
-- Name: s_target_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_target_bphtb
    ADD CONSTRAINT s_target_bphtb_pkey PRIMARY KEY (s_id_target_bphtb);


--
-- Name: s_target_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_target_status
    ADD CONSTRAINT s_target_status_pkey PRIMARY KEY (s_id_target_status);


--
-- Name: s_tarif_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_tarif_bphtb
    ADD CONSTRAINT s_tarif_bphtb_pkey PRIMARY KEY (s_idtarifbphtb);


--
-- Name: s_tarifnpoptkp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_tarifnpoptkp
    ADD CONSTRAINT s_tarifnpoptkp_pkey PRIMARY KEY (s_idtarifnpoptkp);


--
-- Name: s_tempo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_tempo
    ADD CONSTRAINT s_tempo_pkey PRIMARY KEY (s_idtempo);


--
-- Name: s_users_api_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_users_api
    ADD CONSTRAINT s_users_api_pkey PRIMARY KEY (s_idusers_api);


--
-- Name: s_wajib_up_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_wajib_up
    ADD CONSTRAINT s_wajib_up_pkey PRIMARY KEY (s_idwajib_up);


--
-- Name: t_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_bpn
    ADD CONSTRAINT t_bpn_pkey PRIMARY KEY (t_idbpn);


--
-- Name: t_file_keringanan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_file_keringanan
    ADD CONSTRAINT t_file_keringanan_pkey PRIMARY KEY (t_idfile_keringanan);


--
-- Name: t_file_objek_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_file_objek
    ADD CONSTRAINT t_file_objek_pkey PRIMARY KEY (t_idfile_objek);


--
-- Name: t_filesyarat_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_pkey PRIMARY KEY (t_id_filesyarat);


--
-- Name: t_inputajb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_inputajb
    ADD CONSTRAINT t_inputajb_pkey PRIMARY KEY (t_idajb);


--
-- Name: t_laporbulanan_head_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_laporbulanan_head
    ADD CONSTRAINT t_laporbulanan_head_pkey PRIMARY KEY (t_idlaporbulanan);


--
-- Name: t_nik_bersama_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_nik_bersama
    ADD CONSTRAINT t_nik_bersama_pkey PRIMARY KEY (t_id_nik_bersama);


--
-- Name: t_notif_validasi_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_pkey PRIMARY KEY (t_id_notif_berkas);


--
-- Name: t_notif_validasi_kabid_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_pkey PRIMARY KEY (t_id_notif_kabid);


--
-- Name: t_pelayanan_angsuran_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_pkey PRIMARY KEY (t_idangsuran);


--
-- Name: t_pelayanan_keberatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_pkey PRIMARY KEY (t_idkeberatan);


--
-- Name: t_pelayanan_keringanan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_pkey PRIMARY KEY (t_idkeringanan);


--
-- Name: t_pelayanan_mutasi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_pkey PRIMARY KEY (t_idmutasi);


--
-- Name: t_pelayanan_pembatalan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_pkey PRIMARY KEY (t_idpembatalan);


--
-- Name: t_pelayanan_penundaan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_pkey PRIMARY KEY (t_idpenundaan);


--
-- Name: t_pembayaran_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_pkey PRIMARY KEY (t_id_pembayaran);


--
-- Name: t_pembayaran_skpdkb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_pkey PRIMARY KEY (t_id_bayar_skpdkb);


--
-- Name: t_pembayaran_skpdkbt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_pkey PRIMARY KEY (t_id_bayar_skpdkb);


--
-- Name: t_pemeriksaan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_pkey PRIMARY KEY (t_idpemeriksa);


--
-- Name: t_pen_denda_ajb_notaris_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_pkey PRIMARY KEY (t_id_ajbdenda);


--
-- Name: t_pen_denda_lpor_notaris_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_pkey PRIMARY KEY (t_id_pendenda);


--
-- Name: t_skpdkb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkb
    ADD CONSTRAINT t_skpdkb_pkey PRIMARY KEY (t_idskpdkb);


--
-- Name: t_skpdkbt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_pkey PRIMARY KEY (t_idskpdkbt);


--
-- Name: t_spt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_pkey PRIMARY KEY (t_idspt);


--
-- Name: t_validasi_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_pkey PRIMARY KEY (t_id_validasi_berkas);


--
-- Name: t_validasi_kabid_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_pkey PRIMARY KEY (t_id_validasi_kabid);


--
-- Name: users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: activity_log_log_name_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX activity_log_log_name_index ON activity_log USING btree (log_name);


--
-- Name: causer; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX causer ON activity_log USING btree (causer_type, causer_id);


--
-- Name: model_has_permissions_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_permissions_model_id_model_type_index ON model_has_permissions USING btree (model_id, model_type);


--
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_roles_model_id_model_type_index ON model_has_roles USING btree (model_id, model_type);


--
-- Name: notifications_notifiable_type_notifiable_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX notifications_notifiable_type_notifiable_id_index ON notifications USING btree (notifiable_type, notifiable_id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: subject; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX subject ON activity_log USING btree (subject_type, subject_id);


--
-- Name: model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES permissions(id) ON DELETE CASCADE;


--
-- Name: model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;


--
-- Name: role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES permissions(id) ON DELETE CASCADE;


--
-- Name: role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;


--
-- Name: s_acuan_jenistanah_id_jenistanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_acuan_jenistanah
    ADD CONSTRAINT s_acuan_jenistanah_id_jenistanah_foreign FOREIGN KEY (id_jenistanah) REFERENCES s_jenistanah(id_jenistanah);


--
-- Name: s_background_s_id_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_background
    ADD CONSTRAINT s_background_s_id_status_foreign FOREIGN KEY (s_id_status) REFERENCES s_status(s_id_status);


--
-- Name: s_jenistransaksi_s_id_dptnpoptkp_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenistransaksi
    ADD CONSTRAINT s_jenistransaksi_s_id_dptnpoptkp_foreign FOREIGN KEY (s_id_dptnpoptkp) REFERENCES s_status_dptnpoptkp(s_id_dptnpoptkp);


--
-- Name: s_jenistransaksi_s_idstatus_pht_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_jenistransaksi
    ADD CONSTRAINT s_jenistransaksi_s_idstatus_pht_foreign FOREIGN KEY (s_idstatus_pht) REFERENCES s_status_perhitungan(s_idstatus_pht);


--
-- Name: s_kelurahan_s_idkecamatan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_kelurahan
    ADD CONSTRAINT s_kelurahan_s_idkecamatan_foreign FOREIGN KEY (s_idkecamatan) REFERENCES s_kecamatan(s_idkecamatan);


--
-- Name: s_persyaratan_s_idwajib_up_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_persyaratan
    ADD CONSTRAINT s_persyaratan_s_idwajib_up_foreign FOREIGN KEY (s_idwajib_up) REFERENCES s_wajib_up(s_idwajib_up);


--
-- Name: s_target_bphtb_s_id_target_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_target_bphtb
    ADD CONSTRAINT s_target_bphtb_s_id_target_status_foreign FOREIGN KEY (s_id_target_status) REFERENCES s_target_status(s_id_target_status);


--
-- Name: s_tarifnpoptkp_s_idjenistransaksinpoptkp_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_tarifnpoptkp
    ADD CONSTRAINT s_tarifnpoptkp_s_idjenistransaksinpoptkp_foreign FOREIGN KEY (s_idjenistransaksinpoptkp) REFERENCES s_jenistransaksi(s_idjenistransaksi);


--
-- Name: s_tarifnpoptkp_s_statusnpoptkp_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_tarifnpoptkp
    ADD CONSTRAINT s_tarifnpoptkp_s_statusnpoptkp_foreign FOREIGN KEY (s_statusnpoptkp) REFERENCES s_status(s_id_status);


--
-- Name: s_tempo_s_id_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s_tempo
    ADD CONSTRAINT s_tempo_s_id_status_foreign FOREIGN KEY (s_id_status) REFERENCES s_status(s_id_status);


--
-- Name: t_file_keringanan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_file_keringanan
    ADD CONSTRAINT t_file_keringanan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_file_keringanan_t_iduser_upload_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_file_keringanan
    ADD CONSTRAINT t_file_keringanan_t_iduser_upload_foreign FOREIGN KEY (t_iduser_upload) REFERENCES users(id);


--
-- Name: t_file_objek_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_file_objek
    ADD CONSTRAINT t_file_objek_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_file_objek_t_iduser_upload_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_file_objek
    ADD CONSTRAINT t_file_objek_t_iduser_upload_foreign FOREIGN KEY (t_iduser_upload) REFERENCES users(id);


--
-- Name: t_filesyarat_bphtb_s_idjenistransaksi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_s_idjenistransaksi_foreign FOREIGN KEY (s_idjenistransaksi) REFERENCES s_jenistransaksi(s_idjenistransaksi);


--
-- Name: t_filesyarat_bphtb_s_idpersyaratan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_s_idpersyaratan_foreign FOREIGN KEY (s_idpersyaratan) REFERENCES s_persyaratan(s_idpersyaratan);


--
-- Name: t_filesyarat_bphtb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_filesyarat_bphtb_t_iduser_upload_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_t_iduser_upload_foreign FOREIGN KEY (t_iduser_upload) REFERENCES users(id);


--
-- Name: t_inputajb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_inputajb
    ADD CONSTRAINT t_inputajb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_inputajb_t_iduser_input_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_inputajb
    ADD CONSTRAINT t_inputajb_t_iduser_input_foreign FOREIGN KEY (t_iduser_input) REFERENCES users(id);


--
-- Name: t_laporbulanan_detail_t_idajb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_laporbulanan_detail
    ADD CONSTRAINT t_laporbulanan_detail_t_idajb_foreign FOREIGN KEY (t_idajb) REFERENCES t_inputajb(t_idajb);


--
-- Name: t_laporbulanan_detail_t_idlaporbulanan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_laporbulanan_detail
    ADD CONSTRAINT t_laporbulanan_detail_t_idlaporbulanan_foreign FOREIGN KEY (t_idlaporbulanan) REFERENCES t_laporbulanan_head(t_idlaporbulanan);


--
-- Name: t_laporbulanan_head_t_iduser_input_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_laporbulanan_head
    ADD CONSTRAINT t_laporbulanan_head_t_iduser_input_foreign FOREIGN KEY (t_iduser_input) REFERENCES users(id);


--
-- Name: t_nik_bersama_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_nik_bersama
    ADD CONSTRAINT t_nik_bersama_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_notif_validasi_berkas_s_id_status_lihat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_s_id_status_lihat_foreign FOREIGN KEY (s_id_status_lihat) REFERENCES users(id);


--
-- Name: t_notif_validasi_berkas_t_dari_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_t_dari_foreign FOREIGN KEY (t_dari) REFERENCES users(id);


--
-- Name: t_notif_validasi_berkas_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_notif_validasi_berkas_t_untuk_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_t_untuk_foreign FOREIGN KEY (t_untuk) REFERENCES users(id);


--
-- Name: t_notif_validasi_kabid_s_id_status_lihat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_s_id_status_lihat_foreign FOREIGN KEY (s_id_status_lihat) REFERENCES users(id);


--
-- Name: t_notif_validasi_kabid_t_dari_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_t_dari_foreign FOREIGN KEY (t_dari) REFERENCES users(id);


--
-- Name: t_notif_validasi_kabid_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_notif_validasi_kabid_t_untuk_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_t_untuk_foreign FOREIGN KEY (t_untuk) REFERENCES users(id);


--
-- Name: t_pelayanan_angsuran_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES users(id);


--
-- Name: t_pelayanan_angsuran_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pelayanan_angsuran_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_angsuran_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES users(id);


--
-- Name: t_pelayanan_keberatan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES users(id);


--
-- Name: t_pelayanan_keberatan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pelayanan_keberatan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_keberatan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES users(id);


--
-- Name: t_pelayanan_keringanan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES users(id);


--
-- Name: t_pelayanan_keringanan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pelayanan_keringanan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_keringanan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES users(id);


--
-- Name: t_pelayanan_mutasi_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES users(id);


--
-- Name: t_pelayanan_mutasi_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pelayanan_mutasi_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_mutasi_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES users(id);


--
-- Name: t_pelayanan_pembatalan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES users(id);


--
-- Name: t_pelayanan_pembatalan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pelayanan_pembatalan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_pembatalan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES users(id);


--
-- Name: t_pelayanan_penundaan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES users(id);


--
-- Name: t_pelayanan_penundaan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pelayanan_penundaan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_penundaan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES users(id);


--
-- Name: t_pembayaran_bphtb_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES s_status_bayar(s_id_status_bayar);


--
-- Name: t_pembayaran_bphtb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pembayaran_bphtb_t_iduser_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_t_iduser_bayar_foreign FOREIGN KEY (t_iduser_bayar) REFERENCES users(id);


--
-- Name: t_pembayaran_skpdkb_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES s_status_bayar(s_id_status_bayar);


--
-- Name: t_pembayaran_skpdkb_t_idskpdkb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_t_idskpdkb_foreign FOREIGN KEY (t_idskpdkb) REFERENCES t_skpdkb(t_idskpdkb);


--
-- Name: t_pembayaran_skpdkb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pembayaran_skpdkb_t_iduser_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_t_iduser_bayar_foreign FOREIGN KEY (t_iduser_bayar) REFERENCES users(id);


--
-- Name: t_pembayaran_skpdkbt_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES s_status_bayar(s_id_status_bayar);


--
-- Name: t_pembayaran_skpdkbt_t_idskpdkbt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_t_idskpdkbt_foreign FOREIGN KEY (t_idskpdkbt) REFERENCES t_skpdkb(t_idskpdkb);


--
-- Name: t_pembayaran_skpdkbt_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pembayaran_skpdkbt_t_iduser_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_t_iduser_bayar_foreign FOREIGN KEY (t_iduser_bayar) REFERENCES users(id);


--
-- Name: t_pemeriksaan_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES users(id);


--
-- Name: t_pemeriksaan_t_idpejabat1_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat1_foreign FOREIGN KEY (t_idpejabat1) REFERENCES s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan_t_idpejabat2_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat2_foreign FOREIGN KEY (t_idpejabat2) REFERENCES s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan_t_idpejabat3_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat3_foreign FOREIGN KEY (t_idpejabat3) REFERENCES s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan_t_idpejabat4_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat4_foreign FOREIGN KEY (t_idpejabat4) REFERENCES s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pemeriksaan_t_idtarifbphtb_pemeriksa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idtarifbphtb_pemeriksa_foreign FOREIGN KEY (t_idtarifbphtb_pemeriksa) REFERENCES s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_pemeriksaan_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES users(id);


--
-- Name: t_pen_denda_ajb_notaris_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES s_status_bayar(s_id_status_bayar);


--
-- Name: t_pen_denda_ajb_notaris_t_idnotaris_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_t_idnotaris_foreign FOREIGN KEY (t_idnotaris) REFERENCES s_notaris(s_idnotaris);


--
-- Name: t_pen_denda_ajb_notaris_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_pen_denda_ajb_notaris_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES users(id);


--
-- Name: t_pen_denda_lpor_notaris_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES s_status_bayar(s_id_status_bayar);


--
-- Name: t_pen_denda_lpor_notaris_t_idlaporbulanan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_t_idlaporbulanan_foreign FOREIGN KEY (t_idlaporbulanan) REFERENCES t_laporbulanan_head(t_idlaporbulanan);


--
-- Name: t_pen_denda_lpor_notaris_t_idnotaris_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_t_idnotaris_foreign FOREIGN KEY (t_idnotaris) REFERENCES s_notaris(s_idnotaris);


--
-- Name: t_pen_denda_lpor_notaris_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES users(id);


--
-- Name: t_skpdkb_t_idjenisketetapan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idjenisketetapan_foreign FOREIGN KEY (t_idjenisketetapan) REFERENCES s_jenisketetapan(s_idjenisketetapan);


--
-- Name: t_skpdkb_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES users(id);


--
-- Name: t_skpdkb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_skpdkb_t_idtarifbphtb_skpdkb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idtarifbphtb_skpdkb_foreign FOREIGN KEY (t_idtarifbphtb_skpdkb) REFERENCES s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_skpdkb_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES users(id);


--
-- Name: t_skpdkbt_t_idjenisketetapan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idjenisketetapan_foreign FOREIGN KEY (t_idjenisketetapan) REFERENCES s_jenisketetapan(s_idjenisketetapan);


--
-- Name: t_skpdkbt_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES users(id);


--
-- Name: t_skpdkbt_t_idskpdkb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idskpdkb_foreign FOREIGN KEY (t_idskpdkb) REFERENCES t_skpdkb(t_idskpdkb);


--
-- Name: t_skpdkbt_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_skpdkbt_t_idtarifbphtb_skpdkbt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idtarifbphtb_skpdkbt_foreign FOREIGN KEY (t_idtarifbphtb_skpdkbt) REFERENCES s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_skpdkbt_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES users(id);


--
-- Name: t_spt_t_b_idkec_pngjwb_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_b_idkec_pngjwb_pembeli_foreign FOREIGN KEY (t_b_idkec_pngjwb_pembeli) REFERENCES s_kecamatan(s_idkecamatan);


--
-- Name: t_spt_t_b_idkec_pngjwb_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_b_idkec_pngjwb_penjual_foreign FOREIGN KEY (t_b_idkec_pngjwb_penjual) REFERENCES s_kecamatan(s_idkecamatan);


--
-- Name: t_spt_t_b_idkel_pngjwb_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_b_idkel_pngjwb_pembeli_foreign FOREIGN KEY (t_b_idkel_pngjwb_pembeli) REFERENCES s_kelurahan(s_idkelurahan);


--
-- Name: t_spt_t_b_idkel_pngjwb_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_b_idkel_pngjwb_penjual_foreign FOREIGN KEY (t_b_idkel_pngjwb_penjual) REFERENCES s_kelurahan(s_idkelurahan);


--
-- Name: t_spt_t_id_jenistanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_id_jenistanah_foreign FOREIGN KEY (t_id_jenistanah) REFERENCES s_jenistanah(id_jenistanah);


--
-- Name: t_spt_t_idbidang_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idbidang_penjual_foreign FOREIGN KEY (t_idbidang_penjual) REFERENCES s_jenis_bidangusaha(s_idbidang_usaha);


--
-- Name: t_spt_t_idbidang_usaha_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idbidang_usaha_foreign FOREIGN KEY (t_idbidang_usaha) REFERENCES s_jenis_bidangusaha(s_idbidang_usaha);


--
-- Name: t_spt_t_idjenisdoktanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idjenisdoktanah_foreign FOREIGN KEY (t_idjenisdoktanah) REFERENCES s_jenisdoktanah(s_iddoktanah);


--
-- Name: t_spt_t_idjenisfasilitas_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idjenisfasilitas_foreign FOREIGN KEY (t_idjenisfasilitas) REFERENCES s_jenisfasilitas(s_idjenisfasilitas);


--
-- Name: t_spt_t_idjenishaktanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idjenishaktanah_foreign FOREIGN KEY (t_idjenishaktanah) REFERENCES s_jenishaktanah(s_idhaktanah);


--
-- Name: t_spt_t_idjenisketetapan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idjenisketetapan_foreign FOREIGN KEY (t_idjenisketetapan) REFERENCES s_jenisketetapan(s_idjenisketetapan);


--
-- Name: t_spt_t_idjenistransaksi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idjenistransaksi_foreign FOREIGN KEY (t_idjenistransaksi) REFERENCES s_jenistransaksi(s_idjenistransaksi);


--
-- Name: t_spt_t_idkec_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idkec_pembeli_foreign FOREIGN KEY (t_idkec_pembeli) REFERENCES s_kecamatan(s_idkecamatan);


--
-- Name: t_spt_t_idkec_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idkec_penjual_foreign FOREIGN KEY (t_idkec_penjual) REFERENCES s_kecamatan(s_idkecamatan);


--
-- Name: t_spt_t_idkel_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idkel_pembeli_foreign FOREIGN KEY (t_idkel_pembeli) REFERENCES s_kelurahan(s_idkelurahan);


--
-- Name: t_spt_t_idkel_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idkel_penjual_foreign FOREIGN KEY (t_idkel_penjual) REFERENCES s_kelurahan(s_idkelurahan);


--
-- Name: t_spt_t_idnotaris_spt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idnotaris_spt_foreign FOREIGN KEY (t_idnotaris_spt) REFERENCES s_notaris(s_idnotaris);


--
-- Name: t_spt_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES users(id);


--
-- Name: t_spt_t_idpengurangan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idpengurangan_foreign FOREIGN KEY (t_idpengurangan) REFERENCES s_jenispengurangan(s_idpengurangan);


--
-- Name: t_spt_t_idstatus_npwpd_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idstatus_npwpd_foreign FOREIGN KEY (t_idstatus_npwpd) REFERENCES s_status_npwpd(s_idstatus_npwpd);


--
-- Name: t_spt_t_idtarifbphtb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_idtarifbphtb_foreign FOREIGN KEY (t_idtarifbphtb) REFERENCES s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_spt_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_spt
    ADD CONSTRAINT t_spt_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES users(id);


--
-- Name: t_validasi_berkas_s_id_status_berkas_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_s_id_status_berkas_foreign FOREIGN KEY (s_id_status_berkas) REFERENCES s_status_berkas(s_id_status_berkas);


--
-- Name: t_validasi_berkas_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_validasi_berkas_t_iduser_validasi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_t_iduser_validasi_foreign FOREIGN KEY (t_iduser_validasi) REFERENCES users(id);


--
-- Name: t_validasi_kabid_s_id_status_kabid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_s_id_status_kabid_foreign FOREIGN KEY (s_id_status_kabid) REFERENCES s_status_kabid(s_id_status_kabid);


--
-- Name: t_validasi_kabid_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES t_spt(t_idspt);


--
-- Name: t_validasi_kabid_t_iduser_validasi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_t_iduser_validasi_foreign FOREIGN KEY (t_iduser_validasi) REFERENCES users(id);


--
-- Name: users_s_id_hakakses_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_s_id_hakakses_foreign FOREIGN KEY (s_id_hakakses) REFERENCES s_hak_akses(s_id_hakakses);


--
-- Name: users_s_idnotaris_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_s_idnotaris_foreign FOREIGN KEY (s_idnotaris) REFERENCES s_notaris(s_idnotaris);


--
-- Name: users_s_idpejabat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_s_idpejabat_foreign FOREIGN KEY (s_idpejabat) REFERENCES s_pejabat(s_idpejabat);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

