--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: activity_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activity_log (
    id bigint NOT NULL,
    log_name character varying(255),
    description text NOT NULL,
    subject_type character varying(255),
    subject_id bigint,
    causer_type character varying(255),
    causer_id bigint,
    properties json,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.activity_log OWNER TO postgres;

--
-- Name: activity_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.activity_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activity_log_id_seq OWNER TO postgres;

--
-- Name: activity_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.activity_log_id_seq OWNED BY public.activity_log.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: his_getbphtb_bpn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.his_getbphtb_bpn (
    t_idhis_getbpn integer NOT NULL,
    t_username character varying(255),
    t_password character varying(255),
    t_nop character varying(255),
    t_ntpd character varying(255),
    t_nik character varying(255),
    t_nama character varying(255),
    t_alamat text,
    t_kelurahan_op character varying(255),
    t_kecamatan_op character varying(255),
    t_kota_kab_op character varying(255),
    t_luastanah double precision,
    t_luasbangunan double precision,
    t_pembayaran character varying(255),
    t_status character varying(255),
    t_tanggal_pembayaran timestamp(0) without time zone,
    t_jenisbayar character varying(255),
    respon_code character varying(255),
    t_iduser integer,
    t_tglby_system timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.his_getbphtb_bpn OWNER TO postgres;

--
-- Name: his_getbphtb_bpn_t_idhis_getbpn_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.his_getbphtb_bpn_t_idhis_getbpn_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.his_getbphtb_bpn_t_idhis_getbpn_seq OWNER TO postgres;

--
-- Name: his_getbphtb_bpn_t_idhis_getbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.his_getbphtb_bpn_t_idhis_getbpn_seq OWNED BY public.his_getbphtb_bpn.t_idhis_getbpn;


--
-- Name: his_getpbb_bpn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.his_getpbb_bpn (
    t_idhis_pbbbpn integer NOT NULL,
    t_username character varying(255) NOT NULL,
    t_password character varying(255) NOT NULL,
    t_nop character varying(255) NOT NULL,
    t_nik character varying(255) NOT NULL,
    t_nama_wp character varying(255) NOT NULL,
    t_alamat_op text NOT NULL,
    t_kecamatan_op character varying(255) NOT NULL,
    t_kelurahan_op character varying(255) NOT NULL,
    t_kota_kab_op character varying(255) NOT NULL,
    t_luas_tanah_op double precision,
    t_luas_bangunan_op double precision,
    t_njop_tanah_op double precision,
    t_njop_bangunan_op double precision,
    t_status_tunggakan character varying(255) NOT NULL,
    t_iduser integer,
    t_tglby_system timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.his_getpbb_bpn OWNER TO postgres;

--
-- Name: his_getpbb_bpn_t_idhis_pbbbpn_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.his_getpbb_bpn_t_idhis_pbbbpn_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.his_getpbb_bpn_t_idhis_pbbbpn_seq OWNER TO postgres;

--
-- Name: his_getpbb_bpn_t_idhis_pbbbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.his_getpbb_bpn_t_idhis_pbbbpn_seq OWNED BY public.his_getpbb_bpn.t_idhis_pbbbpn;


--
-- Name: his_postdatabpn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.his_postdatabpn (
    t_idhis_posbpn integer NOT NULL,
    t_username character varying(255),
    t_password character varying(255),
    t_aktaid character varying(255),
    t_tgl_akta timestamp(0) without time zone,
    t_nop character varying(255),
    t_nik character varying(255),
    t_nib character varying(255),
    t_npwp character varying(255),
    t_nama_wp character varying(255),
    t_alamat_op text,
    t_kecamatan_op character varying(255),
    t_kelurahan_op character varying(255),
    t_kota_kab_op character varying(255),
    t_luastanah_op double precision,
    t_luasbangunan_op double precision,
    t_no_sertipikat character varying(255),
    t_no_akta character varying(255),
    t_ppat character varying(255),
    t_status character varying(255),
    t_iduser integer,
    t_tglby_system timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.his_postdatabpn OWNER TO postgres;

--
-- Name: his_postdatabpn_t_idhis_posbpn_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.his_postdatabpn_t_idhis_posbpn_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.his_postdatabpn_t_idhis_posbpn_seq OWNER TO postgres;

--
-- Name: his_postdatabpn_t_idhis_posbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.his_postdatabpn_t_idhis_posbpn_seq OWNED BY public.his_postdatabpn.t_idhis_posbpn;


--
-- Name: integrasi_sertifikat_bpn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.integrasi_sertifikat_bpn (
    t_idsertifikat integer NOT NULL,
    aktaid character varying(255) NOT NULL,
    tgl_akta timestamp(0) without time zone NOT NULL,
    nop character varying(255) NOT NULL,
    nib character varying(255) NOT NULL,
    nik character varying(255) NOT NULL,
    npwp character varying(255) NOT NULL,
    nama_wp character varying(255) NOT NULL,
    alamat_op character varying(255) NOT NULL,
    kelurahan_op character varying(255) NOT NULL,
    kecamatan_op character varying(255) NOT NULL,
    kota_op character varying(255) NOT NULL,
    luastanah_op double precision,
    luasbangunan_op double precision,
    ppat character varying(255) NOT NULL,
    no_sertipikat character varying(255) NOT NULL,
    no_akta character varying(255) NOT NULL,
    t_iduser integer,
    t_tgltransaksi timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.integrasi_sertifikat_bpn OWNER TO postgres;

--
-- Name: integrasi_sertifikat_bpn_t_idsertifikat_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.integrasi_sertifikat_bpn_t_idsertifikat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.integrasi_sertifikat_bpn_t_idsertifikat_seq OWNER TO postgres;

--
-- Name: integrasi_sertifikat_bpn_t_idsertifikat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.integrasi_sertifikat_bpn_t_idsertifikat_seq OWNED BY public.integrasi_sertifikat_bpn.t_idsertifikat;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: model_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_permissions OWNER TO postgres;

--
-- Name: model_has_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_roles OWNER TO postgres;

--
-- Name: notifications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notifications (
    t_id_notif integer NOT NULL,
    t_id_user_buat integer,
    t_idspt integer,
    read_pemda_at timestamp(0) without time zone,
    t_user_id integer,
    read_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.notifications OWNER TO postgres;

--
-- Name: notifications_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notifications_message (
    t_id_notif_detail integer NOT NULL,
    t_id_notif integer,
    t_id_jenis_notif integer,
    t_user_id integer,
    pesan character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.notifications_message OWNER TO postgres;

--
-- Name: notifications_message_t_id_notif_detail_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notifications_message_t_id_notif_detail_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notifications_message_t_id_notif_detail_seq OWNER TO postgres;

--
-- Name: notifications_message_t_id_notif_detail_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notifications_message_t_id_notif_detail_seq OWNED BY public.notifications_message.t_id_notif_detail;


--
-- Name: notifications_t_id_notif_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notifications_t_id_notif_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notifications_t_id_notif_seq OWNER TO postgres;

--
-- Name: notifications_t_id_notif_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notifications_t_id_notif_seq OWNED BY public.notifications.t_id_notif;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permissions OWNER TO postgres;

--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO postgres;

--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- Name: role_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.role_has_permissions OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: s_acuan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_acuan (
    s_idacuan integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_kd_blok character varying(3),
    s_permetertanah double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_acuan OWNER TO postgres;

--
-- Name: s_acuan_jenistanah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_acuan_jenistanah (
    s_idacuan_jenis integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_kd_blok character varying(3),
    id_jenistanah integer,
    s_permetertanah double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_acuan_jenistanah OWNER TO postgres;

--
-- Name: s_acuan_jenistanah_s_idacuan_jenis_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_acuan_jenistanah_s_idacuan_jenis_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_acuan_jenistanah_s_idacuan_jenis_seq OWNER TO postgres;

--
-- Name: s_acuan_jenistanah_s_idacuan_jenis_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_acuan_jenistanah_s_idacuan_jenis_seq OWNED BY public.s_acuan_jenistanah.s_idacuan_jenis;


--
-- Name: s_acuan_s_idacuan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_acuan_s_idacuan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_acuan_s_idacuan_seq OWNER TO postgres;

--
-- Name: s_acuan_s_idacuan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_acuan_s_idacuan_seq OWNED BY public.s_acuan.s_idacuan;


--
-- Name: s_background; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_background (
    id bigint NOT NULL,
    s_thumbnail text,
    s_id_status integer NOT NULL,
    s_iduser integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_background OWNER TO postgres;

--
-- Name: s_background_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_background_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_background_id_seq OWNER TO postgres;

--
-- Name: s_background_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_background_id_seq OWNED BY public.s_background.id;


--
-- Name: s_bpn_ws; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_bpn_ws (
    id integer NOT NULL,
    s_username character varying(255),
    s_password character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_bpn_ws OWNER TO postgres;

--
-- Name: s_bpn_ws_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_bpn_ws_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_bpn_ws_id_seq OWNER TO postgres;

--
-- Name: s_bpn_ws_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_bpn_ws_id_seq OWNED BY public.s_bpn_ws.id;


--
-- Name: s_hak_akses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_hak_akses (
    s_id_hakakses integer NOT NULL,
    s_nama_hakakses character varying(255)
);


ALTER TABLE public.s_hak_akses OWNER TO postgres;

--
-- Name: s_hak_akses_s_id_hakakses_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_hak_akses_s_id_hakakses_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_hak_akses_s_id_hakakses_seq OWNER TO postgres;

--
-- Name: s_hak_akses_s_id_hakakses_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_hak_akses_s_id_hakakses_seq OWNED BY public.s_hak_akses.s_id_hakakses;


--
-- Name: s_harga_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_harga_history (
    s_idhistory integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_kd_blok character varying(3),
    s_kd_urut character varying(4),
    s_kd_jenis integer,
    s_tahun_sppt integer,
    s_permetertanah double precision,
    id_jenistanah integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_harga_history OWNER TO postgres;

--
-- Name: s_harga_history_s_idhistory_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_harga_history_s_idhistory_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_harga_history_s_idhistory_seq OWNER TO postgres;

--
-- Name: s_harga_history_s_idhistory_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_harga_history_s_idhistory_seq OWNED BY public.s_harga_history.s_idhistory;


--
-- Name: s_jenis_bidangusaha; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_jenis_bidangusaha (
    s_idbidang_usaha integer NOT NULL,
    s_nama_bidangusaha character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenis_bidangusaha OWNER TO postgres;

--
-- Name: s_jenis_bidangusaha_s_idbidang_usaha_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_jenis_bidangusaha_s_idbidang_usaha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenis_bidangusaha_s_idbidang_usaha_seq OWNER TO postgres;

--
-- Name: s_jenis_bidangusaha_s_idbidang_usaha_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_jenis_bidangusaha_s_idbidang_usaha_seq OWNED BY public.s_jenis_bidangusaha.s_idbidang_usaha;


--
-- Name: s_jenisdoktanah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_jenisdoktanah (
    s_iddoktanah integer NOT NULL,
    s_namadoktanah text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenisdoktanah OWNER TO postgres;

--
-- Name: s_jenisdoktanah_s_iddoktanah_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_jenisdoktanah_s_iddoktanah_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenisdoktanah_s_iddoktanah_seq OWNER TO postgres;

--
-- Name: s_jenisdoktanah_s_iddoktanah_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_jenisdoktanah_s_iddoktanah_seq OWNED BY public.s_jenisdoktanah.s_iddoktanah;


--
-- Name: s_jenisfasilitas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_jenisfasilitas (
    s_idjenisfasilitas integer NOT NULL,
    s_kodejenisfasilitas character varying(5),
    s_namajenisfasilitas character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenisfasilitas OWNER TO postgres;

--
-- Name: s_jenisfasilitas_s_idjenisfasilitas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_jenisfasilitas_s_idjenisfasilitas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenisfasilitas_s_idjenisfasilitas_seq OWNER TO postgres;

--
-- Name: s_jenisfasilitas_s_idjenisfasilitas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_jenisfasilitas_s_idjenisfasilitas_seq OWNED BY public.s_jenisfasilitas.s_idjenisfasilitas;


--
-- Name: s_jenishaktanah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_jenishaktanah (
    s_idhaktanah integer NOT NULL,
    s_kodehaktanah character varying(10),
    s_namahaktanah text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenishaktanah OWNER TO postgres;

--
-- Name: s_jenishaktanah_s_idhaktanah_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_jenishaktanah_s_idhaktanah_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenishaktanah_s_idhaktanah_seq OWNER TO postgres;

--
-- Name: s_jenishaktanah_s_idhaktanah_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_jenishaktanah_s_idhaktanah_seq OWNED BY public.s_jenishaktanah.s_idhaktanah;


--
-- Name: s_jenisketetapan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_jenisketetapan (
    s_idjenisketetapan integer NOT NULL,
    s_namajenisketetapan character varying(255) NOT NULL,
    s_namasingkatjenisketetapan character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenisketetapan OWNER TO postgres;

--
-- Name: s_jenisketetapan_s_idjenisketetapan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_jenisketetapan_s_idjenisketetapan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenisketetapan_s_idjenisketetapan_seq OWNER TO postgres;

--
-- Name: s_jenisketetapan_s_idjenisketetapan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_jenisketetapan_s_idjenisketetapan_seq OWNED BY public.s_jenisketetapan.s_idjenisketetapan;


--
-- Name: s_jenispengurangan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_jenispengurangan (
    s_idpengurangan integer NOT NULL,
    s_persentase integer,
    s_namapengurangan text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenispengurangan OWNER TO postgres;

--
-- Name: s_jenispengurangan_s_idpengurangan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_jenispengurangan_s_idpengurangan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenispengurangan_s_idpengurangan_seq OWNER TO postgres;

--
-- Name: s_jenispengurangan_s_idpengurangan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_jenispengurangan_s_idpengurangan_seq OWNED BY public.s_jenispengurangan.s_idpengurangan;


--
-- Name: s_jenistanah; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_jenistanah (
    id_jenistanah integer NOT NULL,
    nama_jenistanah character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenistanah OWNER TO postgres;

--
-- Name: s_jenistanah_id_jenistanah_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_jenistanah_id_jenistanah_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenistanah_id_jenistanah_seq OWNER TO postgres;

--
-- Name: s_jenistanah_id_jenistanah_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_jenistanah_id_jenistanah_seq OWNED BY public.s_jenistanah.id_jenistanah;


--
-- Name: s_jenistransaksi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_jenistransaksi (
    s_idjenistransaksi integer NOT NULL,
    s_kodejenistransaksi character varying(5),
    s_namajenistransaksi character varying(255),
    s_idstatus_pht integer,
    s_id_dptnpoptkp integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenistransaksi OWNER TO postgres;

--
-- Name: s_jenistransaksi_s_idjenistransaksi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_jenistransaksi_s_idjenistransaksi_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenistransaksi_s_idjenistransaksi_seq OWNER TO postgres;

--
-- Name: s_jenistransaksi_s_idjenistransaksi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_jenistransaksi_s_idjenistransaksi_seq OWNED BY public.s_jenistransaksi.s_idjenistransaksi;


--
-- Name: s_kecamatan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_kecamatan (
    s_idkecamatan integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_namakecamatan text NOT NULL,
    s_latitude character varying(255),
    s_longitude character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_kecamatan OWNER TO postgres;

--
-- Name: s_kecamatan_s_idkecamatan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_kecamatan_s_idkecamatan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_kecamatan_s_idkecamatan_seq OWNER TO postgres;

--
-- Name: s_kecamatan_s_idkecamatan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_kecamatan_s_idkecamatan_seq OWNED BY public.s_kecamatan.s_idkecamatan;


--
-- Name: s_kelurahan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_kelurahan (
    s_idkelurahan integer NOT NULL,
    s_idkecamatan integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_namakelurahan text NOT NULL,
    s_latitude character varying(255),
    s_longitude character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_kelurahan OWNER TO postgres;

--
-- Name: s_kelurahan_s_idkelurahan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_kelurahan_s_idkelurahan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_kelurahan_s_idkelurahan_seq OWNER TO postgres;

--
-- Name: s_kelurahan_s_idkelurahan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_kelurahan_s_idkelurahan_seq OWNED BY public.s_kelurahan.s_idkelurahan;


--
-- Name: s_koderekening; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_koderekening (
    s_korekid integer NOT NULL,
    s_korektipe character varying(2) NOT NULL,
    s_korekkelompok character varying(2) NOT NULL,
    s_korekjenis character varying(2) NOT NULL,
    s_korekobjek character varying(4) NOT NULL,
    s_korekrincian character varying(4) NOT NULL,
    s_korekrinciansub character varying(10) NOT NULL,
    s_koreknama character varying(255) NOT NULL,
    s_korekketerangan text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_koderekening OWNER TO postgres;

--
-- Name: s_koderekening_s_korekid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_koderekening_s_korekid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_koderekening_s_korekid_seq OWNER TO postgres;

--
-- Name: s_koderekening_s_korekid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_koderekening_s_korekid_seq OWNED BY public.s_koderekening.s_korekid;


--
-- Name: s_notaris; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_notaris (
    s_idnotaris integer NOT NULL,
    s_namanotaris character varying(255),
    s_alamatnotaris text,
    s_kodenotaris character varying(255),
    s_sknotaris character varying(255),
    s_noid_bpn character varying(255),
    s_tgl1notaris timestamp(0) without time zone,
    s_tgl2notaris timestamp(0) without time zone,
    s_statusnotaris integer,
    s_npwpd_notaris character varying(255),
    s_daerahkerja text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_notaris OWNER TO postgres;

--
-- Name: s_notaris_s_idnotaris_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_notaris_s_idnotaris_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_notaris_s_idnotaris_seq OWNER TO postgres;

--
-- Name: s_notaris_s_idnotaris_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_notaris_s_idnotaris_seq OWNED BY public.s_notaris.s_idnotaris;


--
-- Name: s_pejabat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_pejabat (
    s_idpejabat integer NOT NULL,
    s_namapejabat text NOT NULL,
    s_jabatanpejabat text,
    s_nippejabat character varying(255),
    s_golonganpejabat text,
    s_alamat text,
    s_filettd text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_pejabat OWNER TO postgres;

--
-- Name: s_pejabat_s_idpejabat_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_pejabat_s_idpejabat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_pejabat_s_idpejabat_seq OWNER TO postgres;

--
-- Name: s_pejabat_s_idpejabat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_pejabat_s_idpejabat_seq OWNED BY public.s_pejabat.s_idpejabat;


--
-- Name: s_pemda; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_pemda (
    s_idpemda integer NOT NULL,
    s_namaprov character varying(255),
    s_namakabkot character varying(255),
    s_namaibukotakabkot character varying(255),
    s_kodeprovinsi character varying(3),
    s_kodekabkot character varying(4),
    s_namainstansi character varying(255),
    s_namasingkatinstansi character varying(255),
    s_alamatinstansi text,
    s_notelinstansi character varying(15),
    s_namabank character varying(255),
    s_norekbank character varying(255),
    s_latitude character varying(255),
    s_longitude character varying(255),
    s_logo text,
    s_namacabang character varying(255),
    s_kodecabang character varying(255),
    s_kodepos character varying(5),
    s_email text,
    s_urlbphtb character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_pemda OWNER TO postgres;

--
-- Name: s_pemda_s_idpemda_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_pemda_s_idpemda_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_pemda_s_idpemda_seq OWNER TO postgres;

--
-- Name: s_pemda_s_idpemda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_pemda_s_idpemda_seq OWNED BY public.s_pemda.s_idpemda;


--
-- Name: s_persyaratan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_persyaratan (
    s_idpersyaratan integer NOT NULL,
    s_idjenistransaksi integer NOT NULL,
    s_namapersyaratan character varying(255),
    s_idwajib_up integer,
    s_lokasimenu integer,
    "order" integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_persyaratan OWNER TO postgres;

--
-- Name: s_persyaratan_s_idpersyaratan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_persyaratan_s_idpersyaratan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_persyaratan_s_idpersyaratan_seq OWNER TO postgres;

--
-- Name: s_persyaratan_s_idpersyaratan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_persyaratan_s_idpersyaratan_seq OWNED BY public.s_persyaratan.s_idpersyaratan;


--
-- Name: s_presentase_wajar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_presentase_wajar (
    s_idpresentase_wajar integer NOT NULL,
    s_nilaimin double precision NOT NULL,
    s_nilaimax double precision NOT NULL,
    s_ketpresentase_wajar character varying(255),
    s_css_warna character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_presentase_wajar OWNER TO postgres;

--
-- Name: s_presentase_wajar_s_idpresentase_wajar_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_presentase_wajar_s_idpresentase_wajar_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_presentase_wajar_s_idpresentase_wajar_seq OWNER TO postgres;

--
-- Name: s_presentase_wajar_s_idpresentase_wajar_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_presentase_wajar_s_idpresentase_wajar_seq OWNED BY public.s_presentase_wajar.s_idpresentase_wajar;


--
-- Name: s_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_status (
    s_id_status integer NOT NULL,
    s_nama_status character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status OWNER TO postgres;

--
-- Name: s_status_bayar; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_status_bayar (
    s_id_status_bayar integer NOT NULL,
    s_nama_status_bayar character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_bayar OWNER TO postgres;

--
-- Name: s_status_bayar_s_id_status_bayar_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_status_bayar_s_id_status_bayar_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_bayar_s_id_status_bayar_seq OWNER TO postgres;

--
-- Name: s_status_bayar_s_id_status_bayar_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_status_bayar_s_id_status_bayar_seq OWNED BY public.s_status_bayar.s_id_status_bayar;


--
-- Name: s_status_berkas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_status_berkas (
    s_id_status_berkas integer NOT NULL,
    s_nama_status_berkas character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_berkas OWNER TO postgres;

--
-- Name: s_status_berkas_s_id_status_berkas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_status_berkas_s_id_status_berkas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_berkas_s_id_status_berkas_seq OWNER TO postgres;

--
-- Name: s_status_berkas_s_id_status_berkas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_status_berkas_s_id_status_berkas_seq OWNED BY public.s_status_berkas.s_id_status_berkas;


--
-- Name: s_status_dptnpoptkp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_status_dptnpoptkp (
    s_id_dptnpoptkp integer NOT NULL,
    s_nama_dptnpoptkp character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_dptnpoptkp OWNER TO postgres;

--
-- Name: s_status_dptnpoptkp_s_id_dptnpoptkp_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_status_dptnpoptkp_s_id_dptnpoptkp_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_dptnpoptkp_s_id_dptnpoptkp_seq OWNER TO postgres;

--
-- Name: s_status_dptnpoptkp_s_id_dptnpoptkp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_status_dptnpoptkp_s_id_dptnpoptkp_seq OWNED BY public.s_status_dptnpoptkp.s_id_dptnpoptkp;


--
-- Name: s_status_kabid; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_status_kabid (
    s_id_status_kabid integer NOT NULL,
    s_nama_status_kabid character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_kabid OWNER TO postgres;

--
-- Name: s_status_kabid_s_id_status_kabid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_status_kabid_s_id_status_kabid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_kabid_s_id_status_kabid_seq OWNER TO postgres;

--
-- Name: s_status_kabid_s_id_status_kabid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_status_kabid_s_id_status_kabid_seq OWNED BY public.s_status_kabid.s_id_status_kabid;


--
-- Name: s_status_lihat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_status_lihat (
    s_id_status_lihat integer NOT NULL,
    s_nama_status_lihat character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_lihat OWNER TO postgres;

--
-- Name: s_status_lihat_s_id_status_lihat_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_status_lihat_s_id_status_lihat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_lihat_s_id_status_lihat_seq OWNER TO postgres;

--
-- Name: s_status_lihat_s_id_status_lihat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_status_lihat_s_id_status_lihat_seq OWNED BY public.s_status_lihat.s_id_status_lihat;


--
-- Name: s_status_npwpd; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_status_npwpd (
    s_idstatus_npwpd integer NOT NULL,
    s_nama_status character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_npwpd OWNER TO postgres;

--
-- Name: s_status_npwpd_s_idstatus_npwpd_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_status_npwpd_s_idstatus_npwpd_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_npwpd_s_idstatus_npwpd_seq OWNER TO postgres;

--
-- Name: s_status_npwpd_s_idstatus_npwpd_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_status_npwpd_s_idstatus_npwpd_seq OWNED BY public.s_status_npwpd.s_idstatus_npwpd;


--
-- Name: s_status_pelayanan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_status_pelayanan (
    s_id_status_layanan integer NOT NULL,
    s_nama_status_layanan character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_pelayanan OWNER TO postgres;

--
-- Name: s_status_pelayanan_s_id_status_layanan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_status_pelayanan_s_id_status_layanan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_pelayanan_s_id_status_layanan_seq OWNER TO postgres;

--
-- Name: s_status_pelayanan_s_id_status_layanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_status_pelayanan_s_id_status_layanan_seq OWNED BY public.s_status_pelayanan.s_id_status_layanan;


--
-- Name: s_status_perhitungan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_status_perhitungan (
    s_idstatus_pht integer NOT NULL,
    s_nama character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_perhitungan OWNER TO postgres;

--
-- Name: s_status_perhitungan_s_idstatus_pht_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_status_perhitungan_s_idstatus_pht_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_perhitungan_s_idstatus_pht_seq OWNER TO postgres;

--
-- Name: s_status_perhitungan_s_idstatus_pht_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_status_perhitungan_s_idstatus_pht_seq OWNED BY public.s_status_perhitungan.s_idstatus_pht;


--
-- Name: s_status_s_id_status_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_status_s_id_status_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_s_id_status_seq OWNER TO postgres;

--
-- Name: s_status_s_id_status_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_status_s_id_status_seq OWNED BY public.s_status.s_id_status;


--
-- Name: s_target_bphtb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_target_bphtb (
    s_id_target_bphtb integer NOT NULL,
    s_id_target_status integer,
    s_tahun_target integer,
    s_nilai_target double precision,
    s_keterangan character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_target_bphtb OWNER TO postgres;

--
-- Name: s_target_bphtb_s_id_target_bphtb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_target_bphtb_s_id_target_bphtb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_target_bphtb_s_id_target_bphtb_seq OWNER TO postgres;

--
-- Name: s_target_bphtb_s_id_target_bphtb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_target_bphtb_s_id_target_bphtb_seq OWNED BY public.s_target_bphtb.s_id_target_bphtb;


--
-- Name: s_target_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_target_status (
    s_id_target_status integer NOT NULL,
    s_nama_status character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_target_status OWNER TO postgres;

--
-- Name: s_target_status_s_id_target_status_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_target_status_s_id_target_status_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_target_status_s_id_target_status_seq OWNER TO postgres;

--
-- Name: s_target_status_s_id_target_status_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_target_status_s_id_target_status_seq OWNED BY public.s_target_status.s_id_target_status;


--
-- Name: s_tarif_bphtb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_tarif_bphtb (
    s_idtarifbphtb integer NOT NULL,
    s_tarif_bphtb double precision,
    s_tgl_awal date,
    s_tgl_akhir date,
    s_dasar_hukum text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_tarif_bphtb OWNER TO postgres;

--
-- Name: s_tarif_bphtb_s_idtarifbphtb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_tarif_bphtb_s_idtarifbphtb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_tarif_bphtb_s_idtarifbphtb_seq OWNER TO postgres;

--
-- Name: s_tarif_bphtb_s_idtarifbphtb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_tarif_bphtb_s_idtarifbphtb_seq OWNED BY public.s_tarif_bphtb.s_idtarifbphtb;


--
-- Name: s_tarifnpoptkp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_tarifnpoptkp (
    s_idtarifnpoptkp integer NOT NULL,
    s_idjenistransaksinpoptkp integer,
    s_tarifnpoptkp double precision,
    s_tarifnpoptkptambahan double precision,
    s_dasarhukumnpoptkp text,
    s_statusnpoptkp integer,
    s_tglberlaku_awal date,
    s_tglberlaku_akhir date,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_tarifnpoptkp OWNER TO postgres;

--
-- Name: s_tarifnpoptkp_s_idtarifnpoptkp_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_tarifnpoptkp_s_idtarifnpoptkp_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_tarifnpoptkp_s_idtarifnpoptkp_seq OWNER TO postgres;

--
-- Name: s_tarifnpoptkp_s_idtarifnpoptkp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_tarifnpoptkp_s_idtarifnpoptkp_seq OWNED BY public.s_tarifnpoptkp.s_idtarifnpoptkp;


--
-- Name: s_tempo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_tempo (
    s_idtempo integer NOT NULL,
    s_haritempo integer NOT NULL,
    s_tglberlaku_awal timestamp(0) without time zone,
    s_tglberlaku_akhir timestamp(0) without time zone,
    s_id_status integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_tempo OWNER TO postgres;

--
-- Name: s_tempo_s_idtempo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_tempo_s_idtempo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_tempo_s_idtempo_seq OWNER TO postgres;

--
-- Name: s_tempo_s_idtempo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_tempo_s_idtempo_seq OWNED BY public.s_tempo.s_idtempo;


--
-- Name: s_users_api; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_users_api (
    s_idusers_api integer NOT NULL,
    s_username character varying(255),
    s_password character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_users_api OWNER TO postgres;

--
-- Name: s_users_api_s_idusers_api_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_users_api_s_idusers_api_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_users_api_s_idusers_api_seq OWNER TO postgres;

--
-- Name: s_users_api_s_idusers_api_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_users_api_s_idusers_api_seq OWNED BY public.s_users_api.s_idusers_api;


--
-- Name: s_wajib_up; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_wajib_up (
    s_idwajib_up integer NOT NULL,
    s_ket_wjb character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_wajib_up OWNER TO postgres;

--
-- Name: s_wajib_up_s_idwajib_up_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.s_wajib_up_s_idwajib_up_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_wajib_up_s_idwajib_up_seq OWNER TO postgres;

--
-- Name: s_wajib_up_s_idwajib_up_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.s_wajib_up_s_idwajib_up_seq OWNED BY public.s_wajib_up.s_idwajib_up;


--
-- Name: t_bpn; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_bpn (
    t_idbpn integer NOT NULL,
    t_noakta character varying(100),
    t_tglakta timestamp(0) without time zone,
    t_namappat character varying(255),
    t_nop character varying(255),
    t_nib character varying(255),
    t_ntpd character varying(255),
    t_nik character varying(255),
    t_npwp character varying(50),
    t_namawp character varying(255),
    t_kelurahanop character varying(255),
    t_kecamatanop character varying(255),
    t_kotaop character varying(255),
    t_luastanahop double precision,
    t_jenishak character varying(255),
    t_koordinat_x character varying(255),
    t_koordinat_y character varying(255),
    t_tgltransaksi timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_bpn OWNER TO postgres;

--
-- Name: t_bpn_t_idbpn_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_bpn_t_idbpn_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_bpn_t_idbpn_seq OWNER TO postgres;

--
-- Name: t_bpn_t_idbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_bpn_t_idbpn_seq OWNED BY public.t_bpn.t_idbpn;


--
-- Name: t_file_keringanan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_file_keringanan (
    t_idfile_keringanan integer NOT NULL,
    t_idspt integer,
    t_iduser_upload integer,
    letak_file text NOT NULL,
    nama_file text NOT NULL,
    t_tgl_upload timestamp(0) without time zone,
    t_keterangan_file text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_file_keringanan OWNER TO postgres;

--
-- Name: t_file_keringanan_t_idfile_keringanan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_file_keringanan_t_idfile_keringanan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_file_keringanan_t_idfile_keringanan_seq OWNER TO postgres;

--
-- Name: t_file_keringanan_t_idfile_keringanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_file_keringanan_t_idfile_keringanan_seq OWNED BY public.t_file_keringanan.t_idfile_keringanan;


--
-- Name: t_file_objek; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_file_objek (
    t_idfile_objek integer NOT NULL,
    t_idspt integer,
    t_iduser_upload integer,
    letak_file text,
    nama_file text,
    t_tgl_upload timestamp(0) without time zone,
    t_keterangan_file text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_file_objek OWNER TO postgres;

--
-- Name: t_file_objek_t_idfile_objek_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_file_objek_t_idfile_objek_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_file_objek_t_idfile_objek_seq OWNER TO postgres;

--
-- Name: t_file_objek_t_idfile_objek_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_file_objek_t_idfile_objek_seq OWNED BY public.t_file_objek.t_idfile_objek;


--
-- Name: t_filesyarat_bphtb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_filesyarat_bphtb (
    t_id_filesyarat integer NOT NULL,
    t_idspt integer,
    s_idpersyaratan integer,
    s_idjenistransaksi integer,
    t_iduser_upload integer,
    letak_file text,
    nama_file text,
    t_tgl_upload timestamp(0) without time zone,
    t_keterangan_file text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_filesyarat_bphtb OWNER TO postgres;

--
-- Name: t_filesyarat_bphtb_t_id_filesyarat_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_filesyarat_bphtb_t_id_filesyarat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_filesyarat_bphtb_t_id_filesyarat_seq OWNER TO postgres;

--
-- Name: t_filesyarat_bphtb_t_id_filesyarat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_filesyarat_bphtb_t_id_filesyarat_seq OWNED BY public.t_filesyarat_bphtb.t_id_filesyarat;


--
-- Name: t_inputajb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_inputajb (
    t_idajb integer NOT NULL,
    t_idspt integer,
    t_tgl_ajb timestamp(0) without time zone,
    t_no_ajb character varying(255) NOT NULL,
    t_iduser_input integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_inputajb OWNER TO postgres;

--
-- Name: t_inputajb_t_idajb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_inputajb_t_idajb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_inputajb_t_idajb_seq OWNER TO postgres;

--
-- Name: t_inputajb_t_idajb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_inputajb_t_idajb_seq OWNED BY public.t_inputajb.t_idajb;


--
-- Name: t_laporbulanan_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_laporbulanan_detail (
    t_idlaporbulanan integer,
    t_idajb integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_laporbulanan_detail OWNER TO postgres;

--
-- Name: t_laporbulanan_head; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_laporbulanan_head (
    t_idlaporbulanan integer NOT NULL,
    t_no_lapor integer,
    t_tgl_lapor timestamp(0) without time zone,
    t_untuk_bulan integer,
    t_untuk_tahun integer,
    t_keterangan text NOT NULL,
    t_iduser_input integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_laporbulanan_head OWNER TO postgres;

--
-- Name: t_laporbulanan_head_t_idlaporbulanan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_laporbulanan_head_t_idlaporbulanan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_laporbulanan_head_t_idlaporbulanan_seq OWNER TO postgres;

--
-- Name: t_laporbulanan_head_t_idlaporbulanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_laporbulanan_head_t_idlaporbulanan_seq OWNED BY public.t_laporbulanan_head.t_idlaporbulanan;


--
-- Name: t_nik_bersama; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_nik_bersama (
    t_id_nik_bersama integer NOT NULL,
    t_idspt integer,
    t_nik_bersama character varying(255),
    t_nama_bersama character varying(255),
    t_nama_jalan text,
    t_rt_bersama character varying(255),
    t_rw_bersama character varying(255),
    t_kecamatan_bersama character varying(255),
    t_kelurahan_bersama character varying(255),
    t_kabkota_bersama character varying(255),
    t_nohp_bersama character varying(255),
    t_kodepos_bersama character varying(255),
    t_npwp_bersama character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_nik_bersama OWNER TO postgres;

--
-- Name: t_nik_bersama_t_id_nik_bersama_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_nik_bersama_t_id_nik_bersama_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_nik_bersama_t_id_nik_bersama_seq OWNER TO postgres;

--
-- Name: t_nik_bersama_t_id_nik_bersama_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_nik_bersama_t_id_nik_bersama_seq OWNED BY public.t_nik_bersama.t_id_nik_bersama;


--
-- Name: t_notif_validasi_berkas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_notif_validasi_berkas (
    t_id_notif_berkas integer NOT NULL,
    t_idspt integer,
    t_isipesan text,
    t_dari integer,
    t_tglkirim timestamp(0) without time zone,
    t_untuk integer,
    s_id_status_lihat integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_notif_validasi_berkas OWNER TO postgres;

--
-- Name: t_notif_validasi_berkas_t_id_notif_berkas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_notif_validasi_berkas_t_id_notif_berkas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_notif_validasi_berkas_t_id_notif_berkas_seq OWNER TO postgres;

--
-- Name: t_notif_validasi_berkas_t_id_notif_berkas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_notif_validasi_berkas_t_id_notif_berkas_seq OWNED BY public.t_notif_validasi_berkas.t_id_notif_berkas;


--
-- Name: t_notif_validasi_kabid; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_notif_validasi_kabid (
    t_id_notif_kabid integer NOT NULL,
    t_idspt integer,
    t_isipesan text,
    t_dari integer,
    t_tglkirim timestamp(0) without time zone,
    t_untuk integer,
    s_id_status_lihat integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_notif_validasi_kabid OWNER TO postgres;

--
-- Name: t_notif_validasi_kabid_t_id_notif_kabid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_notif_validasi_kabid_t_id_notif_kabid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_notif_validasi_kabid_t_id_notif_kabid_seq OWNER TO postgres;

--
-- Name: t_notif_validasi_kabid_t_id_notif_kabid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_notif_validasi_kabid_t_id_notif_kabid_seq OWNED BY public.t_notif_validasi_kabid.t_id_notif_kabid;


--
-- Name: t_pelayanan_angsuran; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pelayanan_angsuran (
    t_idangsuran integer NOT NULL,
    t_idspt integer,
    t_noangsuran integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_angsuran character varying(255),
    t_jmlhangsuran double precision,
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_angsuran OWNER TO postgres;

--
-- Name: t_pelayanan_angsuran_t_idangsuran_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pelayanan_angsuran_t_idangsuran_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_angsuran_t_idangsuran_seq OWNER TO postgres;

--
-- Name: t_pelayanan_angsuran_t_idangsuran_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pelayanan_angsuran_t_idangsuran_seq OWNED BY public.t_pelayanan_angsuran.t_idangsuran;


--
-- Name: t_pelayanan_keberatan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pelayanan_keberatan (
    t_idkeberatan integer NOT NULL,
    t_idspt integer,
    t_nokeberatan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_keberatan character varying(255),
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    t_jmlhpotongan_disetujui double precision,
    t_persentase_disetujui double precision,
    t_jmlh_spt_sebenarnya double precision,
    t_jmlh_spt_hasilpot double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_keberatan OWNER TO postgres;

--
-- Name: t_pelayanan_keberatan_t_idkeberatan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pelayanan_keberatan_t_idkeberatan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_keberatan_t_idkeberatan_seq OWNER TO postgres;

--
-- Name: t_pelayanan_keberatan_t_idkeberatan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pelayanan_keberatan_t_idkeberatan_seq OWNED BY public.t_pelayanan_keberatan.t_idkeberatan;


--
-- Name: t_pelayanan_keringanan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pelayanan_keringanan (
    t_idkeringanan integer NOT NULL,
    t_idspt integer,
    t_nokeringanan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_keringanan character varying(255),
    t_jmlhpotongan double precision,
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    t_jmlhpotongan_disetujui double precision,
    t_persentase_disetujui double precision,
    t_jmlh_spt_sebenarnya double precision,
    t_jmlh_spt_hasilpot double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_keringanan OWNER TO postgres;

--
-- Name: t_pelayanan_keringanan_t_idkeringanan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pelayanan_keringanan_t_idkeringanan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_keringanan_t_idkeringanan_seq OWNER TO postgres;

--
-- Name: t_pelayanan_keringanan_t_idkeringanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pelayanan_keringanan_t_idkeringanan_seq OWNED BY public.t_pelayanan_keringanan.t_idkeringanan;


--
-- Name: t_pelayanan_mutasi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pelayanan_mutasi (
    t_idmutasi integer NOT NULL,
    t_idspt integer,
    t_nomutasi integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_mutasi character varying(255),
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_mutasi OWNER TO postgres;

--
-- Name: t_pelayanan_mutasi_t_idmutasi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pelayanan_mutasi_t_idmutasi_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_mutasi_t_idmutasi_seq OWNER TO postgres;

--
-- Name: t_pelayanan_mutasi_t_idmutasi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pelayanan_mutasi_t_idmutasi_seq OWNED BY public.t_pelayanan_mutasi.t_idmutasi;


--
-- Name: t_pelayanan_pembatalan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pelayanan_pembatalan (
    t_idpembatalan integer NOT NULL,
    t_idspt integer,
    t_nopembatalan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_keterangan_pembatalan text,
    t_iduser_pengajuan integer,
    t_nosk_pembatalan character varying(255) NOT NULL,
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_pembatalan OWNER TO postgres;

--
-- Name: t_pelayanan_pembatalan_t_idpembatalan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pelayanan_pembatalan_t_idpembatalan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_pembatalan_t_idpembatalan_seq OWNER TO postgres;

--
-- Name: t_pelayanan_pembatalan_t_idpembatalan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pelayanan_pembatalan_t_idpembatalan_seq OWNED BY public.t_pelayanan_pembatalan.t_idpembatalan;


--
-- Name: t_pelayanan_penundaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pelayanan_penundaan (
    t_idpenundaan integer NOT NULL,
    t_idspt integer,
    t_nopenundaan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_penundaan character varying(255),
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_penundaan OWNER TO postgres;

--
-- Name: t_pelayanan_penundaan_t_idpenundaan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pelayanan_penundaan_t_idpenundaan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_penundaan_t_idpenundaan_seq OWNER TO postgres;

--
-- Name: t_pelayanan_penundaan_t_idpenundaan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pelayanan_penundaan_t_idpenundaan_seq OWNED BY public.t_pelayanan_penundaan.t_idpenundaan;


--
-- Name: t_pembayaran_bphtb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pembayaran_bphtb (
    t_id_pembayaran integer NOT NULL,
    t_idspt integer,
    t_nourut_bayar integer,
    s_id_status_bayar integer,
    t_iduser_bayar integer,
    t_tglpembayaran_pokok timestamp(0) without time zone,
    t_jmlhbayar_pokok double precision,
    t_tglpembayaran_denda timestamp(0) without time zone,
    t_jmlhbayar_denda double precision,
    t_jmlhbayar_total double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pembayaran_bphtb OWNER TO postgres;

--
-- Name: t_pembayaran_bphtb_t_id_pembayaran_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pembayaran_bphtb_t_id_pembayaran_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pembayaran_bphtb_t_id_pembayaran_seq OWNER TO postgres;

--
-- Name: t_pembayaran_bphtb_t_id_pembayaran_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pembayaran_bphtb_t_id_pembayaran_seq OWNED BY public.t_pembayaran_bphtb.t_id_pembayaran;


--
-- Name: t_pembayaran_skpdkb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pembayaran_skpdkb (
    t_id_bayar_skpdkb integer NOT NULL,
    t_idspt integer,
    t_idskpdkb integer,
    t_nourut_bayar integer,
    s_id_status_bayar integer,
    t_iduser_bayar integer,
    t_tglpembayaran_skpdkb timestamp(0) without time zone,
    t_jmlhbayar_skpdkb double precision,
    t_tglpembayaran_denda timestamp(0) without time zone,
    t_jmlhbayar_denda double precision,
    t_jmlhbayar_total double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pembayaran_skpdkb OWNER TO postgres;

--
-- Name: t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq OWNER TO postgres;

--
-- Name: t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq OWNED BY public.t_pembayaran_skpdkb.t_id_bayar_skpdkb;


--
-- Name: t_pembayaran_skpdkbt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pembayaran_skpdkbt (
    t_id_bayar_skpdkb integer NOT NULL,
    t_idspt integer,
    t_idskpdkbt integer,
    t_nourut_bayar integer,
    s_id_status_bayar integer,
    t_iduser_bayar integer,
    t_tglpembayaran_skpdkbt timestamp(0) without time zone,
    t_jmlhbayar_skpdkbt double precision,
    t_tglpembayaran_denda timestamp(0) without time zone,
    t_jmlhbayar_denda double precision,
    t_jmlhbayar_total double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pembayaran_skpdkbt OWNER TO postgres;

--
-- Name: t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq OWNER TO postgres;

--
-- Name: t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq OWNED BY public.t_pembayaran_skpdkbt.t_id_bayar_skpdkb;


--
-- Name: t_pemeriksaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pemeriksaan (
    t_idpemeriksa integer NOT NULL,
    t_idspt integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    t_iduser_buat integer,
    t_idpejabat1 integer,
    t_idpejabat2 integer,
    t_idpejabat3 integer,
    t_idpejabat4 integer,
    t_noperiksa character varying(255),
    t_keterangan text,
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_luastanah_pemeriksa double precision,
    t_njoptanah_pemeriksa double precision,
    t_totalnjoptanah_pemeriksa double precision,
    t_luasbangunan_pemeriksa double precision,
    t_njopbangunan_pemeriksa double precision,
    t_totalnjopbangunan_pemeriksa double precision,
    t_grandtotalnjop_pemeriksa double precision,
    t_nilaitransaksispt_pemeriksa double precision,
    t_trf_aphb_kali_pemeriksa double precision,
    t_trf_aphb_bagi_pemeriksa double precision,
    t_permeter_tanah_pemeriksa double precision,
    t_idtarifbphtb_pemeriksa integer,
    t_persenbphtb_pemeriksa double precision,
    t_npop_bphtb_pemeriksa double precision,
    t_npoptkp_bphtb_pemeriksa double precision,
    t_npopkp_bphtb_pemeriksa double precision,
    t_nilaibphtb_pemeriksa double precision,
    t_idpersetujuan_pemeriksa integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pemeriksaan OWNER TO postgres;

--
-- Name: t_pemeriksaan_t_idpemeriksa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pemeriksaan_t_idpemeriksa_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pemeriksaan_t_idpemeriksa_seq OWNER TO postgres;

--
-- Name: t_pemeriksaan_t_idpemeriksa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pemeriksaan_t_idpemeriksa_seq OWNED BY public.t_pemeriksaan.t_idpemeriksa;


--
-- Name: t_pen_denda_ajb_notaris; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pen_denda_ajb_notaris (
    t_id_ajbdenda integer NOT NULL,
    t_idspt integer,
    t_idnotaris integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_nourut_ajbdenda integer,
    t_nilai_ajbdenda double precision,
    t_iduser_buat integer,
    s_id_status_bayar integer,
    t_tglbayar_ajbdenda timestamp(0) without time zone,
    t_jmlhbayar_ajbdenda double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pen_denda_ajb_notaris OWNER TO postgres;

--
-- Name: t_pen_denda_ajb_notaris_t_id_ajbdenda_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pen_denda_ajb_notaris_t_id_ajbdenda_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pen_denda_ajb_notaris_t_id_ajbdenda_seq OWNER TO postgres;

--
-- Name: t_pen_denda_ajb_notaris_t_id_ajbdenda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pen_denda_ajb_notaris_t_id_ajbdenda_seq OWNED BY public.t_pen_denda_ajb_notaris.t_id_ajbdenda;


--
-- Name: t_pen_denda_lpor_notaris; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_pen_denda_lpor_notaris (
    t_id_pendenda integer NOT NULL,
    t_idlaporbulanan integer,
    t_idnotaris integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_nourut_pendenda integer,
    t_nilai_pendenda double precision,
    t_iduser_buat integer,
    s_id_status_bayar integer,
    t_tglbayar_pendenda timestamp(0) without time zone,
    t_jmlhbayar_pendenda double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pen_denda_lpor_notaris OWNER TO postgres;

--
-- Name: t_pen_denda_lpor_notaris_t_id_pendenda_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_pen_denda_lpor_notaris_t_id_pendenda_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pen_denda_lpor_notaris_t_id_pendenda_seq OWNER TO postgres;

--
-- Name: t_pen_denda_lpor_notaris_t_id_pendenda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_pen_denda_lpor_notaris_t_id_pendenda_seq OWNED BY public.t_pen_denda_lpor_notaris.t_id_pendenda;


--
-- Name: t_skpdkb; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_skpdkb (
    t_idskpdkb integer NOT NULL,
    t_idspt integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    t_iduser_buat integer,
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_luastanah_skpdkb double precision,
    t_njoptanah_skpdkb double precision,
    t_totalnjoptanah_skpdkb double precision,
    t_luasbangunan_skpdkb double precision,
    t_njopbangunan_skpdkb double precision,
    t_totalnjopbangunan_skpdkb double precision,
    t_grandtotalnjop_skpdkb double precision,
    t_nilaitransaksispt_skpdkb double precision,
    t_trf_aphb_kali_skpdkb double precision,
    t_trf_aphb_bagi_skpdkb double precision,
    t_permeter_tanah_skpdkb double precision,
    t_idtarifbphtb_skpdkb integer,
    t_persenbphtb_skpdkb double precision,
    t_npop_bphtb_skpdkb double precision,
    t_npoptkp_bphtb_skpdkb double precision,
    t_npopkp_bphtb_skpdkb double precision,
    t_nilai_bayar_sblumnya double precision,
    t_nilai_pokok_skpdkb double precision,
    t_jmlh_blndenda double precision,
    t_jmlh_dendaskpdkb double precision,
    t_jmlh_totalskpdkb double precision,
    t_tglbuat_kodebyar timestamp(0) without time zone,
    t_nourut_kodebayar integer,
    t_kodebayar_skpdkb character varying(255),
    t_tgljatuhtempo_skpdkb timestamp(0) without time zone,
    t_idjenisketetapan integer,
    t_idpersetujuan_skpdkb integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_skpdkb OWNER TO postgres;

--
-- Name: t_skpdkb_t_idskpdkb_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_skpdkb_t_idskpdkb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_skpdkb_t_idskpdkb_seq OWNER TO postgres;

--
-- Name: t_skpdkb_t_idskpdkb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_skpdkb_t_idskpdkb_seq OWNED BY public.t_skpdkb.t_idskpdkb;


--
-- Name: t_skpdkbt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_skpdkbt (
    t_idskpdkbt integer NOT NULL,
    t_idspt integer,
    t_idskpdkb integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    t_iduser_buat integer,
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_luastanah_skpdkbt double precision,
    t_njoptanah_skpdkbt double precision,
    t_totalnjoptanah_skpdkbt double precision,
    t_luasbangunan_skpdkbt double precision,
    t_njopbangunan_skpdkbt double precision,
    t_totalnjopbangunan_skpdkbt double precision,
    t_grandtotalnjop_skpdkbt double precision,
    t_nilaitransaksispt_skpdkbt double precision,
    t_trf_aphb_kali_skpdkbt double precision,
    t_trf_aphb_bagi_skpdkbt double precision,
    t_permeter_tanah_skpdkbt double precision,
    t_idtarifbphtb_skpdkbt integer,
    t_persenbphtb_skpdkbt double precision,
    t_npop_bphtb_skpdkbt double precision,
    t_npoptkp_bphtb_skpdkbt double precision,
    t_npopkp_bphtb_skpdkbt double precision,
    t_nilai_bayar_sblumnya double precision,
    t_nilai_pokok_skpdkbt double precision,
    t_jmlh_blndenda double precision,
    t_jmlh_dendaskpdkbt double precision,
    t_jmlh_totalskpdkbt double precision,
    t_tglbuat_kodebyar timestamp(0) without time zone,
    t_nourut_kodebayar integer,
    t_kodebayar_skpdkbt character varying(255),
    t_tgljatuhtempo_skpdkbt timestamp(0) without time zone,
    t_idjenisketetapan integer,
    t_idpersetujuan_skpdkb integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_skpdkbt OWNER TO postgres;

--
-- Name: t_skpdkbt_t_idskpdkbt_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_skpdkbt_t_idskpdkbt_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_skpdkbt_t_idskpdkbt_seq OWNER TO postgres;

--
-- Name: t_skpdkbt_t_idskpdkbt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_skpdkbt_t_idskpdkbt_seq OWNED BY public.t_skpdkbt.t_idskpdkbt;


--
-- Name: t_spt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_spt (
    t_idspt integer NOT NULL,
    t_uuidspt character varying(255),
    t_kohirspt integer,
    t_tgldaftar_spt timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    t_periodespt integer,
    t_iduser_buat integer,
    t_idjenistransaksi integer,
    t_idnotaris_spt integer,
    t_npwpd character varying(255),
    t_idstatus_npwpd integer,
    t_idbidang_usaha integer,
    t_nama_pembeli character varying(255),
    t_nik_pembeli character varying(255),
    t_npwp_pembeli character varying(255),
    t_jalan_pembeli text,
    t_kabkota_pembeli character varying(255),
    t_idkec_pembeli integer,
    t_namakecamatan_pembeli character varying(255),
    t_idkel_pembeli integer,
    t_namakelurahan_pembeli character varying(255),
    t_rt_pembeli character varying(255),
    t_rw_pembeli character varying(255),
    t_nohp_pembeli character varying(255),
    t_notelp_pembeli character varying(255),
    t_email_pembeli character varying(255),
    t_kodepos_pembeli character varying(255),
    t_siup_pembeli character varying(255),
    t_nib_pembeli character varying(255),
    t_ketdomisili_pembeli character varying(255),
    t_b_nama_pngjwb_pembeli character varying(255),
    t_b_nik_pngjwb_pembeli character varying(255),
    t_b_npwp_pngjwb_pembeli character varying(255),
    t_b_statusjab_pngjwb_pembeli character varying(255),
    t_b_jalan_pngjwb_pembeli text,
    t_b_kabkota_pngjwb_pembeli character varying(255),
    t_b_idkec_pngjwb_pembeli integer,
    t_b_namakec_pngjwb_pembeli character varying(255),
    t_b_idkel_pngjwb_pembeli integer,
    t_b_namakel_pngjwb_pembeli character varying(255),
    t_b_rt_pngjwb_pembeli character varying(255),
    t_b_rw_pngjwb_pembeli character varying(255),
    t_b_nohp_pngjwb_pembeli character varying(255),
    t_b_notelp_pngjwb_pembeli character varying(255),
    t_b_email_pngjwb_pembeli character varying(255),
    t_b_kodepos_pngjwb_pembeli character varying(255),
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_nama_sppt character varying(255),
    t_jalan_sppt character varying(255),
    t_kabkota_sppt character varying(255),
    t_kecamatan_sppt character varying(255),
    t_kelurahan_sppt character varying(255),
    t_rt_sppt character varying(255),
    t_rw_sppt character varying(255),
    t_luastanah_sismiop double precision,
    t_luasbangunan_sismiop double precision,
    t_njoptanah_sismiop double precision,
    t_njopbangunan_sismiop double precision,
    t_luastanah double precision,
    t_njoptanah double precision,
    t_totalnjoptanah double precision,
    t_luasbangunan double precision,
    t_njopbangunan double precision,
    t_totalnjopbangunan double precision,
    t_grandtotalnjop double precision,
    t_nilaitransaksispt double precision,
    t_tarif_pembagian_aphb_kali double precision,
    t_tarif_pembagian_aphb_bagi double precision,
    t_permeter_tanah double precision,
    t_idjenisfasilitas integer,
    t_idjenishaktanah integer,
    t_idjenisdoktanah integer,
    t_idpengurangan integer,
    t_id_jenistanah integer,
    t_nosertifikathaktanah character varying(255),
    t_tgldok_tanah timestamp(0) without time zone,
    s_latitude character varying(255),
    s_longitude character varying(255),
    t_idtarifbphtb integer,
    t_persenbphtb double precision,
    t_npop_bphtb double precision,
    t_npoptkp_bphtb double precision,
    t_npopkp_bphtb double precision,
    t_nilai_bphtb_fix double precision,
    t_nilai_bphtb_real double precision,
    t_idbidang_penjual integer,
    t_nama_penjual character varying(255),
    t_nik_penjual character varying(255),
    t_npwp_penjual character varying(255),
    t_jalan_penjual text,
    t_kabkota_penjual character varying(255),
    t_idkec_penjual integer,
    t_namakec_penjual character varying(255),
    t_idkel_penjual integer,
    t_namakel_penjual character varying(255),
    t_rt_penjual character varying(255),
    t_rw_penjual character varying(255),
    t_nohp_penjual character varying(255),
    t_notelp_penjual character varying(255),
    t_email_penjual character varying(255),
    t_kodepos_penjual character varying(255),
    t_siup_penjual character varying(255),
    t_nib_penjual character varying(255),
    t_ketdomisili_penjual character varying(255),
    t_b_nama_pngjwb_penjual character varying(255),
    t_b_nik_pngjwb_penjual character varying(255),
    t_b_npwp_pngjwb_penjual character varying(255),
    t_b_statusjab_pngjwb_penjual character varying(255),
    t_b_jalan_pngjwb_penjual text,
    t_b_kabkota_pngjwb_penjual character varying(255),
    t_b_idkec_pngjwb_penjual integer,
    t_b_namakec_pngjwb_penjual character varying(255),
    t_b_idkel_pngjwb_penjual integer,
    t_b_namakel_pngjwb_penjual character varying(255),
    t_b_rt_pngjwb_penjual character varying(255),
    t_b_rw_pngjwb_penjual character varying(255),
    t_b_nohp_pngjwb_penjual character varying(255),
    t_b_notelp_pngjwb_penjual character varying(255),
    t_b_email_pngjwb_penjual character varying(255),
    t_b_kodepos_pngjwb_penjual character varying(255),
    t_tglbuat_kodebyar timestamp(0) without time zone,
    t_nourut_kodebayar integer,
    t_kodebayar_bphtb character varying(255),
    t_tglketetapan_spt timestamp(0) without time zone,
    t_tgljatuhtempo_spt timestamp(0) without time zone,
    t_idjenisketetapan integer,
    t_idpersetujuan_bphtb integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    t_ntpd character varying(255)
);


ALTER TABLE public.t_spt OWNER TO postgres;

--
-- Name: t_spt_t_idspt_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_spt_t_idspt_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_spt_t_idspt_seq OWNER TO postgres;

--
-- Name: t_spt_t_idspt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_spt_t_idspt_seq OWNED BY public.t_spt.t_idspt;


--
-- Name: t_validasi_berkas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_validasi_berkas (
    t_id_validasi_berkas integer NOT NULL,
    t_idspt integer,
    s_id_status_berkas integer,
    t_tglvalidasi timestamp(0) without time zone,
    t_iduser_validasi integer,
    t_keterangan_berkas text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    t_persyaratan_validasi_berkas character varying(255)
);


ALTER TABLE public.t_validasi_berkas OWNER TO postgres;

--
-- Name: t_validasi_berkas_t_id_validasi_berkas_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_validasi_berkas_t_id_validasi_berkas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_validasi_berkas_t_id_validasi_berkas_seq OWNER TO postgres;

--
-- Name: t_validasi_berkas_t_id_validasi_berkas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_validasi_berkas_t_id_validasi_berkas_seq OWNED BY public.t_validasi_berkas.t_id_validasi_berkas;


--
-- Name: t_validasi_kabid; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.t_validasi_kabid (
    t_id_validasi_kabid integer NOT NULL,
    t_idspt integer,
    s_id_status_kabid integer,
    t_tglvalidasi timestamp(0) without time zone,
    t_iduser_validasi integer,
    t_keterangan_kabid text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_validasi_kabid OWNER TO postgres;

--
-- Name: t_validasi_kabid_t_id_validasi_kabid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.t_validasi_kabid_t_id_validasi_kabid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_validasi_kabid_t_id_validasi_kabid_seq OWNER TO postgres;

--
-- Name: t_validasi_kabid_t_id_validasi_kabid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.t_validasi_kabid_t_id_validasi_kabid_seq OWNED BY public.t_validasi_kabid.t_id_validasi_kabid;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    username character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    telegram_chat_id character varying(255),
    s_gambar character varying(255),
    s_tipe_pejabat integer,
    s_idpejabat integer,
    s_idnotaris integer,
    s_id_hakakses integer,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: activity_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity_log ALTER COLUMN id SET DEFAULT nextval('public.activity_log_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: his_getbphtb_bpn t_idhis_getbpn; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.his_getbphtb_bpn ALTER COLUMN t_idhis_getbpn SET DEFAULT nextval('public.his_getbphtb_bpn_t_idhis_getbpn_seq'::regclass);


--
-- Name: his_getpbb_bpn t_idhis_pbbbpn; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.his_getpbb_bpn ALTER COLUMN t_idhis_pbbbpn SET DEFAULT nextval('public.his_getpbb_bpn_t_idhis_pbbbpn_seq'::regclass);


--
-- Name: his_postdatabpn t_idhis_posbpn; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.his_postdatabpn ALTER COLUMN t_idhis_posbpn SET DEFAULT nextval('public.his_postdatabpn_t_idhis_posbpn_seq'::regclass);


--
-- Name: integrasi_sertifikat_bpn t_idsertifikat; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.integrasi_sertifikat_bpn ALTER COLUMN t_idsertifikat SET DEFAULT nextval('public.integrasi_sertifikat_bpn_t_idsertifikat_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: notifications t_id_notif; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notifications ALTER COLUMN t_id_notif SET DEFAULT nextval('public.notifications_t_id_notif_seq'::regclass);


--
-- Name: notifications_message t_id_notif_detail; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notifications_message ALTER COLUMN t_id_notif_detail SET DEFAULT nextval('public.notifications_message_t_id_notif_detail_seq'::regclass);


--
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: s_acuan s_idacuan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_acuan ALTER COLUMN s_idacuan SET DEFAULT nextval('public.s_acuan_s_idacuan_seq'::regclass);


--
-- Name: s_acuan_jenistanah s_idacuan_jenis; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_acuan_jenistanah ALTER COLUMN s_idacuan_jenis SET DEFAULT nextval('public.s_acuan_jenistanah_s_idacuan_jenis_seq'::regclass);


--
-- Name: s_background id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_background ALTER COLUMN id SET DEFAULT nextval('public.s_background_id_seq'::regclass);


--
-- Name: s_bpn_ws id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_bpn_ws ALTER COLUMN id SET DEFAULT nextval('public.s_bpn_ws_id_seq'::regclass);


--
-- Name: s_hak_akses s_id_hakakses; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_hak_akses ALTER COLUMN s_id_hakakses SET DEFAULT nextval('public.s_hak_akses_s_id_hakakses_seq'::regclass);


--
-- Name: s_harga_history s_idhistory; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_harga_history ALTER COLUMN s_idhistory SET DEFAULT nextval('public.s_harga_history_s_idhistory_seq'::regclass);


--
-- Name: s_jenis_bidangusaha s_idbidang_usaha; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenis_bidangusaha ALTER COLUMN s_idbidang_usaha SET DEFAULT nextval('public.s_jenis_bidangusaha_s_idbidang_usaha_seq'::regclass);


--
-- Name: s_jenisdoktanah s_iddoktanah; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenisdoktanah ALTER COLUMN s_iddoktanah SET DEFAULT nextval('public.s_jenisdoktanah_s_iddoktanah_seq'::regclass);


--
-- Name: s_jenisfasilitas s_idjenisfasilitas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenisfasilitas ALTER COLUMN s_idjenisfasilitas SET DEFAULT nextval('public.s_jenisfasilitas_s_idjenisfasilitas_seq'::regclass);


--
-- Name: s_jenishaktanah s_idhaktanah; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenishaktanah ALTER COLUMN s_idhaktanah SET DEFAULT nextval('public.s_jenishaktanah_s_idhaktanah_seq'::regclass);


--
-- Name: s_jenisketetapan s_idjenisketetapan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenisketetapan ALTER COLUMN s_idjenisketetapan SET DEFAULT nextval('public.s_jenisketetapan_s_idjenisketetapan_seq'::regclass);


--
-- Name: s_jenispengurangan s_idpengurangan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenispengurangan ALTER COLUMN s_idpengurangan SET DEFAULT nextval('public.s_jenispengurangan_s_idpengurangan_seq'::regclass);


--
-- Name: s_jenistanah id_jenistanah; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenistanah ALTER COLUMN id_jenistanah SET DEFAULT nextval('public.s_jenistanah_id_jenistanah_seq'::regclass);


--
-- Name: s_jenistransaksi s_idjenistransaksi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenistransaksi ALTER COLUMN s_idjenistransaksi SET DEFAULT nextval('public.s_jenistransaksi_s_idjenistransaksi_seq'::regclass);


--
-- Name: s_kecamatan s_idkecamatan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_kecamatan ALTER COLUMN s_idkecamatan SET DEFAULT nextval('public.s_kecamatan_s_idkecamatan_seq'::regclass);


--
-- Name: s_kelurahan s_idkelurahan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_kelurahan ALTER COLUMN s_idkelurahan SET DEFAULT nextval('public.s_kelurahan_s_idkelurahan_seq'::regclass);


--
-- Name: s_koderekening s_korekid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_koderekening ALTER COLUMN s_korekid SET DEFAULT nextval('public.s_koderekening_s_korekid_seq'::regclass);


--
-- Name: s_notaris s_idnotaris; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_notaris ALTER COLUMN s_idnotaris SET DEFAULT nextval('public.s_notaris_s_idnotaris_seq'::regclass);


--
-- Name: s_pejabat s_idpejabat; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_pejabat ALTER COLUMN s_idpejabat SET DEFAULT nextval('public.s_pejabat_s_idpejabat_seq'::regclass);


--
-- Name: s_pemda s_idpemda; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_pemda ALTER COLUMN s_idpemda SET DEFAULT nextval('public.s_pemda_s_idpemda_seq'::regclass);


--
-- Name: s_persyaratan s_idpersyaratan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_persyaratan ALTER COLUMN s_idpersyaratan SET DEFAULT nextval('public.s_persyaratan_s_idpersyaratan_seq'::regclass);


--
-- Name: s_presentase_wajar s_idpresentase_wajar; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_presentase_wajar ALTER COLUMN s_idpresentase_wajar SET DEFAULT nextval('public.s_presentase_wajar_s_idpresentase_wajar_seq'::regclass);


--
-- Name: s_status s_id_status; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status ALTER COLUMN s_id_status SET DEFAULT nextval('public.s_status_s_id_status_seq'::regclass);


--
-- Name: s_status_bayar s_id_status_bayar; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_bayar ALTER COLUMN s_id_status_bayar SET DEFAULT nextval('public.s_status_bayar_s_id_status_bayar_seq'::regclass);


--
-- Name: s_status_berkas s_id_status_berkas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_berkas ALTER COLUMN s_id_status_berkas SET DEFAULT nextval('public.s_status_berkas_s_id_status_berkas_seq'::regclass);


--
-- Name: s_status_dptnpoptkp s_id_dptnpoptkp; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_dptnpoptkp ALTER COLUMN s_id_dptnpoptkp SET DEFAULT nextval('public.s_status_dptnpoptkp_s_id_dptnpoptkp_seq'::regclass);


--
-- Name: s_status_kabid s_id_status_kabid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_kabid ALTER COLUMN s_id_status_kabid SET DEFAULT nextval('public.s_status_kabid_s_id_status_kabid_seq'::regclass);


--
-- Name: s_status_lihat s_id_status_lihat; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_lihat ALTER COLUMN s_id_status_lihat SET DEFAULT nextval('public.s_status_lihat_s_id_status_lihat_seq'::regclass);


--
-- Name: s_status_npwpd s_idstatus_npwpd; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_npwpd ALTER COLUMN s_idstatus_npwpd SET DEFAULT nextval('public.s_status_npwpd_s_idstatus_npwpd_seq'::regclass);


--
-- Name: s_status_pelayanan s_id_status_layanan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_pelayanan ALTER COLUMN s_id_status_layanan SET DEFAULT nextval('public.s_status_pelayanan_s_id_status_layanan_seq'::regclass);


--
-- Name: s_status_perhitungan s_idstatus_pht; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_perhitungan ALTER COLUMN s_idstatus_pht SET DEFAULT nextval('public.s_status_perhitungan_s_idstatus_pht_seq'::regclass);


--
-- Name: s_target_bphtb s_id_target_bphtb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_target_bphtb ALTER COLUMN s_id_target_bphtb SET DEFAULT nextval('public.s_target_bphtb_s_id_target_bphtb_seq'::regclass);


--
-- Name: s_target_status s_id_target_status; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_target_status ALTER COLUMN s_id_target_status SET DEFAULT nextval('public.s_target_status_s_id_target_status_seq'::regclass);


--
-- Name: s_tarif_bphtb s_idtarifbphtb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_tarif_bphtb ALTER COLUMN s_idtarifbphtb SET DEFAULT nextval('public.s_tarif_bphtb_s_idtarifbphtb_seq'::regclass);


--
-- Name: s_tarifnpoptkp s_idtarifnpoptkp; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_tarifnpoptkp ALTER COLUMN s_idtarifnpoptkp SET DEFAULT nextval('public.s_tarifnpoptkp_s_idtarifnpoptkp_seq'::regclass);


--
-- Name: s_tempo s_idtempo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_tempo ALTER COLUMN s_idtempo SET DEFAULT nextval('public.s_tempo_s_idtempo_seq'::regclass);


--
-- Name: s_users_api s_idusers_api; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_users_api ALTER COLUMN s_idusers_api SET DEFAULT nextval('public.s_users_api_s_idusers_api_seq'::regclass);


--
-- Name: s_wajib_up s_idwajib_up; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_wajib_up ALTER COLUMN s_idwajib_up SET DEFAULT nextval('public.s_wajib_up_s_idwajib_up_seq'::regclass);


--
-- Name: t_bpn t_idbpn; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_bpn ALTER COLUMN t_idbpn SET DEFAULT nextval('public.t_bpn_t_idbpn_seq'::regclass);


--
-- Name: t_file_keringanan t_idfile_keringanan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_file_keringanan ALTER COLUMN t_idfile_keringanan SET DEFAULT nextval('public.t_file_keringanan_t_idfile_keringanan_seq'::regclass);


--
-- Name: t_file_objek t_idfile_objek; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_file_objek ALTER COLUMN t_idfile_objek SET DEFAULT nextval('public.t_file_objek_t_idfile_objek_seq'::regclass);


--
-- Name: t_filesyarat_bphtb t_id_filesyarat; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_filesyarat_bphtb ALTER COLUMN t_id_filesyarat SET DEFAULT nextval('public.t_filesyarat_bphtb_t_id_filesyarat_seq'::regclass);


--
-- Name: t_inputajb t_idajb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_inputajb ALTER COLUMN t_idajb SET DEFAULT nextval('public.t_inputajb_t_idajb_seq'::regclass);


--
-- Name: t_laporbulanan_head t_idlaporbulanan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_laporbulanan_head ALTER COLUMN t_idlaporbulanan SET DEFAULT nextval('public.t_laporbulanan_head_t_idlaporbulanan_seq'::regclass);


--
-- Name: t_nik_bersama t_id_nik_bersama; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_nik_bersama ALTER COLUMN t_id_nik_bersama SET DEFAULT nextval('public.t_nik_bersama_t_id_nik_bersama_seq'::regclass);


--
-- Name: t_notif_validasi_berkas t_id_notif_berkas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_berkas ALTER COLUMN t_id_notif_berkas SET DEFAULT nextval('public.t_notif_validasi_berkas_t_id_notif_berkas_seq'::regclass);


--
-- Name: t_notif_validasi_kabid t_id_notif_kabid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_kabid ALTER COLUMN t_id_notif_kabid SET DEFAULT nextval('public.t_notif_validasi_kabid_t_id_notif_kabid_seq'::regclass);


--
-- Name: t_pelayanan_angsuran t_idangsuran; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_angsuran ALTER COLUMN t_idangsuran SET DEFAULT nextval('public.t_pelayanan_angsuran_t_idangsuran_seq'::regclass);


--
-- Name: t_pelayanan_keberatan t_idkeberatan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keberatan ALTER COLUMN t_idkeberatan SET DEFAULT nextval('public.t_pelayanan_keberatan_t_idkeberatan_seq'::regclass);


--
-- Name: t_pelayanan_keringanan t_idkeringanan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keringanan ALTER COLUMN t_idkeringanan SET DEFAULT nextval('public.t_pelayanan_keringanan_t_idkeringanan_seq'::regclass);


--
-- Name: t_pelayanan_mutasi t_idmutasi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_mutasi ALTER COLUMN t_idmutasi SET DEFAULT nextval('public.t_pelayanan_mutasi_t_idmutasi_seq'::regclass);


--
-- Name: t_pelayanan_pembatalan t_idpembatalan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan ALTER COLUMN t_idpembatalan SET DEFAULT nextval('public.t_pelayanan_pembatalan_t_idpembatalan_seq'::regclass);


--
-- Name: t_pelayanan_penundaan t_idpenundaan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_penundaan ALTER COLUMN t_idpenundaan SET DEFAULT nextval('public.t_pelayanan_penundaan_t_idpenundaan_seq'::regclass);


--
-- Name: t_pembayaran_bphtb t_id_pembayaran; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_bphtb ALTER COLUMN t_id_pembayaran SET DEFAULT nextval('public.t_pembayaran_bphtb_t_id_pembayaran_seq'::regclass);


--
-- Name: t_pembayaran_skpdkb t_id_bayar_skpdkb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb ALTER COLUMN t_id_bayar_skpdkb SET DEFAULT nextval('public.t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq'::regclass);


--
-- Name: t_pembayaran_skpdkbt t_id_bayar_skpdkb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt ALTER COLUMN t_id_bayar_skpdkb SET DEFAULT nextval('public.t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq'::regclass);


--
-- Name: t_pemeriksaan t_idpemeriksa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pemeriksaan ALTER COLUMN t_idpemeriksa SET DEFAULT nextval('public.t_pemeriksaan_t_idpemeriksa_seq'::regclass);


--
-- Name: t_pen_denda_ajb_notaris t_id_ajbdenda; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris ALTER COLUMN t_id_ajbdenda SET DEFAULT nextval('public.t_pen_denda_ajb_notaris_t_id_ajbdenda_seq'::regclass);


--
-- Name: t_pen_denda_lpor_notaris t_id_pendenda; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris ALTER COLUMN t_id_pendenda SET DEFAULT nextval('public.t_pen_denda_lpor_notaris_t_id_pendenda_seq'::regclass);


--
-- Name: t_skpdkb t_idskpdkb; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkb ALTER COLUMN t_idskpdkb SET DEFAULT nextval('public.t_skpdkb_t_idskpdkb_seq'::regclass);


--
-- Name: t_skpdkbt t_idskpdkbt; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkbt ALTER COLUMN t_idskpdkbt SET DEFAULT nextval('public.t_skpdkbt_t_idskpdkbt_seq'::regclass);


--
-- Name: t_spt t_idspt; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt ALTER COLUMN t_idspt SET DEFAULT nextval('public.t_spt_t_idspt_seq'::regclass);


--
-- Name: t_validasi_berkas t_id_validasi_berkas; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_validasi_berkas ALTER COLUMN t_id_validasi_berkas SET DEFAULT nextval('public.t_validasi_berkas_t_id_validasi_berkas_seq'::regclass);


--
-- Name: t_validasi_kabid t_id_validasi_kabid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_validasi_kabid ALTER COLUMN t_id_validasi_kabid SET DEFAULT nextval('public.t_validasi_kabid_t_id_validasi_kabid_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: activity_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.activity_log (id, log_name, description, subject_type, subject_id, causer_type, causer_id, properties, created_at, updated_at) FROM stdin;
1	default	updated	App\\Models\\Setting\\Pejabat	1	App\\Models\\User	1	{"attributes":{"s_namapejabat":"JEVERMON MALO, SE","s_jabatanpejabat":"Kabid Pendapatan","s_nippejabat":"196801202002121003","s_golonganpejabat":"Pembina \\/IVa","s_filettd":"","s_alamat":"-"},"old":{"s_namapejabat":"Rafiansyah, SE, M.Si","s_jabatanpejabat":"Kasubbid Pendataan, Pendaftaran, Penetapan dan Penilaian PBB dan BPHTB","s_nippejabat":"19730709 199303 1 005","s_golonganpejabat":"Penata Tingkat I - III D","s_filettd":"","s_alamat":"Jalan raya no 1 Kel. Serasan Jaya Kec. Sekayu"}}	2021-09-09 11:55:59	2021-09-09 11:55:59
2	default	updated	App\\Models\\Setting\\Pejabat	2	App\\Models\\User	1	{"attributes":{"s_namapejabat":"IMELDA V. L. LESAWENGEN, SE","s_jabatanpejabat":"Kasubid Penetapan, Keberatan dan Penyelesaian Tuunggakan","s_nippejabat":"198002162009032001","s_golonganpejabat":"Penata, III\\/c","s_filettd":"","s_alamat":"-"},"old":{"s_namapejabat":"H. RIKI JUNAIDI, AP, M. Si","s_jabatanpejabat":"Kepala Badan Pengelola Pajak dan Retribusi Daerah","s_nippejabat":"19740615 199311 1 001","s_golonganpejabat":"Pembina Utama Muda - IV C","s_filettd":"","s_alamat":"Jalan melati no 3 Kel. Serasan Jaya Kec. Sekayu"}}	2021-09-09 12:00:00	2021-09-09 12:00:00
3	default	created	App\\Models\\Setting\\Pejabat	3	App\\Models\\User	1	{"attributes":{"s_namapejabat":"BENYAMIN MANDIANGAN A. m.Ak","s_jabatanpejabat":"Penata, III\\/c","s_nippejabat":"197604292002121005","s_golonganpejabat":"Kasubid Pendataan, Pendaftaran dan Penilaian","s_filettd":null,"s_alamat":"-"}}	2021-09-09 12:01:16	2021-09-09 12:01:16
4	default	updated	App\\Models\\Setting\\Pejabat	3	App\\Models\\User	1	{"attributes":{"s_namapejabat":"BENYAMIN MANDIANGAN A. m.Ak","s_jabatanpejabat":"Kasubid Pendataan, Pendaftaran dan Penilaian","s_nippejabat":"197604292002121005","s_golonganpejabat":"Penata, III\\/c","s_filettd":null,"s_alamat":"-"},"old":{"s_namapejabat":"BENYAMIN MANDIANGAN A. m.Ak","s_jabatanpejabat":"Penata, III\\/c","s_nippejabat":"197604292002121005","s_golonganpejabat":"Kasubid Pendataan, Pendaftaran dan Penilaian","s_filettd":null,"s_alamat":"-"}}	2021-09-09 12:01:51	2021-09-09 12:01:51
5	default	created	App\\Models\\Setting\\Pejabat	4	App\\Models\\User	1	{"attributes":{"s_namapejabat":"YANSYE ERLINA MANUMPIL","s_jabatanpejabat":"Staf Pelaksana","s_nippejabat":"197501012000032009","s_golonganpejabat":"Penata Muda Tingkat I, III\\/b","s_filettd":null,"s_alamat":"-"}}	2021-09-09 12:03:24	2021-09-09 12:03:24
6	default	created	App\\Models\\Setting\\Pejabat	5	App\\Models\\User	1	{"attributes":{"s_namapejabat":"SINUS A RAKINAUNG","s_jabatanpejabat":"Staf Bidang Pendapatan","s_nippejabat":"196910162006041004","s_golonganpejabat":"Pengatur Tk. I, II\\/d","s_filettd":null,"s_alamat":"-"}}	2021-09-09 12:05:01	2021-09-09 12:05:01
7	default	created	App\\Models\\Setting\\Pejabat	6	App\\Models\\User	1	{"attributes":{"s_namapejabat":"WELLIAM M. MEKUTIKA","s_jabatanpejabat":"Staf Pelaksana","s_nippejabat":"197905072006041016","s_golonganpejabat":"Pengatur Muda Tingkat I, II\\/B","s_filettd":null,"s_alamat":"-"}}	2021-09-09 12:06:15	2021-09-09 12:06:15
8	default	created	App\\Models\\Setting\\Pejabat	7	App\\Models\\User	1	{"attributes":{"s_namapejabat":"ARDYANTO TALOLANG, SS","s_jabatanpejabat":"Tenaga Honorer","s_nippejabat":"000000000000000000","s_golonganpejabat":"-","s_filettd":null,"s_alamat":"-"}}	2021-09-09 12:07:19	2021-09-09 12:07:19
9	default	created	App\\Models\\User	2	App\\Models\\User	1	{"attributes":{"name":"IMELDA V. L. LESAWENGEN, SE","username":"IMELDA","email":"imelda@gmail.com","s_gambar":null}}	2021-09-09 12:13:05	2021-09-09 12:13:05
10	default	created	App\\Models\\User	3	App\\Models\\User	1	{"attributes":{"name":"BENYAMIN MANDIANGAN A. m.Ak","username":"BENYAMIN","email":"benyamin@gmail.com","s_gambar":null}}	2021-09-09 12:15:09	2021-09-09 12:15:09
11	default	created	App\\Models\\User	4	App\\Models\\User	1	{"attributes":{"name":"YANSYE ERLINA MANUMPIL","username":"YANSYE","email":"yansye@gmail.com","s_gambar":null}}	2021-09-09 12:16:22	2021-09-09 12:16:22
12	default	created	App\\Models\\User	5	App\\Models\\User	1	{"attributes":{"name":"SINUS A RAKINAUNG","username":"SINUS","email":"sinus@gmail.com","s_gambar":null}}	2021-09-09 12:18:06	2021-09-09 12:18:06
13	default	created	App\\Models\\User	7	App\\Models\\User	1	{"attributes":{"name":"WELLIAM M. MEKUTIKA","username":"WELLIAM","email":"welliam@gmail.com","s_gambar":null}}	2021-09-09 12:19:14	2021-09-09 12:19:14
14	default	created	App\\Models\\User	8	App\\Models\\User	1	{"attributes":{"name":"ARDYANTO TALOLANG, SS","username":"ARDYANTO","email":"ardyanto@gmail.com","s_gambar":null}}	2021-09-09 12:21:12	2021-09-09 12:21:12
15	default	created	App\\Models\\Spt\\Spt	22	App\\Models\\User	2	{"attributes":{"t_iduser_buat":2,"t_uuidspt":"916acd2c9dfc4f2c9c7474b26b277042","t_kohirspt":1,"t_tgldaftar_spt":"2021-09-13 01:09:16","t_tglby_system":"2021-09-13 14:10:57","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":1,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"IMELDA","t_nik_pembeli":"7123454548878710","t_npwp_pembeli":null,"t_jalan_pembeli":"JALAN MALAHASA NO. 28","t_kabkota_pembeli":"SANGIHE","t_idkec_pembeli":22,"t_namakecamatan_pembeli":"TAHUNA","t_idkel_pembeli":316,"t_namakelurahan_pembeli":"SOATALOARA I","t_rt_pembeli":"006","t_rw_pembeli":"003","t_nohp_pembeli":"081244405151","t_notelp_pembeli":null,"t_email_pembeli":null,"t_kodepos_pembeli":null,"t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-09-13 13:10:57	2021-09-13 13:10:57
16	default	created	App\\Models\\Spt\\Filesyarat	1	App\\Models\\User	2	{"attributes":{"t_idspt":22,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":2,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"IMG_20210810_151232.jpg","t_tgl_upload":"2021-09-13 13:18:42","t_keterangan_file":null,"created_at":"2021-09-13T06:18:42.000000Z","updated_at":"2021-09-13T06:18:42.000000Z"}}	2021-09-13 13:18:42	2021-09-13 13:18:42
17	default	created	App\\Models\\Spt\\Filesyarat	2	App\\Models\\User	2	{"attributes":{"t_idspt":22,"s_idpersyaratan":2,"s_idjenistransaksi":1,"t_iduser_upload":2,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"IMG_20210810_151232-284108286.jpg","t_tgl_upload":"2021-09-13 13:18:54","t_keterangan_file":null,"created_at":"2021-09-13T06:18:54.000000Z","updated_at":"2021-09-13T06:18:54.000000Z"}}	2021-09-13 13:18:54	2021-09-13 13:18:54
18	default	created	App\\Models\\Spt\\Filesyarat	3	App\\Models\\User	2	{"attributes":{"t_idspt":22,"s_idpersyaratan":3,"s_idjenistransaksi":1,"t_iduser_upload":2,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"IMG_20210810_151232-1592621721.jpg","t_tgl_upload":"2021-09-13 13:19:05","t_keterangan_file":null,"created_at":"2021-09-13T06:19:05.000000Z","updated_at":"2021-09-13T06:19:05.000000Z"}}	2021-09-13 13:19:05	2021-09-13 13:19:05
49	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	9	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-14 11:10:32","t_iduser_validasi":3,"t_keterangan_kabid":null}}	2021-09-14 11:10:32	2021-09-14 11:10:32
19	default	created	App\\Models\\Spt\\Filesyarat	4	App\\Models\\User	2	{"attributes":{"t_idspt":22,"s_idpersyaratan":4,"s_idjenistransaksi":1,"t_iduser_upload":2,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"IMG_20210810_151232-1983710395.jpg","t_tgl_upload":"2021-09-13 13:20:17","t_keterangan_file":null,"created_at":"2021-09-13T06:20:17.000000Z","updated_at":"2021-09-13T06:20:17.000000Z"}}	2021-09-13 13:20:17	2021-09-13 13:20:17
20	default	created	App\\Models\\Spt\\Filesyarat	5	App\\Models\\User	2	{"attributes":{"t_idspt":22,"s_idpersyaratan":5,"s_idjenistransaksi":1,"t_iduser_upload":2,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"IMG_20210810_151232-671230211.jpg","t_tgl_upload":"2021-09-13 13:20:25","t_keterangan_file":null,"created_at":"2021-09-13T06:20:25.000000Z","updated_at":"2021-09-13T06:20:25.000000Z"}}	2021-09-13 13:20:25	2021-09-13 13:20:25
21	default	created	App\\Models\\Spt\\Filesyarat	6	App\\Models\\User	2	{"attributes":{"t_idspt":22,"s_idpersyaratan":6,"s_idjenistransaksi":1,"t_iduser_upload":2,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"IMG_20210810_151232-1965441430.jpg","t_tgl_upload":"2021-09-13 13:20:47","t_keterangan_file":null,"created_at":"2021-09-13T06:20:47.000000Z","updated_at":"2021-09-13T06:20:47.000000Z"}}	2021-09-13 13:20:47	2021-09-13 13:20:47
22	default	created	App\\Models\\Spt\\Filesyarat	7	App\\Models\\User	2	{"attributes":{"t_idspt":22,"s_idpersyaratan":7,"s_idjenistransaksi":1,"t_iduser_upload":2,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"IMG_20210810_151232-1042812685.jpg","t_tgl_upload":"2021-09-13 13:20:55","t_keterangan_file":null,"created_at":"2021-09-13T06:20:55.000000Z","updated_at":"2021-09-13T06:20:55.000000Z"}}	2021-09-13 13:20:56	2021-09-13 13:20:56
23	default	created	App\\Models\\Spt\\Filefotoobjek	1	App\\Models\\User	2	{"attributes":{"t_idspt":22,"t_iduser_upload":2,"letak_file":"upload\\/file_fotoobjek\\/2021\\/jualbeli\\/22\\/","nama_file":"IMG_20210810_151232.jpg","t_tgl_upload":"2021-09-13 13:21:15","t_keterangan_file":null,"created_at":"2021-09-13T06:21:15.000000Z","updated_at":"2021-09-13T06:21:15.000000Z"}}	2021-09-13 13:21:15	2021-09-13 13:21:15
24	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	12	App\\Models\\User	2	{"attributes":{"t_idspt":22,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-13 13:26:06","t_iduser_validasi":2,"t_keterangan_berkas":"SUDAH","t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-13 13:26:06	2021-09-13 13:26:06
25	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	8	App\\Models\\User	2	{"attributes":{"t_idspt":22,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-13 13:27:54","t_iduser_validasi":2,"t_keterangan_kabid":null}}	2021-09-13 13:27:55	2021-09-13 13:27:55
26	default	created	App\\Models\\Skpdkb\\Skpdkb	1	App\\Models\\User	2	{"attributes":{"t_idspt":22,"t_tglpenetapan":"2021-09-13 13:31:28","t_tglby_system":"2021-09-13 13:31:28","t_iduser_buat":2,"t_nop_sppt":"71.04.080.017.003.0198.0","t_tahun_sppt":"2020","t_luastanah_skpdkb":"600","t_njoptanah_skpdkb":"36000","t_totalnjoptanah_skpdkb":"21600000","t_luasbangunan_skpdkb":"0","t_njopbangunan_skpdkb":"0","t_totalnjopbangunan_skpdkb":"0","t_grandtotalnjop_skpdkb":"21600000","t_nilaitransaksispt_skpdkb":"70000000","t_trf_aphb_kali_skpdkb":null,"t_trf_aphb_bagi_skpdkb":null,"t_permeter_tanah_skpdkb":null,"t_idtarifbphtb_skpdkb":null,"t_persenbphtb_skpdkb":"5","t_npop_bphtb_skpdkb":"70000000","t_npoptkp_bphtb_skpdkb":"60000000","t_npopkp_bphtb_skpdkb":"10000000","t_nilai_bayar_sblumnya":"0","t_nilai_pokok_skpdkb":"500000","t_jmlh_blndenda":"0","t_jmlh_dendaskpdkb":"0","t_jmlh_totalskpdkb":"500000","t_tglbuat_kodebyar":"2021-09-13 13:31:28","t_nourut_kodebayar":1,"t_kodebayar_skpdkb":"710312022100001","t_tgljatuhtempo_skpdkb":"2021-10-13 00:00:00","t_idjenisketetapan":2,"t_idpersetujuan_skpdkb":null,"isdeleted":null,"t_idoperator_deleted":null}}	2021-09-13 13:31:28	2021-09-13 13:31:28
27	default	updated	App\\Models\\User	2	App\\Models\\User	2	{"attributes":{"name":"IMELDA V. L. LESAWENGEN, SE","username":"IMELDA","email":"imelda@gmail.com","s_gambar":"upload\\/profil\\/IMG_20210810_151232.jpg"},"old":{"name":"IMELDA V. L. LESAWENGEN, SE","username":"IMELDA","email":"imelda@gmail.com","s_gambar":null}}	2021-09-13 13:39:32	2021-09-13 13:39:32
28	default	created	App\\Models\\Bpn\\HisPostDataBpn	1	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.003.0198.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-13 14:19:06	2021-09-13 14:19:06
29	default	created	App\\Models\\Bpn\\HisPostDataBpn	2	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.003.0198.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-13 14:19:44	2021-09-13 14:19:44
30	default	created	App\\Models\\Bpn\\HisPostDataBpn	3	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.003.0198.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-13 14:23:15	2021-09-13 14:23:15
31	default	created	App\\Models\\Bpn\\HisPostDataBpn	4	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.003.0198.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-13 14:45:58	2021-09-13 14:45:58
32	default	created	App\\Models\\Bpn\\HisPostDataBpn	5	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.003.0198.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-13 15:29:37	2021-09-13 15:29:37
33	default	created	App\\Models\\Bpn\\HisPostDataBpn	6	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.003.0198.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-13 15:29:42	2021-09-13 15:29:42
95	default	created	App\\Models\\Setting\\Notaris	3	App\\Models\\User	1	{"attributes":{"s_namanotaris":"CAMAT TAHUNA, RONIJAYA PASIAYE S.SoS, ME","s_alamatnotaris":"-","s_kodenotaris":"03","s_sknotaris":"-","s_statusnotaris":1}}	2021-09-20 13:58:56	2021-09-20 13:58:56
34	default	created	App\\Models\\Bpn\\HisPostDataBpn	7	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.003.0198.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-13 15:30:01	2021-09-13 15:30:01
35	default	created	App\\Models\\Bpn\\HisPostDataBpn	8	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.003.0198.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-14 09:56:03	2021-09-14 09:56:03
36	default	created	App\\Models\\Spt\\Spt	23	App\\Models\\User	3	{"attributes":{"t_iduser_buat":3,"t_uuidspt":"653d39691e6247578857054c4cb6b960","t_kohirspt":2,"t_tgldaftar_spt":"2021-09-14 10:35:03","t_tglby_system":"2021-09-14 11:42:35","t_periodespt":2021,"t_idjenistransaksi":5,"t_idnotaris_spt":2,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"FERRY PATRAS","t_nik_pembeli":"7171030106650001","t_npwp_pembeli":null,"t_jalan_pembeli":"JALAN SINGKIL II KEC. SINGKIL KOTA MANADO","t_kabkota_pembeli":"KOTA MANADO","t_idkec_pembeli":1,"t_namakecamatan_pembeli":"KEC. SINGKIL","t_idkel_pembeli":1,"t_namakelurahan_pembeli":"KEL. SINGKIL II","t_rt_pembeli":"01","t_rw_pembeli":"03","t_nohp_pembeli":"081300000007","t_notelp_pembeli":null,"t_email_pembeli":null,"t_kodepos_pembeli":null,"t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-09-14 10:42:35	2021-09-14 10:42:35
37	default	created	App\\Models\\Spt\\Filesyarat	8	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_idpersyaratan":34,"s_idjenistransaksi":5,"t_iduser_upload":3,"letak_file":"upload\\/file_syarat\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715.JPG","t_tgl_upload":"2021-09-14 10:57:36","t_keterangan_file":null,"created_at":"2021-09-14T03:57:36.000000Z","updated_at":"2021-09-14T03:57:36.000000Z"}}	2021-09-14 10:57:36	2021-09-14 10:57:36
38	default	created	App\\Models\\Spt\\Filesyarat	9	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_idpersyaratan":35,"s_idjenistransaksi":5,"t_iduser_upload":3,"letak_file":"upload\\/file_syarat\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715-1722929431.JPG","t_tgl_upload":"2021-09-14 10:57:57","t_keterangan_file":null,"created_at":"2021-09-14T03:57:57.000000Z","updated_at":"2021-09-14T03:57:57.000000Z"}}	2021-09-14 10:57:57	2021-09-14 10:57:57
39	default	created	App\\Models\\Spt\\Filesyarat	10	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_idpersyaratan":36,"s_idjenistransaksi":5,"t_iduser_upload":3,"letak_file":"upload\\/file_syarat\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715-305700366.JPG","t_tgl_upload":"2021-09-14 10:58:13","t_keterangan_file":null,"created_at":"2021-09-14T03:58:13.000000Z","updated_at":"2021-09-14T03:58:13.000000Z"}}	2021-09-14 10:58:13	2021-09-14 10:58:13
40	default	created	App\\Models\\Spt\\Filesyarat	11	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_idpersyaratan":37,"s_idjenistransaksi":5,"t_iduser_upload":3,"letak_file":"upload\\/file_syarat\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715-1124829951.JPG","t_tgl_upload":"2021-09-14 10:58:37","t_keterangan_file":null,"created_at":"2021-09-14T03:58:37.000000Z","updated_at":"2021-09-14T03:58:37.000000Z"}}	2021-09-14 10:58:37	2021-09-14 10:58:37
41	default	created	App\\Models\\Spt\\Filesyarat	12	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_idpersyaratan":38,"s_idjenistransaksi":5,"t_iduser_upload":3,"letak_file":"upload\\/file_syarat\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715-1087718567.JPG","t_tgl_upload":"2021-09-14 10:58:51","t_keterangan_file":null,"created_at":"2021-09-14T03:58:51.000000Z","updated_at":"2021-09-14T03:58:51.000000Z"}}	2021-09-14 10:58:51	2021-09-14 10:58:51
42	default	created	App\\Models\\Spt\\Filesyarat	13	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_idpersyaratan":39,"s_idjenistransaksi":5,"t_iduser_upload":3,"letak_file":"upload\\/file_syarat\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715-352832013.JPG","t_tgl_upload":"2021-09-14 10:59:07","t_keterangan_file":null,"created_at":"2021-09-14T03:59:07.000000Z","updated_at":"2021-09-14T03:59:07.000000Z"}}	2021-09-14 10:59:07	2021-09-14 10:59:07
43	default	created	App\\Models\\Spt\\Filesyarat	14	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_idpersyaratan":40,"s_idjenistransaksi":5,"t_iduser_upload":3,"letak_file":"upload\\/file_syarat\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715-698120796.JPG","t_tgl_upload":"2021-09-14 10:59:16","t_keterangan_file":null,"created_at":"2021-09-14T03:59:16.000000Z","updated_at":"2021-09-14T03:59:16.000000Z"}}	2021-09-14 10:59:16	2021-09-14 10:59:16
44	default	created	App\\Models\\Spt\\Filesyarat	15	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_idpersyaratan":41,"s_idjenistransaksi":5,"t_iduser_upload":3,"letak_file":"upload\\/file_syarat\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715-402866001.JPG","t_tgl_upload":"2021-09-14 10:59:47","t_keterangan_file":null,"created_at":"2021-09-14T03:59:47.000000Z","updated_at":"2021-09-14T03:59:47.000000Z"}}	2021-09-14 10:59:47	2021-09-14 10:59:47
45	default	created	App\\Models\\Spt\\Filesyarat	16	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_idpersyaratan":42,"s_idjenistransaksi":5,"t_iduser_upload":3,"letak_file":"upload\\/file_syarat\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715-1266154903.JPG","t_tgl_upload":"2021-09-14 10:59:54","t_keterangan_file":null,"created_at":"2021-09-14T03:59:54.000000Z","updated_at":"2021-09-14T03:59:54.000000Z"}}	2021-09-14 10:59:54	2021-09-14 10:59:54
46	default	created	App\\Models\\Spt\\Filesyarat	17	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_idpersyaratan":43,"s_idjenistransaksi":5,"t_iduser_upload":3,"letak_file":"upload\\/file_syarat\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715-1245350748.JPG","t_tgl_upload":"2021-09-14 11:00:00","t_keterangan_file":null,"created_at":"2021-09-14T04:00:00.000000Z","updated_at":"2021-09-14T04:00:00.000000Z"}}	2021-09-14 11:00:00	2021-09-14 11:00:00
47	default	created	App\\Models\\Spt\\Filefotoobjek	2	App\\Models\\User	3	{"attributes":{"t_idspt":23,"t_iduser_upload":3,"letak_file":"upload\\/file_fotoobjek\\/2021\\/waris\\/23\\/","nama_file":"Snapshot_20210715.JPG","t_tgl_upload":"2021-09-14 11:00:39","t_keterangan_file":null,"created_at":"2021-09-14T04:00:39.000000Z","updated_at":"2021-09-14T04:00:39.000000Z"}}	2021-09-14 11:00:39	2021-09-14 11:00:39
48	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	13	App\\Models\\User	3	{"attributes":{"t_idspt":23,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-14 11:07:13","t_iduser_validasi":3,"t_keterangan_berkas":"BERKAS SUDAH SESUA","t_persyaratan_validasi_berkas":"34,35,36,37,38,39,40,41,42,43"}}	2021-09-14 11:07:13	2021-09-14 11:07:13
50	default	created	App\\Models\\Skpdkb\\Skpdkb	2	App\\Models\\User	3	{"attributes":{"t_idspt":23,"t_tglpenetapan":"2021-09-14 11:18:23","t_tglby_system":"2021-09-14 11:18:23","t_iduser_buat":3,"t_nop_sppt":"71.04.080.017.003.0198.0","t_tahun_sppt":"2020","t_luastanah_skpdkb":"700","t_njoptanah_skpdkb":"36000","t_totalnjoptanah_skpdkb":"25200000","t_luasbangunan_skpdkb":"0","t_njopbangunan_skpdkb":"0","t_totalnjopbangunan_skpdkb":"0","t_grandtotalnjop_skpdkb":"25200000","t_nilaitransaksispt_skpdkb":"301000000","t_trf_aphb_kali_skpdkb":null,"t_trf_aphb_bagi_skpdkb":null,"t_permeter_tanah_skpdkb":null,"t_idtarifbphtb_skpdkb":null,"t_persenbphtb_skpdkb":"5","t_npop_bphtb_skpdkb":"301000000","t_npoptkp_bphtb_skpdkb":"300000000","t_npopkp_bphtb_skpdkb":"1000000","t_nilai_bayar_sblumnya":"0","t_nilai_pokok_skpdkb":"50000","t_jmlh_blndenda":"0","t_jmlh_dendaskpdkb":"0","t_jmlh_totalskpdkb":"50000","t_tglbuat_kodebyar":"2021-09-14 11:18:23","t_nourut_kodebayar":2,"t_kodebayar_skpdkb":"710312022100002","t_tgljatuhtempo_skpdkb":"2021-10-14 00:00:00","t_idjenisketetapan":2,"t_idpersetujuan_skpdkb":null,"isdeleted":null,"t_idoperator_deleted":null}}	2021-09-14 11:18:23	2021-09-14 11:18:23
51	default	updated	App\\Models\\User	3	App\\Models\\User	3	{"attributes":{"name":"BENYAMIN MANDIANGAN A. m.Ak","username":"BENYAMIN","email":"benyamin@gmail.com","s_gambar":"upload\\/profil\\/Snapshot_20210715.JPG"},"old":{"name":"BENYAMIN MANDIANGAN A. m.Ak","username":"BENYAMIN","email":"benyamin@gmail.com","s_gambar":null}}	2021-09-14 11:25:04	2021-09-14 11:25:04
52	default	updated	App\\Models\\User	3	App\\Models\\User	3	{"attributes":{"name":"BENYAMIN MANDIANGAN A. m.Ak","username":"BENYAMIN","email":"benyamin@gmail.com","s_gambar":"upload\\/profil\\/WhatsApp_Image_2021_07_15_at_11_03_263.jpg"},"old":{"name":"BENYAMIN MANDIANGAN A. m.Ak","username":"BENYAMIN","email":"benyamin@gmail.com","s_gambar":"upload\\/profil\\/Snapshot_20210715.JPG"}}	2021-09-14 11:27:25	2021-09-14 11:27:25
53	default	created	App\\Models\\Pelaporan\\InputAjb	1	App\\Models\\User	1	{"attributes":{"t_idspt":22,"t_tgl_ajb":"2021-09-10 00:00:00","t_no_ajb":"32422423424","t_iduser_input":1}}	2021-09-14 13:23:23	2021-09-14 13:23:23
54	default	created	App\\Models\\User	9	App\\Models\\User	1	{"attributes":{"name":"notaris","username":"notaris","email":"notaris@gmail.com","s_gambar":null}}	2021-09-14 13:28:05	2021-09-14 13:28:05
55	default	created	App\\Models\\Pelaporan\\LaporBulananHead	1	App\\Models\\User	9	{"attributes":{"t_no_lapor":1,"t_tgl_lapor":"2021-09-14 00:00:00","t_untuk_bulan":9,"t_untuk_tahun":2021,"t_keterangan":"september laporan","t_iduser_input":9}}	2021-09-14 13:29:34	2021-09-14 13:29:34
56	default	created	App\\Models\\Bpn\\HisPostDataBpn	9	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.003.0198.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-14 14:55:48	2021-09-14 14:55:48
57	default	created	App\\Models\\Spt\\Spt	24	App\\Models\\User	1	{"attributes":{"t_iduser_buat":1,"t_uuidspt":"61ed243bcf5b4d238c38afd9a4f0a479","t_kohirspt":3,"t_tgldaftar_spt":"2021-09-15 10:07:52","t_tglby_system":"2021-09-15 11:09:23","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":2,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"FERRY PATRAS","t_nik_pembeli":"7171030106650001","t_npwp_pembeli":null,"t_jalan_pembeli":"JALAN SINGKIL II KEC. SINGKIL KOTA MANADO","t_kabkota_pembeli":"KOTA MANADO","t_idkec_pembeli":14,"t_namakecamatan_pembeli":"MANGANITU SELATAN","t_idkel_pembeli":237,"t_namakelurahan_pembeli":"MAWIRA","t_rt_pembeli":"01","t_rw_pembeli":"03","t_nohp_pembeli":null,"t_notelp_pembeli":null,"t_email_pembeli":null,"t_kodepos_pembeli":null,"t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-09-15 10:09:23	2021-09-15 10:09:23
58	default	created	App\\Models\\Spt\\Spt	25	App\\Models\\User	9	{"attributes":{"t_iduser_buat":9,"t_uuidspt":"6783f7a373764ab283e2008a999e0bd6","t_kohirspt":4,"t_tgldaftar_spt":"2021-09-16 10:45:50","t_tglby_system":"2021-09-16 11:46:48","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":1,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"GUSTIMASAH","t_nik_pembeli":"2112333333333333","t_npwp_pembeli":null,"t_jalan_pembeli":"Jalan satu","t_kabkota_pembeli":"SANGIHE","t_idkec_pembeli":14,"t_namakecamatan_pembeli":"MANGANITU SELATAN","t_idkel_pembeli":256,"t_namakelurahan_pembeli":"MANGSEL","t_rt_pembeli":"002","t_rw_pembeli":"003","t_nohp_pembeli":null,"t_notelp_pembeli":null,"t_email_pembeli":null,"t_kodepos_pembeli":null,"t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-09-16 10:46:48	2021-09-16 10:46:48
59	default	created	App\\Models\\Spt\\Spt	26	App\\Models\\User	9	{"attributes":{"t_iduser_buat":9,"t_uuidspt":"b38f9f66d39a4ba9b3fe419370b4bb8e","t_kohirspt":5,"t_tgldaftar_spt":"2021-09-16 11:15:17","t_tglby_system":"2021-09-16 12:19:25","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":1,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"aci","t_nik_pembeli":"1561655555555555","t_npwp_pembeli":null,"t_jalan_pembeli":"jalan tahuna","t_kabkota_pembeli":"JAKARTA SELATAN","t_idkec_pembeli":1,"t_namakecamatan_pembeli":"TEBET","t_idkel_pembeli":1,"t_namakelurahan_pembeli":"TEBET TIMUR","t_rt_pembeli":"003","t_rw_pembeli":"02","t_nohp_pembeli":null,"t_notelp_pembeli":null,"t_email_pembeli":null,"t_kodepos_pembeli":null,"t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-09-16 11:19:25	2021-09-16 11:19:25
60	default	created	App\\Models\\Spt\\Filesyarat	18	App\\Models\\User	9	{"attributes":{"t_idspt":26,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/26\\/","nama_file":"SUKET NJOP AGUSTINA.pdf","t_tgl_upload":"2021-09-16 11:26:34","t_keterangan_file":null,"created_at":"2021-09-16T04:26:34.000000Z","updated_at":"2021-09-16T04:26:34.000000Z"}}	2021-09-16 11:26:34	2021-09-16 11:26:34
61	default	created	App\\Models\\Spt\\Filesyarat	19	App\\Models\\User	9	{"attributes":{"t_idspt":26,"s_idpersyaratan":2,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/26\\/","nama_file":"KTP Asri Merlita.pdf","t_tgl_upload":"2021-09-16 11:29:46","t_keterangan_file":null,"created_at":"2021-09-16T04:29:46.000000Z","updated_at":"2021-09-16T04:29:46.000000Z"}}	2021-09-16 11:29:46	2021-09-16 11:29:46
62	default	created	App\\Models\\Spt\\Filesyarat	20	App\\Models\\User	9	{"attributes":{"t_idspt":26,"s_idpersyaratan":3,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/26\\/","nama_file":"KUASA CEK 3 SONNY.pdf","t_tgl_upload":"2021-09-16 11:33:10","t_keterangan_file":null,"created_at":"2021-09-16T04:33:10.000000Z","updated_at":"2021-09-16T04:33:10.000000Z"}}	2021-09-16 11:33:10	2021-09-16 11:33:10
63	default	created	App\\Models\\Spt\\Filesyarat	21	App\\Models\\User	9	{"attributes":{"t_idspt":26,"s_idpersyaratan":4,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/26\\/","nama_file":"KUASA CEK MACHDY.pdf","t_tgl_upload":"2021-09-16 11:33:58","t_keterangan_file":null,"created_at":"2021-09-16T04:33:58.000000Z","updated_at":"2021-09-16T04:33:58.000000Z"}}	2021-09-16 11:33:58	2021-09-16 11:33:58
64	default	created	App\\Models\\Spt\\Filesyarat	22	App\\Models\\User	9	{"attributes":{"t_idspt":26,"s_idpersyaratan":5,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/26\\/","nama_file":"KTP JESLY.pdf","t_tgl_upload":"2021-09-16 11:34:16","t_keterangan_file":null,"created_at":"2021-09-16T04:34:16.000000Z","updated_at":"2021-09-16T04:34:16.000000Z"}}	2021-09-16 11:34:16	2021-09-16 11:34:16
65	default	created	App\\Models\\Spt\\Filesyarat	23	App\\Models\\User	9	{"attributes":{"t_idspt":26,"s_idpersyaratan":6,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/26\\/","nama_file":"KTP Asri Merlita-864814471.pdf","t_tgl_upload":"2021-09-16 11:34:32","t_keterangan_file":null,"created_at":"2021-09-16T04:34:32.000000Z","updated_at":"2021-09-16T04:34:32.000000Z"}}	2021-09-16 11:34:32	2021-09-16 11:34:32
66	default	created	App\\Models\\Spt\\Filesyarat	24	App\\Models\\User	9	{"attributes":{"t_idspt":26,"s_idpersyaratan":7,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/26\\/","nama_file":"kUASA CEK MARGARETE (2).pdf","t_tgl_upload":"2021-09-16 11:35:04","t_keterangan_file":null,"created_at":"2021-09-16T04:35:04.000000Z","updated_at":"2021-09-16T04:35:04.000000Z"}}	2021-09-16 11:35:05	2021-09-16 11:35:05
67	default	created	App\\Models\\Spt\\Filefotoobjek	3	App\\Models\\User	9	{"attributes":{"t_idspt":26,"t_iduser_upload":9,"letak_file":"upload\\/file_fotoobjek\\/2021\\/jualbeli\\/26\\/","nama_file":"kuasa cek Maya Makawata.pdf","t_tgl_upload":"2021-09-16 11:36:15","t_keterangan_file":null,"created_at":"2021-09-16T04:36:15.000000Z","updated_at":"2021-09-16T04:36:15.000000Z"}}	2021-09-16 11:36:15	2021-09-16 11:36:15
68	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	14	App\\Models\\User	1	{"attributes":{"t_idspt":26,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-16 11:48:57","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-16 11:48:57	2021-09-16 11:48:57
69	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	10	App\\Models\\User	1	{"attributes":{"t_idspt":26,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-16 11:49:10","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-16 11:49:10	2021-09-16 11:49:10
70	default	created	App\\Models\\Pelaporan\\InputAjb	2	App\\Models\\User	9	{"attributes":{"t_idspt":26,"t_tgl_ajb":"2021-09-16 00:00:00","t_no_ajb":"1","t_iduser_input":9}}	2021-09-16 11:51:48	2021-09-16 11:51:48
71	default	created	App\\Models\\Pelaporan\\LaporBulananHead	2	App\\Models\\User	9	{"attributes":{"t_no_lapor":2,"t_tgl_lapor":"2021-09-16 00:00:00","t_untuk_bulan":9,"t_untuk_tahun":2021,"t_keterangan":"lapor september","t_iduser_input":9}}	2021-09-16 11:53:17	2021-09-16 11:53:17
72	default	created	App\\Models\\Spt\\Spt	27	App\\Models\\User	9	{"attributes":{"t_iduser_buat":9,"t_uuidspt":"de11e4d4558e41c281f51276840d320f","t_kohirspt":6,"t_tgldaftar_spt":"2021-09-16 12:54:06","t_tglby_system":"2021-09-16 13:57:51","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":1,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"VERONIKA BALANTUKANG","t_nik_pembeli":"7103237009810001","t_npwp_pembeli":null,"t_jalan_pembeli":"-","t_kabkota_pembeli":"SANGIHE","t_idkec_pembeli":23,"t_namakecamatan_pembeli":"TAHUNA BARAT","t_idkel_pembeli":219,"t_namakelurahan_pembeli":"BEHA BARU","t_rt_pembeli":"000","t_rw_pembeli":"00","t_nohp_pembeli":null,"t_notelp_pembeli":null,"t_email_pembeli":null,"t_kodepos_pembeli":null,"t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-09-16 12:57:51	2021-09-16 12:57:51
73	default	created	App\\Models\\Spt\\Filesyarat	25	App\\Models\\User	9	{"attributes":{"t_idspt":27,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/27\\/","nama_file":"SPPT PBB.pdf","t_tgl_upload":"2021-09-16 13:06:43","t_keterangan_file":null,"created_at":"2021-09-16T06:06:43.000000Z","updated_at":"2021-09-16T06:06:43.000000Z"}}	2021-09-16 13:06:43	2021-09-16 13:06:43
74	default	created	App\\Models\\Spt\\Filesyarat	26	App\\Models\\User	9	{"attributes":{"t_idspt":27,"s_idpersyaratan":2,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/27\\/","nama_file":"KTP DEBITUR.pdf","t_tgl_upload":"2021-09-16 13:07:39","t_keterangan_file":null,"created_at":"2021-09-16T06:07:39.000000Z","updated_at":"2021-09-16T06:07:39.000000Z"}}	2021-09-16 13:07:39	2021-09-16 13:07:39
75	default	created	App\\Models\\Spt\\Filesyarat	27	App\\Models\\User	9	{"attributes":{"t_idspt":27,"s_idpersyaratan":3,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/27\\/","nama_file":"KTP PERSETUJUAN.pdf","t_tgl_upload":"2021-09-16 13:07:58","t_keterangan_file":null,"created_at":"2021-09-16T06:07:58.000000Z","updated_at":"2021-09-16T06:07:58.000000Z"}}	2021-09-16 13:07:58	2021-09-16 13:07:58
76	default	created	App\\Models\\Spt\\Filesyarat	28	App\\Models\\User	9	{"attributes":{"t_idspt":27,"s_idpersyaratan":4,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/27\\/","nama_file":"SURAT PERNYATAAN.pdf","t_tgl_upload":"2021-09-16 13:08:36","t_keterangan_file":null,"created_at":"2021-09-16T06:08:36.000000Z","updated_at":"2021-09-16T06:08:36.000000Z"}}	2021-09-16 13:08:36	2021-09-16 13:08:36
77	default	created	App\\Models\\Spt\\Filesyarat	29	App\\Models\\User	9	{"attributes":{"t_idspt":27,"s_idpersyaratan":5,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/27\\/","nama_file":"KK DEBITUR.pdf","t_tgl_upload":"2021-09-16 13:08:53","t_keterangan_file":null,"created_at":"2021-09-16T06:08:53.000000Z","updated_at":"2021-09-16T06:08:53.000000Z"}}	2021-09-16 13:08:53	2021-09-16 13:08:53
78	default	created	App\\Models\\Spt\\Filesyarat	30	App\\Models\\User	9	{"attributes":{"t_idspt":27,"s_idpersyaratan":6,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/27\\/","nama_file":"KTP DEBITUR-1520944845.pdf","t_tgl_upload":"2021-09-16 13:09:12","t_keterangan_file":null,"created_at":"2021-09-16T06:09:12.000000Z","updated_at":"2021-09-16T06:09:12.000000Z"}}	2021-09-16 13:09:12	2021-09-16 13:09:12
79	default	created	App\\Models\\Spt\\Filesyarat	31	App\\Models\\User	9	{"attributes":{"t_idspt":27,"s_idpersyaratan":7,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/27\\/","nama_file":"AKTA APHT.pdf","t_tgl_upload":"2021-09-16 13:09:41","t_keterangan_file":null,"created_at":"2021-09-16T06:09:41.000000Z","updated_at":"2021-09-16T06:09:41.000000Z"}}	2021-09-16 13:09:42	2021-09-16 13:09:42
80	default	created	App\\Models\\Spt\\Spt	28	App\\Models\\User	9	{"attributes":{"t_iduser_buat":9,"t_uuidspt":"9db15334c1f1431d8353a16249d29508","t_kohirspt":7,"t_tgldaftar_spt":"2021-09-16 01:56:30","t_tglby_system":"2021-09-16 14:58:46","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":1,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"ade","t_nik_pembeli":"2345677777777766","t_npwp_pembeli":null,"t_jalan_pembeli":"jalan tahuna","t_kabkota_pembeli":"SANGIHE","t_idkec_pembeli":22,"t_namakecamatan_pembeli":"TAHUNA","t_idkel_pembeli":205,"t_namakelurahan_pembeli":"BUNGALAWANG","t_rt_pembeli":"000","t_rw_pembeli":"00","t_nohp_pembeli":null,"t_notelp_pembeli":null,"t_email_pembeli":null,"t_kodepos_pembeli":null,"t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-09-16 13:58:45	2021-09-16 13:58:45
81	default	created	App\\Models\\Spt\\Filesyarat	32	App\\Models\\User	9	{"attributes":{"t_idspt":28,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/28\\/","nama_file":"E-sertifikat Webinar Erastus Lohonusa Towoliu (1).pdf","t_tgl_upload":"2021-09-16 14:02:11","t_keterangan_file":null,"created_at":"2021-09-16T07:02:11.000000Z","updated_at":"2021-09-16T07:02:11.000000Z"}}	2021-09-16 14:02:11	2021-09-16 14:02:11
82	default	created	App\\Models\\Spt\\Filesyarat	33	App\\Models\\User	9	{"attributes":{"t_idspt":28,"s_idpersyaratan":2,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/28\\/","nama_file":"E-sertifikat Webinar Erastus Lohonusa Towoliu (1)-1257890993.pdf","t_tgl_upload":"2021-09-16 14:02:57","t_keterangan_file":null,"created_at":"2021-09-16T07:02:57.000000Z","updated_at":"2021-09-16T07:02:57.000000Z"}}	2021-09-16 14:02:57	2021-09-16 14:02:57
83	default	created	App\\Models\\Spt\\Filesyarat	34	App\\Models\\User	9	{"attributes":{"t_idspt":28,"s_idpersyaratan":3,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/28\\/","nama_file":"E-sertifikat Webinar Erastus Lohonusa Towoliu (1)-584811699.pdf","t_tgl_upload":"2021-09-16 14:03:09","t_keterangan_file":null,"created_at":"2021-09-16T07:03:09.000000Z","updated_at":"2021-09-16T07:03:09.000000Z"}}	2021-09-16 14:03:09	2021-09-16 14:03:09
84	default	created	App\\Models\\Spt\\Filesyarat	35	App\\Models\\User	9	{"attributes":{"t_idspt":28,"s_idpersyaratan":4,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/28\\/","nama_file":"E-sertifikat Webinar Erastus Lohonusa Towoliu (1)-710291954.pdf","t_tgl_upload":"2021-09-16 14:03:12","t_keterangan_file":null,"created_at":"2021-09-16T07:03:12.000000Z","updated_at":"2021-09-16T07:03:12.000000Z"}}	2021-09-16 14:03:12	2021-09-16 14:03:12
85	default	created	App\\Models\\Spt\\Filesyarat	36	App\\Models\\User	9	{"attributes":{"t_idspt":28,"s_idpersyaratan":5,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/28\\/","nama_file":"E-sertifikat Webinar Erastus Lohonusa Towoliu (1)-1072539485.pdf","t_tgl_upload":"2021-09-16 14:03:13","t_keterangan_file":null,"created_at":"2021-09-16T07:03:13.000000Z","updated_at":"2021-09-16T07:03:13.000000Z"}}	2021-09-16 14:03:13	2021-09-16 14:03:13
86	default	created	App\\Models\\Spt\\Filesyarat	37	App\\Models\\User	9	{"attributes":{"t_idspt":28,"s_idpersyaratan":6,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/28\\/","nama_file":"E-sertifikat Webinar Erastus Lohonusa Towoliu (1)-1650491791.pdf","t_tgl_upload":"2021-09-16 14:03:16","t_keterangan_file":null,"created_at":"2021-09-16T07:03:16.000000Z","updated_at":"2021-09-16T07:03:16.000000Z"}}	2021-09-16 14:03:16	2021-09-16 14:03:16
87	default	created	App\\Models\\Spt\\Filesyarat	38	App\\Models\\User	9	{"attributes":{"t_idspt":28,"s_idpersyaratan":7,"s_idjenistransaksi":1,"t_iduser_upload":9,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/28\\/","nama_file":"E-sertifikat Webinar Erastus Lohonusa Towoliu (1)-2044459073.pdf","t_tgl_upload":"2021-09-16 14:03:23","t_keterangan_file":null,"created_at":"2021-09-16T07:03:23.000000Z","updated_at":"2021-09-16T07:03:23.000000Z"}}	2021-09-16 14:03:23	2021-09-16 14:03:23
88	default	created	App\\Models\\Spt\\Filefotoobjek	4	App\\Models\\User	9	{"attributes":{"t_idspt":28,"t_iduser_upload":9,"letak_file":"upload\\/file_fotoobjek\\/2021\\/jualbeli\\/28\\/","nama_file":"E-sertifikat Webinar Erastus Lohonusa Towoliu (1).pdf","t_tgl_upload":"2021-09-16 14:03:51","t_keterangan_file":null,"created_at":"2021-09-16T07:03:51.000000Z","updated_at":"2021-09-16T07:03:51.000000Z"}}	2021-09-16 14:03:51	2021-09-16 14:03:51
89	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	15	App\\Models\\User	1	{"attributes":{"t_idspt":28,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-16 14:08:02","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-16 14:08:02	2021-09-16 14:08:02
90	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	11	App\\Models\\User	1	{"attributes":{"t_idspt":28,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-16 14:08:38","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-16 14:08:38	2021-09-16 14:08:38
91	default	created	App\\Models\\Pelaporan\\InputAjb	3	App\\Models\\User	9	{"attributes":{"t_idspt":28,"t_tgl_ajb":"2021-09-15 00:00:00","t_no_ajb":"456789","t_iduser_input":9}}	2021-09-16 14:10:11	2021-09-16 14:10:11
92	default	created	App\\Models\\Pelaporan\\LaporBulananHead	3	App\\Models\\User	9	{"attributes":{"t_no_lapor":3,"t_tgl_lapor":"2021-09-16 00:00:00","t_untuk_bulan":9,"t_untuk_tahun":2021,"t_keterangan":"LAPOR SEPTEMBER AWAL","t_iduser_input":9}}	2021-09-16 14:11:11	2021-09-16 14:11:11
93	default	updated	App\\Models\\Setting\\Notaris	1	App\\Models\\User	1	{"attributes":{"s_namanotaris":"AMELIA NOVITA DANDEL, SH,M.Kn","s_alamatnotaris":"-","s_kodenotaris":"01","s_sknotaris":"-","s_statusnotaris":1},"old":{"s_namanotaris":"Gustimansah, SH, M.Kn","s_alamatnotaris":"Jalan Kolonel Wahid Udin Kel. Serasan Jaya Kec. Sekayu","s_kodenotaris":"01","s_sknotaris":"01\\/01\\/2018","s_statusnotaris":1}}	2021-09-20 13:58:07	2021-09-20 13:58:07
94	default	updated	App\\Models\\Setting\\Notaris	2	App\\Models\\User	1	{"attributes":{"s_namanotaris":"KUSTANTI, S.H.,M. Kn","s_alamatnotaris":"-","s_kodenotaris":"02","s_sknotaris":"-","s_statusnotaris":1},"old":{"s_namanotaris":"Marleni, SH, M.Kn","s_alamatnotaris":"Jalan Mawar no 2 Kel. Serasan Jaya Kec. Sekayu","s_kodenotaris":"01","s_sknotaris":"01\\/01\\/2018","s_statusnotaris":1}}	2021-09-20 13:58:31	2021-09-20 13:58:31
96	default	created	App\\Models\\Setting\\Notaris	4	App\\Models\\User	1	{"attributes":{"s_namanotaris":"CAMAT TAHUNA TIMUR, STEVEN R. ANDAKI, SP","s_alamatnotaris":"-","s_kodenotaris":"04","s_sknotaris":"-","s_statusnotaris":1}}	2021-09-20 13:59:39	2021-09-20 13:59:39
97	default	created	App\\Models\\Setting\\Notaris	5	App\\Models\\User	1	{"attributes":{"s_namanotaris":"CAMAT TABUT HASYIM SAMALAN S. SoS","s_alamatnotaris":"-","s_kodenotaris":"05","s_sknotaris":"-","s_statusnotaris":1}}	2021-09-20 14:00:11	2021-09-20 14:00:11
98	default	created	App\\Models\\Setting\\Notaris	6	App\\Models\\User	1	{"attributes":{"s_namanotaris":"BPKPD PELAYANAN","s_alamatnotaris":"-","s_kodenotaris":"06","s_sknotaris":"-","s_statusnotaris":1}}	2021-09-20 14:01:10	2021-09-20 14:01:10
99	default	created	App\\Models\\Setting\\Notaris	7	App\\Models\\User	1	{"attributes":{"s_namanotaris":"KEPALA BPN, D. D. BULAMEI","s_alamatnotaris":"-","s_kodenotaris":"06","s_sknotaris":"-","s_statusnotaris":1}}	2021-09-20 14:02:15	2021-09-20 14:02:15
100	default	created	App\\Models\\Setting\\Notaris	8	App\\Models\\User	1	{"attributes":{"s_namanotaris":"STEVENSON ANDAKI, SP","s_alamatnotaris":"-","s_kodenotaris":"07","s_sknotaris":"-","s_statusnotaris":1}}	2021-09-20 14:03:24	2021-09-20 14:03:24
101	default	created	App\\Models\\Setting\\Notaris	9	App\\Models\\User	1	{"attributes":{"s_namanotaris":"CHRESTIAN D SALILO, A.Pnth","s_alamatnotaris":"-","s_kodenotaris":"08","s_sknotaris":"-","s_statusnotaris":1}}	2021-09-20 14:04:00	2021-09-20 14:04:00
102	default	created	App\\Models\\Setting\\Notaris	10	App\\Models\\User	1	{"attributes":{"s_namanotaris":"GRIETHA E. KARAMBUT, S.STP","s_alamatnotaris":"-","s_kodenotaris":"09","s_sknotaris":"-","s_statusnotaris":1}}	2021-09-20 14:05:53	2021-09-20 14:05:53
103	default	updated	App\\Models\\User	9	App\\Models\\User	1	{"attributes":{"name":"AMELIA","username":"AMELIA","email":"amelia@gmail.com","s_gambar":null},"old":{"name":"notaris","username":"notaris","email":"notaris@gmail.com","s_gambar":null}}	2021-09-20 14:07:27	2021-09-20 14:07:27
104	default	created	App\\Models\\User	10	App\\Models\\User	1	{"attributes":{"name":"KUSTANTI, S.H.,M. Kn","username":"KUSTANTI","email":"kustanti@gmail.com","s_gambar":null}}	2021-09-20 14:08:26	2021-09-20 14:08:26
105	default	created	App\\Models\\User	11	App\\Models\\User	1	{"attributes":{"name":"RONIJAYA PASIAYE S.SoS, ME","username":"RONIJAYA","email":"ronyjaya@gmail.com","s_gambar":null}}	2021-09-20 14:09:29	2021-09-20 14:09:29
106	default	updated	App\\Models\\User	9	App\\Models\\User	1	{"attributes":{"name":"AMELIA NOVITA DANDEL, SH,M.Kn","username":"AMELIA","email":"amelia@gmail.com","s_gambar":null},"old":{"name":"AMELIA","username":"AMELIA","email":"amelia@gmail.com","s_gambar":null}}	2021-09-20 14:09:58	2021-09-20 14:09:58
107	default	created	App\\Models\\Spt\\Filesyarat	39	App\\Models\\User	1	{"attributes":{"t_idspt":81,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/81\\/","nama_file":"sangihe.png","t_tgl_upload":"2021-09-25 07:39:50","t_keterangan_file":null,"created_at":"2021-09-25T00:39:50.000000Z","updated_at":"2021-09-25T00:39:50.000000Z"}}	2021-09-25 07:39:50	2021-09-25 07:39:50
108	default	created	App\\Models\\Spt\\Filesyarat	40	App\\Models\\User	1	{"attributes":{"t_idspt":81,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/81\\/","nama_file":"garuda.png","t_tgl_upload":"2021-09-25 07:40:37","t_keterangan_file":null,"created_at":"2021-09-25T00:40:37.000000Z","updated_at":"2021-09-25T00:40:37.000000Z"}}	2021-09-25 07:40:37	2021-09-25 07:40:37
109	default	updated	App\\Models\\Setting\\Persyaratan	6	App\\Models\\User	1	{"attributes":{"s_idjenistransaksi":1,"s_namapersyaratan":"Foto copy KTP Pembeli","order":6,"s_idwajib_up":2,"s_lokasimenu":1},"old":{"s_idjenistransaksi":1,"s_namapersyaratan":"Foto copy KTP Pembeli (untuk Akta Jual Beli)","order":6,"s_idwajib_up":2,"s_lokasimenu":1}}	2021-09-25 17:36:44	2021-09-25 17:36:44
110	default	created	App\\Models\\Setting\\Persyaratan	152	App\\Models\\User	1	{"attributes":{"s_idjenistransaksi":1,"s_namapersyaratan":"Denah Objek Jual Beli","order":8,"s_idwajib_up":2,"s_lokasimenu":1}}	2021-09-26 17:47:05	2021-09-26 17:47:05
111	default	created	App\\Models\\Setting\\Persyaratan	153	App\\Models\\User	1	{"attributes":{"s_idjenistransaksi":1,"s_namapersyaratan":"Foto copy NPWP ( Pembeli )","order":9,"s_idwajib_up":2,"s_lokasimenu":1}}	2021-09-26 17:48:10	2021-09-26 17:48:10
112	default	created	App\\Models\\Setting\\Persyaratan	154	App\\Models\\User	1	{"attributes":{"s_idjenistransaksi":1,"s_namapersyaratan":"Foto copy Kartu Keluarga( Pembeli )","order":10,"s_idwajib_up":1,"s_lokasimenu":1}}	2021-09-26 17:48:47	2021-09-26 17:48:47
113	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	16	App\\Models\\User	1	{"attributes":{"t_idspt":1,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:13","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:14	2021-09-27 08:23:14
114	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	17	App\\Models\\User	1	{"attributes":{"t_idspt":2,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:14","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:15	2021-09-27 08:23:15
115	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	18	App\\Models\\User	1	{"attributes":{"t_idspt":3,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:15","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:15	2021-09-27 08:23:15
116	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	19	App\\Models\\User	1	{"attributes":{"t_idspt":4,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:15","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:15	2021-09-27 08:23:15
117	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	20	App\\Models\\User	1	{"attributes":{"t_idspt":5,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:15","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:15	2021-09-27 08:23:15
118	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	21	App\\Models\\User	1	{"attributes":{"t_idspt":6,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:15","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:15	2021-09-27 08:23:15
119	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	22	App\\Models\\User	1	{"attributes":{"t_idspt":7,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:15","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:15	2021-09-27 08:23:15
120	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	23	App\\Models\\User	1	{"attributes":{"t_idspt":8,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:15","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:15	2021-09-27 08:23:15
121	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	24	App\\Models\\User	1	{"attributes":{"t_idspt":9,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:15","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
122	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	25	App\\Models\\User	1	{"attributes":{"t_idspt":10,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
123	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	26	App\\Models\\User	1	{"attributes":{"t_idspt":11,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
124	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	27	App\\Models\\User	1	{"attributes":{"t_idspt":12,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
125	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	28	App\\Models\\User	1	{"attributes":{"t_idspt":13,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
126	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	29	App\\Models\\User	1	{"attributes":{"t_idspt":14,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
127	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	30	App\\Models\\User	1	{"attributes":{"t_idspt":15,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
128	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	31	App\\Models\\User	1	{"attributes":{"t_idspt":16,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
129	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	32	App\\Models\\User	1	{"attributes":{"t_idspt":17,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
130	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	33	App\\Models\\User	1	{"attributes":{"t_idspt":18,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
131	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	34	App\\Models\\User	1	{"attributes":{"t_idspt":19,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
132	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	35	App\\Models\\User	1	{"attributes":{"t_idspt":20,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
133	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	36	App\\Models\\User	1	{"attributes":{"t_idspt":21,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
134	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	37	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
135	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	38	App\\Models\\User	1	{"attributes":{"t_idspt":23,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:16	2021-09-27 08:23:16
136	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	39	App\\Models\\User	1	{"attributes":{"t_idspt":24,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:16","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:17	2021-09-27 08:23:17
137	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	40	App\\Models\\User	1	{"attributes":{"t_idspt":25,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:17","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:17	2021-09-27 08:23:17
138	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	41	App\\Models\\User	1	{"attributes":{"t_idspt":26,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:17","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:17	2021-09-27 08:23:17
139	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	42	App\\Models\\User	1	{"attributes":{"t_idspt":27,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:17","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:17	2021-09-27 08:23:17
140	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	43	App\\Models\\User	1	{"attributes":{"t_idspt":28,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:17","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:17	2021-09-27 08:23:17
141	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	44	App\\Models\\User	1	{"attributes":{"t_idspt":29,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:17","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:17	2021-09-27 08:23:17
142	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	45	App\\Models\\User	1	{"attributes":{"t_idspt":30,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:17","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:17	2021-09-27 08:23:17
143	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	46	App\\Models\\User	1	{"attributes":{"t_idspt":31,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:17","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:18	2021-09-27 08:23:18
144	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	47	App\\Models\\User	1	{"attributes":{"t_idspt":32,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:18","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:18	2021-09-27 08:23:18
145	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	48	App\\Models\\User	1	{"attributes":{"t_idspt":33,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:18","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:18	2021-09-27 08:23:18
146	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	49	App\\Models\\User	1	{"attributes":{"t_idspt":34,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:18","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
147	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	50	App\\Models\\User	1	{"attributes":{"t_idspt":35,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
148	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	51	App\\Models\\User	1	{"attributes":{"t_idspt":36,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
149	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	52	App\\Models\\User	1	{"attributes":{"t_idspt":37,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
150	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	53	App\\Models\\User	1	{"attributes":{"t_idspt":38,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
151	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	54	App\\Models\\User	1	{"attributes":{"t_idspt":39,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
152	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	55	App\\Models\\User	1	{"attributes":{"t_idspt":40,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
153	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	56	App\\Models\\User	1	{"attributes":{"t_idspt":41,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
154	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	57	App\\Models\\User	1	{"attributes":{"t_idspt":42,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
155	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	58	App\\Models\\User	1	{"attributes":{"t_idspt":43,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
156	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	59	App\\Models\\User	1	{"attributes":{"t_idspt":44,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
157	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	60	App\\Models\\User	1	{"attributes":{"t_idspt":45,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
158	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	61	App\\Models\\User	1	{"attributes":{"t_idspt":46,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
159	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	62	App\\Models\\User	1	{"attributes":{"t_idspt":47,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
160	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	63	App\\Models\\User	1	{"attributes":{"t_idspt":48,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:19	2021-09-27 08:23:19
161	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	64	App\\Models\\User	1	{"attributes":{"t_idspt":49,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:19","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:20	2021-09-27 08:23:20
162	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	65	App\\Models\\User	1	{"attributes":{"t_idspt":50,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:20","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:20	2021-09-27 08:23:20
163	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	66	App\\Models\\User	1	{"attributes":{"t_idspt":51,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:20","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:20	2021-09-27 08:23:20
164	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	67	App\\Models\\User	1	{"attributes":{"t_idspt":52,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:20","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:20	2021-09-27 08:23:20
165	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	68	App\\Models\\User	1	{"attributes":{"t_idspt":53,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:20","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:21	2021-09-27 08:23:21
166	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	69	App\\Models\\User	1	{"attributes":{"t_idspt":54,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:21","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:21	2021-09-27 08:23:21
167	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	70	App\\Models\\User	1	{"attributes":{"t_idspt":55,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:21","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:21	2021-09-27 08:23:21
168	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	71	App\\Models\\User	1	{"attributes":{"t_idspt":56,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:21","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:21	2021-09-27 08:23:21
169	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	72	App\\Models\\User	1	{"attributes":{"t_idspt":57,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:22","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:22	2021-09-27 08:23:22
170	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	73	App\\Models\\User	1	{"attributes":{"t_idspt":58,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:22","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:22	2021-09-27 08:23:22
171	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	74	App\\Models\\User	1	{"attributes":{"t_idspt":59,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:22","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:22	2021-09-27 08:23:22
172	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	75	App\\Models\\User	1	{"attributes":{"t_idspt":60,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:22","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:22	2021-09-27 08:23:22
173	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	76	App\\Models\\User	1	{"attributes":{"t_idspt":61,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:22","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:22	2021-09-27 08:23:22
174	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	77	App\\Models\\User	1	{"attributes":{"t_idspt":62,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:22","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:22	2021-09-27 08:23:22
175	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	78	App\\Models\\User	1	{"attributes":{"t_idspt":63,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:22","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:22	2021-09-27 08:23:22
176	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	79	App\\Models\\User	1	{"attributes":{"t_idspt":64,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:22","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:22	2021-09-27 08:23:22
177	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	80	App\\Models\\User	1	{"attributes":{"t_idspt":65,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:22","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:23	2021-09-27 08:23:23
178	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	81	App\\Models\\User	1	{"attributes":{"t_idspt":66,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:23","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:23	2021-09-27 08:23:23
179	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	82	App\\Models\\User	1	{"attributes":{"t_idspt":67,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:24","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:25	2021-09-27 08:23:25
180	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	83	App\\Models\\User	1	{"attributes":{"t_idspt":68,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:25","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:25	2021-09-27 08:23:25
181	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	84	App\\Models\\User	1	{"attributes":{"t_idspt":69,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:25","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:25	2021-09-27 08:23:25
182	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	85	App\\Models\\User	1	{"attributes":{"t_idspt":70,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:25","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:25	2021-09-27 08:23:25
183	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	86	App\\Models\\User	1	{"attributes":{"t_idspt":71,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:25","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:26	2021-09-27 08:23:26
184	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	87	App\\Models\\User	1	{"attributes":{"t_idspt":72,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:27","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:27	2021-09-27 08:23:27
185	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	88	App\\Models\\User	1	{"attributes":{"t_idspt":73,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:27","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:29	2021-09-27 08:23:29
186	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	89	App\\Models\\User	1	{"attributes":{"t_idspt":74,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:29","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:29	2021-09-27 08:23:29
187	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	90	App\\Models\\User	1	{"attributes":{"t_idspt":75,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:29","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:29	2021-09-27 08:23:29
188	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	91	App\\Models\\User	1	{"attributes":{"t_idspt":76,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:29","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:29	2021-09-27 08:23:29
189	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	92	App\\Models\\User	1	{"attributes":{"t_idspt":77,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:29","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:29	2021-09-27 08:23:29
190	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	93	App\\Models\\User	1	{"attributes":{"t_idspt":78,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:29","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:29	2021-09-27 08:23:29
191	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	94	App\\Models\\User	1	{"attributes":{"t_idspt":79,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:29","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:29	2021-09-27 08:23:29
192	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	95	App\\Models\\User	1	{"attributes":{"t_idspt":80,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:29","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:29	2021-09-27 08:23:29
193	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	96	App\\Models\\User	1	{"attributes":{"t_idspt":81,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 08:23:30","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-09-27 08:23:30	2021-09-27 08:23:30
194	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	12	App\\Models\\User	1	{"attributes":{"t_idspt":1,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:15	2021-09-27 08:40:15
195	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	13	App\\Models\\User	1	{"attributes":{"t_idspt":2,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:15	2021-09-27 08:40:15
196	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	14	App\\Models\\User	1	{"attributes":{"t_idspt":3,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:15	2021-09-27 08:40:15
197	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	15	App\\Models\\User	1	{"attributes":{"t_idspt":4,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:15	2021-09-27 08:40:15
198	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	16	App\\Models\\User	1	{"attributes":{"t_idspt":5,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:15	2021-09-27 08:40:15
199	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	17	App\\Models\\User	1	{"attributes":{"t_idspt":6,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:15	2021-09-27 08:40:15
200	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	18	App\\Models\\User	1	{"attributes":{"t_idspt":7,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:15	2021-09-27 08:40:15
201	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	19	App\\Models\\User	1	{"attributes":{"t_idspt":8,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:15	2021-09-27 08:40:15
202	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	20	App\\Models\\User	1	{"attributes":{"t_idspt":9,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:15	2021-09-27 08:40:15
203	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	21	App\\Models\\User	1	{"attributes":{"t_idspt":10,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:15	2021-09-27 08:40:15
204	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	22	App\\Models\\User	1	{"attributes":{"t_idspt":11,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:15","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
205	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	23	App\\Models\\User	1	{"attributes":{"t_idspt":12,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
206	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	24	App\\Models\\User	1	{"attributes":{"t_idspt":13,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
207	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	25	App\\Models\\User	1	{"attributes":{"t_idspt":14,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
208	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	26	App\\Models\\User	1	{"attributes":{"t_idspt":15,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
209	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	27	App\\Models\\User	1	{"attributes":{"t_idspt":16,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
210	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	28	App\\Models\\User	1	{"attributes":{"t_idspt":17,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
211	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	29	App\\Models\\User	1	{"attributes":{"t_idspt":18,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
212	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	30	App\\Models\\User	1	{"attributes":{"t_idspt":19,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
213	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	31	App\\Models\\User	1	{"attributes":{"t_idspt":20,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
214	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	32	App\\Models\\User	1	{"attributes":{"t_idspt":21,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
215	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	33	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
216	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	34	App\\Models\\User	1	{"attributes":{"t_idspt":23,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:16	2021-09-27 08:40:16
217	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	35	App\\Models\\User	1	{"attributes":{"t_idspt":24,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:16","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
218	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	36	App\\Models\\User	1	{"attributes":{"t_idspt":25,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
219	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	37	App\\Models\\User	1	{"attributes":{"t_idspt":26,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
220	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	38	App\\Models\\User	1	{"attributes":{"t_idspt":27,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
221	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	39	App\\Models\\User	1	{"attributes":{"t_idspt":28,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
222	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	40	App\\Models\\User	1	{"attributes":{"t_idspt":29,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
223	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	41	App\\Models\\User	1	{"attributes":{"t_idspt":30,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
224	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	42	App\\Models\\User	1	{"attributes":{"t_idspt":31,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
225	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	43	App\\Models\\User	1	{"attributes":{"t_idspt":32,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
226	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	44	App\\Models\\User	1	{"attributes":{"t_idspt":33,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
227	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	45	App\\Models\\User	1	{"attributes":{"t_idspt":34,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
228	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	46	App\\Models\\User	1	{"attributes":{"t_idspt":35,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
229	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	47	App\\Models\\User	1	{"attributes":{"t_idspt":36,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:17","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:17	2021-09-27 08:40:17
230	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	48	App\\Models\\User	1	{"attributes":{"t_idspt":37,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
231	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	49	App\\Models\\User	1	{"attributes":{"t_idspt":38,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
232	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	50	App\\Models\\User	1	{"attributes":{"t_idspt":39,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
233	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	51	App\\Models\\User	1	{"attributes":{"t_idspt":40,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
234	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	52	App\\Models\\User	1	{"attributes":{"t_idspt":41,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
235	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	53	App\\Models\\User	1	{"attributes":{"t_idspt":42,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
236	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	54	App\\Models\\User	1	{"attributes":{"t_idspt":43,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
237	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	55	App\\Models\\User	1	{"attributes":{"t_idspt":44,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
238	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	56	App\\Models\\User	1	{"attributes":{"t_idspt":45,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
239	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	57	App\\Models\\User	1	{"attributes":{"t_idspt":46,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
240	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	58	App\\Models\\User	1	{"attributes":{"t_idspt":47,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:18","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:18	2021-09-27 08:40:18
241	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	59	App\\Models\\User	1	{"attributes":{"t_idspt":48,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:19	2021-09-27 08:40:19
242	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	60	App\\Models\\User	1	{"attributes":{"t_idspt":49,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:19	2021-09-27 08:40:19
243	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	61	App\\Models\\User	1	{"attributes":{"t_idspt":50,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:19	2021-09-27 08:40:19
244	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	62	App\\Models\\User	1	{"attributes":{"t_idspt":51,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:19	2021-09-27 08:40:19
245	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	63	App\\Models\\User	1	{"attributes":{"t_idspt":52,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:19	2021-09-27 08:40:19
246	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	64	App\\Models\\User	1	{"attributes":{"t_idspt":53,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:19	2021-09-27 08:40:19
247	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	65	App\\Models\\User	1	{"attributes":{"t_idspt":54,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:19	2021-09-27 08:40:19
248	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	66	App\\Models\\User	1	{"attributes":{"t_idspt":55,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:19	2021-09-27 08:40:19
249	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	67	App\\Models\\User	1	{"attributes":{"t_idspt":56,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:19	2021-09-27 08:40:19
250	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	68	App\\Models\\User	1	{"attributes":{"t_idspt":57,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:19	2021-09-27 08:40:19
251	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	69	App\\Models\\User	1	{"attributes":{"t_idspt":58,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:19","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
252	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	70	App\\Models\\User	1	{"attributes":{"t_idspt":59,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
253	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	71	App\\Models\\User	1	{"attributes":{"t_idspt":60,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
254	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	72	App\\Models\\User	1	{"attributes":{"t_idspt":61,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
255	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	73	App\\Models\\User	1	{"attributes":{"t_idspt":62,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
256	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	74	App\\Models\\User	1	{"attributes":{"t_idspt":63,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
257	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	75	App\\Models\\User	1	{"attributes":{"t_idspt":64,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
258	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	76	App\\Models\\User	1	{"attributes":{"t_idspt":65,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
259	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	77	App\\Models\\User	1	{"attributes":{"t_idspt":66,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
260	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	78	App\\Models\\User	1	{"attributes":{"t_idspt":67,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
261	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	79	App\\Models\\User	1	{"attributes":{"t_idspt":68,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
262	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	80	App\\Models\\User	1	{"attributes":{"t_idspt":69,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
263	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	81	App\\Models\\User	1	{"attributes":{"t_idspt":70,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
264	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	82	App\\Models\\User	1	{"attributes":{"t_idspt":71,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
265	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	83	App\\Models\\User	1	{"attributes":{"t_idspt":72,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
266	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	84	App\\Models\\User	1	{"attributes":{"t_idspt":73,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
267	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	85	App\\Models\\User	1	{"attributes":{"t_idspt":74,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:20","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:20	2021-09-27 08:40:20
268	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	86	App\\Models\\User	1	{"attributes":{"t_idspt":75,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:21","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:21	2021-09-27 08:40:21
269	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	87	App\\Models\\User	1	{"attributes":{"t_idspt":76,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:21","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:21	2021-09-27 08:40:21
270	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	88	App\\Models\\User	1	{"attributes":{"t_idspt":77,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:21","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:21	2021-09-27 08:40:21
271	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	89	App\\Models\\User	1	{"attributes":{"t_idspt":78,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:21","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:21	2021-09-27 08:40:21
272	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	90	App\\Models\\User	1	{"attributes":{"t_idspt":79,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:21","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:21	2021-09-27 08:40:21
273	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	91	App\\Models\\User	1	{"attributes":{"t_idspt":80,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:21","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:21	2021-09-27 08:40:21
274	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	92	App\\Models\\User	1	{"attributes":{"t_idspt":81,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 08:40:21","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 08:40:21	2021-09-27 08:40:21
275	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	9	App\\Models\\User	1	{"attributes":{"t_idspt":1,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-01-06 14:22:25","t_jmlhbayar_pokok":"500000","t_tglpembayaran_denda":"2021-01-06 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"500000"}}	2021-09-27 08:44:54	2021-09-27 08:44:54
276	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	10	App\\Models\\User	1	{"attributes":{"t_idspt":2,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-01-08 14:22:25","t_jmlhbayar_pokok":"7000000","t_tglpembayaran_denda":"2021-01-08 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"7000000"}}	2021-09-27 08:44:54	2021-09-27 08:44:54
277	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	11	App\\Models\\User	1	{"attributes":{"t_idspt":3,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-01-11 14:22:25","t_jmlhbayar_pokok":"750000","t_tglpembayaran_denda":"2021-01-11 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"750000"}}	2021-09-27 08:44:54	2021-09-27 08:44:54
278	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	12	App\\Models\\User	1	{"attributes":{"t_idspt":4,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-01-13 14:22:25","t_jmlhbayar_pokok":"12000000","t_tglpembayaran_denda":"2021-01-13 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"12000000"}}	2021-09-27 08:44:54	2021-09-27 08:44:54
279	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	13	App\\Models\\User	1	{"attributes":{"t_idspt":5,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-01-13 14:22:25","t_jmlhbayar_pokok":"9500000","t_tglpembayaran_denda":"2021-01-13 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"9500000"}}	2021-09-27 08:44:54	2021-09-27 08:44:54
280	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	14	App\\Models\\User	1	{"attributes":{"t_idspt":6,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-01-14 14:22:25","t_jmlhbayar_pokok":"7000000","t_tglpembayaran_denda":"2021-01-14 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"7000000"}}	2021-09-27 08:44:54	2021-09-27 08:44:54
281	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	15	App\\Models\\User	1	{"attributes":{"t_idspt":7,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-01-21 14:22:25","t_jmlhbayar_pokok":"5071800","t_tglpembayaran_denda":"2021-01-21 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"5071800"}}	2021-09-27 08:44:54	2021-09-27 08:44:54
282	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	16	App\\Models\\User	1	{"attributes":{"t_idspt":8,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-01-27 14:22:25","t_jmlhbayar_pokok":"2597700","t_tglpembayaran_denda":"2021-01-27 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2597700"}}	2021-09-27 08:44:54	2021-09-27 08:44:54
283	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	17	App\\Models\\User	1	{"attributes":{"t_idspt":9,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-01-28 14:22:25","t_jmlhbayar_pokok":"5118100","t_tglpembayaran_denda":"2021-01-28 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"5118100"}}	2021-09-27 08:44:54	2021-09-27 08:44:54
284	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	18	App\\Models\\User	1	{"attributes":{"t_idspt":10,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-02-05 14:22:25","t_jmlhbayar_pokok":"9500000","t_tglpembayaran_denda":"2021-02-05 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"9500000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
285	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	19	App\\Models\\User	1	{"attributes":{"t_idspt":11,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-02-09 14:22:25","t_jmlhbayar_pokok":"6310150","t_tglpembayaran_denda":"2021-02-09 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"6310150"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
286	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	20	App\\Models\\User	1	{"attributes":{"t_idspt":12,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-02-15 14:22:25","t_jmlhbayar_pokok":"6347800","t_tglpembayaran_denda":"2021-02-15 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"6347800"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
287	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	21	App\\Models\\User	1	{"attributes":{"t_idspt":13,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-02-15 14:22:25","t_jmlhbayar_pokok":"28200","t_tglpembayaran_denda":"2021-02-15 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"28200"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
288	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	22	App\\Models\\User	1	{"attributes":{"t_idspt":14,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-02-17 14:22:25","t_jmlhbayar_pokok":"7000000","t_tglpembayaran_denda":"2021-02-17 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"7000000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
289	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	23	App\\Models\\User	1	{"attributes":{"t_idspt":15,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-02-19 14:22:25","t_jmlhbayar_pokok":"4166000","t_tglpembayaran_denda":"2021-02-19 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"4166000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
290	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	24	App\\Models\\User	1	{"attributes":{"t_idspt":16,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-02-19 14:22:25","t_jmlhbayar_pokok":"6975800","t_tglpembayaran_denda":"2021-02-19 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"6975800"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
291	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	25	App\\Models\\User	1	{"attributes":{"t_idspt":17,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-02-23 14:22:25","t_jmlhbayar_pokok":"351750","t_tglpembayaran_denda":"2021-02-23 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"351750"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
292	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	26	App\\Models\\User	1	{"attributes":{"t_idspt":18,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-03-02 14:22:25","t_jmlhbayar_pokok":"3450000","t_tglpembayaran_denda":"2021-03-02 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"3450000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
293	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	27	App\\Models\\User	1	{"attributes":{"t_idspt":19,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-03-09 14:22:25","t_jmlhbayar_pokok":"2354000","t_tglpembayaran_denda":"2021-03-09 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2354000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
294	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	28	App\\Models\\User	1	{"attributes":{"t_idspt":20,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-03-22 14:22:25","t_jmlhbayar_pokok":"750000","t_tglpembayaran_denda":"2021-03-22 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"750000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
295	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	29	App\\Models\\User	1	{"attributes":{"t_idspt":21,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-03-29 14:22:25","t_jmlhbayar_pokok":"9500000","t_tglpembayaran_denda":"2021-03-29 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"9500000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
296	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	30	App\\Models\\User	1	{"attributes":{"t_idspt":22,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-03-29 14:22:25","t_jmlhbayar_pokok":"309600","t_tglpembayaran_denda":"2021-03-29 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"309600"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
297	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	31	App\\Models\\User	1	{"attributes":{"t_idspt":23,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-06 14:22:25","t_jmlhbayar_pokok":"1838400","t_tglpembayaran_denda":"2021-04-06 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"1838400"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
298	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	32	App\\Models\\User	1	{"attributes":{"t_idspt":24,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-12 14:22:25","t_jmlhbayar_pokok":"1750000","t_tglpembayaran_denda":"2021-04-12 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"1750000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
299	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	33	App\\Models\\User	1	{"attributes":{"t_idspt":25,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-19 14:22:25","t_jmlhbayar_pokok":"2000000","t_tglpembayaran_denda":"2021-04-19 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2000000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
300	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	34	App\\Models\\User	1	{"attributes":{"t_idspt":26,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-22 14:22:25","t_jmlhbayar_pokok":"4500000","t_tglpembayaran_denda":"2021-04-22 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"4500000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
301	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	35	App\\Models\\User	1	{"attributes":{"t_idspt":27,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-22 14:22:25","t_jmlhbayar_pokok":"622400","t_tglpembayaran_denda":"2021-04-22 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"622400"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
302	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	36	App\\Models\\User	1	{"attributes":{"t_idspt":28,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-23 14:22:25","t_jmlhbayar_pokok":"18347800","t_tglpembayaran_denda":"2021-04-23 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"18347800"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
303	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	37	App\\Models\\User	1	{"attributes":{"t_idspt":29,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-23 14:22:25","t_jmlhbayar_pokok":"6652800","t_tglpembayaran_denda":"2021-04-23 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"6652800"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
304	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	38	App\\Models\\User	1	{"attributes":{"t_idspt":30,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-23 14:22:25","t_jmlhbayar_pokok":"7707000","t_tglpembayaran_denda":"2021-04-23 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"7707000"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
305	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	39	App\\Models\\User	1	{"attributes":{"t_idspt":31,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-23 14:22:25","t_jmlhbayar_pokok":"7874500","t_tglpembayaran_denda":"2021-04-23 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"7874500"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
306	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	40	App\\Models\\User	1	{"attributes":{"t_idspt":32,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-23 14:22:25","t_jmlhbayar_pokok":"7204400","t_tglpembayaran_denda":"2021-04-23 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"7204400"}}	2021-09-27 08:44:55	2021-09-27 08:44:55
307	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	41	App\\Models\\User	1	{"attributes":{"t_idspt":33,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-27 14:22:25","t_jmlhbayar_pokok":"3750000","t_tglpembayaran_denda":"2021-04-27 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"3750000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
308	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	42	App\\Models\\User	1	{"attributes":{"t_idspt":34,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-04-30 14:22:25","t_jmlhbayar_pokok":"4500000","t_tglpembayaran_denda":"2021-04-30 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"4500000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
309	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	43	App\\Models\\User	1	{"attributes":{"t_idspt":35,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-03 14:22:25","t_jmlhbayar_pokok":"7775000","t_tglpembayaran_denda":"2021-05-03 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"7775000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
310	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	44	App\\Models\\User	1	{"attributes":{"t_idspt":36,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-03 14:22:25","t_jmlhbayar_pokok":"2000000","t_tglpembayaran_denda":"2021-05-03 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2000000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
311	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	45	App\\Models\\User	1	{"attributes":{"t_idspt":37,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-03 14:22:25","t_jmlhbayar_pokok":"2000000","t_tglpembayaran_denda":"2021-05-03 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2000000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
312	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	46	App\\Models\\User	1	{"attributes":{"t_idspt":38,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-05 14:22:25","t_jmlhbayar_pokok":"1000000","t_tglpembayaran_denda":"2021-05-05 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"1000000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
313	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	47	App\\Models\\User	1	{"attributes":{"t_idspt":39,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-06 14:22:25","t_jmlhbayar_pokok":"2250000","t_tglpembayaran_denda":"2021-05-06 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2250000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
314	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	48	App\\Models\\User	1	{"attributes":{"t_idspt":40,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-06 14:22:25","t_jmlhbayar_pokok":"2500000","t_tglpembayaran_denda":"2021-05-06 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2500000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
315	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	49	App\\Models\\User	1	{"attributes":{"t_idspt":41,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-10 14:22:25","t_jmlhbayar_pokok":"23066000","t_tglpembayaran_denda":"2021-05-10 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"23066000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
316	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	50	App\\Models\\User	1	{"attributes":{"t_idspt":42,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-10 14:22:25","t_jmlhbayar_pokok":"2165450","t_tglpembayaran_denda":"2021-05-10 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2165450"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
317	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	51	App\\Models\\User	1	{"attributes":{"t_idspt":43,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-10 14:22:25","t_jmlhbayar_pokok":"1727900","t_tglpembayaran_denda":"2021-05-10 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"1727900"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
318	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	52	App\\Models\\User	1	{"attributes":{"t_idspt":44,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-18 14:22:25","t_jmlhbayar_pokok":"2000000","t_tglpembayaran_denda":"2021-05-18 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2000000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
319	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	53	App\\Models\\User	1	{"attributes":{"t_idspt":45,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-18 14:22:25","t_jmlhbayar_pokok":"750000","t_tglpembayaran_denda":"2021-05-18 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"750000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
320	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	54	App\\Models\\User	1	{"attributes":{"t_idspt":46,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-18 14:22:25","t_jmlhbayar_pokok":"3413000","t_tglpembayaran_denda":"2021-05-18 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"3413000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
321	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	55	App\\Models\\User	1	{"attributes":{"t_idspt":47,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-18 14:22:25","t_jmlhbayar_pokok":"8817000","t_tglpembayaran_denda":"2021-05-18 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"8817000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
322	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	56	App\\Models\\User	1	{"attributes":{"t_idspt":48,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-19 14:22:25","t_jmlhbayar_pokok":"1452250","t_tglpembayaran_denda":"2021-05-19 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"1452250"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
323	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	57	App\\Models\\User	1	{"attributes":{"t_idspt":49,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-20 14:22:25","t_jmlhbayar_pokok":"4825000","t_tglpembayaran_denda":"2021-05-20 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"4825000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
324	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	58	App\\Models\\User	1	{"attributes":{"t_idspt":50,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-25 14:22:25","t_jmlhbayar_pokok":"23066000","t_tglpembayaran_denda":"2021-05-25 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"23066000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
325	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	59	App\\Models\\User	1	{"attributes":{"t_idspt":51,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-27 14:22:25","t_jmlhbayar_pokok":"2219550","t_tglpembayaran_denda":"2021-05-27 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2219550"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
326	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	60	App\\Models\\User	1	{"attributes":{"t_idspt":52,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-28 14:22:25","t_jmlhbayar_pokok":"210600","t_tglpembayaran_denda":"2021-05-28 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"210600"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
327	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	61	App\\Models\\User	1	{"attributes":{"t_idspt":53,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-28 14:22:25","t_jmlhbayar_pokok":"230300","t_tglpembayaran_denda":"2021-05-28 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"230300"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
328	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	62	App\\Models\\User	1	{"attributes":{"t_idspt":54,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-05-31 14:22:25","t_jmlhbayar_pokok":"350000","t_tglpembayaran_denda":"2021-05-31 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"350000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
329	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	63	App\\Models\\User	1	{"attributes":{"t_idspt":55,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-06-02 14:22:25","t_jmlhbayar_pokok":"1000000","t_tglpembayaran_denda":"2021-06-02 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"1000000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
330	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	64	App\\Models\\User	1	{"attributes":{"t_idspt":56,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-06-04 14:22:25","t_jmlhbayar_pokok":"7000000","t_tglpembayaran_denda":"2021-06-04 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"7000000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
331	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	65	App\\Models\\User	1	{"attributes":{"t_idspt":57,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-06-04 14:22:25","t_jmlhbayar_pokok":"5000000","t_tglpembayaran_denda":"2021-06-04 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"5000000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
332	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	66	App\\Models\\User	1	{"attributes":{"t_idspt":58,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-06-07 14:22:25","t_jmlhbayar_pokok":"2000000","t_tglpembayaran_denda":"2021-06-07 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2000000"}}	2021-09-27 08:44:56	2021-09-27 08:44:56
333	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	67	App\\Models\\User	1	{"attributes":{"t_idspt":59,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-06-07 14:22:25","t_jmlhbayar_pokok":"3653500","t_tglpembayaran_denda":"2021-06-07 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"3653500"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
334	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	68	App\\Models\\User	1	{"attributes":{"t_idspt":60,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-06-18 14:22:25","t_jmlhbayar_pokok":"2000000","t_tglpembayaran_denda":"2021-06-18 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2000000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
335	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	69	App\\Models\\User	1	{"attributes":{"t_idspt":61,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-06-18 14:22:25","t_jmlhbayar_pokok":"1500000","t_tglpembayaran_denda":"2021-06-18 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"1500000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
336	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	70	App\\Models\\User	1	{"attributes":{"t_idspt":62,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-07-08 14:22:25","t_jmlhbayar_pokok":"6494350","t_tglpembayaran_denda":"2021-07-08 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"6494350"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
337	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	71	App\\Models\\User	1	{"attributes":{"t_idspt":63,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-07-14 14:22:25","t_jmlhbayar_pokok":"9240250","t_tglpembayaran_denda":"2021-07-14 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"9240250"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
338	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	72	App\\Models\\User	1	{"attributes":{"t_idspt":64,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-07-15 14:22:25","t_jmlhbayar_pokok":"2064000","t_tglpembayaran_denda":"2021-07-15 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"2064000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
339	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	73	App\\Models\\User	1	{"attributes":{"t_idspt":65,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-07-21 14:22:25","t_jmlhbayar_pokok":"12000000","t_tglpembayaran_denda":"2021-07-21 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"12000000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
340	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	74	App\\Models\\User	1	{"attributes":{"t_idspt":66,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-07-21 14:22:25","t_jmlhbayar_pokok":"8250000","t_tglpembayaran_denda":"2021-07-21 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"8250000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
341	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	75	App\\Models\\User	1	{"attributes":{"t_idspt":67,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-07-28 14:22:25","t_jmlhbayar_pokok":"1500000","t_tglpembayaran_denda":"2021-07-28 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"1500000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
342	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	76	App\\Models\\User	1	{"attributes":{"t_idspt":68,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-07-29 14:22:25","t_jmlhbayar_pokok":"1000000","t_tglpembayaran_denda":"2021-07-29 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"1000000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
343	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	77	App\\Models\\User	1	{"attributes":{"t_idspt":69,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-07-30 14:22:25","t_jmlhbayar_pokok":"500000","t_tglpembayaran_denda":"2021-07-30 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"500000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
344	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	78	App\\Models\\User	1	{"attributes":{"t_idspt":70,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-02 14:22:25","t_jmlhbayar_pokok":"28137000","t_tglpembayaran_denda":"2021-08-02 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"28137000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
345	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	79	App\\Models\\User	1	{"attributes":{"t_idspt":71,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-02 14:22:25","t_jmlhbayar_pokok":"26829950","t_tglpembayaran_denda":"2021-08-02 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"26829950"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
346	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	80	App\\Models\\User	1	{"attributes":{"t_idspt":72,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-02 14:22:25","t_jmlhbayar_pokok":"65226200","t_tglpembayaran_denda":"2021-08-02 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"65226200"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
347	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	81	App\\Models\\User	1	{"attributes":{"t_idspt":73,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-02 14:22:25","t_jmlhbayar_pokok":"13838500","t_tglpembayaran_denda":"2021-08-02 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"13838500"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
348	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	82	App\\Models\\User	1	{"attributes":{"t_idspt":74,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-03 14:22:25","t_jmlhbayar_pokok":"4903000","t_tglpembayaran_denda":"2021-08-03 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"4903000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
349	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	83	App\\Models\\User	1	{"attributes":{"t_idspt":75,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-13 14:22:25","t_jmlhbayar_pokok":"7000000","t_tglpembayaran_denda":"2021-08-13 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"7000000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
350	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	84	App\\Models\\User	1	{"attributes":{"t_idspt":76,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-16 14:22:25","t_jmlhbayar_pokok":"1000000","t_tglpembayaran_denda":"2021-08-16 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"1000000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
351	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	85	App\\Models\\User	1	{"attributes":{"t_idspt":77,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-02 14:22:25","t_jmlhbayar_pokok":"4600000","t_tglpembayaran_denda":"2021-08-02 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"4600000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
352	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	86	App\\Models\\User	1	{"attributes":{"t_idspt":78,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-02 14:22:25","t_jmlhbayar_pokok":"5318800","t_tglpembayaran_denda":"2021-08-02 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"5318800"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
353	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	87	App\\Models\\User	1	{"attributes":{"t_idspt":79,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-02 14:22:25","t_jmlhbayar_pokok":"5608750","t_tglpembayaran_denda":"2021-08-02 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"5608750"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
354	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	88	App\\Models\\User	1	{"attributes":{"t_idspt":80,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-08-02 14:22:25","t_jmlhbayar_pokok":"9608750","t_tglpembayaran_denda":"2021-08-02 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"9608750"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
355	default	created	App\\Models\\Pembayaran\\PembayaranBphtb	89	App\\Models\\User	1	{"attributes":{"t_idspt":81,"t_nourut_bayar":null,"s_id_status_bayar":1,"t_iduser_bayar":1,"t_tglpembayaran_pokok":"2021-09-15 14:22:25","t_jmlhbayar_pokok":"250000","t_tglpembayaran_denda":"2021-09-15 14:22:25","t_jmlhbayar_denda":"0","t_jmlhbayar_total":"250000"}}	2021-09-27 08:44:57	2021-09-27 08:44:57
356	default	created	App\\Models\\Spt\\Spt	83	App\\Models\\User	1	{"attributes":{"t_iduser_buat":1,"t_uuidspt":"7cfc993cd0a0403a9409c63a5da99235","t_kohirspt":82,"t_tgldaftar_spt":"2021-09-27 09:26:53","t_tglby_system":"2021-09-27 10:28:05","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":6,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"ade test","t_nik_pembeli":"2111111111111111","t_npwp_pembeli":null,"t_jalan_pembeli":"jalan satu","t_kabkota_pembeli":"SANGIHE","t_idkec_pembeli":13,"t_namakecamatan_pembeli":"MANGANITU","t_idkel_pembeli":303,"t_namakelurahan_pembeli":"KARATUNG I","t_rt_pembeli":"020","t_rw_pembeli":"003","t_nohp_pembeli":null,"t_notelp_pembeli":null,"t_email_pembeli":null,"t_kodepos_pembeli":null,"t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-09-27 09:28:05	2021-09-27 09:28:05
357	default	created	App\\Models\\User	12	App\\Models\\User	1	{"attributes":{"name":"BPN","username":"BPN","email":"bpn@gmail.com","s_gambar":null}}	2021-09-27 10:54:40	2021-09-27 10:54:40
358	default	created	App\\Models\\Spt\\Spt	84	App\\Models\\User	1	{"attributes":{"t_iduser_buat":1,"t_uuidspt":"3ec0889c52b34bf5955955022dc4426e","t_kohirspt":82,"t_tgldaftar_spt":"2021-09-27 11:00:29","t_tglby_system":"2021-09-27 12:01:13","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":6,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"Frennandi Ade Ilyas","t_nik_pembeli":"3174012201000001","t_npwp_pembeli":null,"t_jalan_pembeli":"jalan tebet","t_kabkota_pembeli":"SANGIHE","t_idkec_pembeli":14,"t_namakecamatan_pembeli":"MANGANITU SELATAN","t_idkel_pembeli":233,"t_namakelurahan_pembeli":"LAPEPAHE","t_rt_pembeli":"03","t_rw_pembeli":"03","t_nohp_pembeli":null,"t_notelp_pembeli":null,"t_email_pembeli":null,"t_kodepos_pembeli":null,"t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-09-27 11:01:13	2021-09-27 11:01:13
359	default	updated	App\\Models\\Setting\\Persyaratan	154	App\\Models\\User	1	{"attributes":{"s_idjenistransaksi":1,"s_namapersyaratan":"Foto copy Kartu Keluarga ( Pembeli )","order":10,"s_idwajib_up":2,"s_lokasimenu":1},"old":{"s_idjenistransaksi":1,"s_namapersyaratan":"Foto copy Kartu Keluarga( Pembeli )","order":10,"s_idwajib_up":1,"s_lokasimenu":1}}	2021-09-27 11:02:51	2021-09-27 11:02:51
360	default	created	App\\Models\\Spt\\Filefotoobjek	5	App\\Models\\User	1	{"attributes":{"t_idspt":84,"t_iduser_upload":1,"letak_file":"upload\\/file_fotoobjek\\/2021\\/jualbeli\\/84\\/","nama_file":"sangihe.png","t_tgl_upload":"2021-09-27 11:03:29","t_keterangan_file":null,"created_at":"2021-09-27T04:03:29.000000Z","updated_at":"2021-09-27T04:03:29.000000Z"}}	2021-09-27 11:03:29	2021-09-27 11:03:29
361	default	created	App\\Models\\Bpn\\HisPostDataBpn	10	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.003.0198.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:06:09	2021-09-27 11:06:09
362	default	created	App\\Models\\Bpn\\HisPostDataBpn	11	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.081.005.004.0180.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:06:31	2021-09-27 11:06:31
363	default	created	App\\Models\\Bpn\\HisPostDataBpn	12	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71..0.4.0.80..017..001..","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:06:46	2021-09-27 11:06:46
364	default	created	App\\Models\\Bpn\\HisPostDataBpn	13	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"DATA TIDAK DITEMUKAN","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:07:22	2021-09-27 11:07:22
365	default	created	App\\Models\\Bpn\\HisPostDataBpn	14	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.002.0020.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:07:38	2021-09-27 11:07:38
366	default	created	App\\Models\\Bpn\\HisPostDataBpn	15	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.002.0020.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:07:48	2021-09-27 11:07:48
367	default	created	App\\Models\\Bpn\\HisPostDataBpn	16	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.002.0020.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:09:48	2021-09-27 11:09:48
368	default	created	App\\Models\\Bpn\\HisPostDataBpn	17	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.002.0020.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:10:14	2021-09-27 11:10:14
369	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	97	App\\Models\\User	1	{"attributes":{"t_idspt":84,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 11:15:46","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7,152,153,154"}}	2021-09-27 11:15:46	2021-09-27 11:15:46
370	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	93	App\\Models\\User	1	{"attributes":{"t_idspt":84,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 11:15:58","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 11:15:58	2021-09-27 11:15:58
371	default	created	App\\Models\\Bpn\\HisPostDataBpn	18	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:17:11	2021-09-27 11:17:11
372	default	created	App\\Models\\Bpn\\HisPostDataBpn	19	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71..0.4.0.80..017..001..","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:24:15	2021-09-27 11:24:15
373	default	created	App\\Models\\Bpn\\HisPostDataBpn	20	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:24:25	2021-09-27 11:24:25
374	default	created	App\\Models\\Bpn\\HisPostDataBpn	21	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:44:25	2021-09-27 11:44:25
375	default	created	App\\Models\\Bpn\\HisPostDataBpn	22	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.9","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:44:32	2021-09-27 11:44:32
376	default	created	App\\Models\\Bpn\\HisPostDataBpn	23	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:44:39	2021-09-27 11:44:39
377	default	created	App\\Models\\Bpn\\HisPostDataBpn	24	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-27 11:45:22	2021-09-27 11:45:22
378	default	created	App\\Models\\Spt\\Spt	85	App\\Models\\User	12	{"attributes":{"t_iduser_buat":12,"t_uuidspt":"47b165758c004d288b60fffe05098fa0","t_kohirspt":83,"t_tgldaftar_spt":"2021-09-27 01:44:04","t_tglby_system":"2021-09-27 14:44:27","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":7,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"VINNY DEVINTHA HAMEL","t_nik_pembeli":"7103245908910001","t_npwp_pembeli":null,"t_jalan_pembeli":"TAPUANG","t_kabkota_pembeli":"KEPULAUAN SANGIHE","t_idkec_pembeli":24,"t_namakecamatan_pembeli":"TAHUNA TIMUR","t_idkel_pembeli":211,"t_namakelurahan_pembeli":"TAPUANG","t_rt_pembeli":"00","t_rw_pembeli":"00","t_nohp_pembeli":null,"t_notelp_pembeli":null,"t_email_pembeli":null,"t_kodepos_pembeli":null,"t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-09-27 13:44:26	2021-09-27 13:44:26
379	default	created	App\\Models\\Spt\\Filesyarat	41	App\\Models\\User	12	{"attributes":{"t_idspt":85,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":12,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/85\\/","nama_file":"SPS Kartini Makaminan.pdf","t_tgl_upload":"2021-09-27 13:50:37","t_keterangan_file":null,"created_at":"2021-09-27T06:50:37.000000Z","updated_at":"2021-09-27T06:50:37.000000Z"}}	2021-09-27 13:50:37	2021-09-27 13:50:37
380	default	created	App\\Models\\Spt\\Filesyarat	42	App\\Models\\User	12	{"attributes":{"t_idspt":85,"s_idpersyaratan":2,"s_idjenistransaksi":1,"t_iduser_upload":12,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/85\\/","nama_file":"Screenshot (1).png","t_tgl_upload":"2021-09-27 13:51:35","t_keterangan_file":null,"created_at":"2021-09-27T06:51:35.000000Z","updated_at":"2021-09-27T06:51:35.000000Z"}}	2021-09-27 13:51:35	2021-09-27 13:51:35
381	default	created	App\\Models\\Spt\\Filesyarat	43	App\\Models\\User	12	{"attributes":{"t_idspt":85,"s_idpersyaratan":3,"s_idjenistransaksi":1,"t_iduser_upload":12,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/85\\/","nama_file":"Screenshot (1)-480565964.png","t_tgl_upload":"2021-09-27 13:51:58","t_keterangan_file":null,"created_at":"2021-09-27T06:51:58.000000Z","updated_at":"2021-09-27T06:51:58.000000Z"}}	2021-09-27 13:51:58	2021-09-27 13:51:58
382	default	created	App\\Models\\Spt\\Filesyarat	44	App\\Models\\User	12	{"attributes":{"t_idspt":85,"s_idpersyaratan":4,"s_idjenistransaksi":1,"t_iduser_upload":12,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/85\\/","nama_file":"Screenshot (1)-78141464.png","t_tgl_upload":"2021-09-27 13:52:01","t_keterangan_file":null,"created_at":"2021-09-27T06:52:01.000000Z","updated_at":"2021-09-27T06:52:01.000000Z"}}	2021-09-27 13:52:01	2021-09-27 13:52:01
383	default	created	App\\Models\\Spt\\Filesyarat	45	App\\Models\\User	12	{"attributes":{"t_idspt":85,"s_idpersyaratan":5,"s_idjenistransaksi":1,"t_iduser_upload":12,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/85\\/","nama_file":"Screenshot (1)-580235537.png","t_tgl_upload":"2021-09-27 13:52:07","t_keterangan_file":null,"created_at":"2021-09-27T06:52:07.000000Z","updated_at":"2021-09-27T06:52:07.000000Z"}}	2021-09-27 13:52:07	2021-09-27 13:52:07
384	default	created	App\\Models\\Spt\\Filesyarat	46	App\\Models\\User	12	{"attributes":{"t_idspt":85,"s_idpersyaratan":6,"s_idjenistransaksi":1,"t_iduser_upload":12,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/85\\/","nama_file":"Screenshot (1)-2073822874.png","t_tgl_upload":"2021-09-27 13:52:10","t_keterangan_file":null,"created_at":"2021-09-27T06:52:10.000000Z","updated_at":"2021-09-27T06:52:10.000000Z"}}	2021-09-27 13:52:10	2021-09-27 13:52:10
385	default	created	App\\Models\\Spt\\Filefotoobjek	6	App\\Models\\User	12	{"attributes":{"t_idspt":85,"t_iduser_upload":12,"letak_file":"upload\\/file_fotoobjek\\/2021\\/jualbeli\\/85\\/","nama_file":"Screenshot (1).png","t_tgl_upload":"2021-09-27 13:52:34","t_keterangan_file":null,"created_at":"2021-09-27T06:52:34.000000Z","updated_at":"2021-09-27T06:52:34.000000Z"}}	2021-09-27 13:52:34	2021-09-27 13:52:34
386	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	98	App\\Models\\User	1	{"attributes":{"t_idspt":85,"s_id_status_berkas":1,"t_tglvalidasi":"2021-09-27 13:57:44","t_iduser_validasi":1,"t_keterangan_berkas":null,"t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7,152,153,154"}}	2021-09-27 13:57:44	2021-09-27 13:57:44
387	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	94	App\\Models\\User	1	{"attributes":{"t_idspt":85,"s_id_status_kabid":1,"t_tglvalidasi":"2021-09-27 13:59:07","t_iduser_validasi":1,"t_keterangan_kabid":null}}	2021-09-27 13:59:07	2021-09-27 13:59:07
388	default	created	App\\Models\\Pelaporan\\InputAjb	2	App\\Models\\User	12	{"attributes":{"t_idspt":85,"t_tgl_ajb":"2021-09-27 00:00:00","t_no_ajb":"456576","t_iduser_input":12}}	2021-09-27 14:06:38	2021-09-27 14:06:38
389	default	updated	App\\Models\\Setting\\Notaris	7	App\\Models\\User	1	{"attributes":{"s_namanotaris":"KEPALA BPN SANGIHE","s_alamatnotaris":"-","s_kodenotaris":"07","s_sknotaris":"-","s_statusnotaris":1},"old":{"s_namanotaris":"KEPALA BPN, D. D. BULAMEI","s_alamatnotaris":"-","s_kodenotaris":"07","s_sknotaris":"-","s_statusnotaris":1}}	2021-09-27 15:05:37	2021-09-27 15:05:37
390	default	created	App\\Models\\Pelaporan\\LaporBulananHead	2	App\\Models\\User	1	{"attributes":{"t_no_lapor":1,"t_tgl_lapor":"2021-09-27 00:00:00","t_untuk_bulan":9,"t_untuk_tahun":2021,"t_keterangan":"test","t_iduser_input":1}}	2021-09-27 15:10:28	2021-09-27 15:10:28
391	default	created	App\\Models\\Bpn\\HisPostDataBpn	25	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-27 20:04:42	2021-09-27 20:04:42
392	default	created	App\\Models\\Bpn\\HisPostDataBpn	26	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 08:19:25	2021-09-28 08:19:25
393	default	created	App\\Models\\Bpn\\HisPostDataBpn	27	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 08:19:45	2021-09-28 08:19:45
394	default	created	App\\Models\\Bpn\\HisPostDataBpn	28	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NTPD tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-28 08:20:01	2021-09-28 08:20:01
395	default	created	App\\Models\\Bpn\\HisPostDataBpn	29	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 08:20:06	2021-09-28 08:20:06
396	default	created	App\\Models\\Bpn\\HisPostDataBpn	30	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.010.2320.","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-28 08:20:11	2021-09-28 08:20:11
397	default	created	App\\Models\\Bpn\\HisPostDataBpn	31	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 08:20:18	2021-09-28 08:20:18
398	default	created	App\\Models\\Bpn\\HisPostDataBpn	32	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"USERNAME OR PASSWORD SALAH","t_iduser":null,"t_tglby_system":null}}	2021-09-28 08:20:23	2021-09-28 08:20:23
399	default	created	App\\Models\\Bpn\\HisPostDataBpn	33	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 08:20:29	2021-09-28 08:20:29
400	default	created	App\\Models\\Bpn\\HisPostDataBpn	34	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 09:05:54	2021-09-28 09:05:54
401	default	created	App\\Models\\Bpn\\HisPostDataBpn	35	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 11:43:18	2021-09-28 11:43:18
402	default	created	App\\Models\\Bpn\\HisPostDataBpn	36	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 12:56:21	2021-09-28 12:56:21
403	default	created	App\\Models\\Bpn\\HisPostDataBpn	37	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 13:26:44	2021-09-28 13:26:44
404	default	created	App\\Models\\Bpn\\HisPostDataBpn	38	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 13:27:06	2021-09-28 13:27:06
405	default	created	App\\Models\\Bpn\\HisPostDataBpn	39	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"DATA TIDAK DITEMUKAN","t_iduser":null,"t_tglby_system":null}}	2021-09-28 13:42:04	2021-09-28 13:42:04
406	default	created	App\\Models\\Bpn\\HisPostDataBpn	40	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.017.001.0232.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"NOP tidak ditemukan","t_iduser":null,"t_tglby_system":null}}	2021-09-28 13:42:51	2021-09-28 13:42:51
407	default	created	App\\Models\\Bpn\\HisPostDataBpn	41	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.015.003.0192.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"DATA TIDAK DITEMUKAN","t_iduser":null,"t_tglby_system":null}}	2021-09-28 13:46:21	2021-09-28 13:46:21
408	default	created	App\\Models\\Bpn\\HisPostDataBpn	42	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.015.003.0192.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 13:46:30	2021-09-28 13:46:30
409	default	created	App\\Models\\Bpn\\HisPostDataBpn	43	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.006.005.0114.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 13:56:05	2021-09-28 13:56:05
410	default	created	App\\Models\\Bpn\\HisPostDataBpn	44	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.081.003.003.0016.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 14:03:51	2021-09-28 14:03:51
411	default	created	App\\Models\\Bpn\\HisPostDataBpn	45	\N	\N	{"attributes":{"s_username":null,"s_password":null,"t_aktaid":null,"t_tgl_akta":null,"t_nop":"71.04.080.006.005.0114.0","t_nik":null,"t_nib":null,"t_npwp":null,"t_nama_wp":null,"t_alamat_op":null,"t_kecamatan_op":null,"t_kelurahan_op":null,"t_kota_kab_op":null,"t_luastanah_op":null,"t_luasbangunan_op":null,"t_no_sertipikat":null,"t_no_akta":null,"t_ppat":null,"t_status":"OK","t_iduser":null,"t_tglby_system":null}}	2021-09-28 14:03:59	2021-09-28 14:03:59
\.


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Data for Name: his_getbphtb_bpn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.his_getbphtb_bpn (t_idhis_getbpn, t_username, t_password, t_nop, t_ntpd, t_nik, t_nama, t_alamat, t_kelurahan_op, t_kecamatan_op, t_kota_kab_op, t_luastanah, t_luasbangunan, t_pembayaran, t_status, t_tanggal_pembayaran, t_jenisbayar, respon_code, t_iduser, t_tglby_system, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: his_getpbb_bpn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.his_getpbb_bpn (t_idhis_pbbbpn, t_username, t_password, t_nop, t_nik, t_nama_wp, t_alamat_op, t_kecamatan_op, t_kelurahan_op, t_kota_kab_op, t_luas_tanah_op, t_luas_bangunan_op, t_njop_tanah_op, t_njop_bangunan_op, t_status_tunggakan, t_iduser, t_tglby_system, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: his_postdatabpn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.his_postdatabpn (t_idhis_posbpn, t_username, t_password, t_aktaid, t_tgl_akta, t_nop, t_nik, t_nib, t_npwp, t_nama_wp, t_alamat_op, t_kecamatan_op, t_kelurahan_op, t_kota_kab_op, t_luastanah_op, t_luasbangunan_op, t_no_sertipikat, t_no_akta, t_ppat, t_status, t_iduser, t_tglby_system, created_at, updated_at) FROM stdin;
1	\N	\N	\N	\N	71.04.080.017.003.0198.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-13 14:19:06	2021-09-13 14:19:06
2	\N	\N	\N	\N	71.04.080.017.003.0198.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-13 14:19:44	2021-09-13 14:19:44
3	\N	\N	\N	\N	71.04.080.017.003.0198.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-13 14:23:15	2021-09-13 14:23:15
4	\N	\N	\N	\N	71.04.080.017.003.0198.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-13 14:45:58	2021-09-13 14:45:58
5	\N	\N	\N	\N	71.04.080.017.003.0198.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-13 15:29:37	2021-09-13 15:29:37
6	\N	\N	\N	\N	71.04.080.017.003.0198.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-13 15:29:42	2021-09-13 15:29:42
7	\N	\N	\N	\N	71.04.080.017.003.0198.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-13 15:30:01	2021-09-13 15:30:01
8	\N	\N	\N	\N	71.04.080.017.003.0198.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-14 09:56:03	2021-09-14 09:56:03
9	\N	\N	\N	\N	71.04.080.017.003.0198.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-14 14:55:48	2021-09-14 14:55:48
10	\N	\N	\N	\N	71.04.080.017.003.0198.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-27 11:06:09	2021-09-27 11:06:09
11	\N	\N	\N	\N	71.04.081.005.004.0180.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-27 11:06:31	2021-09-27 11:06:31
12	\N	\N	\N	\N	71..0.4.0.80..017..001..	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-27 11:06:46	2021-09-27 11:06:46
13	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	DATA TIDAK DITEMUKAN	\N	\N	2021-09-27 11:07:22	2021-09-27 11:07:22
14	\N	\N	\N	\N	71.04.080.017.002.0020.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-27 11:07:38	2021-09-27 11:07:38
15	\N	\N	\N	\N	71.04.080.017.002.0020.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-27 11:07:48	2021-09-27 11:07:48
16	\N	\N	\N	\N	71.04.080.017.002.0020.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-27 11:09:48	2021-09-27 11:09:48
17	\N	\N	\N	\N	71.04.080.017.002.0020.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-27 11:10:14	2021-09-27 11:10:14
18	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-27 11:17:11	2021-09-27 11:17:11
19	\N	\N	\N	\N	71..0.4.0.80..017..001..	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-27 11:24:15	2021-09-27 11:24:15
20	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-27 11:24:25	2021-09-27 11:24:25
21	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-27 11:44:25	2021-09-27 11:44:25
22	\N	\N	\N	\N	71.04.080.017.001.0232.9	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-27 11:44:32	2021-09-27 11:44:32
23	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-27 11:44:39	2021-09-27 11:44:39
24	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-27 11:45:22	2021-09-27 11:45:22
25	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-27 20:04:42	2021-09-27 20:04:42
26	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 08:19:25	2021-09-28 08:19:25
27	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 08:19:45	2021-09-28 08:19:45
28	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NTPD tidak ditemukan	\N	\N	2021-09-28 08:20:01	2021-09-28 08:20:01
29	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 08:20:06	2021-09-28 08:20:06
30	\N	\N	\N	\N	71.04.080.017.010.2320.	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-28 08:20:11	2021-09-28 08:20:11
31	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 08:20:18	2021-09-28 08:20:18
32	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	USERNAME OR PASSWORD SALAH	\N	\N	2021-09-28 08:20:23	2021-09-28 08:20:23
33	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 08:20:29	2021-09-28 08:20:29
34	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 09:05:54	2021-09-28 09:05:54
35	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 11:43:18	2021-09-28 11:43:18
36	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 12:56:20	2021-09-28 12:56:20
37	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 13:26:44	2021-09-28 13:26:44
38	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 13:27:05	2021-09-28 13:27:05
39	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	DATA TIDAK DITEMUKAN	\N	\N	2021-09-28 13:42:04	2021-09-28 13:42:04
40	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	NOP tidak ditemukan	\N	\N	2021-09-28 13:42:51	2021-09-28 13:42:51
41	\N	\N	\N	\N	71.04.080.015.003.0192.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	DATA TIDAK DITEMUKAN	\N	\N	2021-09-28 13:46:21	2021-09-28 13:46:21
42	\N	\N	\N	\N	71.04.080.015.003.0192.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 13:46:30	2021-09-28 13:46:30
43	\N	\N	\N	\N	71.04.080.006.005.0114.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 13:56:05	2021-09-28 13:56:05
44	\N	\N	\N	\N	71.04.081.003.003.0016.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 14:03:51	2021-09-28 14:03:51
45	\N	\N	\N	\N	71.04.080.006.005.0114.0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	OK	\N	\N	2021-09-28 14:03:59	2021-09-28 14:03:59
\.


--
-- Data for Name: integrasi_sertifikat_bpn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.integrasi_sertifikat_bpn (t_idsertifikat, aktaid, tgl_akta, nop, nib, nik, npwp, nama_wp, alamat_op, kelurahan_op, kecamatan_op, kota_op, luastanah_op, luasbangunan_op, ppat, no_sertipikat, no_akta, t_iduser, t_tgltransaksi, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2020_11_24_000000_create_s_pejabat_table	1
2	2020_11_24_000001_create_s_notaris_table	1
3	2020_11_24_000002_create_users_table	1
4	2020_11_24_000003_create_failed_jobs_table	1
5	2020_11_24_000004_create_password_resets_table	1
6	2020_11_25_000001_create_s_status_table	1
7	2020_11_25_000002_create_s_acuan_table	1
8	2020_11_25_000003_create_s_harga_history_table	1
9	2020_11_25_000004_create_s_jenisdoktanah_table	1
10	2020_11_25_000005_create_s_jenishaktanah_table	1
11	2020_11_25_000006_create_s_jenisketetapan_table	1
12	2020_11_25_000007_create_s_jenispengurangan_table	1
13	2020_11_25_000008_create_s_jenistanah_table	1
14	2020_11_25_000009_create_s_jenistransaksi_table	1
15	2020_11_25_000010_create_s_kecamatan_table	1
16	2020_11_25_000011_create_s_kelurahan_table	1
17	2020_11_25_000012_create_s_koderekening_table	1
18	2020_11_25_000013_create_s_pemda_table	1
19	2020_11_25_000014_create_s_presentase_wajar_table	1
20	2020_11_25_000015_create_s_wajib_up_table	1
21	2020_11_25_000016_create_s_tarifbphtb_table	1
22	2020_11_25_000017_create_s_tarifnpoptkp_table	1
23	2020_11_25_000018_create_s_tempo_table	1
24	2020_11_25_000019_create_s_background_table	1
25	2020_11_25_000020_create_s_persyaratan	1
26	2020_11_25_000021_create_permission_tables	1
27	2020_11_25_000022_create_activity_log_table	1
28	2020_11_25_000023_create_s_jenisfasilitas_table	1
29	2020_11_25_000024_create_t_spt_table	1
30	2020_11_25_000025_create_pelayanan_keringanans_table	1
31	2020_11_25_000026_create_pelayanan_pembatalans_table	1
32	2020_11_26_000001_create_integrasi_sertifikat_bpn_table	1
33	2020_11_26_000002_create_his_get_bphtb_bpn_table	1
34	2020_11_26_000002_create_his_getpbb_bpn_table	1
35	2020_11_26_000003_create_his_postpbb_bpn_table	1
36	2020_11_26_000004_create_t_inputajb_table	1
37	2020_11_26_000005_create_s_target_table	1
38	2020_11_26_000006_create_s_acuan_jenistanah_table	1
39	2020_11_26_000007_create_t_validasi_berkas_table	1
40	2021_01_12_140000_create_notifications_table	1
41	2021_01_12_184458_add_column_to_validasi_berkas	1
42	2021_01_13_112232_notifications_message	1
43	2021_02_24_150805_alter_spt_table	1
44	2021_09_13_152346_s_bpn_ws	2
\.


--
-- Data for Name: model_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.model_has_permissions (permission_id, model_type, model_id) FROM stdin;
\.


--
-- Data for Name: model_has_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.model_has_roles (role_id, model_type, model_id) FROM stdin;
1	App\\Models\\User	1
2	App\\Models\\User	2
3	App\\Models\\User	3
6	App\\Models\\User	6
2	App\\Models\\User	5
2	App\\Models\\User	4
3	App\\Models\\User	7
2	App\\Models\\User	8
4	App\\Models\\User	10
4	App\\Models\\User	11
4	App\\Models\\User	9
4	App\\Models\\User	12
\.


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notifications (t_id_notif, t_id_user_buat, t_idspt, read_pemda_at, t_user_id, read_at, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: notifications_message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notifications_message (t_id_notif_detail, t_id_notif, t_id_jenis_notif, t_user_id, pesan, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permissions (id, name, guard_name, created_at, updated_at) FROM stdin;
1	MenuPendaftaran	web	2021-09-07 13:54:51	2021-09-07 13:54:51
2	SettingPemda	web	2021-09-07 13:54:51	2021-09-07 13:54:51
3	SettingKecamatan	web	2021-09-07 13:54:51	2021-09-07 13:54:51
4	SettingKelurahan	web	2021-09-07 13:54:51	2021-09-07 13:54:51
5	SettingPejabat	web	2021-09-07 13:54:51	2021-09-07 13:54:51
6	SettingNotaris	web	2021-09-07 13:54:51	2021-09-07 13:54:51
7	SettingJenisTransaksi	web	2021-09-07 13:54:51	2021-09-07 13:54:51
8	SettingPersyaratan	web	2021-09-07 13:54:51	2021-09-07 13:54:51
9	SettingHakTanah	web	2021-09-07 13:54:51	2021-09-07 13:54:51
10	SettingTarifBphtb	web	2021-09-07 13:54:51	2021-09-07 13:54:51
11	SettingTarifNpoptkp	web	2021-09-07 13:54:51	2021-09-07 13:54:51
12	SettingLoginBackground	web	2021-09-07 13:54:51	2021-09-07 13:54:51
13	SettingUser	web	2021-09-07 13:54:51	2021-09-07 13:54:51
14	SettingRole	web	2021-09-07 13:54:51	2021-09-07 13:54:51
15	SettingPermission	web	2021-09-07 13:54:51	2021-09-07 13:54:51
16	SettingAssignPermissionToRole	web	2021-09-07 13:54:51	2021-09-07 13:54:51
17	SettingAssignRoleToUser	web	2021-09-07 13:54:51	2021-09-07 13:54:51
18	SettingPersentasewajar	web	2021-09-07 13:54:51	2021-09-07 13:54:51
19	PelaporanAjb	web	2021-09-07 13:54:51	2021-09-07 13:54:51
20	Informasiop	web	2021-09-07 13:54:51	2021-09-07 13:54:51
21	PelayananKeringanan	web	2021-09-07 13:54:51	2021-09-07 13:54:51
22	PelayananKeberatan	web	2021-09-07 13:54:51	2021-09-07 13:54:51
23	PelayananAngsuran	web	2021-09-07 13:54:51	2021-09-07 13:54:51
24	PelayananPenundaan	web	2021-09-07 13:54:51	2021-09-07 13:54:51
25	PelayananPermohonanmutasi	web	2021-09-07 13:54:51	2021-09-07 13:54:51
26	PelayananPembatalan	web	2021-09-07 13:54:51	2021-09-07 13:54:51
27	Bpn	web	2021-09-07 13:54:51	2021-09-07 13:54:51
28	KppPratama	web	2021-09-07 13:54:51	2021-09-07 13:54:51
29	HistoryActivity	web	2021-09-07 13:54:51	2021-09-07 13:54:51
30	MenuValidasiBerkas	web	2021-09-07 13:54:51	2021-09-07 13:54:51
31	MenuValidasiKabid	web	2021-09-07 13:54:51	2021-09-07 13:54:51
32	Pemeriksaan	web	2021-09-07 13:54:51	2021-09-07 13:54:51
33	MenuHargaWajar	web	2021-09-07 13:54:51	2021-09-07 13:54:51
34	Pembayaran	web	2021-09-07 13:54:51	2021-09-07 13:54:51
35	Menucekniknib	web	2021-09-07 13:54:51	2021-09-07 13:54:51
36	MenuPelaporanNotaris	web	2021-09-07 13:54:51	2021-09-07 13:54:51
37	MenuHistoryTransaksi	web	2021-09-07 13:54:51	2021-09-07 13:54:51
38	SettingPass	web	2021-09-07 13:54:51	2021-09-07 13:54:51
39	DendaLaporanAjb	web	2021-09-07 13:54:51	2021-09-07 13:54:51
40	DendaAjb	web	2021-09-07 13:54:51	2021-09-07 13:54:51
41	MenuPemeriksaan	web	2021-09-07 13:54:51	2021-09-07 13:54:51
42	Skpdkb	web	2021-09-07 13:54:51	2021-09-07 13:54:51
43	Skpdkbt	web	2021-09-07 13:54:51	2021-09-07 13:54:51
44	MenuCetakLaporan	web	2021-09-07 13:54:51	2021-09-07 13:54:51
45	UserManual	web	2021-09-23 14:22:08	2021-09-23 14:22:08
\.


--
-- Data for Name: role_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role_has_permissions (permission_id, role_id) FROM stdin;
27	5
28	6
34	7
26	8
32	9
1	2
19	2
20	2
21	2
22	2
23	2
24	2
25	2
26	2
27	2
28	2
30	2
31	2
33	2
35	2
36	2
37	2
38	2
42	2
43	2
44	2
1	3
19	3
20	3
21	3
22	3
23	3
24	3
25	3
26	3
27	3
29	3
30	3
31	3
32	3
33	3
35	3
36	3
37	3
38	3
41	3
42	3
43	3
44	3
1	4
19	4
20	4
35	4
36	4
37	4
38	4
45	4
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
13	1
14	1
15	1
16	1
17	1
18	1
19	1
20	1
21	1
22	1
23	1
24	1
25	1
26	1
27	1
28	1
29	1
30	1
31	1
32	1
33	1
34	1
35	1
45	1
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name, guard_name, created_at, updated_at) FROM stdin;
1	admin	web	2021-09-07 13:54:51	2021-09-07 13:54:51
4	notaris	web	2021-09-07 13:54:51	2021-09-07 13:54:51
5	bpn	web	2021-09-07 13:54:51	2021-09-07 13:54:51
6	kpppratama	web	2021-09-07 13:54:51	2021-09-07 13:54:51
7	bendahara	web	2021-09-07 13:54:51	2021-09-07 13:54:51
8	wajibpajak	web	2021-09-07 13:54:51	2021-09-07 13:54:51
9	pemeriksa	web	2021-09-07 13:54:51	2021-09-07 13:54:51
3	validasi + pemeriksa	web	2021-09-07 13:54:51	2021-09-09 12:08:37
2	validasi	web	2021-09-07 13:54:51	2021-09-09 12:08:46
\.


--
-- Data for Name: s_acuan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_acuan (s_idacuan, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_kd_blok, s_permetertanah, created_at, updated_at) FROM stdin;
1	63	10	010	036	001	1000000	2021-09-07 13:54:51	2021-09-07 13:54:51
2	63	10	010	036	002	2000000	2021-09-07 13:54:51	2021-09-07 13:54:51
3	63	10	010	036	003	500000	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_acuan_jenistanah; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_acuan_jenistanah (s_idacuan_jenis, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_kd_blok, id_jenistanah, s_permetertanah, created_at, updated_at) FROM stdin;
1	63	10	010	036	003	1	500000	2021-09-07 13:54:51	2021-09-07 13:54:51
2	63	10	010	036	003	2	1500000	2021-09-07 13:54:51	2021-09-07 13:54:51
3	63	10	010	036	003	1	300000	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_background; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_background (id, s_thumbnail, s_id_status, s_iduser, created_at, updated_at) FROM stdin;
1	images/login-background/230321093050.jpg	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_bpn_ws; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_bpn_ws (id, s_username, s_password, created_at, updated_at) FROM stdin;
1	bpnsangihe	admin	2021-09-13 16:28:32	2021-09-13 16:28:34
\.


--
-- Data for Name: s_hak_akses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_hak_akses (s_id_hakakses, s_nama_hakakses) FROM stdin;
1	admin
2	operatorberkas
3	operatorkabid
4	notaris
5	bpn
6	kpppratama
7	operatorbendahara
8	wp
9	pemeriksa
\.


--
-- Data for Name: s_harga_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_harga_history (s_idhistory, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_kd_blok, s_kd_urut, s_kd_jenis, s_tahun_sppt, s_permetertanah, id_jenistanah, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: s_jenis_bidangusaha; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_jenis_bidangusaha (s_idbidang_usaha, s_nama_bidangusaha, created_at, updated_at) FROM stdin;
1	Pribadi	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Badan Usaha	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_jenisdoktanah; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_jenisdoktanah (s_iddoktanah, s_namadoktanah, created_at, updated_at) FROM stdin;
1	Sertifikat	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Warkah	2021-09-07 13:54:51	2021-09-07 13:54:51
3	Girik	2021-09-07 13:54:51	2021-09-07 13:54:51
4	Letter C	2021-09-07 13:54:51	2021-09-07 13:54:51
5	SPT	2021-09-07 13:54:51	2021-09-07 13:54:51
6	NIB (Nomor Identifikasi Bidang Tanah)	2021-09-07 13:54:51	2021-09-07 13:54:51
7	Akta	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_jenisfasilitas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_jenisfasilitas (s_idjenisfasilitas, s_kodejenisfasilitas, s_namajenisfasilitas, created_at, updated_at) FROM stdin;
1	01	Perwakilan diplomatik, konsulat berdasarkan asas perlakuan timbal balik	2021-09-07 13:54:51	2021-09-07 13:54:51
2	02	Negara untuk penyelenggaraan pemerintah dan atau untuk pelaksanaan pembangunan guna kepentingan umum	2021-09-07 13:54:51	2021-09-07 13:54:51
3	03	Badan atau perwakilan organisasi internasional yang ditetapkan oleh Menteri Keuangan	2021-09-07 13:54:51	2021-09-07 13:54:51
4	04	Orang pribadi atau badan karena konversi hak dan perbuatan hukum lain dengan tidak adanya perubahan nama	2021-09-07 13:54:51	2021-09-07 13:54:51
5	05	Karena wakaf atau warisan	2021-09-07 13:54:51	2021-09-07 13:54:51
6	06	Untuk digunakan kepentingan ibadah	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_jenishaktanah; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_jenishaktanah (s_idhaktanah, s_kodehaktanah, s_namahaktanah, created_at, updated_at) FROM stdin;
1	01	Hak Milik	2021-09-07 13:54:51	2021-09-07 13:54:51
2	02	Hak Guna Usaha	2021-09-07 13:54:51	2021-09-07 13:54:51
3	03	Hak Guna Bangunan	2021-09-07 13:54:51	2021-09-07 13:54:51
4	04	Hak Pakai	2021-09-07 13:54:51	2021-09-07 13:54:51
5	05	Hak Milik Atas Satuan Rumah	2021-09-07 13:54:51	2021-09-07 13:54:51
6	05	Hak Pengelolaan	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_jenisketetapan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_jenisketetapan (s_idjenisketetapan, s_namajenisketetapan, s_namasingkatjenisketetapan, created_at, updated_at) FROM stdin;
1	Surat Setoran Pajak Daerah	SSPD	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Surat Ketetapan Pajak Daerah Kurang Bayar	SKPDKB	2021-09-07 13:54:51	2021-09-07 13:54:51
3	Surat Ketetapan Pajak Daerah Kurang Bayar Tambahan	SKPDKBT	2021-09-07 13:54:51	2021-09-07 13:54:51
4	Surat Ketetapan Pajak Daerah Lebih Bayar	SKPDLB	2021-09-07 13:54:51	2021-09-07 13:54:51
5	Surat Ketetapan Pajak Daerah Nihil	SKPDN	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_jenispengurangan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_jenispengurangan (s_idpengurangan, s_persentase, s_namapengurangan, created_at, updated_at) FROM stdin;
1	50	Waris / Hibah Wasiat	2021-09-07 13:54:51	2021-09-07 13:54:51
2	75	Pensiunan TNI/POLRI 2	2021-09-07 13:54:51	2021-09-07 13:54:51
3	75	Perumahan Dinas TNI/POLRI	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_jenistanah; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_jenistanah (id_jenistanah, nama_jenistanah, created_at, updated_at) FROM stdin;
1	Bukan Perumahan	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Perumahan	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_jenistransaksi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_jenistransaksi (s_idjenistransaksi, s_kodejenistransaksi, s_namajenistransaksi, s_idstatus_pht, s_id_dptnpoptkp, created_at, updated_at) FROM stdin;
1	01	Jual Beli	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
2	02	Tukar Menukar	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
3	03	Hibah	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
4	04	Hibah Wasiat	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
5	05	Waris	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
6	06	Pemasukan Dalam Perseroan atau Badan Hukum Lainnya	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
7	07	Pemisahan Hak yang Mengakibatkan Peralihan	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
8	08	Penunjukan Pembeli Dalam Lelang	3	2	2021-09-07 13:54:51	2021-09-07 13:54:51
9	09	Pelaksanaan Putusan Hakim yang Mempunyai Kekuatan Hukum Tetap	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
10	10	Penggabungan Usaha	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
11	11	Peleburan Usaha	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
12	12	Pemekaran Usaha	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
13	13	Hadiah	3	2	2021-09-07 13:54:51	2021-09-07 13:54:51
14	14	Perolehan Hak Rumah Sederhana Sehat dan RSS Melalui KPR Bersubsidi	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
15	15	Pemberian Hak Baru Sebagai Kelanjutan Pelepasan Hak	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
16	16	Pemberian Hak Baru Diluar Pelepasan Hak	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_kecamatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_kecamatan (s_idkecamatan, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_namakecamatan, s_latitude, s_longitude, created_at, updated_at) FROM stdin;
12	71	04	100	KENDAHE	\N	\N	\N	\N
13	71	04	070	MANGANITU	\N	\N	\N	\N
14	71	04	170	MANGANITU SELATAN	\N	\N	\N	\N
15	71	04	150	NUSA TABUKAN	\N	\N	\N	\N
16	71	04	050	TABUKAN SELATAN	\N	\N	\N	\N
17	71	04	180	TABUKAN SELATAN TENGAH	\N	\N	\N	\N
18	71	04	190	TABUKAN SELATAN TENGGARA	\N	\N	\N	\N
19	71	04	060	TABUKAN TENGAH	\N	\N	\N	\N
20	71	04	090	TABUKAN UTARA	\N	\N	\N	\N
21	71	04	091	KEPULAUAN MARORE	\N	\N	\N	\N
22	71	04	080	TAHUNA	\N	\N	\N	\N
23	71	04	082	TAHUNA BARAT	\N	\N	\N	\N
24	71	04	081	TAHUNA TIMUR	\N	\N	\N	\N
25	71	04	040	TAMAKO	\N	\N	\N	\N
26	71	04	160	TATOARENG	\N	\N	\N	\N
1	71	04	000	LUAR DAERAH	\N	\N	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_kelurahan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_kelurahan (s_idkelurahan, s_idkecamatan, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_namakelurahan, s_latitude, s_longitude, created_at, updated_at) FROM stdin;
1	1	63	10	000	000	LUAR DAERAH	\N	\N	2021-09-07 13:54:51	2021-09-07 13:54:51
176	21	71	04	091	018	MARORE	\N	\N	\N	\N
177	21	71	04	091	009	KAWIO	\N	\N	\N	\N
178	15	71	04	150	001	NUSA	\N	\N	\N	\N
179	15	71	04	150	002	NANEDAKELE	\N	\N	\N	\N
180	15	71	04	150	003	BUKIDE	\N	\N	\N	\N
181	15	71	04	150	004	BUKIDE TIMUR	\N	\N	\N	\N
182	26	71	04	160	001	PARA	\N	\N	\N	\N
183	26	71	04	160	002	MAHENGETANG	\N	\N	\N	\N
184	26	71	04	160	003	KAHAKITANG	\N	\N	\N	\N
185	26	71	04	160	004	KALAMA	\N	\N	\N	\N
186	21	71	04	091	003	MATUTUANG	\N	\N	\N	\N
187	17	71	04	180	001	LEHUPU	\N	\N	\N	\N
188	17	71	04	180	002	TAMBUNG	\N	\N	\N	\N
189	20	71	04	090	016	PETTA TIMUR	\N	\N	\N	\N
190	20	71	04	090	015	PUSUNGE	\N	\N	\N	\N
191	19	71	04	060	013	TIMBELANG	\N	\N	\N	\N
192	19	71	04	060	014	RENDINGAN	\N	\N	\N	\N
193	16	71	04	050	001	BATUWINGKUNG	\N	\N	\N	\N
194	25	71	04	040	004	MENGGAWA II	\N	\N	\N	\N
195	17	71	04	180	003	BEENG DARAT	\N	\N	\N	\N
196	17	71	04	180	004	SALURANG	\N	\N	\N	\N
197	17	71	04	180	005	HANGKE	\N	\N	\N	\N
198	17	71	04	180	006	BOWONE	\N	\N	\N	\N
199	18	71	04	190	001	PINTARENG	\N	\N	\N	\N
200	18	71	04	190	002	BASAUH	\N	\N	\N	\N
201	18	71	04	190	003	TUMALEDE	\N	\N	\N	\N
202	25	71	04	040	003	LELIPANG	\N	\N	\N	\N
203	19	71	04	060	015	PALAHANAENG	\N	\N	\N	\N
204	22	71	04	080	015	SOATALOARA II	\N	\N	\N	\N
205	22	71	04	080	016	BUNGALAWANG	\N	\N	\N	\N
206	22	71	04	080	017	SANTIAGO	\N	\N	\N	\N
207	22	71	04	080	018	MANENTE	\N	\N	\N	\N
208	24	71	04	081	001	LESA	\N	\N	\N	\N
209	24	71	04	081	019	ENENGPAHEMBANG	\N	\N	\N	\N
210	24	71	04	081	002	BATULEWEHE	\N	\N	\N	\N
211	24	71	04	081	020	TAPUANG	\N	\N	\N	\N
212	24	71	04	081	003	TIDORE	\N	\N	\N	\N
213	24	71	04	081	004	TONA I	\N	\N	\N	\N
214	24	71	04	081	021	TONA II	\N	\N	\N	\N
215	23	71	04	082	010	PANANEKENG	\N	\N	\N	\N
216	23	71	04	082	011	ANGGES	\N	\N	\N	\N
217	23	71	04	082	012	KOLONGAN MITUNG	\N	\N	\N	\N
218	23	71	04	082	013	KOLONGAN BEHA	\N	\N	\N	\N
219	23	71	04	082	022	BEHA BARU	\N	\N	\N	\N
220	23	71	04	082	014	KOLONGAN AKEMBAWI	\N	\N	\N	\N
221	20	71	04	090	017	MOADE	\N	\N	\N	\N
222	22	71	04	080	019	ENENGPAHEMBANG	\N	\N	\N	\N
223	18	71	04	190	004	SAMPAKANG	\N	\N	\N	\N
224	18	71	04	190	005	DALOKAWENG	\N	\N	\N	\N
225	18	71	04	190	006	MALISADE	\N	\N	\N	\N
226	24	71	04	081	005	DUMUHUNG	\N	\N	\N	\N
227	20	71	04	090	019	PETTA BARAT	\N	\N	\N	\N
228	20	71	04	090	020	PETTA SELATAN	\N	\N	\N	\N
229	20	71	04	090	021	LIKUANG	\N	\N	\N	\N
230	14	71	04	170	004	PINDANG	\N	\N	\N	\N
231	16	71	04	050	002	BUKIDE	\N	\N	\N	\N
232	16	71	04	050	003	LAOTONGAN	\N	\N	\N	\N
233	14	71	04	170	003	LAPEPAHE	\N	\N	\N	\N
234	17	71	04	180	007	BEENG LAUT	\N	\N	\N	\N
235	26	71	04	160	005	TALEKO	\N	\N	\N	\N
236	26	71	04	160	006	DALAKO BEMBANEHE	\N	\N	\N	\N
237	14	71	04	170	002	MAWIRA	\N	\N	\N	\N
238	25	71	04	040	002	KALAMA DARAT	\N	\N	\N	\N
239	20	71	04	090	022	RAKU	\N	\N	\N	\N
240	20	71	04	090	023	KALEKUBE I	\N	\N	\N	\N
241	19	71	04	060	016	PELELANGENG	\N	\N	\N	\N
242	25	71	04	040	018	MAHUMU I	\N	\N	\N	\N
243	25	71	04	040	019	MAHUMU II	\N	\N	\N	\N
244	25	71	04	040	001	HESANG	\N	\N	\N	\N
245	16	71	04	050	004	KALAGHENG	\N	\N	\N	\N
246	16	71	04	050	005	LESABE I	\N	\N	\N	\N
247	13	71	04	070	015	BENGKA	\N	\N	\N	\N
248	25	71	04	040	020	KALINDA I	\N	\N	\N	\N
249	19	71	04	060	017	MALUENG	\N	\N	\N	\N
250	26	71	04	160	007	PARA I	\N	\N	\N	\N
251	19	71	04	060	018	KUMA I	\N	\N	\N	\N
252	15	71	04	150	005	NANUSA	\N	\N	\N	\N
253	16	71	04	050	006	BULO	\N	\N	\N	\N
254	17	71	04	180	008	TENDA	\N	\N	\N	\N
255	17	71	04	180	009	AHA PATUNG	\N	\N	\N	\N
256	14	71	04	170	001	MANGSEL	\N	\N	\N	\N
257	14	71	04	170	013	LEHIMI TARIANG	\N	\N	\N	\N
258	14	71	04	170	014	LAPANGO I	\N	\N	\N	\N
259	13	71	04	070	020	TALOARANE I	\N	\N	\N	\N
260	13	71	04	070	014	PINEBENTENGANG	\N	\N	\N	\N
261	20	71	04	090	024	NAHA I	\N	\N	\N	\N
262	20	71	04	090	025	BOWONGKULU I	\N	\N	\N	\N
263	13	71	04	070	012	HIUNG	\N	\N	\N	\N
264	13	71	04	070	013	BAKALAENG	\N	\N	\N	\N
265	25	71	04	040	005	NAGHA DUA	\N	\N	\N	\N
266	25	71	04	040	006	ULUNGPELIANG	\N	\N	\N	\N
267	25	71	04	040	007	BINALA	\N	\N	\N	\N
268	25	71	04	040	008	BALANE	\N	\N	\N	\N
269	25	71	04	040	009	NAGHA SATU	\N	\N	\N	\N
270	25	71	04	040	010	POKOL	\N	\N	\N	\N
271	25	71	04	040	011	MENGGAWA	\N	\N	\N	\N
272	25	71	04	040	012	KALINDA	\N	\N	\N	\N
273	25	71	04	040	013	BEBU	\N	\N	\N	\N
274	25	71	04	040	014	PANANARU	\N	\N	\N	\N
275	25	71	04	040	015	DAGHO	\N	\N	\N	\N
276	25	71	04	040	016	MAHUMU	\N	\N	\N	\N
277	25	71	04	040	017	MAKALEKUHE	\N	\N	\N	\N
278	16	71	04	050	009	MANDOI	\N	\N	\N	\N
279	16	71	04	050	010	BINEBASE	\N	\N	\N	\N
280	16	71	04	050	012	PALARENG	\N	\N	\N	\N
281	16	71	04	050	013	MALAMENGGU	\N	\N	\N	\N
282	16	71	04	050	014	LESABE	\N	\N	\N	\N
283	16	71	04	050	015	BENTUNG	\N	\N	\N	\N
284	16	71	04	050	016	SIMUENG	\N	\N	\N	\N
285	16	71	04	050	017	BIRAHI	\N	\N	\N	\N
286	19	71	04	060	001	BOWONGKALI	\N	\N	\N	\N
287	19	71	04	060	002	KULUR II	\N	\N	\N	\N
288	19	71	04	060	003	KULUR I	\N	\N	\N	\N
289	19	71	04	060	004	BIRA	\N	\N	\N	\N
290	19	71	04	060	005	KUMA	\N	\N	\N	\N
291	19	71	04	060	006	BUNGALAWANG	\N	\N	\N	\N
292	19	71	04	060	007	MIULU	\N	\N	\N	\N
293	19	71	04	060	008	GUNUNG	\N	\N	\N	\N
294	19	71	04	060	009	TALENGEN	\N	\N	\N	\N
295	19	71	04	060	010	BIRU	\N	\N	\N	\N
296	19	71	04	060	011	TARIANG BARU	\N	\N	\N	\N
297	19	71	04	060	012	SENSONG	\N	\N	\N	\N
298	13	71	04	070	001	BARANGKALANG	\N	\N	\N	\N
299	13	71	04	070	002	BELENGANG	\N	\N	\N	\N
300	13	71	04	070	003	LEBO	\N	\N	\N	\N
301	13	71	04	070	004	SESIWUNG	\N	\N	\N	\N
302	13	71	04	070	005	KAUHIS	\N	\N	\N	\N
303	13	71	04	070	006	KARATUNG I	\N	\N	\N	\N
304	13	71	04	070	007	MALA	\N	\N	\N	\N
305	13	71	04	070	008	TALOARANE	\N	\N	\N	\N
306	13	71	04	070	009	MANUMPITAENG	\N	\N	\N	\N
307	13	71	04	070	010	BARANGKA	\N	\N	\N	\N
308	13	71	04	070	011	TAWOALI	\N	\N	\N	\N
309	13	71	04	070	018	KARATUNG II	\N	\N	\N	\N
310	13	71	04	070	019	NAHEPESE	\N	\N	\N	\N
311	22	71	04	080	001	LESA	\N	\N	\N	\N
312	22	71	04	080	002	BATULEWEHE	\N	\N	\N	\N
313	22	71	04	080	003	TIDORE	\N	\N	\N	\N
314	22	71	04	080	004	TONA	\N	\N	\N	\N
315	22	71	04	080	005	DUMUHUNG	\N	\N	\N	\N
316	22	71	04	080	006	SOATALOARA I	\N	\N	\N	\N
317	22	71	04	080	007	SAWANG BENDAR	\N	\N	\N	\N
318	22	71	04	080	008	APENGSEMBEKA	\N	\N	\N	\N
319	22	71	04	080	009	MAHENA	\N	\N	\N	\N
320	22	71	04	080	011	ANGGES	\N	\N	\N	\N
321	20	71	04	090	001	BOWONGKULU	\N	\N	\N	\N
322	20	71	04	090	002	PETTA	\N	\N	\N	\N
323	20	71	04	090	003	BENGKETANG	\N	\N	\N	\N
324	20	71	04	090	004	TOLA	\N	\N	\N	\N
325	20	71	04	090	005	TAROLANG	\N	\N	\N	\N
326	20	71	04	090	006	LENGANENG	\N	\N	\N	\N
327	20	71	04	090	007	UTAURANO	\N	\N	\N	\N
328	20	71	04	090	008	KALURAE	\N	\N	\N	\N
329	20	71	04	090	009	NAHA	\N	\N	\N	\N
330	20	71	04	090	010	BEHA	\N	\N	\N	\N
331	20	71	04	090	011	KALEKUBE	\N	\N	\N	\N
332	20	71	04	090	012	MALA	\N	\N	\N	\N
333	20	71	04	090	013	BAHU	\N	\N	\N	\N
334	20	71	04	090	014	KALASUGE	\N	\N	\N	\N
335	20	71	04	090	018	MARORE	\N	\N	\N	\N
336	12	71	04	100	001	KENDAHE II	\N	\N	\N	\N
337	12	71	04	100	002	KENDAHE I	\N	\N	\N	\N
338	12	71	04	100	003	TALAWID	\N	\N	\N	\N
339	12	71	04	100	004	TARIANG LAMA	\N	\N	\N	\N
340	12	71	04	100	005	PEMPALARAENG	\N	\N	\N	\N
341	12	71	04	100	006	MOHONGSAWANG	\N	\N	\N	\N
342	12	71	04	100	007	LIPAENG	\N	\N	\N	\N
343	12	71	04	100	008	KAWALUSO	\N	\N	\N	\N
344	12	71	04	100	009	KAWIO	\N	\N	\N	\N
345	14	71	04	170	005	NGALIPAENG I	\N	\N	\N	\N
346	14	71	04	170	006	NGALIPAENG II	\N	\N	\N	\N
347	14	71	04	170	007	BEBALANG	\N	\N	\N	\N
348	14	71	04	170	008	BATUNDERANG	\N	\N	\N	\N
349	14	71	04	170	009	SOWAENG	\N	\N	\N	\N
350	14	71	04	170	010	LAPANGO	\N	\N	\N	\N
351	14	71	04	170	011	LAINE	\N	\N	\N	\N
352	14	71	04	170	012	KALUWATU	\N	\N	\N	\N
\.


--
-- Data for Name: s_koderekening; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_koderekening (s_korekid, s_korektipe, s_korekkelompok, s_korekjenis, s_korekobjek, s_korekrincian, s_korekrinciansub, s_koreknama, s_korekketerangan, created_at, updated_at) FROM stdin;
1	4	1	1	11			Bea Perolehan Hak Atas Tanah dan Bangunan (BPHTB) - LRA Bea Perolehan Hak Atas Tanah dan Bangunan (BPHTB) - LRA		2021-09-07 13:54:51	2021-09-07 13:54:51
2	4	1	1	11	01		BPHTB		2021-09-07 13:54:51	2021-09-07 13:54:51
3	4	1	7	11	01		Denda BPHTB		2021-09-07 13:54:51	2021-09-07 13:54:51
4	4	1	7	11	02		Sanksi BPHTB		2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_notaris; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_notaris (s_idnotaris, s_namanotaris, s_alamatnotaris, s_kodenotaris, s_sknotaris, s_noid_bpn, s_tgl1notaris, s_tgl2notaris, s_statusnotaris, s_npwpd_notaris, s_daerahkerja, created_at, updated_at) FROM stdin;
1	AMELIA NOVITA DANDEL, SH,M.Kn	-	01	-	1	2020-01-01 00:00:00	2020-12-31 00:00:00	1	123	Lilin	2021-09-07 13:54:49	2021-09-20 13:58:07
3	CAMAT TAHUNA, RONIJAYA PASIAYE S.SoS, ME	-	03	-	\N	\N	\N	1	\N	\N	2021-09-20 13:58:56	2021-09-20 13:58:56
4	CAMAT TAHUNA TIMUR, STEVEN R. ANDAKI, SP	-	04	-	\N	\N	\N	1	\N	\N	2021-09-20 13:59:39	2021-09-20 13:59:39
5	CAMAT TABUT HASYIM SAMALAN S. SoS	-	05	-	\N	\N	\N	1	\N	\N	2021-09-20 14:00:11	2021-09-20 14:00:11
6	BPKPD PELAYANAN	-	06	-	\N	\N	\N	1	\N	\N	2021-09-20 14:01:10	2021-09-20 14:01:10
8	STEVENSON ANDAKI, SP	-	08	-	\N	\N	\N	1	\N	\N	2021-09-20 14:03:24	2021-09-20 14:03:24
9	CHRESTIAN D SALILO, A.Pnth	-	09	-	\N	\N	\N	1	\N	\N	2021-09-20 14:04:00	2021-09-20 14:04:00
10	GRIETHA E. KARAMBUT, S.STP	-	10	-	\N	\N	\N	1	\N	\N	2021-09-20 14:05:53	2021-09-20 14:05:53
2	KUSTANTI, S.H.,M. Kn	-	02	-	2	2021-09-21 00:00:00	2020-12-31 00:00:00	1	123	Lilin	2021-09-07 13:54:49	2021-09-20 13:58:31
7	KEPALA BPN SANGIHE	-	07	-	\N	\N	\N	1	\N	\N	2021-09-20 14:02:15	2021-09-27 15:05:37
\.


--
-- Data for Name: s_pejabat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_pejabat (s_idpejabat, s_namapejabat, s_jabatanpejabat, s_nippejabat, s_golonganpejabat, s_alamat, s_filettd, created_at, updated_at) FROM stdin;
1	JEVERMON MALO, SE	Kabid Pendapatan	196801202002121003	Pembina /IVa	-		2021-09-07 13:54:49	2021-09-09 11:55:59
2	IMELDA V. L. LESAWENGEN, SE	Kasubid Penetapan, Keberatan dan Penyelesaian Tuunggakan	198002162009032001	Penata, III/c	-		2021-09-07 13:54:49	2021-09-09 12:00:00
3	BENYAMIN MANDIANGAN A. m.Ak	Kasubid Pendataan, Pendaftaran dan Penilaian	197604292002121005	Penata, III/c	-	\N	2021-09-09 12:01:16	2021-09-09 12:01:51
4	YANSYE ERLINA MANUMPIL	Staf Pelaksana	197501012000032009	Penata Muda Tingkat I, III/b	-	\N	2021-09-09 12:03:24	2021-09-09 12:03:24
5	SINUS A RAKINAUNG	Staf Bidang Pendapatan	196910162006041004	Pengatur Tk. I, II/d	-	\N	2021-09-09 12:05:01	2021-09-09 12:05:01
6	WELLIAM M. MEKUTIKA	Staf Pelaksana	197905072006041016	Pengatur Muda Tingkat I, II/B	-	\N	2021-09-09 12:06:15	2021-09-09 12:06:15
7	ARDYANTO TALOLANG, SS	Tenaga Honorer	000000000000000000	-	-	\N	2021-09-09 12:07:19	2021-09-09 12:07:19
\.


--
-- Data for Name: s_pemda; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_pemda (s_idpemda, s_namaprov, s_namakabkot, s_namaibukotakabkot, s_kodeprovinsi, s_kodekabkot, s_namainstansi, s_namasingkatinstansi, s_alamatinstansi, s_notelinstansi, s_namabank, s_norekbank, s_latitude, s_longitude, s_logo, s_namacabang, s_kodecabang, s_kodepos, s_email, s_urlbphtb, created_at, updated_at) FROM stdin;
1	Sulawesi Utara	Kabupaten Kepulauan Sangihe	Tahuna	71	03	Badan Pengelola Keuangan dan Pendapatan Daerah	BPKPD	JF6X+VVC, Soataloara I	(0355) 320098	BPKPD	149.30.00001	-8.65111430448619	116.54075028714753	public/dist/img/uguYnYYPEeuSB7dVBE361NkPuhrqM2Z0aCZ2bdzN.png	-	08	83612	bpkpd@sangihe.co.id	\N	2021-09-07 13:54:51	2021-09-25 07:39:22
\.


--
-- Data for Name: s_persyaratan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_persyaratan (s_idpersyaratan, s_idjenistransaksi, s_namapersyaratan, s_idwajib_up, s_lokasimenu, "order", created_at, updated_at) FROM stdin;
4	1	Bukti Kwitansi / Surat Pernyataan perolehan harga transaksi Jual Beli	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
3	1	Foto Copy Kartu Keluarga ( Penjual )	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
5	1	Foto Copy KTP Suami dan Istri ( Penjual )	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
2	1	Foto copy Akte Perkawinan / Surat Nikah ( Penjual )	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
8	2	FC. Kartu Keluarga / KK	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
9	2	FC. KTP Penerima Hak Tukar	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
10	2	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
11	2	Pernyataan Harga Transaksi	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
12	2	Surat Kuasa materai 6000	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
13	2	Foto copy KTP yang diberi kuasa	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
14	2	FC. KTP Pemberi Hak Tukar	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
15	2	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
16	2	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	9	2021-09-07 13:54:51	2021-09-07 13:54:51
17	3	Surat Kuasa materai 6000	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
18	3	Foto copy KTP Pemberi Hibah	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
19	3	FC. Kartu Identitas / KTP / Surat Domisili Dari Desa	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
20	3	Foto copy KTP yang diberi kuasa	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
21	3	Pernyataan Harga Transaksi	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
22	3	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
23	3	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
24	3	FC. Kartu Keluarga / KK	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
25	4	Surat Kuasa materai 6000	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
26	4	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
27	4	FC. Surat Kematian	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
28	4	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
29	4	Foto copy KTP yang diberi kuasa	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
30	4	FC. Kartu Identitas / KTP / Surat Domisili Dari Desa	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
31	4	Pernyataan Harga Transaksi	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
32	4	FC. Kartu Keluarga / KK	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
33	4	Surat Wasiat	2	1	9	2021-09-07 13:54:51	2021-09-07 13:54:51
34	5	KTP Penerima Kuasa	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
35	5	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
36	5	Foto Lokasi Objek Pajak 	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
37	5	FC. Kartu Keluarga / KK	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
38	5	FC. Kartu Identitas / KTP / Surat Domisili Dari Desa	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
39	5	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
40	5	FC. Surat Kematian	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
41	5	FC. Surat Keterangan Waris / SKW	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
42	5	Surat Kuasa bermaterei 6 ribu	2	1	9	2021-09-07 13:54:51	2021-09-07 13:54:51
43	5	Surat Pernyataan Nilai Pasar	2	1	10	2021-09-07 13:54:51	2021-09-07 13:54:51
44	6	Foto copy KTP yang diberi kuasa	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
45	6	FC. KTP	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
46	6	Akta Badan Hukum	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
47	6	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
48	6	FC. Kartu Keluarga / KK	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
49	6	Pernyataan Harga Transaksi	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
50	6	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
52	6	Surat Kuasa materai 6000	2	1	9	2021-09-07 13:54:51	2021-09-07 13:54:51
53	7	Foto copy KTP yang diberi kuasa	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
54	7	Surat Kuasa materai 6000	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
55	7	Pernyataan Harga Transaksi	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
56	7	FC. Kartu Keluarga / KK	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
57	7	FC. Bukti Kepemilikan (Sertifikat, Warkah. SPT)	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
58	7	FC. KTP	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
59	7	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
60	7	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
61	8	FC. KTP	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
62	8	FC. Bukti Kepemilikan (Sertifikat, Warkah. SPT)	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
63	8	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
64	8	Pernyataan Harga Transaksi	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
65	8	Surat Kuasa materai 6000	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
66	8	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
67	8	Risalah/Tanda Penerima Uang Hasil lelang dari Kantor Pelayanan Kakayaan Negara dan Lelang Malang	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
68	8	Foto copy KTP yang diberi kuasa	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
69	8	FC. Kartu Keluarga / KK	2	1	9	2021-09-07 13:54:51	2021-09-07 13:54:51
70	9	Surat Kuasa materai 6000	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
71	9	Fotokopi Sertifikat	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
72	9	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
73	9	FC. KTP	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
74	9	FC. Kartu Keluarga / KK	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
75	9	Putusan Pengadilan	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
76	9	Foto copy KTP yang diberi kuasa	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
6	1	Foto copy KTP Pembeli	2	1	6	2021-09-07 13:54:51	2021-09-25 17:36:44
1	1	Foto copy SPPT tahun bersangkutan dan Foto copy Bukti pembayaran PBB / STTS	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
78	9	Pernyataan Harga Transaksi	2	1	9	2021-09-07 13:54:51	2021-09-07 13:54:51
79	10	Pernyataan Harga Transaksi	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
80	10	Fotokopi Sertifikat	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
81	10	FC. KTP	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
82	10	Surat Kuasa materai 6000	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
83	10	Foto copy KTP yang diberi kuasa	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
85	10	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
86	10	FC. Kartu Keluarga / KK	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
87	11	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
88	11	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
89	11	FC. Kartu Keluarga / KK	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
90	11	FC. KTP	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
91	11	Foto copy KTP yang diberi kuasa	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
92	11	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
93	11	Pernyataan Harga Transaksi	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
94	11	Surat Kuasa materai 6000	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
95	12	Pernyataan Harga Transaksi	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
96	12	SPPT PBB Tahun Terakhir	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
97	12	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
98	12	Foto copy KTP yang diberi kuasa	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
99	12	Surat Kuasa materai 6000	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
100	12	FC. Kartu Keluarga / KK	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
101	12	FC. KTP	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
102	12	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
103	13	Pernyataan Harga Transaksi	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
104	13	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
105	13	Fotokopi Sertifikat	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
106	13	FC. Kartu Keluarga / KK	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
107	13	FC. KTP Penerima Hadiah	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
108	13	FC. KTP Pemberi Hadiah	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
109	13	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
110	13	Surat Kuasa materai 6000	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
111	13	Foto copy KTP yang diberi kuasa	2	1	9	2021-09-07 13:54:51	2021-09-07 13:54:51
112	14	Foto copy KTP yang diberi kuasa	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
113	14	FC. Kartu Keluarga / KK	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
115	14	Surat Kuasa materai 6000	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
116	14	Pernyataan Harga Transaksi	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
117	14	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
118	14	FC. KTP	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
119	14	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
120	15	FC. KTP	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
121	15	Fotokopi SPT (Surat Tanah)	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
122	15	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
123	15	 Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
124	15	Surat Kuasa materai 6000	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
125	15	Foto copy KTP yang diberi kuasa	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
126	15	FC. Kartu Keluarga / KK	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
127	15	Pernyataan Harga Transaksi	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
128	16	 Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
129	16	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
130	16	FC. Kartu Keluarga / KK	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
131	16	Pernyataan Harga Transaksi	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
132	16	Foto copy KTP yang diberi kuasa	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
133	16	FC. KTP	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
134	16	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
135	16	Surat Kuasa materai 6000	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
136	17	Foto copy KTP yang diberi kuasa	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
137	17	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
138	17	FC. Kartu Keluarga / KK	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
139	17	Surat Kuasa materai 6000	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
140	17	FC. KTP	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
141	17	Pernyataan Harga Transaksi	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
142	17	 Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
144	18	Akta PHB	2	1	1	2021-09-07 13:54:51	2021-09-07 13:54:51
145	18	FC.Kartu Keluarga/KK	2	1	2	2021-09-07 13:54:51	2021-09-07 13:54:51
146	18	FC. Surat Kematian	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
147	18	FC. Surat Keterangan Waris / SKW	2	1	4	2021-09-07 13:54:51	2021-09-07 13:54:51
148	18	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	5	2021-09-07 13:54:51	2021-09-07 13:54:51
149	18	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
150	18	Formulir Surat Setoran Pajak Daerah BPHTB/ SSPD BPHTB	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
151	18	FC. Kartu Identitas/ KTP/ Surat Domisili dari desa	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
7	1	Foto copy NPWP ( Penjual )	2	1	7	2021-09-07 13:54:51	2021-09-07 13:54:51
51	6	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
77	9	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
84	10	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	6	2021-09-07 13:54:51	2021-09-07 13:54:51
114	14	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	2	1	3	2021-09-07 13:54:51	2021-09-07 13:54:51
143	17	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	2	1	8	2021-09-07 13:54:51	2021-09-07 13:54:51
152	1	Denah Objek Jual Beli	2	1	8	2021-09-26 17:47:05	2021-09-26 17:47:05
153	1	Foto copy NPWP ( Pembeli )	2	1	9	2021-09-26 17:48:10	2021-09-26 17:48:10
154	1	Foto copy Kartu Keluarga ( Pembeli )	2	1	10	2021-09-26 17:48:47	2021-09-27 11:02:51
\.


--
-- Data for Name: s_presentase_wajar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_presentase_wajar (s_idpresentase_wajar, s_nilaimin, s_nilaimax, s_ketpresentase_wajar, s_css_warna, created_at, updated_at) FROM stdin;
1	0	50	Tidak Wajar	red	2021-09-07 13:54:51	2021-09-07 13:54:51
2	50.1	60	Kurang Wajar	yellow	2021-09-07 13:54:51	2021-09-07 13:54:51
3	60.1	80	Sedang	blue	2021-09-07 13:54:51	2021-09-07 13:54:51
4	80.1	100	Wajar	green	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_status (s_id_status, s_nama_status, created_at, updated_at) FROM stdin;
1	Aktif	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Tidak	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_status_bayar; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_status_bayar (s_id_status_bayar, s_nama_status_bayar, created_at, updated_at) FROM stdin;
1	Sudah	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Belum	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_status_berkas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_status_berkas (s_id_status_berkas, s_nama_status_berkas, created_at, updated_at) FROM stdin;
1	Lengkap	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Tidak Lengkap	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_status_dptnpoptkp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_status_dptnpoptkp (s_id_dptnpoptkp, s_nama_dptnpoptkp, created_at, updated_at) FROM stdin;
1	NPOPTKP Setiap Transaksi Dapat	2021-09-07 13:54:51	2021-09-07 13:54:51
2	NPOPTKP 1 Tahun Sekali dapatnya	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_status_kabid; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_status_kabid (s_id_status_kabid, s_nama_status_kabid, created_at, updated_at) FROM stdin;
1	Setuju	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Tidak Setuju	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_status_lihat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_status_lihat (s_id_status_lihat, s_nama_status_lihat, created_at, updated_at) FROM stdin;
1	Sudah	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Belum	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_status_npwpd; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_status_npwpd (s_idstatus_npwpd, s_nama_status, created_at, updated_at) FROM stdin;
1	Ada	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Tidak Ada	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_status_pelayanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_status_pelayanan (s_id_status_layanan, s_nama_status_layanan, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: s_status_perhitungan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_status_perhitungan (s_idstatus_pht, s_nama, created_at, updated_at) FROM stdin;
1	Dari Perbandingan NJOP dengan Nilai Transaksi	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Dari NJOP PBB	2021-09-07 13:54:51	2021-09-07 13:54:51
3	Dari Nilai Transaksi	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_target_bphtb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_target_bphtb (s_id_target_bphtb, s_id_target_status, s_tahun_target, s_nilai_target, s_keterangan, created_at, updated_at) FROM stdin;
1	1	2020	1000000000	Target BPHTB Murni	2021-09-07 13:54:51	2021-09-07 13:54:51
2	2	2020	1600000000	Target BPHTB Perubahan	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_target_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_target_status (s_id_target_status, s_nama_status, created_at, updated_at) FROM stdin;
1	Murni	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Perubahan	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_tarif_bphtb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_tarif_bphtb (s_idtarifbphtb, s_tarif_bphtb, s_tgl_awal, s_tgl_akhir, s_dasar_hukum, created_at, updated_at) FROM stdin;
1	5	2015-01-01	2030-12-01		2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_tarifnpoptkp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_tarifnpoptkp (s_idtarifnpoptkp, s_idjenistransaksinpoptkp, s_tarifnpoptkp, s_tarifnpoptkptambahan, s_dasarhukumnpoptkp, s_statusnpoptkp, s_tglberlaku_awal, s_tglberlaku_akhir, created_at, updated_at) FROM stdin;
1	1	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
2	2	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
3	3	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
4	4	300000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
5	5	300000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
6	6	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
7	7	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
8	8	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
9	9	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
10	10	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
11	11	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
12	12	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
13	13	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
14	14	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
15	15	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
16	16	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_tempo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_tempo (s_idtempo, s_haritempo, s_tglberlaku_awal, s_tglberlaku_akhir, s_id_status, created_at, updated_at) FROM stdin;
1	5	2015-01-01 00:00:00	2030-12-01 00:00:00	1	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: s_users_api; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_users_api (s_idusers_api, s_username, s_password, created_at, updated_at) FROM stdin;
1	bapendakabsangihe	a	2021-09-20 14:56:27	2021-09-20 14:56:28
\.


--
-- Data for Name: s_wajib_up; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_wajib_up (s_idwajib_up, s_ket_wjb, created_at, updated_at) FROM stdin;
1	Wajib	2021-09-07 13:54:51	2021-09-07 13:54:51
2	Tidak	2021-09-07 13:54:51	2021-09-07 13:54:51
\.


--
-- Data for Name: t_bpn; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_bpn (t_idbpn, t_noakta, t_tglakta, t_namappat, t_nop, t_nib, t_ntpd, t_nik, t_npwp, t_namawp, t_kelurahanop, t_kecamatanop, t_kotaop, t_luastanahop, t_jenishak, t_koordinat_x, t_koordinat_y, t_tgltransaksi, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_file_keringanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_file_keringanan (t_idfile_keringanan, t_idspt, t_iduser_upload, letak_file, nama_file, t_tgl_upload, t_keterangan_file, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_file_objek; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_file_objek (t_idfile_objek, t_idspt, t_iduser_upload, letak_file, nama_file, t_tgl_upload, t_keterangan_file, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_filesyarat_bphtb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_filesyarat_bphtb (t_id_filesyarat, t_idspt, s_idpersyaratan, s_idjenistransaksi, t_iduser_upload, letak_file, nama_file, t_tgl_upload, t_keterangan_file, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_inputajb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_inputajb (t_idajb, t_idspt, t_tgl_ajb, t_no_ajb, t_iduser_input, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_laporbulanan_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_laporbulanan_detail (t_idlaporbulanan, t_idajb, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_laporbulanan_head; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_laporbulanan_head (t_idlaporbulanan, t_no_lapor, t_tgl_lapor, t_untuk_bulan, t_untuk_tahun, t_keterangan, t_iduser_input, created_at, updated_at) FROM stdin;
2	1	2021-09-27 00:00:00	9	2021	test	1	2021-09-27 15:10:28	2021-09-27 15:10:28
\.


--
-- Data for Name: t_nik_bersama; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_nik_bersama (t_id_nik_bersama, t_idspt, t_nik_bersama, t_nama_bersama, t_nama_jalan, t_rt_bersama, t_rw_bersama, t_kecamatan_bersama, t_kelurahan_bersama, t_kabkota_bersama, t_nohp_bersama, t_kodepos_bersama, t_npwp_bersama, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_notif_validasi_berkas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_notif_validasi_berkas (t_id_notif_berkas, t_idspt, t_isipesan, t_dari, t_tglkirim, t_untuk, s_id_status_lihat, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_notif_validasi_kabid; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_notif_validasi_kabid (t_id_notif_kabid, t_idspt, t_isipesan, t_dari, t_tglkirim, t_untuk, s_id_status_lihat, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_angsuran; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pelayanan_angsuran (t_idangsuran, t_idspt, t_noangsuran, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_angsuran, t_jmlhangsuran, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_keberatan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pelayanan_keberatan (t_idkeberatan, t_idspt, t_nokeberatan, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_keberatan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, t_jmlhpotongan_disetujui, t_persentase_disetujui, t_jmlh_spt_sebenarnya, t_jmlh_spt_hasilpot, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_keringanan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pelayanan_keringanan (t_idkeringanan, t_idspt, t_nokeringanan, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_keringanan, t_jmlhpotongan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, t_jmlhpotongan_disetujui, t_persentase_disetujui, t_jmlh_spt_sebenarnya, t_jmlh_spt_hasilpot, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_mutasi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pelayanan_mutasi (t_idmutasi, t_idspt, t_nomutasi, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_mutasi, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_pembatalan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pelayanan_pembatalan (t_idpembatalan, t_idspt, t_nopembatalan, t_tglpengajuan, t_keterangan_pembatalan, t_iduser_pengajuan, t_nosk_pembatalan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_penundaan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pelayanan_penundaan (t_idpenundaan, t_idspt, t_nopenundaan, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_penundaan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pembayaran_bphtb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pembayaran_bphtb (t_id_pembayaran, t_idspt, t_nourut_bayar, s_id_status_bayar, t_iduser_bayar, t_tglpembayaran_pokok, t_jmlhbayar_pokok, t_tglpembayaran_denda, t_jmlhbayar_denda, t_jmlhbayar_total, created_at, updated_at) FROM stdin;
9	1	\N	1	1	2021-01-06 14:22:25	500000	2021-01-06 14:22:25	0	500000	2021-09-27 08:44:54	2021-09-27 08:44:54
10	2	\N	1	1	2021-01-08 14:22:25	7000000	2021-01-08 14:22:25	0	7000000	2021-09-27 08:44:54	2021-09-27 08:44:54
11	3	\N	1	1	2021-01-11 14:22:25	750000	2021-01-11 14:22:25	0	750000	2021-09-27 08:44:54	2021-09-27 08:44:54
12	4	\N	1	1	2021-01-13 14:22:25	12000000	2021-01-13 14:22:25	0	12000000	2021-09-27 08:44:54	2021-09-27 08:44:54
13	5	\N	1	1	2021-01-13 14:22:25	9500000	2021-01-13 14:22:25	0	9500000	2021-09-27 08:44:54	2021-09-27 08:44:54
14	6	\N	1	1	2021-01-14 14:22:25	7000000	2021-01-14 14:22:25	0	7000000	2021-09-27 08:44:54	2021-09-27 08:44:54
15	7	\N	1	1	2021-01-21 14:22:25	5071800	2021-01-21 14:22:25	0	5071800	2021-09-27 08:44:54	2021-09-27 08:44:54
16	8	\N	1	1	2021-01-27 14:22:25	2597700	2021-01-27 14:22:25	0	2597700	2021-09-27 08:44:54	2021-09-27 08:44:54
17	9	\N	1	1	2021-01-28 14:22:25	5118100	2021-01-28 14:22:25	0	5118100	2021-09-27 08:44:54	2021-09-27 08:44:54
18	10	\N	1	1	2021-02-05 14:22:25	9500000	2021-02-05 14:22:25	0	9500000	2021-09-27 08:44:54	2021-09-27 08:44:54
19	11	\N	1	1	2021-02-09 14:22:25	6310150	2021-02-09 14:22:25	0	6310150	2021-09-27 08:44:55	2021-09-27 08:44:55
20	12	\N	1	1	2021-02-15 14:22:25	6347800	2021-02-15 14:22:25	0	6347800	2021-09-27 08:44:55	2021-09-27 08:44:55
21	13	\N	1	1	2021-02-15 14:22:25	28200	2021-02-15 14:22:25	0	28200	2021-09-27 08:44:55	2021-09-27 08:44:55
22	14	\N	1	1	2021-02-17 14:22:25	7000000	2021-02-17 14:22:25	0	7000000	2021-09-27 08:44:55	2021-09-27 08:44:55
23	15	\N	1	1	2021-02-19 14:22:25	4166000	2021-02-19 14:22:25	0	4166000	2021-09-27 08:44:55	2021-09-27 08:44:55
24	16	\N	1	1	2021-02-19 14:22:25	6975800	2021-02-19 14:22:25	0	6975800	2021-09-27 08:44:55	2021-09-27 08:44:55
25	17	\N	1	1	2021-02-23 14:22:25	351750	2021-02-23 14:22:25	0	351750	2021-09-27 08:44:55	2021-09-27 08:44:55
26	18	\N	1	1	2021-03-02 14:22:25	3450000	2021-03-02 14:22:25	0	3450000	2021-09-27 08:44:55	2021-09-27 08:44:55
27	19	\N	1	1	2021-03-09 14:22:25	2354000	2021-03-09 14:22:25	0	2354000	2021-09-27 08:44:55	2021-09-27 08:44:55
28	20	\N	1	1	2021-03-22 14:22:25	750000	2021-03-22 14:22:25	0	750000	2021-09-27 08:44:55	2021-09-27 08:44:55
29	21	\N	1	1	2021-03-29 14:22:25	9500000	2021-03-29 14:22:25	0	9500000	2021-09-27 08:44:55	2021-09-27 08:44:55
30	22	\N	1	1	2021-03-29 14:22:25	309600	2021-03-29 14:22:25	0	309600	2021-09-27 08:44:55	2021-09-27 08:44:55
31	23	\N	1	1	2021-04-06 14:22:25	1838400	2021-04-06 14:22:25	0	1838400	2021-09-27 08:44:55	2021-09-27 08:44:55
32	24	\N	1	1	2021-04-12 14:22:25	1750000	2021-04-12 14:22:25	0	1750000	2021-09-27 08:44:55	2021-09-27 08:44:55
33	25	\N	1	1	2021-04-19 14:22:25	2000000	2021-04-19 14:22:25	0	2000000	2021-09-27 08:44:55	2021-09-27 08:44:55
34	26	\N	1	1	2021-04-22 14:22:25	4500000	2021-04-22 14:22:25	0	4500000	2021-09-27 08:44:55	2021-09-27 08:44:55
35	27	\N	1	1	2021-04-22 14:22:25	622400	2021-04-22 14:22:25	0	622400	2021-09-27 08:44:55	2021-09-27 08:44:55
36	28	\N	1	1	2021-04-23 14:22:25	18347800	2021-04-23 14:22:25	0	18347800	2021-09-27 08:44:55	2021-09-27 08:44:55
37	29	\N	1	1	2021-04-23 14:22:25	6652800	2021-04-23 14:22:25	0	6652800	2021-09-27 08:44:55	2021-09-27 08:44:55
38	30	\N	1	1	2021-04-23 14:22:25	7707000	2021-04-23 14:22:25	0	7707000	2021-09-27 08:44:55	2021-09-27 08:44:55
39	31	\N	1	1	2021-04-23 14:22:25	7874500	2021-04-23 14:22:25	0	7874500	2021-09-27 08:44:55	2021-09-27 08:44:55
40	32	\N	1	1	2021-04-23 14:22:25	7204400	2021-04-23 14:22:25	0	7204400	2021-09-27 08:44:55	2021-09-27 08:44:55
41	33	\N	1	1	2021-04-27 14:22:25	3750000	2021-04-27 14:22:25	0	3750000	2021-09-27 08:44:55	2021-09-27 08:44:55
42	34	\N	1	1	2021-04-30 14:22:25	4500000	2021-04-30 14:22:25	0	4500000	2021-09-27 08:44:56	2021-09-27 08:44:56
43	35	\N	1	1	2021-05-03 14:22:25	7775000	2021-05-03 14:22:25	0	7775000	2021-09-27 08:44:56	2021-09-27 08:44:56
44	36	\N	1	1	2021-05-03 14:22:25	2000000	2021-05-03 14:22:25	0	2000000	2021-09-27 08:44:56	2021-09-27 08:44:56
45	37	\N	1	1	2021-05-03 14:22:25	2000000	2021-05-03 14:22:25	0	2000000	2021-09-27 08:44:56	2021-09-27 08:44:56
46	38	\N	1	1	2021-05-05 14:22:25	1000000	2021-05-05 14:22:25	0	1000000	2021-09-27 08:44:56	2021-09-27 08:44:56
47	39	\N	1	1	2021-05-06 14:22:25	2250000	2021-05-06 14:22:25	0	2250000	2021-09-27 08:44:56	2021-09-27 08:44:56
48	40	\N	1	1	2021-05-06 14:22:25	2500000	2021-05-06 14:22:25	0	2500000	2021-09-27 08:44:56	2021-09-27 08:44:56
49	41	\N	1	1	2021-05-10 14:22:25	23066000	2021-05-10 14:22:25	0	23066000	2021-09-27 08:44:56	2021-09-27 08:44:56
50	42	\N	1	1	2021-05-10 14:22:25	2165450	2021-05-10 14:22:25	0	2165450	2021-09-27 08:44:56	2021-09-27 08:44:56
51	43	\N	1	1	2021-05-10 14:22:25	1727900	2021-05-10 14:22:25	0	1727900	2021-09-27 08:44:56	2021-09-27 08:44:56
52	44	\N	1	1	2021-05-18 14:22:25	2000000	2021-05-18 14:22:25	0	2000000	2021-09-27 08:44:56	2021-09-27 08:44:56
53	45	\N	1	1	2021-05-18 14:22:25	750000	2021-05-18 14:22:25	0	750000	2021-09-27 08:44:56	2021-09-27 08:44:56
54	46	\N	1	1	2021-05-18 14:22:25	3413000	2021-05-18 14:22:25	0	3413000	2021-09-27 08:44:56	2021-09-27 08:44:56
55	47	\N	1	1	2021-05-18 14:22:25	8817000	2021-05-18 14:22:25	0	8817000	2021-09-27 08:44:56	2021-09-27 08:44:56
56	48	\N	1	1	2021-05-19 14:22:25	1452250	2021-05-19 14:22:25	0	1452250	2021-09-27 08:44:56	2021-09-27 08:44:56
57	49	\N	1	1	2021-05-20 14:22:25	4825000	2021-05-20 14:22:25	0	4825000	2021-09-27 08:44:56	2021-09-27 08:44:56
58	50	\N	1	1	2021-05-25 14:22:25	23066000	2021-05-25 14:22:25	0	23066000	2021-09-27 08:44:56	2021-09-27 08:44:56
59	51	\N	1	1	2021-05-27 14:22:25	2219550	2021-05-27 14:22:25	0	2219550	2021-09-27 08:44:56	2021-09-27 08:44:56
60	52	\N	1	1	2021-05-28 14:22:25	210600	2021-05-28 14:22:25	0	210600	2021-09-27 08:44:56	2021-09-27 08:44:56
61	53	\N	1	1	2021-05-28 14:22:25	230300	2021-05-28 14:22:25	0	230300	2021-09-27 08:44:56	2021-09-27 08:44:56
62	54	\N	1	1	2021-05-31 14:22:25	350000	2021-05-31 14:22:25	0	350000	2021-09-27 08:44:56	2021-09-27 08:44:56
63	55	\N	1	1	2021-06-02 14:22:25	1000000	2021-06-02 14:22:25	0	1000000	2021-09-27 08:44:56	2021-09-27 08:44:56
64	56	\N	1	1	2021-06-04 14:22:25	7000000	2021-06-04 14:22:25	0	7000000	2021-09-27 08:44:56	2021-09-27 08:44:56
65	57	\N	1	1	2021-06-04 14:22:25	5000000	2021-06-04 14:22:25	0	5000000	2021-09-27 08:44:56	2021-09-27 08:44:56
66	58	\N	1	1	2021-06-07 14:22:25	2000000	2021-06-07 14:22:25	0	2000000	2021-09-27 08:44:56	2021-09-27 08:44:56
67	59	\N	1	1	2021-06-07 14:22:25	3653500	2021-06-07 14:22:25	0	3653500	2021-09-27 08:44:56	2021-09-27 08:44:56
68	60	\N	1	1	2021-06-18 14:22:25	2000000	2021-06-18 14:22:25	0	2000000	2021-09-27 08:44:57	2021-09-27 08:44:57
69	61	\N	1	1	2021-06-18 14:22:25	1500000	2021-06-18 14:22:25	0	1500000	2021-09-27 08:44:57	2021-09-27 08:44:57
70	62	\N	1	1	2021-07-08 14:22:25	6494350	2021-07-08 14:22:25	0	6494350	2021-09-27 08:44:57	2021-09-27 08:44:57
71	63	\N	1	1	2021-07-14 14:22:25	9240250	2021-07-14 14:22:25	0	9240250	2021-09-27 08:44:57	2021-09-27 08:44:57
72	64	\N	1	1	2021-07-15 14:22:25	2064000	2021-07-15 14:22:25	0	2064000	2021-09-27 08:44:57	2021-09-27 08:44:57
73	65	\N	1	1	2021-07-21 14:22:25	12000000	2021-07-21 14:22:25	0	12000000	2021-09-27 08:44:57	2021-09-27 08:44:57
74	66	\N	1	1	2021-07-21 14:22:25	8250000	2021-07-21 14:22:25	0	8250000	2021-09-27 08:44:57	2021-09-27 08:44:57
75	67	\N	1	1	2021-07-28 14:22:25	1500000	2021-07-28 14:22:25	0	1500000	2021-09-27 08:44:57	2021-09-27 08:44:57
76	68	\N	1	1	2021-07-29 14:22:25	1000000	2021-07-29 14:22:25	0	1000000	2021-09-27 08:44:57	2021-09-27 08:44:57
77	69	\N	1	1	2021-07-30 14:22:25	500000	2021-07-30 14:22:25	0	500000	2021-09-27 08:44:57	2021-09-27 08:44:57
78	70	\N	1	1	2021-08-02 14:22:25	28137000	2021-08-02 14:22:25	0	28137000	2021-09-27 08:44:57	2021-09-27 08:44:57
79	71	\N	1	1	2021-08-02 14:22:25	26829950	2021-08-02 14:22:25	0	26829950	2021-09-27 08:44:57	2021-09-27 08:44:57
80	72	\N	1	1	2021-08-02 14:22:25	65226200	2021-08-02 14:22:25	0	65226200	2021-09-27 08:44:57	2021-09-27 08:44:57
81	73	\N	1	1	2021-08-02 14:22:25	13838500	2021-08-02 14:22:25	0	13838500	2021-09-27 08:44:57	2021-09-27 08:44:57
82	74	\N	1	1	2021-08-03 14:22:25	4903000	2021-08-03 14:22:25	0	4903000	2021-09-27 08:44:57	2021-09-27 08:44:57
83	75	\N	1	1	2021-08-13 14:22:25	7000000	2021-08-13 14:22:25	0	7000000	2021-09-27 08:44:57	2021-09-27 08:44:57
84	76	\N	1	1	2021-08-16 14:22:25	1000000	2021-08-16 14:22:25	0	1000000	2021-09-27 08:44:57	2021-09-27 08:44:57
85	77	\N	1	1	2021-08-02 14:22:25	4600000	2021-08-02 14:22:25	0	4600000	2021-09-27 08:44:57	2021-09-27 08:44:57
86	78	\N	1	1	2021-08-02 14:22:25	5318800	2021-08-02 14:22:25	0	5318800	2021-09-27 08:44:57	2021-09-27 08:44:57
87	79	\N	1	1	2021-08-02 14:22:25	5608750	2021-08-02 14:22:25	0	5608750	2021-09-27 08:44:57	2021-09-27 08:44:57
88	80	\N	1	1	2021-08-02 14:22:25	9608750	2021-08-02 14:22:25	0	9608750	2021-09-27 08:44:57	2021-09-27 08:44:57
89	81	\N	1	1	2021-09-15 14:22:25	250000	2021-09-15 14:22:25	0	250000	2021-09-27 08:44:57	2021-09-27 08:44:57
\.


--
-- Data for Name: t_pembayaran_skpdkb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pembayaran_skpdkb (t_id_bayar_skpdkb, t_idspt, t_idskpdkb, t_nourut_bayar, s_id_status_bayar, t_iduser_bayar, t_tglpembayaran_skpdkb, t_jmlhbayar_skpdkb, t_tglpembayaran_denda, t_jmlhbayar_denda, t_jmlhbayar_total, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pembayaran_skpdkbt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pembayaran_skpdkbt (t_id_bayar_skpdkb, t_idspt, t_idskpdkbt, t_nourut_bayar, s_id_status_bayar, t_iduser_bayar, t_tglpembayaran_skpdkbt, t_jmlhbayar_skpdkbt, t_tglpembayaran_denda, t_jmlhbayar_denda, t_jmlhbayar_total, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pemeriksaan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pemeriksaan (t_idpemeriksa, t_idspt, t_tglpenetapan, t_tglby_system, t_iduser_buat, t_idpejabat1, t_idpejabat2, t_idpejabat3, t_idpejabat4, t_noperiksa, t_keterangan, t_nop_sppt, t_tahun_sppt, t_luastanah_pemeriksa, t_njoptanah_pemeriksa, t_totalnjoptanah_pemeriksa, t_luasbangunan_pemeriksa, t_njopbangunan_pemeriksa, t_totalnjopbangunan_pemeriksa, t_grandtotalnjop_pemeriksa, t_nilaitransaksispt_pemeriksa, t_trf_aphb_kali_pemeriksa, t_trf_aphb_bagi_pemeriksa, t_permeter_tanah_pemeriksa, t_idtarifbphtb_pemeriksa, t_persenbphtb_pemeriksa, t_npop_bphtb_pemeriksa, t_npoptkp_bphtb_pemeriksa, t_npopkp_bphtb_pemeriksa, t_nilaibphtb_pemeriksa, t_idpersetujuan_pemeriksa, isdeleted, t_idoperator_deleted, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pen_denda_ajb_notaris; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pen_denda_ajb_notaris (t_id_ajbdenda, t_idspt, t_idnotaris, t_tglpenetapan, t_nourut_ajbdenda, t_nilai_ajbdenda, t_iduser_buat, s_id_status_bayar, t_tglbayar_ajbdenda, t_jmlhbayar_ajbdenda, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pen_denda_lpor_notaris; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_pen_denda_lpor_notaris (t_id_pendenda, t_idlaporbulanan, t_idnotaris, t_tglpenetapan, t_nourut_pendenda, t_nilai_pendenda, t_iduser_buat, s_id_status_bayar, t_tglbayar_pendenda, t_jmlhbayar_pendenda, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_skpdkb; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_skpdkb (t_idskpdkb, t_idspt, t_tglpenetapan, t_tglby_system, t_iduser_buat, t_nop_sppt, t_tahun_sppt, t_luastanah_skpdkb, t_njoptanah_skpdkb, t_totalnjoptanah_skpdkb, t_luasbangunan_skpdkb, t_njopbangunan_skpdkb, t_totalnjopbangunan_skpdkb, t_grandtotalnjop_skpdkb, t_nilaitransaksispt_skpdkb, t_trf_aphb_kali_skpdkb, t_trf_aphb_bagi_skpdkb, t_permeter_tanah_skpdkb, t_idtarifbphtb_skpdkb, t_persenbphtb_skpdkb, t_npop_bphtb_skpdkb, t_npoptkp_bphtb_skpdkb, t_npopkp_bphtb_skpdkb, t_nilai_bayar_sblumnya, t_nilai_pokok_skpdkb, t_jmlh_blndenda, t_jmlh_dendaskpdkb, t_jmlh_totalskpdkb, t_tglbuat_kodebyar, t_nourut_kodebayar, t_kodebayar_skpdkb, t_tgljatuhtempo_skpdkb, t_idjenisketetapan, t_idpersetujuan_skpdkb, isdeleted, t_idoperator_deleted, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_skpdkbt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_skpdkbt (t_idskpdkbt, t_idspt, t_idskpdkb, t_tglpenetapan, t_tglby_system, t_iduser_buat, t_nop_sppt, t_tahun_sppt, t_luastanah_skpdkbt, t_njoptanah_skpdkbt, t_totalnjoptanah_skpdkbt, t_luasbangunan_skpdkbt, t_njopbangunan_skpdkbt, t_totalnjopbangunan_skpdkbt, t_grandtotalnjop_skpdkbt, t_nilaitransaksispt_skpdkbt, t_trf_aphb_kali_skpdkbt, t_trf_aphb_bagi_skpdkbt, t_permeter_tanah_skpdkbt, t_idtarifbphtb_skpdkbt, t_persenbphtb_skpdkbt, t_npop_bphtb_skpdkbt, t_npoptkp_bphtb_skpdkbt, t_npopkp_bphtb_skpdkbt, t_nilai_bayar_sblumnya, t_nilai_pokok_skpdkbt, t_jmlh_blndenda, t_jmlh_dendaskpdkbt, t_jmlh_totalskpdkbt, t_tglbuat_kodebyar, t_nourut_kodebayar, t_kodebayar_skpdkbt, t_tgljatuhtempo_skpdkbt, t_idjenisketetapan, t_idpersetujuan_skpdkb, isdeleted, t_idoperator_deleted, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_spt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_spt (t_idspt, t_uuidspt, t_kohirspt, t_tgldaftar_spt, t_tglby_system, t_periodespt, t_iduser_buat, t_idjenistransaksi, t_idnotaris_spt, t_npwpd, t_idstatus_npwpd, t_idbidang_usaha, t_nama_pembeli, t_nik_pembeli, t_npwp_pembeli, t_jalan_pembeli, t_kabkota_pembeli, t_idkec_pembeli, t_namakecamatan_pembeli, t_idkel_pembeli, t_namakelurahan_pembeli, t_rt_pembeli, t_rw_pembeli, t_nohp_pembeli, t_notelp_pembeli, t_email_pembeli, t_kodepos_pembeli, t_siup_pembeli, t_nib_pembeli, t_ketdomisili_pembeli, t_b_nama_pngjwb_pembeli, t_b_nik_pngjwb_pembeli, t_b_npwp_pngjwb_pembeli, t_b_statusjab_pngjwb_pembeli, t_b_jalan_pngjwb_pembeli, t_b_kabkota_pngjwb_pembeli, t_b_idkec_pngjwb_pembeli, t_b_namakec_pngjwb_pembeli, t_b_idkel_pngjwb_pembeli, t_b_namakel_pngjwb_pembeli, t_b_rt_pngjwb_pembeli, t_b_rw_pngjwb_pembeli, t_b_nohp_pngjwb_pembeli, t_b_notelp_pngjwb_pembeli, t_b_email_pngjwb_pembeli, t_b_kodepos_pngjwb_pembeli, t_nop_sppt, t_tahun_sppt, t_nama_sppt, t_jalan_sppt, t_kabkota_sppt, t_kecamatan_sppt, t_kelurahan_sppt, t_rt_sppt, t_rw_sppt, t_luastanah_sismiop, t_luasbangunan_sismiop, t_njoptanah_sismiop, t_njopbangunan_sismiop, t_luastanah, t_njoptanah, t_totalnjoptanah, t_luasbangunan, t_njopbangunan, t_totalnjopbangunan, t_grandtotalnjop, t_nilaitransaksispt, t_tarif_pembagian_aphb_kali, t_tarif_pembagian_aphb_bagi, t_permeter_tanah, t_idjenisfasilitas, t_idjenishaktanah, t_idjenisdoktanah, t_idpengurangan, t_id_jenistanah, t_nosertifikathaktanah, t_tgldok_tanah, s_latitude, s_longitude, t_idtarifbphtb, t_persenbphtb, t_npop_bphtb, t_npoptkp_bphtb, t_npopkp_bphtb, t_nilai_bphtb_fix, t_nilai_bphtb_real, t_idbidang_penjual, t_nama_penjual, t_nik_penjual, t_npwp_penjual, t_jalan_penjual, t_kabkota_penjual, t_idkec_penjual, t_namakec_penjual, t_idkel_penjual, t_namakel_penjual, t_rt_penjual, t_rw_penjual, t_nohp_penjual, t_notelp_penjual, t_email_penjual, t_kodepos_penjual, t_siup_penjual, t_nib_penjual, t_ketdomisili_penjual, t_b_nama_pngjwb_penjual, t_b_nik_pngjwb_penjual, t_b_npwp_pngjwb_penjual, t_b_statusjab_pngjwb_penjual, t_b_jalan_pngjwb_penjual, t_b_kabkota_pngjwb_penjual, t_b_idkec_pngjwb_penjual, t_b_namakec_pngjwb_penjual, t_b_idkel_pngjwb_penjual, t_b_namakel_pngjwb_penjual, t_b_rt_pngjwb_penjual, t_b_rw_pngjwb_penjual, t_b_nohp_pngjwb_penjual, t_b_notelp_pngjwb_penjual, t_b_email_pngjwb_penjual, t_b_kodepos_pngjwb_penjual, t_tglbuat_kodebyar, t_nourut_kodebayar, t_kodebayar_bphtb, t_tglketetapan_spt, t_tgljatuhtempo_spt, t_idjenisketetapan, t_idpersetujuan_bphtb, isdeleted, t_idoperator_deleted, created_at, updated_at, t_ntpd) FROM stdin;
7	5bb679861d544c0dad10df2824c7b055	7	2021-01-21 14:22:25	2021-01-21 14:22:25	2021	\N	1	3	\N	\N	\N	DJAFAR PITALAU	\N	\N	KEL.SOATALOARA I,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.006.005.0050.0	\N	\N	\N	\N	\N	\N	\N	\N	201	171	103000	823000	201	103000	20703000	171	823000	140733000	161436000	161436171	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	161436000	60000000	101436000	5071800	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-01-21 14:22:25	7	7103012100007	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100007
8	2c3858d783d34abc96a7808268706cd1	8	2021-01-27 14:22:25	2021-01-27 14:22:25	2021	\N	1	4	\N	\N	\N	ADOLF WANGKA	\N	\N	KEL. TONA II KEC. TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.021.005.0039.0	\N	\N	\N	\N	\N	\N	\N	\N	129	126	64000	823000	129	64000	8256000	126	823000	103698000	111954000	111954126	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	111954000	60000000	51954000	2597700	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-01-27 14:22:25	8	7103012100008	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100008
5	702b34d91531442db903b168d82fd586	5	2021-01-13 14:22:25	2021-01-13 14:22:25	2021	\N	1	3	\N	\N	\N	JHON TAMAKA	\N	\N	KEL.SOATALOARA II,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.015.003.0192.0	\N	\N	\N	\N	\N	\N	\N	\N	220	70	243000	823000	220	243000	53460000	70	823000	57610000	111070000	111070070	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	250000000	60000000	190000000	9500000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-01-13 14:22:25	5	7103012100005	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100005
9	bd782c7b023949ec8f5efe50dc05ca84	9	2021-01-28 14:22:25	2021-01-28 14:22:25	2021	\N	1	3	\N	\N	\N	CHRSITIANA LABESI	\N	\N	KEL.MANENTE,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.018.003.0448.0	\N	\N	\N	\N	\N	\N	\N	\N	304	150	128000	823000	304	128000	38912000	150	823000	123450000	162362000	162362150	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	162362000	60000000	102362000	5118100	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-01-28 14:22:25	9	7103012100009	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100009
10	79c30a1005f54b5691048eb4e95913e4	10	2021-02-05 14:22:25	2021-02-05 14:22:25	2021	\N	1	2	\N	\N	\N	JACOB MOKODASER	\N	\N	KEL.TUMINTING, KOTA MANADO	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.080.110.0330	\N	\N	\N	\N	\N	\N	\N	\N	524	0	335000	0	524	335000	175540000	0	0	0	175540000	175540000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	250000000	60000000	190000000	9500000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-02-05 14:22:25	10	7103012100010	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100010
11	833781b611d1482a9d69022f52aff07d	11	2021-02-09 14:22:25	2021-02-09 14:22:25	2021	\N	1	3	\N	\N	\N	GAGOLA PARERA	\N	\N	KEL.SAWANGBENDAR,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0058.0	\N	\N	\N	\N	\N	\N	\N	\N	392	87	394000	365000	392	394000	154448000	87	365000	31755000	186203000	186203000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	186203000	60000000	126203000	6310150	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-02-09 14:22:25	11	7103012100011	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100011
16	efb2f34a4b644e2e842dec61345b8d55	16	2021-02-19 14:22:25	2021-02-19 14:22:25	2021	\N	1	7	\N	\N	\N	HEIDY CHANDRA	\N	\N	KEL.SAWANGBENDAR,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0080.0	\N	\N	\N	\N	\N	\N	\N	\N	198	290	802000	968000	198	802000	158796000	290	968000	280720000	439516000	439516290	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	439516000	300000000	139516000	6975800	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-02-19 14:22:25	16	7103012100016	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100016
17	2104d3ca0f654b0cbda7f3df8565e4d7	17	2021-02-23 14:22:25	2021-02-23 14:22:25	2021	\N	1	3	\N	\N	\N	OKRINTJE MONA	\N	\N	KEL.TONA I,KEC.TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.004.008.0200.0	\N	\N	\N	\N	\N	\N	\N	\N	460	103	64000	365000	460	64000	29440000	103	365000	37595000	67035000	67035103	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	67035000	60000000	7035000	351750	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-02-23 14:22:25	17	7103012100017	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100017
22	545f0ca36d6c4037a0c096a111b53d4c	22	2021-03-29 14:22:25	2021-03-29 14:22:25	2021	\N	1	1	\N	\N	\N	SERGIUS LUMASUGE	\N	\N	KARATUNG I, KEC. MANGANITU	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.070.006.006.0000.0	\N	\N	\N	\N	\N	\N	\N	\N	336	96	27000	595000	336	27000	9072000	96	595000	57120000	66192000	66192096	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	66192000	60000000	6192000	309600	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-03-29 14:22:25	22	7103012100022	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100022
27	c69d44a0826c43279a2713a062d31673	27	2021-04-22 14:22:25	2021-04-22 14:22:25	2021	\N	1	9	\N	\N	\N	SUFARTA SENDENG	\N	\N	KEL.TONA I, KEC.TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.004.006.0252.0	\N	\N	\N	\N	\N	\N	\N	\N	566	0	128000	0	566	128000	72448000	0	0	0	72448000	72448000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25000000	60000000	12448000	622400	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-22 14:22:25	27	7103012100027	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100027
37	754d003ad01c41a4b65ef84d6a25652e	37	2021-05-03 14:22:25	2021-05-03 14:22:25	2021	\N	1	8	\N	\N	\N	RIZAL BASALAMAH	\N	\N	KEL. BUNGALAWANG, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.016.001.0030.0	\N	\N	\N	\N	\N	\N	\N	\N	323	42	160000	429000	323	160000	51680000	42	429000	18018000	69698000	69698042	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	100000000	60000000	40000000	2000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-03 14:22:25	37	7103012100037	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100037
2	eeab8f2ae60d4e4f936591f2ba90a8e6	2	2021-01-08 14:22:25	2021-01-08 14:22:25	2021	\N	1	3	\N	\N	\N	LUKAS KUNDRAD P PAPENDANG	\N	\N	KEL.TONA II,KEC.TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.005.002.0163.0	\N	\N	\N	\N	\N	\N	\N	\N	615	0	20000	0	615	20000	12300000	0	0	0	12300000	12300000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	200000000	60000000	140000000	7000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-01-08 14:22:25	2	7103012100002	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100002
56	0815e01bad10449b95ef0d6159b8038e	56	2021-06-04 14:22:25	2021-06-04 14:22:25	2021	\N	1	1	\N	\N	\N	JOIS TANDRIS	\N	\N	KEL. SAWANG BENDAR KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.005.0100.0	\N	\N	\N	\N	\N	\N	\N	\N	161	16	103000	823000	161	103000	16583000	16	823000	13168000	29751000	29751016	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	200000000	60000000	140000000	7000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-06-04 14:22:25	56	7103012100056	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100056
61	c9d78a2325ca42fba21ddd2dd9b85ee8	61	2021-06-18 14:22:25	2021-06-18 14:22:25	2021	\N	1	8	\N	\N	\N	MARIANA RIGME PILAT	\N	\N	KEL. MANENTE KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.018.003.0129.0	\N	\N	\N	\N	\N	\N	\N	\N	208	99	128000	595000	208	128000	26624000	99	595000	58905000	85529000	85529099	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	90000000	60000000	30000000	1500000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-06-18 14:22:25	61	7103012100061	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100061
75	bcfb5b0ad4e9459dbd60008c6171cbd4	75	2021-08-13 14:22:25	2021-08-13 14:22:25	2021	\N	1	1	\N	\N	\N	PUJI WIDYAWATI	\N	\N	KEL.TIDORE KEC. TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.003.003.0016.0	\N	\N	\N	\N	\N	\N	\N	\N	254	63	200000	162000	254	200000	50800000	63	162000	10206000	61006000	61006063	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	200000000	60000000	140000000	7000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-13 14:22:25	75	7103012100075	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:21	012100075
80	81ca5cbf08514c5dab31e5e7637f330e	80	2021-08-02 14:22:25	2021-08-02 14:22:25	2021	\N	1	8	\N	\N	\N	KARLOS PATRAS	\N	\N	KEL. SAWANG BENDAR KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.005.004.0180.0	\N	\N	\N	\N	\N	\N	\N	\N	800	75	200000	429000	800	200000	160000000	75	429000	32175000	192175000	192175075	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	192175000	0	192175000	9608750	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-02 14:22:25	80	7103012100080	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:21	012100080
4	4ac5cfff6e994286bd75ef2ed8264f8e	4	2021-01-13 14:22:25	2021-01-13 14:22:25	2021	\N	1	1	\N	\N	\N	FRETS HONTONG	\N	\N	KPG.TALOARANE,KEC.MANGANITU	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.070.010.001.0038.0	\N	\N	\N	\N	\N	\N	\N	\N	1217	360	64000	595000	1217	64000	77888000	360	595000	214200000	292088000	292088360	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	300000000	60000000	240000000	12000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-01-13 14:22:25	4	7103012100004	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100004
46	2d2b62c9865047e69ab15237a9673b75	46	2021-05-18 14:22:25	2021-05-18 14:22:25	2021	\N	1	8	\N	\N	\N	N.KANGIRAS	\N	\N	KEL.APENGSEMBEKA,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.008.011.0061.0	\N	\N	\N	\N	\N	\N	\N	\N	344	42	335000	310000	344	335000	115240000	42	310000	13020000	128260000	128260042	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	128260000	60000000	68260000	3413000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-18 14:22:25	46	7103012100046	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:18	012100046
6	ee926b1434bc47adaf0dc91fcda77bfb	6	2021-01-14 14:22:25	2021-01-14 14:22:25	2021	\N	1	4	\N	\N	\N	ABNER AMPAGE	7103242904600000	\N	KEL.TONA I, KEC.TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.004.008.0049.0	\N	\N	\N	\N	\N	\N	\N	\N	241	44	160000	310000	241	160000	38560000	44	310000	13640000	52200000	52200044	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	200000000	60000000	140000000	7000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-01-14 14:22:25	6	7103012100006	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100006
23	985625d06ce04b589b2cd8b453a15d6d	23	2021-04-06 14:22:25	2021-04-06 14:22:25	2021	\N	1	7	\N	\N	\N	LILY KARIBATO	\N	\N	KEL. PANANEKENG, KEC. TAHUNA BARAT	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.018.003.0355.0	\N	\N	\N	\N	\N	\N	\N	\N	756	0	128000	0	756	128000	96768000	0	0	0	96768000	96768000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	96768000	60000000	36768000	1838400	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-06 14:22:25	23	7103012100023	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100023
21	7cf23ba190a047eb805609005ce2166e	21	2021-03-29 14:22:25	2021-03-29 14:22:25	2021	\N	1	1	\N	\N	\N	FANLY KABIMBANG	7103170506900000	\N	KEL.TONA, KEC.TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.021.001.0071.0	\N	\N	\N	\N	\N	\N	\N	\N	394	130	103000	823000	394	103000	40582000	130	823000	106990000	147572000	147572130	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	250000000	60000000	250000000	9500000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-03-29 14:22:25	21	7103012100021	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100021
24	f43c31ea4a1d4b7f90e1af4da3ef7b9a	24	2021-04-12 14:22:25	2021-04-12 14:22:25	2021	\N	1	7	\N	\N	\N	ALDRIKE TAHULENDING	\N	\N	KEL.APENGSEMBEKA,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.008.010.0009.0	\N	\N	\N	\N	\N	\N	\N	\N	84	45	64000	429000	84	64000	5376000	45	429000	19305000	24681000	24681045	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	95000000	60000000	35000000	1750000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-12 14:22:25	24	7103012100024	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100024
25	bcf9eb00f0e54b099dcd5d206c9a6f7c	25	2021-04-19 14:22:25	2021-04-19 14:22:25	2021	\N	1	2	\N	\N	\N	MARTINCE DOLASE	\N	\N	KEL. TONA I, KEC. TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.004.008.0236.0	\N	\N	\N	\N	\N	\N	\N	\N	436	0	103000	0	436	103000	44908000	0	0	0	44908000	44908000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	100000000	60000000	40000000	2000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-19 14:22:25	25	7103012100025	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100025
29	9629b6b0c86a463a81838e9ce757c840	29	2021-04-23 14:22:25	2021-04-23 14:22:25	2021	\N	1	8	\N	\N	\N	EUSABEIA E.K, SILFANUS	\N	\N	KEL. SAWANG BENDAR, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0155.0	\N	\N	\N	\N	\N	\N	\N	\N	114	180	394000	823000	114	394000	44916000	180	823000	148140000	193056000	193056180	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25000000	60000000	133056000	6652800	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-23 14:22:25	29	7103012100029	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100029
30	e26b85c57731445890cbac928d023f09	30	2021-04-23 14:22:25	2021-04-23 14:22:25	2021	\N	1	8	\N	\N	\N	YOKI JUMILA TITAH	3171016306740000	\N	KEL. SAWANG BENDAR, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0153.0	\N	\N	\N	\N	\N	\N	\N	\N	148	170	335000	968000	148	335000	49580000	170	968000	164560000	214140000	214140170	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25000000	60000000	154140000	7707000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-23 14:22:25	30	7103012100030	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100030
31	807e9f491a184719bbc5b6ff14595bfa	31	2021-04-23 14:22:25	2021-04-23 14:22:25	2021	\N	1	8	\N	\N	\N	TANO TITAH	7171020606760000	\N	KEL. SAWANG BENDAR, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0008.0	\N	\N	\N	\N	\N	\N	\N	\N	158	170	335000	968000	158	335000	52930000	170	968000	164560000	217490000	217490170	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25000000	60000000	157490000	7874500	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-23 14:22:25	31	7103012100031	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100031
34	074724e643a84b81b9e04641130569c6	34	2021-04-30 14:22:25	2021-04-30 14:22:25	2021	\N	1	8	\N	\N	\N	TIFFANY BRIGITA TINUNGKI	\N	\N	KEL. BUNGALAWANG, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.160.010.0050	\N	\N	\N	\N	\N	\N	\N	\N	306	48	160000	700000	306	160000	48960000	48	700000	33600000	82560000	82560048	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	150000000	60000000	90000000	4500000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-30 14:22:25	34	7103012100034	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100034
36	1ec4a885a1044422afcce2a576cb5d57	36	2021-05-03 14:22:25	2021-05-03 14:22:25	2021	\N	1	8	\N	\N	\N	O. K  MAKAGANSA	\N	\N	KEL. BUNGALAWANG, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.016.001.0025.0	\N	\N	\N	\N	\N	\N	\N	\N	433	0	160000	0	433	160000	69280000	0	0	0	69280000	69280000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	100000000	60000000	40000000	2000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-03 14:22:25	36	7103012100036	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100036
38	76f44395464549d8a38da63ce968aa89	38	2021-05-05 14:22:25	2021-05-05 14:22:25	2021	\N	1	2	\N	\N	\N	APRELIA MARTINA TOMASOA	\N	\N	KEL. MANENTE, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.018.003.0262.0	\N	\N	\N	\N	\N	\N	\N	\N	102	36	128000	595000	102	128000	13056000	36	595000	21420000	34476000	34476036	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	80000000	60000000	20000000	1000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-05 14:22:25	38	7103012100038	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:18	012100038
39	44bc31adb79745438f3c3532b3c5ef70	39	2021-05-06 14:22:25	2021-05-06 14:22:25	2021	\N	1	8	\N	\N	\N	AFAN HIA EDY THUNGARI	\N	\N	KEL. SANTIAGO, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.017.003.0254.0	\N	\N	\N	\N	\N	\N	\N	\N	125	0	128000	0	125	128000	16000000	0	0	0	16000000	16000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	0	45000000	2250000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-06 14:22:25	39	7103012100039	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:18	012100039
40	1ba3589ad172437588efa64da4aa8fcc	40	2021-05-06 14:22:25	2021-05-06 14:22:25	2021	\N	1	8	\N	\N	\N	AFAN HIA EDY THUNGARI	\N	\N	KEL. SANTIAGO, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.017.003.0254.0	\N	\N	\N	\N	\N	\N	\N	\N	132	0	128000	0	132	128000	16896000	0	0	0	16896000	16896000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	0	50000000	2500000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-06 14:22:25	40	7103012100040	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:18	012100040
15	c0423f1aae1a44218e3902248b36c58b	15	2021-02-19 14:22:25	2021-02-19 14:22:25	2021	\N	1	7	\N	\N	\N	HEIDY CHANDRA	\N	\N	KEL.SAWANGBENDAR,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0080.0	\N	\N	\N	\N	\N	\N	\N	\N	140	280	802000	968000	140	802000	112280000	280	968000	271040000	383320000	383320280	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	383320000	300000000	83320000	4166000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-02-19 14:22:25	15	7103012100015	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100015
48	697e67a38a0348c8b3aba2052f095c18	48	2021-05-19 14:22:25	2021-05-19 14:22:25	2021	\N	1	6	\N	\N	\N	REPLIUS DALAWIR	7103241402740000	\N	KEL. LESA. KEC. TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.001.002.0200.0	\N	\N	\N	\N	\N	\N	\N	\N	385	90	5000	968000	385	5000	1925000	90	968000	87120000	89045000	89045000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	89045000	60000000	29045000	1452250	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-19 14:22:25	48	7103012100048	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100048
19	f69f9faba609411ebe61a9f23a085bc4	19	2021-03-09 14:22:25	2021-03-09 14:22:25	2021	\N	1	5	\N	\N	\N	WILLIAM KOROMPIS	\N	\N	KPG.PETTA, KEC.TABKUAN UTARA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.090.016.001.0102.0	\N	\N	\N	\N	\N	\N	\N	\N	223	120	160000	595000	223	160000	35680000	120	595000	71400000	107080000	107080120	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	107080000	60000000	47080000	2354000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-03-09 14:22:25	19	7103012100019	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100019
53	ae38c126da2c4ea8b2a286cc4960e21f	53	2021-05-28 14:22:25	2021-05-28 14:22:25	2021	\N	1	8	\N	\N	\N	LIANTJE TITAH	\N	\N	KEL. SAWANG BENDAR, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.090.023.003.0215.0	\N	\N	\N	\N	\N	\N	\N	\N	47	56	394000	823000	47	394000	18518000	56	823000	46088000	64606000	64606056	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	64606000	60000000	4606000	230300	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-28 14:22:25	53	7103012100053	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100053
70	0d241c52957549cea1a91290d499b36f	70	2021-08-02 14:22:25	2021-08-02 14:22:25	2021	\N	1	8	\N	\N	\N	DAHLIA BASALAMAH	\N	\N	KEL.ISTIQLAL MANADO	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.004.0065.0	\N	\N	\N	\N	\N	\N	\N	\N	620	1310	802000	50000	620	802000	497240000	1310	50000	65500000	562740000	562741310	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	562740000	0	562740000	28137000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-02 14:22:25	70	7103012100070	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100070
18	c2ec618533b7443ea7f932062d27a2dc	18	2021-03-02 14:22:25	2021-03-02 14:22:25	2021	\N	1	2	\N	\N	\N	PT.RAPHAEL DUTA PERKASA	\N	\N	KEL. RAKU, KEC TABUKAN UTARA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.090.022.001.0117.0	\N	\N	\N	\N	\N	\N	\N	\N	9167	\N	14000	0	9167	14000	128338000	0	0	0	128338000	128338000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	129000000	60000000	69000000	3450000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-03-02 14:22:25	18	7103012100018	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100018
44	fc9bb5e23dae46f4be8d2bf8826c9992	44	2021-05-18 14:22:25	2021-05-18 14:22:25	2021	\N	1	8	\N	\N	\N	LISBET PANGANDAHENG	\N	\N	KEL. SOATALOARA II, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.015.003.0150.0	\N	\N	\N	\N	\N	\N	\N	\N	203	0	243000	0	203	243000	49329000	0	0	0	49329000	49329000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	100000000	60000000	40000000	2000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-18 14:22:25	44	7103012100044	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:18	012100044
54	a749bd1ff67d4a06afd87776f133e734	54	2021-05-31 14:22:25	2021-05-31 14:22:25	2021	\N	1	10	\N	\N	\N	NONTJE VIRTJE KUMARALO	\N	\N	KEL. ANGGES, KEC. TAHUNA BARAT	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.082.011.003.0119.0	\N	\N	\N	\N	\N	\N	\N	\N	6242	0	7150	0	6242	7150	44630300	0	0	0	44630300	44630300	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	67000000	60000000	7000000	350000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-31 14:22:25	54	7103012100054	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100054
55	76b55b3e045d44cd9951a26ff824c868	55	2021-06-02 14:22:25	2021-06-02 14:22:25	2021	\N	1	8	\N	\N	\N	ALBERTH DUMALANG	\N	\N	KEL. SANTIAGO KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.017.002.0285.0	\N	\N	\N	\N	\N	\N	\N	\N	542	0	103000	823000	542	103000	55826000	0	823000	0	55826000	55826000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	80000000	60000000	20000000	1000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-06-02 14:22:25	55	7103012100055	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100055
57	74513aae3be4454ba657a3e5ac9c22f7	57	2021-06-04 14:22:25	2021-06-04 14:22:25	2021	\N	1	1	\N	\N	\N	JOIS TANDRIS	\N	\N	KEL. SAWANG BENDAR KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.005.0217.0	\N	\N	\N	\N	\N	\N	\N	\N	131	0	103000	0	131	103000	13493000	0	0	0	13493000	13493000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	100000000	0	100000000	5000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-06-04 14:22:25	57	7103012100057	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100057
59	5a3b6d4348394ebdae61a5ce3ce888c6	59	2021-06-07 14:22:25	2021-06-07 14:22:25	2021	\N	1	8	\N	\N	\N	AGUNG DAMAI HARIKATANG	7103241909950000	\N	KEL. TONA II, KEC. TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.006.005.0114.0	\N	\N	\N	\N	\N	\N	\N	\N	644	70	160000	429000	644	160000	103040000	70	429000	30030000	133070000	133070070	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	133070000	60000000	73070000	3653500	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-06-07 14:22:25	59	7103012100059	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100059
60	fcdf24738b6742808d0e2cfd8d8f3c4e	60	2021-06-18 14:22:25	2021-06-18 14:22:25	2021	\N	1	1	\N	\N	\N	AZHAR HAFID	\N	\N	KPG. PETTA KEC. TABUKAN UTARA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.090.002.001.0066.0	\N	\N	\N	\N	\N	\N	\N	\N	178	100	160000	429000	178	160000	28480000	100	429000	42900000	71380000	71380100	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	100000000	60000000	40000000	2000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-06-18 14:22:25	60	7103012100060	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100060
62	e725fe7ed96b4745a060fa9851b86e77	62	2021-07-08 14:22:25	2021-07-08 14:22:25	2021	\N	1	8	\N	\N	\N	SJENI LIANDO	7171025109570000	\N	KEL.TUMUMPA SATU KOTA MANADO	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.008.009.0004.0	\N	\N	\N	\N	\N	\N	\N	\N	480	89	243000	823000	480	243000	116640000	89	823000	73247000	189887000	189887089	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	189887000	60000000	129887000	6494350	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-07-08 14:22:25	62	7103012100062	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100062
63	73aebfbfaaf64949b3763ba97fb34445	63	2021-07-14 14:22:25	2021-07-14 14:22:25	2021	\N	1	8	\N	\N	\N	MARTIN LIANDO	\N	\N	KEL.APENGSEMBEKA, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.008.010.0074.0	\N	\N	\N	\N	\N	\N	\N	\N	706	89	243000	823000	706	243000	171558000	89	823000	73247000	244805000	244805089	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	184805000	60000000	184805000	9240250	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-07-14 14:22:25	63	7103012100063	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100063
64	3269d007b337403abf4acfac9ad1bbe2	64	2021-07-15 14:22:25	2021-07-15 14:22:25	2021	\N	1	8	\N	\N	\N	KARLOS TINUNGKI	\N	\N	KEL. BUNGALAWANG, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.016.004.0002.0	\N	\N	\N	\N	\N	\N	\N	\N	360	120	48000	700000	360	48000	17280000	120	700000	84000000	101280000	101280120	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	184805000	60000000	41280000	2064000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-07-15 14:22:25	64	7103012100064	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100064
67	400132368cc94575bbb4502adc0376e7	67	2021-07-28 14:22:25	2021-07-28 14:22:25	2021	\N	1	2	\N	\N	\N	MEILAND TELENG	\N	\N	KPG. KUMA I KEC. TABUKAN TENGAH	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.060.018.003.0188.0	\N	\N	\N	\N	\N	\N	\N	\N	311	30	27000	191000	311	27000	8397000	30	191000	5730000	14127000	14127030	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	90000000	60000000	30000000	1500000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-07-28 14:22:25	67	7103012100067	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100067
68	fe704567fcff41348c0d1e1995b2a526	68	2021-07-29 14:22:25	2021-07-29 14:22:25	2021	\N	1	2	\N	\N	\N	EFENDY LAHIMANG, SE	3578311009700000	\N	KPG. BALANE KEC. TAMAKO	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.040.008.001.0084.0	\N	\N	\N	\N	\N	\N	\N	\N	425	0	48000	0	425	48000	20400000	0	0	0	20400000	20400000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	80000000	60000000	20000000	1000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-07-29 14:22:25	68	7103012100068	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100068
69	421e79e050ff46c88c4fdc1c1cc78f10	69	2021-07-30 14:22:25	2021-07-30 14:22:25	2021	\N	1	2	\N	\N	\N	KUSTANTI	3216194105740000	\N	KPG. BAKALAENG, KEC. MANGANITU	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.070.013.003.0172.0	\N	\N	\N	\N	\N	\N	\N	\N	431	0	3500	0	431	3500	1508500	0	0	0	1508000	1508000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	10000000	0	10000000	500000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-07-30 14:22:25	69	7103012100069	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100069
71	13767b1de2484212bb076c034bcb7344	71	2021-08-02 14:22:25	2021-08-02 14:22:25	2021	\N	1	8	\N	\N	\N	DAHLIA BASALAMAH	\N	\N	KEL.ISTIQLAL MANADO	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.004.0075.0	\N	\N	\N	\N	\N	\N	\N	\N	235	423	802000	823000	235	802000	188470000	423	823000	348129000	536599000	536599423	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	536599000	0	536599000	26829950	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-02 14:22:25	71	7103012100071	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100071
72	cf59b1c80f3e4b3d99a67b09e60147a3	72	2021-08-02 14:22:25	2021-08-02 14:22:25	2021	\N	1	8	\N	\N	\N	DAHLIA BASALAMAH	\N	\N	KEL.ISTIQLAL MANADO	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.004.0102.0	\N	\N	\N	\N	\N	\N	\N	\N	862	561	802000	1200000	862	802000	691324000	561	1200000	673200000	1364524000	1364524561	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	1364524000	60000000	1304524000	65226200	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-02 14:22:25	72	7103012100072	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100072
77	8dfb746ff3354d908dbc3791edae3e99	77	2021-08-02 14:22:25	2021-08-02 14:22:25	2021	\N	1	8	\N	\N	\N	KARLOS PATRAS	\N	\N	KEL. SAWANG BENDAR KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.017.002.0399.0	\N	\N	\N	\N	\N	\N	\N	\N	575	0	160000	0	575	160000	92000000	0	0	0	92000000	92000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	92000000	0	92000000	4600000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-02 14:22:25	77	7103012100077	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:21	012100077
78	14c05f4ab519471daf30b65d94a16910	78	2021-08-02 14:22:25	2021-08-02 14:22:25	2021	\N	1	8	\N	\N	\N	KARLOS PATRAS	\N	\N	KEL. SAWANG BENDAR KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.017.002.0020.0	\N	\N	\N	\N	\N	\N	\N	\N	1042	390	128000	700000	1042	128000	133376000	390	700000	273000000	406376000	406376390	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	406376000	300000000	106376000	5318800	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-02 14:22:25	78	7103012100078	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:21	012100078
79	416d4fa6a10146feb4f69a7378ec70aa	79	2021-08-02 14:22:25	2021-08-02 14:22:25	2021	\N	1	8	\N	\N	\N	KARLOS PATRAS	\N	\N	KEL. SAWANG BENDAR KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.005.004.0178.0	\N	\N	\N	\N	\N	\N	\N	\N	400	75	200000	429000	400	200000	80000000	75	429000	32175000	112175000	112175075	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	112175000	0	112175000	5608750	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-02 14:22:25	79	7103012100079	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:21	012100079
45	a3b8eafa913d42b5b4ff561505b3b7b5	45	2021-05-18 14:22:25	2021-05-18 14:22:25	2021	\N	1	1	\N	\N	\N	RUAIDA NAPAY	7501025502810000	\N	KPG. LAPANGO, KEC. MANGANITU SELATAN	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.170.010.001.0181.0	\N	\N	\N	\N	\N	\N	\N	\N	100	42	48000	310000	100	48000	4800000	42	310000	13020000	17820000	17820042	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	75000000	60000000	15000000	750000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-18 14:22:25	45	7103012100045	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:18	012100045
50	8eb8b27507814e5b8b2163b74b2030a8	50	2021-05-25 14:22:25	2021-05-25 14:22:25	2021	\N	1	8	\N	\N	\N	HEIDY CHANDRA	\N	\N	KEL. SAWANG BENDAR, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0143.0	\N	\N	\N	\N	\N	\N	\N	\N	300	290	802000	968000	300	802000	240600000	290	968000	280720000	521320000	521320290	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	521320000	60000000	461320000	23066000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-25 14:22:25	50	7103012100050	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100050
49	6354db9afa13420db505ba06f59b8c5c	49	2021-05-20 14:22:25	2021-05-20 14:22:25	2021	\N	1	2	\N	\N	\N	HIRNANSI RINIAWATI KAGUASEHI	\N	\N	KEL. RAKU, KEC. TABUKAN UTARA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.090.022.001.0812.0	\N	\N	\N	\N	\N	\N	\N	\N	99	36	14000	968000	99	14000	1386000	36	968000	34848000	36234000	36234036	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	156500000	60000000	96500000	4825000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-20 14:22:25	49	7103012100049	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100049
43	49b5b3f4fabd47b69eb79b1d99d9fa7d	43	2021-05-10 14:22:25	2021-05-10 14:22:25	2021	\N	1	9	\N	\N	\N	OKTAVIA VIVI HARIAWANG	7103106210860000	\N	KPG. LAPANGO I, KEC. MANGANITU SELATAN	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.170.014.002.0120.0	\N	\N	\N	\N	\N	\N	\N	\N	634	80	27000	968000	634	27000	17118000	80	968000	77440000	94558000	94558080	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	60000000	34558000	1727900	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-10 14:22:25	43	7103012100043	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:18	012100043
47	c55b097bf5d446f090e2974e74c1842f	47	2021-05-18 14:22:25	2021-05-18 14:22:25	2021	\N	1	8	\N	\N	\N	STINJE TIHO	\N	\N	KEL.APENGSEMBEKA,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.008.012.0015.0	\N	\N	\N	\N	\N	\N	\N	\N	535	70	128000	365000	535	128000	68480000	70	365000	25550000	94030000	94030070	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	236340000	60000000	176340000	8817000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-18 14:22:25	47	7103012100047	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:18	012100047
58	16c3b8633e324fcda1416889b9d58fe8	58	2021-06-07 14:22:25	2021-06-07 14:22:25	2021	\N	1	2	\N	\N	\N	FAACHRIYAH BACHMID	3578164409690000	\N	KEL. TIDORE. TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.003.002.0137.0	\N	\N	\N	\N	\N	\N	\N	\N	202	45	200000	225000	202	200000	40400000	45	225000	10125000	50525000	50525045	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	100000000	60000000	40000000	2000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-06-07 14:22:25	58	7103012100058	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100058
52	8beaa2da91664a96957a922eaedd0c1f	52	2021-05-28 14:22:25	2021-05-28 14:22:25	2021	\N	1	8	\N	\N	\N	EUSABELA E.K. SILFANUS	3273235110650000	\N	KEL. SAWANG BENDAR, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.090.023.003.0214.0	\N	\N	\N	\N	\N	\N	\N	\N	46	56	394000	823000	46	394000	18124000	56	823000	46088000	64212000	64212056	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	64212000	60000000	4212000	210600	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-28 14:22:25	52	7103012100052	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100052
42	599b49aba9eb42fa9e0dbacf2b1d7fba	42	2021-05-10 14:22:25	2021-05-10 14:22:25	2021	\N	1	8	\N	\N	\N	ALBETR DUMALANG	\N	\N	KEL. SANTIAGO, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.017.002.0235.0	\N	\N	\N	\N	\N	\N	\N	\N	1003	0	103000	0	1003	103000	103309000	0	0	0	103309000	103309000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	60000000	43309000	2165450	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-10 14:22:25	42	7103012100042	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:18	012100042
3	a78cd0db4ff74451a4a83585bcdafb3c	3	2021-01-11 14:22:25	2021-01-11 14:22:25	2021	\N	1	3	\N	\N	\N	MARGARETE MERLIN TOGELANG	\N	\N	KEL.SAWANGBENDAR,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.017.004.0406.0	\N	\N	\N	\N	\N	\N	\N	\N	4220	0	10000	0	4220	10000	42200000	0	0	0	75000000	75000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	60000000	15000000	45000000	750000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-01-11 14:22:25	3	7103012100003	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100003
14	304cbbd9d67f4b9ca022bc418b330d81	14	2021-02-17 14:22:25	2021-02-17 14:22:25	2021	\N	1	1	\N	\N	\N	APRIKONUS DAUD LORIS	7103242904810000	\N	KEL.TONA I,KEC.TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.004.006.0141.0	\N	\N	\N	\N	\N	\N	\N	\N	465	0	160000	0	465	160000	74400000	0	0	0	74400000	74400000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	200000000	60000000	140000000	7000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-02-17 14:22:25	14	7103012100014	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100014
20	8c24b13ffa2b468eb896e9ac05f6b8d9	20	2021-03-22 14:22:25	2021-03-22 14:22:25	2021	\N	1	2	\N	\N	\N	MARCELLINO JULIO TOGELANG	7103131806730000	\N	KEL.TALOARANE I, KEC.MANGANITU	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.070.020.002.0106.0	\N	\N	\N	\N	\N	\N	\N	\N	1280	0	14000	0	1280	14000	17920000	0	0	0	17920000	17920000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	75000000	60000000	15000000	750000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-03-22 14:22:25	20	7103012100020	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100020
28	cb403cbff0c04b5990168b89e8c21a9f	28	2021-04-23 14:22:25	2021-04-23 14:22:25	2021	\N	1	8	\N	\N	\N	TAJO TITAH	7103171610710000	\N	KEL. SAWANG BENDAR, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0052.0	\N	\N	\N	\N	\N	\N	\N	\N	269	390	394000	823000	269	394000	105986000	390	823000	320970000	426956000	426956390	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25000000	60000000	366956000	18347800	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-23 14:22:25	28	7103012100028	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100028
35	4c83f24b88aa4c7ab1203d5dc5ee39b6	35	2021-05-03 14:22:25	2021-05-03 14:22:25	2021	\N	1	1	\N	\N	\N	TITIK RAHAYU KUSUMAHATI	\N	\N	KEL. SOATALOARA II, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.015.004.0146.0	\N	\N	\N	\N	\N	\N	\N	\N	281	66	243000	429000	281	243000	68283000	66	429000	28314000	96597000	96597066	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	215500000	60000000	155500000	7775000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-03 14:22:25	35	7103012100035	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100035
41	921ea8e0829344ebaa202671f45ae575	41	2021-05-10 14:22:25	2021-05-10 14:22:25	2021	\N	1	8	\N	\N	\N	LISCHA WURANGIAN	\N	\N	KEL. SAWANG BENDAR, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0143.0	\N	\N	\N	\N	\N	\N	\N	\N	300	290	802000	968000	300	802000	240600000	290	968000	280720000	521320000	521320290	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	100000000	60000000	461320000	23066000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-10 14:22:25	41	7103012100041	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:18	012100041
66	7d7bef5a39d344c6bd0ad275224e7852	66	2021-07-21 14:22:25	2021-07-21 14:22:25	2021	\N	1	8	\N	\N	\N	CALVIN SELIANG	\N	\N	KEL. SOATALOARA II KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.015.003.0131.0	\N	\N	\N	\N	\N	\N	\N	\N	499	168	243000	429000	499	243000	121257000	168	429000	72072000	193329000	193329168	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	225000000	60000000	165000000	8250000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-07-21 14:22:25	66	7103012100066	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100066
26	5231eceab0af445ab09f1a82fcad8b6c	26	2021-04-22 14:22:25	2021-04-22 14:22:25	2021	\N	1	8	\N	\N	\N	AFAN NIA EDY ZION THUNGARI	\N	\N	KEL. SANTIAGO, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.017.003.0301.7	\N	\N	\N	\N	\N	\N	\N	\N	160	0	128000	0	160	128000	20480000	0	0	0	20480000	20480000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	150000000	60000000	90000000	4500000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-22 14:22:25	26	7103012100026	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100026
65	c179bd553d7541b69f73b6a57db82572	65	2021-07-21 14:22:25	2021-07-21 14:22:25	2021	\N	1	8	\N	\N	\N	ADITYA JOHANES SELIANG	7103170501870300	\N	KEL. MANENTE KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.018.002.0114.0	\N	\N	\N	\N	\N	\N	\N	\N	373	88	128000	823000	373	128000	47744000	88	823000	72424000	120168000	120168088	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	300000000	60000000	240000000	12000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-07-21 14:22:25	65	7103012100065	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100065
73	3ffed142e5634c838afb44dc1eef760c	73	2021-08-02 14:22:25	2021-08-02 14:22:25	2021	\N	1	8	\N	\N	\N	DAHLIA BASALAMAH	\N	\N	KEL.ISTIQLAL MANADO	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.004.0102.0	\N	\N	\N	\N	\N	\N	\N	\N	240	182	702000	595000	240	702000	168480000	182	595000	108290000	276770000	276770182	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	276770000	0	276770000	13838500	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-02 14:22:25	73	7103012100073	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100073
13	f7198b096fd94d4d9691f8e0ef59d52c	13	2021-02-15 14:22:25	2021-02-15 14:22:25	2021	\N	1	5	\N	\N	\N	PELAPIUS MUMU	\N	\N	KPG.NAHA. KEC.TABUKAN UTARA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.090.024.002.0073.0	\N	\N	\N	\N	\N	\N	\N	\N	588	0	103000	0	588	103000	60564000	0	0	0	60564000	60564000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	60564000	60000000	564000	28200	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-02-15 14:22:25	13	7103012100013	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100013
12	9cc624effd394c7e8cb54a1fa90b9857	12	2021-02-15 14:22:25	2021-02-15 14:22:25	2021	\N	1	3	\N	\N	\N	TAJO TITAH	7103171610710000	\N	KEL.SAWANGBENDAR,KEC.TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0052.0	\N	\N	\N	\N	\N	\N	\N	\N	269	390	394000	823000	269	394000	105986000	390	823000	320970000	426956000	426956390	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	426956000	300000000	126956000	6347800	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-02-15 14:22:25	12	7103012100012	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:16	012100012
32	f8bcd7904e704205bc108ffc138126f9	32	2021-04-23 14:22:25	2021-04-23 14:22:25	2021	\N	1	8	\N	\N	\N	LIANTJE TITAH	3273235110650000	\N	KEL. SAWANG BENDAR, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.007.003.0154.0	\N	\N	\N	\N	\N	\N	\N	\N	142	180	394000	823000	142	394000	55948000	180	823000	148140000	204088000	204088180	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	25000000	60000000	144088000	7204400	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-23 14:22:25	32	7103012100032	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100032
1	e12a3211868449088382f5b8078f7e1d	1	2021-01-06 14:22:25	2021-01-06 14:22:25	2021	\N	1	4	\N	\N	\N	GERY ANDIKA PUTRA	3175062406890010	\N	KEL.DUMUHUNG,KEC.TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.082.014.001.0014.0	\N	\N	\N	\N	\N	\N	\N	\N	524	35	36000	505000	524	36000	18864000	35	505000	17675000	36539000	36539035	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	70000000	60000000	10000000	500000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-01-06 14:22:25	1	7103012100001	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:15	012100001
81	0f72d607fdfb449693e7c8f61bef51f1	81	2021-09-15 14:22:25	2021-09-15 14:22:25	2021	\N	1	8	\N	\N	\N	SONNY TOGELANG	7103172303510000	\N	KEL. SAWANG BENDAR KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.017.001.0232.0	\N	\N	\N	\N	\N	\N	\N	\N	286	36	64000	310000	286	64000	18304000	36	310000	11160000	29464000	29464036	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	65000000	60000000	5000000	250000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-09-15 14:22:25	81	7103012100081	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:21	012100081
33	c943e65bbabd45b3b6f0b8c8d502425d	33	2021-04-27 14:22:25	2021-04-27 14:22:25	2021	\N	1	1	\N	\N	\N	APRIKONUS DAUD LORIS	7103242904810000	\N	KEL. TONA II, KEC. TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.081.021.004.0210.0	\N	\N	\N	\N	\N	\N	\N	\N	735	0	10000	0	735	10000	7350000	0	0	0	75000000	75000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	0	75000000	3750000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-04-27 14:22:25	33	7103012100033	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:17	012100033
51	89baf5c072354b958354af8c3637a4ad	51	2021-05-27 14:22:25	2021-05-27 14:22:25	2021	\N	1	8	\N	\N	\N	CHRISMIATI MALORINGAN	\N	\N	KEL. SOATALOAR II, KEC. TAHUNA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.015.003.0131.0	\N	\N	\N	\N	\N	\N	\N	\N	133	168	243000	429000	133	243000	32319000	168	429000	72072000	104391000	104391168	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	26000000	60000000	44391000	2219550	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-05-27 14:22:25	51	7103012100051	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:19	012100051
74	f25c904109d943898ea19af8ff708b10	74	2021-08-03 14:22:25	2021-08-03 14:22:25	2021	\N	1	6	\N	\N	\N	MUKTAR HAFID	7103081011630000	\N	KPG. PETTA KEC. TABUKAN UTARA	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.090.002.001.0215.0	\N	\N	\N	\N	\N	\N	\N	\N	7903	0	20000	0	7903	20000	158060000	0	0	0	158060000	158060000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	158060000	60000000	98060000	4903000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-03 14:22:25	74	7103012100074	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:20	012100074
76	de9a137d1147422e9f063d02eb2f821d	76	2021-08-16 14:22:25	2021-08-16 14:22:25	2021	\N	1	8	\N	\N	\N	SJENI LIANDO	7171025109570000	\N	KEL. MANENTE KEC. TAHUNA TIMUR	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	71.04.080.018.003.0475.0	\N	\N	\N	\N	\N	\N	\N	\N	180	0	36000	0	180	36000	6480000	0	0	0	6480000	6480000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	20000000	0	20000000	1000000	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-08-16 14:22:25	76	7103012100076	\N	\N	1	1	\N	\N	\N	2021-09-27 08:40:21	012100076
\.


--
-- Data for Name: t_validasi_berkas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_validasi_berkas (t_id_validasi_berkas, t_idspt, s_id_status_berkas, t_tglvalidasi, t_iduser_validasi, t_keterangan_berkas, created_at, updated_at, t_persyaratan_validasi_berkas) FROM stdin;
16	1	1	2021-09-27 08:23:13	1	\N	2021-09-27 08:23:13	2021-09-27 08:23:13	1,2,3,4,5,6,7
17	2	1	2021-09-27 08:23:14	1	\N	2021-09-27 08:23:14	2021-09-27 08:23:14	1,2,3,4,5,6,7
18	3	1	2021-09-27 08:23:15	1	\N	2021-09-27 08:23:15	2021-09-27 08:23:15	1,2,3,4,5,6,7
19	4	1	2021-09-27 08:23:15	1	\N	2021-09-27 08:23:15	2021-09-27 08:23:15	1,2,3,4,5,6,7
20	5	1	2021-09-27 08:23:15	1	\N	2021-09-27 08:23:15	2021-09-27 08:23:15	1,2,3,4,5,6,7
21	6	1	2021-09-27 08:23:15	1	\N	2021-09-27 08:23:15	2021-09-27 08:23:15	1,2,3,4,5,6,7
22	7	1	2021-09-27 08:23:15	1	\N	2021-09-27 08:23:15	2021-09-27 08:23:15	1,2,3,4,5,6,7
23	8	1	2021-09-27 08:23:15	1	\N	2021-09-27 08:23:15	2021-09-27 08:23:15	1,2,3,4,5,6,7
24	9	1	2021-09-27 08:23:15	1	\N	2021-09-27 08:23:15	2021-09-27 08:23:15	1,2,3,4,5,6,7
25	10	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
26	11	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
27	12	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
28	13	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
29	14	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
30	15	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
31	16	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
32	17	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
33	18	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
34	19	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
35	20	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
36	21	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
37	22	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
38	23	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
39	24	1	2021-09-27 08:23:16	1	\N	2021-09-27 08:23:16	2021-09-27 08:23:16	1,2,3,4,5,6,7
40	25	1	2021-09-27 08:23:17	1	\N	2021-09-27 08:23:17	2021-09-27 08:23:17	1,2,3,4,5,6,7
41	26	1	2021-09-27 08:23:17	1	\N	2021-09-27 08:23:17	2021-09-27 08:23:17	1,2,3,4,5,6,7
42	27	1	2021-09-27 08:23:17	1	\N	2021-09-27 08:23:17	2021-09-27 08:23:17	1,2,3,4,5,6,7
43	28	1	2021-09-27 08:23:17	1	\N	2021-09-27 08:23:17	2021-09-27 08:23:17	1,2,3,4,5,6,7
44	29	1	2021-09-27 08:23:17	1	\N	2021-09-27 08:23:17	2021-09-27 08:23:17	1,2,3,4,5,6,7
45	30	1	2021-09-27 08:23:17	1	\N	2021-09-27 08:23:17	2021-09-27 08:23:17	1,2,3,4,5,6,7
46	31	1	2021-09-27 08:23:17	1	\N	2021-09-27 08:23:17	2021-09-27 08:23:17	1,2,3,4,5,6,7
47	32	1	2021-09-27 08:23:18	1	\N	2021-09-27 08:23:18	2021-09-27 08:23:18	1,2,3,4,5,6,7
48	33	1	2021-09-27 08:23:18	1	\N	2021-09-27 08:23:18	2021-09-27 08:23:18	1,2,3,4,5,6,7
49	34	1	2021-09-27 08:23:18	1	\N	2021-09-27 08:23:18	2021-09-27 08:23:18	1,2,3,4,5,6,7
50	35	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
51	36	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
52	37	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
53	38	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
54	39	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
55	40	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
56	41	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
57	42	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
58	43	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
59	44	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
60	45	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
61	46	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
62	47	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
63	48	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
64	49	1	2021-09-27 08:23:19	1	\N	2021-09-27 08:23:19	2021-09-27 08:23:19	1,2,3,4,5,6,7
65	50	1	2021-09-27 08:23:20	1	\N	2021-09-27 08:23:20	2021-09-27 08:23:20	1,2,3,4,5,6,7
66	51	1	2021-09-27 08:23:20	1	\N	2021-09-27 08:23:20	2021-09-27 08:23:20	1,2,3,4,5,6,7
67	52	1	2021-09-27 08:23:20	1	\N	2021-09-27 08:23:20	2021-09-27 08:23:20	1,2,3,4,5,6,7
68	53	1	2021-09-27 08:23:20	1	\N	2021-09-27 08:23:20	2021-09-27 08:23:20	1,2,3,4,5,6,7
69	54	1	2021-09-27 08:23:21	1	\N	2021-09-27 08:23:21	2021-09-27 08:23:21	1,2,3,4,5,6,7
70	55	1	2021-09-27 08:23:21	1	\N	2021-09-27 08:23:21	2021-09-27 08:23:21	1,2,3,4,5,6,7
71	56	1	2021-09-27 08:23:21	1	\N	2021-09-27 08:23:21	2021-09-27 08:23:21	1,2,3,4,5,6,7
72	57	1	2021-09-27 08:23:22	1	\N	2021-09-27 08:23:22	2021-09-27 08:23:22	1,2,3,4,5,6,7
73	58	1	2021-09-27 08:23:22	1	\N	2021-09-27 08:23:22	2021-09-27 08:23:22	1,2,3,4,5,6,7
74	59	1	2021-09-27 08:23:22	1	\N	2021-09-27 08:23:22	2021-09-27 08:23:22	1,2,3,4,5,6,7
75	60	1	2021-09-27 08:23:22	1	\N	2021-09-27 08:23:22	2021-09-27 08:23:22	1,2,3,4,5,6,7
76	61	1	2021-09-27 08:23:22	1	\N	2021-09-27 08:23:22	2021-09-27 08:23:22	1,2,3,4,5,6,7
77	62	1	2021-09-27 08:23:22	1	\N	2021-09-27 08:23:22	2021-09-27 08:23:22	1,2,3,4,5,6,7
78	63	1	2021-09-27 08:23:22	1	\N	2021-09-27 08:23:22	2021-09-27 08:23:22	1,2,3,4,5,6,7
79	64	1	2021-09-27 08:23:22	1	\N	2021-09-27 08:23:22	2021-09-27 08:23:22	1,2,3,4,5,6,7
80	65	1	2021-09-27 08:23:22	1	\N	2021-09-27 08:23:22	2021-09-27 08:23:22	1,2,3,4,5,6,7
81	66	1	2021-09-27 08:23:23	1	\N	2021-09-27 08:23:23	2021-09-27 08:23:23	1,2,3,4,5,6,7
82	67	1	2021-09-27 08:23:24	1	\N	2021-09-27 08:23:24	2021-09-27 08:23:24	1,2,3,4,5,6,7
83	68	1	2021-09-27 08:23:25	1	\N	2021-09-27 08:23:25	2021-09-27 08:23:25	1,2,3,4,5,6,7
84	69	1	2021-09-27 08:23:25	1	\N	2021-09-27 08:23:25	2021-09-27 08:23:25	1,2,3,4,5,6,7
85	70	1	2021-09-27 08:23:25	1	\N	2021-09-27 08:23:25	2021-09-27 08:23:25	1,2,3,4,5,6,7
86	71	1	2021-09-27 08:23:25	1	\N	2021-09-27 08:23:25	2021-09-27 08:23:25	1,2,3,4,5,6,7
87	72	1	2021-09-27 08:23:27	1	\N	2021-09-27 08:23:27	2021-09-27 08:23:27	1,2,3,4,5,6,7
88	73	1	2021-09-27 08:23:27	1	\N	2021-09-27 08:23:27	2021-09-27 08:23:27	1,2,3,4,5,6,7
89	74	1	2021-09-27 08:23:29	1	\N	2021-09-27 08:23:29	2021-09-27 08:23:29	1,2,3,4,5,6,7
90	75	1	2021-09-27 08:23:29	1	\N	2021-09-27 08:23:29	2021-09-27 08:23:29	1,2,3,4,5,6,7
91	76	1	2021-09-27 08:23:29	1	\N	2021-09-27 08:23:29	2021-09-27 08:23:29	1,2,3,4,5,6,7
92	77	1	2021-09-27 08:23:29	1	\N	2021-09-27 08:23:29	2021-09-27 08:23:29	1,2,3,4,5,6,7
93	78	1	2021-09-27 08:23:29	1	\N	2021-09-27 08:23:29	2021-09-27 08:23:29	1,2,3,4,5,6,7
94	79	1	2021-09-27 08:23:29	1	\N	2021-09-27 08:23:29	2021-09-27 08:23:29	1,2,3,4,5,6,7
95	80	1	2021-09-27 08:23:29	1	\N	2021-09-27 08:23:29	2021-09-27 08:23:29	1,2,3,4,5,6,7
96	81	1	2021-09-27 08:23:30	1	\N	2021-09-27 08:23:30	2021-09-27 08:23:30	1,2,3,4,5,6,7
\.


--
-- Data for Name: t_validasi_kabid; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.t_validasi_kabid (t_id_validasi_kabid, t_idspt, s_id_status_kabid, t_tglvalidasi, t_iduser_validasi, t_keterangan_kabid, created_at, updated_at) FROM stdin;
12	1	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
13	2	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
14	3	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
15	4	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
16	5	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
17	6	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
18	7	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
19	8	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
20	9	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
21	10	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
22	11	1	2021-09-27 08:40:15	1	\N	2021-09-27 08:40:15	2021-09-27 08:40:15
23	12	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
24	13	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
25	14	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
26	15	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
27	16	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
28	17	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
29	18	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
30	19	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
31	20	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
32	21	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
33	22	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
34	23	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
35	24	1	2021-09-27 08:40:16	1	\N	2021-09-27 08:40:16	2021-09-27 08:40:16
36	25	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
37	26	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
38	27	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
39	28	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
40	29	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
41	30	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
42	31	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
43	32	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
44	33	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
45	34	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
46	35	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
47	36	1	2021-09-27 08:40:17	1	\N	2021-09-27 08:40:17	2021-09-27 08:40:17
48	37	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
49	38	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
50	39	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
51	40	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
52	41	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
53	42	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
54	43	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
55	44	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
56	45	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
57	46	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
58	47	1	2021-09-27 08:40:18	1	\N	2021-09-27 08:40:18	2021-09-27 08:40:18
59	48	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
60	49	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
61	50	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
62	51	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
63	52	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
64	53	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
65	54	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
66	55	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
67	56	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
68	57	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
69	58	1	2021-09-27 08:40:19	1	\N	2021-09-27 08:40:19	2021-09-27 08:40:19
70	59	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
71	60	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
72	61	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
73	62	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
74	63	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
75	64	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
76	65	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
77	66	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
78	67	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
79	68	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
80	69	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
81	70	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
82	71	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
83	72	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
84	73	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
85	74	1	2021-09-27 08:40:20	1	\N	2021-09-27 08:40:20	2021-09-27 08:40:20
86	75	1	2021-09-27 08:40:21	1	\N	2021-09-27 08:40:21	2021-09-27 08:40:21
87	76	1	2021-09-27 08:40:21	1	\N	2021-09-27 08:40:21	2021-09-27 08:40:21
88	77	1	2021-09-27 08:40:21	1	\N	2021-09-27 08:40:21	2021-09-27 08:40:21
89	78	1	2021-09-27 08:40:21	1	\N	2021-09-27 08:40:21	2021-09-27 08:40:21
90	79	1	2021-09-27 08:40:21	1	\N	2021-09-27 08:40:21	2021-09-27 08:40:21
91	80	1	2021-09-27 08:40:21	1	\N	2021-09-27 08:40:21	2021-09-27 08:40:21
92	81	1	2021-09-27 08:40:21	1	\N	2021-09-27 08:40:21	2021-09-27 08:40:21
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, username, email_verified_at, password, telegram_chat_id, s_gambar, s_tipe_pejabat, s_idpejabat, s_idnotaris, s_id_hakakses, remember_token, created_at, updated_at) FROM stdin;
1	admin	admin@mail.com	admin	\N	$2y$10$jfIk5TbIgQowYks6mvYrd.O1iLKDsIpnpwS67FxMLbTVJxO8KINpW	\N	\N	\N	\N	\N	1	\N	2021-09-07 13:54:49	2021-09-07 13:54:49
6	kpppratama	kpppratama@mail.com	kpppratama	\N	$2y$10$0YWGfjj7gcnRGMFyU5P.febKA8PFk/SVJDwPj/8K1YsDFKBHEc3hy	\N	\N	\N	\N	\N	6	\N	2021-09-07 13:54:50	2021-09-07 13:54:50
4	YANSYE ERLINA MANUMPIL	yansye@gmail.com	YANSYE	\N	$2y$10$pXivydpL/6/gBRnGvWVcpOyPkoYwYLCz9GOhRFVY18s2lixmktkIa	\N	\N	1	4	\N	2	\N	2021-09-09 12:16:22	2021-09-09 12:16:22
5	SINUS A RAKINAUNG	sinus@gmail.com	SINUS	\N	$2y$10$nurciP16CK3BXcRP.qiBh.vpc9jsXjYZ13BBNmrTnhir.jzyJLngG	\N	\N	1	5	\N	2	\N	2021-09-09 12:18:06	2021-09-09 12:18:06
7	WELLIAM M. MEKUTIKA	welliam@gmail.com	WELLIAM	\N	$2y$10$kUr/IquA7NNeprcNzb31uukrWDfV8NWcdWOpyaG474T1yIui3SRZ6	\N	\N	1	6	\N	3	\N	2021-09-09 12:19:14	2021-09-09 12:19:14
8	ARDYANTO TALOLANG, SS	ardyanto@gmail.com	ARDYANTO	\N	$2y$10$KoYw7FavS2u3PGBlll7hge4ZanX4zjrFPZ1vO8ED.ZLbxX6ML85cS	\N	\N	1	7	\N	2	\N	2021-09-09 12:21:12	2021-09-09 12:21:12
2	IMELDA V. L. LESAWENGEN, SE	imelda@gmail.com	IMELDA	\N	$2y$10$99dX/nv81O6mw3l./RGQw.QB31EgEDjqtARgVycuTzq8qyQW4kSXO	\N	upload/profil/IMG_20210810_151232.jpg	1	2	\N	2	\N	2021-09-09 12:13:04	2021-09-13 13:39:32
3	BENYAMIN MANDIANGAN A. m.Ak	benyamin@gmail.com	BENYAMIN	\N	$2y$10$F8e6ks2pkc6ikiq/eSAkpe121FMUBrEdoO7UxMd5/b/jcRAk.vOvC	\N	upload/profil/WhatsApp_Image_2021_07_15_at_11_03_263.jpg	1	3	\N	3	\N	2021-09-09 12:15:09	2021-09-14 11:27:25
10	KUSTANTI, S.H.,M. Kn	kustanti@gmail.com	KUSTANTI	\N	$2y$10$ZQuQVoNh4.AfFmYqC3hNOODzvQj2ZKN9.7KWTzoY7BOoyEzDWlfYu	\N	\N	2	\N	2	4	\N	2021-09-20 14:08:26	2021-09-20 14:08:26
11	RONIJAYA PASIAYE S.SoS, ME	ronyjaya@gmail.com	RONIJAYA	\N	$2y$10$t5.xXElmyeH1xB22iqyCcOEMSsAUcpFCQMkvCb8IiE/ZoWWTRU2BO	\N	\N	2	\N	3	4	\N	2021-09-20 14:09:29	2021-09-20 14:09:29
9	AMELIA NOVITA DANDEL, SH,M.Kn	amelia@gmail.com	AMELIA	\N	$2y$10$/oRcGFhjpI7Zp0u9/v86DeTCsmhq1FVq28RIGo9mmOTG1UT8mKhrC	\N	\N	2	\N	1	4	\N	2021-09-14 13:28:05	2021-09-20 14:09:58
12	BPN	bpn@gmail.com	BPN	\N	$2y$10$lUIi/uejvmci0b99wx.mC.BIP4ETmetNQYRpfG5RaLmCCOTslXRr.	\N	\N	2	\N	7	4	\N	2021-09-27 10:54:40	2021-09-27 10:54:40
\.


--
-- Name: activity_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.activity_log_id_seq', 411, true);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: his_getbphtb_bpn_t_idhis_getbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.his_getbphtb_bpn_t_idhis_getbpn_seq', 1, false);


--
-- Name: his_getpbb_bpn_t_idhis_pbbbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.his_getpbb_bpn_t_idhis_pbbbpn_seq', 1, false);


--
-- Name: his_postdatabpn_t_idhis_posbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.his_postdatabpn_t_idhis_posbpn_seq', 45, true);


--
-- Name: integrasi_sertifikat_bpn_t_idsertifikat_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.integrasi_sertifikat_bpn_t_idsertifikat_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 44, true);


--
-- Name: notifications_message_t_id_notif_detail_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notifications_message_t_id_notif_detail_seq', 1, false);


--
-- Name: notifications_t_id_notif_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notifications_t_id_notif_seq', 1, false);


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.permissions_id_seq', 45, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 1, true);


--
-- Name: s_acuan_jenistanah_s_idacuan_jenis_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_acuan_jenistanah_s_idacuan_jenis_seq', 4, false);


--
-- Name: s_acuan_s_idacuan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_acuan_s_idacuan_seq', 4, false);


--
-- Name: s_background_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_background_id_seq', 3, false);


--
-- Name: s_bpn_ws_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_bpn_ws_id_seq', 1, false);


--
-- Name: s_hak_akses_s_id_hakakses_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_hak_akses_s_id_hakakses_seq', 10, false);


--
-- Name: s_harga_history_s_idhistory_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_harga_history_s_idhistory_seq', 1, false);


--
-- Name: s_jenis_bidangusaha_s_idbidang_usaha_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_jenis_bidangusaha_s_idbidang_usaha_seq', 3, false);


--
-- Name: s_jenisdoktanah_s_iddoktanah_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_jenisdoktanah_s_iddoktanah_seq', 8, false);


--
-- Name: s_jenisfasilitas_s_idjenisfasilitas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_jenisfasilitas_s_idjenisfasilitas_seq', 7, false);


--
-- Name: s_jenishaktanah_s_idhaktanah_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_jenishaktanah_s_idhaktanah_seq', 7, false);


--
-- Name: s_jenisketetapan_s_idjenisketetapan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_jenisketetapan_s_idjenisketetapan_seq', 6, false);


--
-- Name: s_jenispengurangan_s_idpengurangan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_jenispengurangan_s_idpengurangan_seq', 4, false);


--
-- Name: s_jenistanah_id_jenistanah_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_jenistanah_id_jenistanah_seq', 3, false);


--
-- Name: s_jenistransaksi_s_idjenistransaksi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_jenistransaksi_s_idjenistransaksi_seq', 17, false);


--
-- Name: s_kecamatan_s_idkecamatan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_kecamatan_s_idkecamatan_seq', 26, true);


--
-- Name: s_kelurahan_s_idkelurahan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_kelurahan_s_idkelurahan_seq', 352, true);


--
-- Name: s_koderekening_s_korekid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_koderekening_s_korekid_seq', 5, false);


--
-- Name: s_notaris_s_idnotaris_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_notaris_s_idnotaris_seq', 10, true);


--
-- Name: s_pejabat_s_idpejabat_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_pejabat_s_idpejabat_seq', 7, true);


--
-- Name: s_pemda_s_idpemda_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_pemda_s_idpemda_seq', 1, false);


--
-- Name: s_persyaratan_s_idpersyaratan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_persyaratan_s_idpersyaratan_seq', 154, true);


--
-- Name: s_presentase_wajar_s_idpresentase_wajar_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_presentase_wajar_s_idpresentase_wajar_seq', 5, false);


--
-- Name: s_status_bayar_s_id_status_bayar_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_status_bayar_s_id_status_bayar_seq', 3, false);


--
-- Name: s_status_berkas_s_id_status_berkas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_status_berkas_s_id_status_berkas_seq', 3, false);


--
-- Name: s_status_dptnpoptkp_s_id_dptnpoptkp_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_status_dptnpoptkp_s_id_dptnpoptkp_seq', 3, false);


--
-- Name: s_status_kabid_s_id_status_kabid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_status_kabid_s_id_status_kabid_seq', 3, false);


--
-- Name: s_status_lihat_s_id_status_lihat_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_status_lihat_s_id_status_lihat_seq', 3, false);


--
-- Name: s_status_npwpd_s_idstatus_npwpd_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_status_npwpd_s_idstatus_npwpd_seq', 3, false);


--
-- Name: s_status_pelayanan_s_id_status_layanan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_status_pelayanan_s_id_status_layanan_seq', 1, false);


--
-- Name: s_status_perhitungan_s_idstatus_pht_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_status_perhitungan_s_idstatus_pht_seq', 4, false);


--
-- Name: s_status_s_id_status_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_status_s_id_status_seq', 3, false);


--
-- Name: s_target_bphtb_s_id_target_bphtb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_target_bphtb_s_id_target_bphtb_seq', 3, false);


--
-- Name: s_target_status_s_id_target_status_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_target_status_s_id_target_status_seq', 3, false);


--
-- Name: s_tarif_bphtb_s_idtarifbphtb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_tarif_bphtb_s_idtarifbphtb_seq', 2, false);


--
-- Name: s_tarifnpoptkp_s_idtarifnpoptkp_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_tarifnpoptkp_s_idtarifnpoptkp_seq', 17, false);


--
-- Name: s_tempo_s_idtempo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_tempo_s_idtempo_seq', 2, false);


--
-- Name: s_users_api_s_idusers_api_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_users_api_s_idusers_api_seq', 1, false);


--
-- Name: s_wajib_up_s_idwajib_up_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.s_wajib_up_s_idwajib_up_seq', 3, false);


--
-- Name: t_bpn_t_idbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_bpn_t_idbpn_seq', 1, false);


--
-- Name: t_file_keringanan_t_idfile_keringanan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_file_keringanan_t_idfile_keringanan_seq', 1, false);


--
-- Name: t_file_objek_t_idfile_objek_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_file_objek_t_idfile_objek_seq', 6, true);


--
-- Name: t_filesyarat_bphtb_t_id_filesyarat_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_filesyarat_bphtb_t_id_filesyarat_seq', 46, true);


--
-- Name: t_inputajb_t_idajb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_inputajb_t_idajb_seq', 2, true);


--
-- Name: t_laporbulanan_head_t_idlaporbulanan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_laporbulanan_head_t_idlaporbulanan_seq', 2, true);


--
-- Name: t_nik_bersama_t_id_nik_bersama_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_nik_bersama_t_id_nik_bersama_seq', 1, false);


--
-- Name: t_notif_validasi_berkas_t_id_notif_berkas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_notif_validasi_berkas_t_id_notif_berkas_seq', 1, false);


--
-- Name: t_notif_validasi_kabid_t_id_notif_kabid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_notif_validasi_kabid_t_id_notif_kabid_seq', 1, false);


--
-- Name: t_pelayanan_angsuran_t_idangsuran_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pelayanan_angsuran_t_idangsuran_seq', 1, false);


--
-- Name: t_pelayanan_keberatan_t_idkeberatan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pelayanan_keberatan_t_idkeberatan_seq', 1, false);


--
-- Name: t_pelayanan_keringanan_t_idkeringanan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pelayanan_keringanan_t_idkeringanan_seq', 1, false);


--
-- Name: t_pelayanan_mutasi_t_idmutasi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pelayanan_mutasi_t_idmutasi_seq', 1, false);


--
-- Name: t_pelayanan_pembatalan_t_idpembatalan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pelayanan_pembatalan_t_idpembatalan_seq', 1, false);


--
-- Name: t_pelayanan_penundaan_t_idpenundaan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pelayanan_penundaan_t_idpenundaan_seq', 1, false);


--
-- Name: t_pembayaran_bphtb_t_id_pembayaran_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pembayaran_bphtb_t_id_pembayaran_seq', 91, true);


--
-- Name: t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq', 1, true);


--
-- Name: t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq', 1, false);


--
-- Name: t_pemeriksaan_t_idpemeriksa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pemeriksaan_t_idpemeriksa_seq', 1, false);


--
-- Name: t_pen_denda_ajb_notaris_t_id_ajbdenda_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pen_denda_ajb_notaris_t_id_ajbdenda_seq', 1, false);


--
-- Name: t_pen_denda_lpor_notaris_t_id_pendenda_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_pen_denda_lpor_notaris_t_id_pendenda_seq', 1, false);


--
-- Name: t_skpdkb_t_idskpdkb_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_skpdkb_t_idskpdkb_seq', 1, true);


--
-- Name: t_skpdkbt_t_idskpdkbt_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_skpdkbt_t_idskpdkbt_seq', 1, false);


--
-- Name: t_spt_t_idspt_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_spt_t_idspt_seq', 85, true);


--
-- Name: t_validasi_berkas_t_id_validasi_berkas_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_validasi_berkas_t_id_validasi_berkas_seq', 98, true);


--
-- Name: t_validasi_kabid_t_id_validasi_kabid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.t_validasi_kabid_t_id_validasi_kabid_seq', 94, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 12, true);


--
-- Name: activity_log activity_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activity_log
    ADD CONSTRAINT activity_log_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);


--
-- Name: his_getbphtb_bpn his_getbphtb_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.his_getbphtb_bpn
    ADD CONSTRAINT his_getbphtb_bpn_pkey PRIMARY KEY (t_idhis_getbpn);


--
-- Name: his_getpbb_bpn his_getpbb_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.his_getpbb_bpn
    ADD CONSTRAINT his_getpbb_bpn_pkey PRIMARY KEY (t_idhis_pbbbpn);


--
-- Name: his_postdatabpn his_postdatabpn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.his_postdatabpn
    ADD CONSTRAINT his_postdatabpn_pkey PRIMARY KEY (t_idhis_posbpn);


--
-- Name: integrasi_sertifikat_bpn integrasi_sertifikat_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.integrasi_sertifikat_bpn
    ADD CONSTRAINT integrasi_sertifikat_bpn_pkey PRIMARY KEY (t_idsertifikat);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: model_has_permissions model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- Name: model_has_roles model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- Name: notifications_message notifications_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notifications_message
    ADD CONSTRAINT notifications_message_pkey PRIMARY KEY (t_id_notif_detail);


--
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (t_id_notif);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: role_has_permissions role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: s_acuan_jenistanah s_acuan_jenistanah_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_acuan_jenistanah
    ADD CONSTRAINT s_acuan_jenistanah_pkey PRIMARY KEY (s_idacuan_jenis);


--
-- Name: s_acuan s_acuan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_acuan
    ADD CONSTRAINT s_acuan_pkey PRIMARY KEY (s_idacuan);


--
-- Name: s_background s_background_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_background
    ADD CONSTRAINT s_background_pkey PRIMARY KEY (id);


--
-- Name: s_bpn_ws s_bpn_ws_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_bpn_ws
    ADD CONSTRAINT s_bpn_ws_pkey PRIMARY KEY (id);


--
-- Name: s_hak_akses s_hak_akses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_hak_akses
    ADD CONSTRAINT s_hak_akses_pkey PRIMARY KEY (s_id_hakakses);


--
-- Name: s_harga_history s_harga_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_harga_history
    ADD CONSTRAINT s_harga_history_pkey PRIMARY KEY (s_idhistory);


--
-- Name: s_jenis_bidangusaha s_jenis_bidangusaha_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenis_bidangusaha
    ADD CONSTRAINT s_jenis_bidangusaha_pkey PRIMARY KEY (s_idbidang_usaha);


--
-- Name: s_jenisdoktanah s_jenisdoktanah_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenisdoktanah
    ADD CONSTRAINT s_jenisdoktanah_pkey PRIMARY KEY (s_iddoktanah);


--
-- Name: s_jenisfasilitas s_jenisfasilitas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenisfasilitas
    ADD CONSTRAINT s_jenisfasilitas_pkey PRIMARY KEY (s_idjenisfasilitas);


--
-- Name: s_jenishaktanah s_jenishaktanah_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenishaktanah
    ADD CONSTRAINT s_jenishaktanah_pkey PRIMARY KEY (s_idhaktanah);


--
-- Name: s_jenisketetapan s_jenisketetapan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenisketetapan
    ADD CONSTRAINT s_jenisketetapan_pkey PRIMARY KEY (s_idjenisketetapan);


--
-- Name: s_jenispengurangan s_jenispengurangan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenispengurangan
    ADD CONSTRAINT s_jenispengurangan_pkey PRIMARY KEY (s_idpengurangan);


--
-- Name: s_jenistanah s_jenistanah_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenistanah
    ADD CONSTRAINT s_jenistanah_pkey PRIMARY KEY (id_jenistanah);


--
-- Name: s_jenistransaksi s_jenistransaksi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenistransaksi
    ADD CONSTRAINT s_jenistransaksi_pkey PRIMARY KEY (s_idjenistransaksi);


--
-- Name: s_kecamatan s_kecamatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_kecamatan
    ADD CONSTRAINT s_kecamatan_pkey PRIMARY KEY (s_idkecamatan);


--
-- Name: s_kelurahan s_kelurahan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_kelurahan
    ADD CONSTRAINT s_kelurahan_pkey PRIMARY KEY (s_idkelurahan);


--
-- Name: s_koderekening s_koderekening_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_koderekening
    ADD CONSTRAINT s_koderekening_pkey PRIMARY KEY (s_korekid);


--
-- Name: s_notaris s_notaris_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_notaris
    ADD CONSTRAINT s_notaris_pkey PRIMARY KEY (s_idnotaris);


--
-- Name: s_pejabat s_pejabat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_pejabat
    ADD CONSTRAINT s_pejabat_pkey PRIMARY KEY (s_idpejabat);


--
-- Name: s_pemda s_pemda_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_pemda
    ADD CONSTRAINT s_pemda_pkey PRIMARY KEY (s_idpemda);


--
-- Name: s_persyaratan s_persyaratan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_persyaratan
    ADD CONSTRAINT s_persyaratan_pkey PRIMARY KEY (s_idpersyaratan);


--
-- Name: s_presentase_wajar s_presentase_wajar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_presentase_wajar
    ADD CONSTRAINT s_presentase_wajar_pkey PRIMARY KEY (s_idpresentase_wajar);


--
-- Name: s_status_bayar s_status_bayar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_bayar
    ADD CONSTRAINT s_status_bayar_pkey PRIMARY KEY (s_id_status_bayar);


--
-- Name: s_status_berkas s_status_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_berkas
    ADD CONSTRAINT s_status_berkas_pkey PRIMARY KEY (s_id_status_berkas);


--
-- Name: s_status_dptnpoptkp s_status_dptnpoptkp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_dptnpoptkp
    ADD CONSTRAINT s_status_dptnpoptkp_pkey PRIMARY KEY (s_id_dptnpoptkp);


--
-- Name: s_status_kabid s_status_kabid_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_kabid
    ADD CONSTRAINT s_status_kabid_pkey PRIMARY KEY (s_id_status_kabid);


--
-- Name: s_status_lihat s_status_lihat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_lihat
    ADD CONSTRAINT s_status_lihat_pkey PRIMARY KEY (s_id_status_lihat);


--
-- Name: s_status_npwpd s_status_npwpd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_npwpd
    ADD CONSTRAINT s_status_npwpd_pkey PRIMARY KEY (s_idstatus_npwpd);


--
-- Name: s_status_pelayanan s_status_pelayanan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_pelayanan
    ADD CONSTRAINT s_status_pelayanan_pkey PRIMARY KEY (s_id_status_layanan);


--
-- Name: s_status_perhitungan s_status_perhitungan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status_perhitungan
    ADD CONSTRAINT s_status_perhitungan_pkey PRIMARY KEY (s_idstatus_pht);


--
-- Name: s_status s_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_status
    ADD CONSTRAINT s_status_pkey PRIMARY KEY (s_id_status);


--
-- Name: s_target_bphtb s_target_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_target_bphtb
    ADD CONSTRAINT s_target_bphtb_pkey PRIMARY KEY (s_id_target_bphtb);


--
-- Name: s_target_status s_target_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_target_status
    ADD CONSTRAINT s_target_status_pkey PRIMARY KEY (s_id_target_status);


--
-- Name: s_tarif_bphtb s_tarif_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_tarif_bphtb
    ADD CONSTRAINT s_tarif_bphtb_pkey PRIMARY KEY (s_idtarifbphtb);


--
-- Name: s_tarifnpoptkp s_tarifnpoptkp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_tarifnpoptkp
    ADD CONSTRAINT s_tarifnpoptkp_pkey PRIMARY KEY (s_idtarifnpoptkp);


--
-- Name: s_tempo s_tempo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_tempo
    ADD CONSTRAINT s_tempo_pkey PRIMARY KEY (s_idtempo);


--
-- Name: s_users_api s_users_api_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_users_api
    ADD CONSTRAINT s_users_api_pkey PRIMARY KEY (s_idusers_api);


--
-- Name: s_wajib_up s_wajib_up_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_wajib_up
    ADD CONSTRAINT s_wajib_up_pkey PRIMARY KEY (s_idwajib_up);


--
-- Name: t_bpn t_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_bpn
    ADD CONSTRAINT t_bpn_pkey PRIMARY KEY (t_idbpn);


--
-- Name: t_file_keringanan t_file_keringanan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_file_keringanan
    ADD CONSTRAINT t_file_keringanan_pkey PRIMARY KEY (t_idfile_keringanan);


--
-- Name: t_file_objek t_file_objek_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_file_objek
    ADD CONSTRAINT t_file_objek_pkey PRIMARY KEY (t_idfile_objek);


--
-- Name: t_filesyarat_bphtb t_filesyarat_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_pkey PRIMARY KEY (t_id_filesyarat);


--
-- Name: t_inputajb t_inputajb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_inputajb
    ADD CONSTRAINT t_inputajb_pkey PRIMARY KEY (t_idajb);


--
-- Name: t_laporbulanan_head t_laporbulanan_head_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_laporbulanan_head
    ADD CONSTRAINT t_laporbulanan_head_pkey PRIMARY KEY (t_idlaporbulanan);


--
-- Name: t_nik_bersama t_nik_bersama_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_nik_bersama
    ADD CONSTRAINT t_nik_bersama_pkey PRIMARY KEY (t_id_nik_bersama);


--
-- Name: t_notif_validasi_berkas t_notif_validasi_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_pkey PRIMARY KEY (t_id_notif_berkas);


--
-- Name: t_notif_validasi_kabid t_notif_validasi_kabid_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_pkey PRIMARY KEY (t_id_notif_kabid);


--
-- Name: t_pelayanan_angsuran t_pelayanan_angsuran_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_pkey PRIMARY KEY (t_idangsuran);


--
-- Name: t_pelayanan_keberatan t_pelayanan_keberatan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_pkey PRIMARY KEY (t_idkeberatan);


--
-- Name: t_pelayanan_keringanan t_pelayanan_keringanan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_pkey PRIMARY KEY (t_idkeringanan);


--
-- Name: t_pelayanan_mutasi t_pelayanan_mutasi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_pkey PRIMARY KEY (t_idmutasi);


--
-- Name: t_pelayanan_pembatalan t_pelayanan_pembatalan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_pkey PRIMARY KEY (t_idpembatalan);


--
-- Name: t_pelayanan_penundaan t_pelayanan_penundaan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_pkey PRIMARY KEY (t_idpenundaan);


--
-- Name: t_pembayaran_bphtb t_pembayaran_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_pkey PRIMARY KEY (t_id_pembayaran);


--
-- Name: t_pembayaran_skpdkb t_pembayaran_skpdkb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_pkey PRIMARY KEY (t_id_bayar_skpdkb);


--
-- Name: t_pembayaran_skpdkbt t_pembayaran_skpdkbt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_pkey PRIMARY KEY (t_id_bayar_skpdkb);


--
-- Name: t_pemeriksaan t_pemeriksaan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_pkey PRIMARY KEY (t_idpemeriksa);


--
-- Name: t_pen_denda_ajb_notaris t_pen_denda_ajb_notaris_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_pkey PRIMARY KEY (t_id_ajbdenda);


--
-- Name: t_pen_denda_lpor_notaris t_pen_denda_lpor_notaris_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_pkey PRIMARY KEY (t_id_pendenda);


--
-- Name: t_skpdkb t_skpdkb_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_pkey PRIMARY KEY (t_idskpdkb);


--
-- Name: t_skpdkbt t_skpdkbt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_pkey PRIMARY KEY (t_idskpdkbt);


--
-- Name: t_spt t_spt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_pkey PRIMARY KEY (t_idspt);


--
-- Name: t_validasi_berkas t_validasi_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_pkey PRIMARY KEY (t_id_validasi_berkas);


--
-- Name: t_validasi_kabid t_validasi_kabid_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_pkey PRIMARY KEY (t_id_validasi_kabid);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: activity_log_log_name_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX activity_log_log_name_index ON public.activity_log USING btree (log_name);


--
-- Name: causer; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX causer ON public.activity_log USING btree (causer_type, causer_id);


--
-- Name: model_has_permissions_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_permissions_model_id_model_type_index ON public.model_has_permissions USING btree (model_id, model_type);


--
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_roles_model_id_model_type_index ON public.model_has_roles USING btree (model_id, model_type);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: subject; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX subject ON public.activity_log USING btree (subject_type, subject_id);


--
-- Name: model_has_permissions model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: model_has_roles model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: notifications_message notifications_message_t_id_notif_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notifications_message
    ADD CONSTRAINT notifications_message_t_id_notif_foreign FOREIGN KEY (t_id_notif) REFERENCES public.notifications(t_id_notif);


--
-- Name: notifications notifications_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: role_has_permissions role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: role_has_permissions role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: s_acuan_jenistanah s_acuan_jenistanah_id_jenistanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_acuan_jenistanah
    ADD CONSTRAINT s_acuan_jenistanah_id_jenistanah_foreign FOREIGN KEY (id_jenistanah) REFERENCES public.s_jenistanah(id_jenistanah);


--
-- Name: s_background s_background_s_id_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_background
    ADD CONSTRAINT s_background_s_id_status_foreign FOREIGN KEY (s_id_status) REFERENCES public.s_status(s_id_status);


--
-- Name: s_jenistransaksi s_jenistransaksi_s_id_dptnpoptkp_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenistransaksi
    ADD CONSTRAINT s_jenistransaksi_s_id_dptnpoptkp_foreign FOREIGN KEY (s_id_dptnpoptkp) REFERENCES public.s_status_dptnpoptkp(s_id_dptnpoptkp);


--
-- Name: s_jenistransaksi s_jenistransaksi_s_idstatus_pht_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_jenistransaksi
    ADD CONSTRAINT s_jenistransaksi_s_idstatus_pht_foreign FOREIGN KEY (s_idstatus_pht) REFERENCES public.s_status_perhitungan(s_idstatus_pht);


--
-- Name: s_kelurahan s_kelurahan_s_idkecamatan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_kelurahan
    ADD CONSTRAINT s_kelurahan_s_idkecamatan_foreign FOREIGN KEY (s_idkecamatan) REFERENCES public.s_kecamatan(s_idkecamatan);


--
-- Name: s_persyaratan s_persyaratan_s_idwajib_up_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_persyaratan
    ADD CONSTRAINT s_persyaratan_s_idwajib_up_foreign FOREIGN KEY (s_idwajib_up) REFERENCES public.s_wajib_up(s_idwajib_up);


--
-- Name: s_target_bphtb s_target_bphtb_s_id_target_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_target_bphtb
    ADD CONSTRAINT s_target_bphtb_s_id_target_status_foreign FOREIGN KEY (s_id_target_status) REFERENCES public.s_target_status(s_id_target_status);


--
-- Name: s_tarifnpoptkp s_tarifnpoptkp_s_idjenistransaksinpoptkp_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_tarifnpoptkp
    ADD CONSTRAINT s_tarifnpoptkp_s_idjenistransaksinpoptkp_foreign FOREIGN KEY (s_idjenistransaksinpoptkp) REFERENCES public.s_jenistransaksi(s_idjenistransaksi);


--
-- Name: s_tarifnpoptkp s_tarifnpoptkp_s_statusnpoptkp_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_tarifnpoptkp
    ADD CONSTRAINT s_tarifnpoptkp_s_statusnpoptkp_foreign FOREIGN KEY (s_statusnpoptkp) REFERENCES public.s_status(s_id_status);


--
-- Name: s_tempo s_tempo_s_id_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_tempo
    ADD CONSTRAINT s_tempo_s_id_status_foreign FOREIGN KEY (s_id_status) REFERENCES public.s_status(s_id_status);


--
-- Name: t_file_keringanan t_file_keringanan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_file_keringanan
    ADD CONSTRAINT t_file_keringanan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_file_keringanan t_file_keringanan_t_iduser_upload_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_file_keringanan
    ADD CONSTRAINT t_file_keringanan_t_iduser_upload_foreign FOREIGN KEY (t_iduser_upload) REFERENCES public.users(id);


--
-- Name: t_file_objek t_file_objek_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_file_objek
    ADD CONSTRAINT t_file_objek_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_file_objek t_file_objek_t_iduser_upload_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_file_objek
    ADD CONSTRAINT t_file_objek_t_iduser_upload_foreign FOREIGN KEY (t_iduser_upload) REFERENCES public.users(id);


--
-- Name: t_filesyarat_bphtb t_filesyarat_bphtb_s_idjenistransaksi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_s_idjenistransaksi_foreign FOREIGN KEY (s_idjenistransaksi) REFERENCES public.s_jenistransaksi(s_idjenistransaksi);


--
-- Name: t_filesyarat_bphtb t_filesyarat_bphtb_s_idpersyaratan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_s_idpersyaratan_foreign FOREIGN KEY (s_idpersyaratan) REFERENCES public.s_persyaratan(s_idpersyaratan);


--
-- Name: t_filesyarat_bphtb t_filesyarat_bphtb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_filesyarat_bphtb t_filesyarat_bphtb_t_iduser_upload_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_t_iduser_upload_foreign FOREIGN KEY (t_iduser_upload) REFERENCES public.users(id);


--
-- Name: t_inputajb t_inputajb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_inputajb
    ADD CONSTRAINT t_inputajb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_inputajb t_inputajb_t_iduser_input_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_inputajb
    ADD CONSTRAINT t_inputajb_t_iduser_input_foreign FOREIGN KEY (t_iduser_input) REFERENCES public.users(id);


--
-- Name: t_laporbulanan_detail t_laporbulanan_detail_t_idajb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_laporbulanan_detail
    ADD CONSTRAINT t_laporbulanan_detail_t_idajb_foreign FOREIGN KEY (t_idajb) REFERENCES public.t_inputajb(t_idajb);


--
-- Name: t_laporbulanan_detail t_laporbulanan_detail_t_idlaporbulanan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_laporbulanan_detail
    ADD CONSTRAINT t_laporbulanan_detail_t_idlaporbulanan_foreign FOREIGN KEY (t_idlaporbulanan) REFERENCES public.t_laporbulanan_head(t_idlaporbulanan);


--
-- Name: t_laporbulanan_head t_laporbulanan_head_t_iduser_input_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_laporbulanan_head
    ADD CONSTRAINT t_laporbulanan_head_t_iduser_input_foreign FOREIGN KEY (t_iduser_input) REFERENCES public.users(id);


--
-- Name: t_nik_bersama t_nik_bersama_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_nik_bersama
    ADD CONSTRAINT t_nik_bersama_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_notif_validasi_berkas t_notif_validasi_berkas_s_id_status_lihat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_s_id_status_lihat_foreign FOREIGN KEY (s_id_status_lihat) REFERENCES public.users(id);


--
-- Name: t_notif_validasi_berkas t_notif_validasi_berkas_t_dari_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_t_dari_foreign FOREIGN KEY (t_dari) REFERENCES public.users(id);


--
-- Name: t_notif_validasi_berkas t_notif_validasi_berkas_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_notif_validasi_berkas t_notif_validasi_berkas_t_untuk_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_t_untuk_foreign FOREIGN KEY (t_untuk) REFERENCES public.users(id);


--
-- Name: t_notif_validasi_kabid t_notif_validasi_kabid_s_id_status_lihat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_s_id_status_lihat_foreign FOREIGN KEY (s_id_status_lihat) REFERENCES public.users(id);


--
-- Name: t_notif_validasi_kabid t_notif_validasi_kabid_t_dari_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_t_dari_foreign FOREIGN KEY (t_dari) REFERENCES public.users(id);


--
-- Name: t_notif_validasi_kabid t_notif_validasi_kabid_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_notif_validasi_kabid t_notif_validasi_kabid_t_untuk_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_t_untuk_foreign FOREIGN KEY (t_untuk) REFERENCES public.users(id);


--
-- Name: t_pelayanan_angsuran t_pelayanan_angsuran_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_angsuran t_pelayanan_angsuran_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_angsuran t_pelayanan_angsuran_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_angsuran t_pelayanan_angsuran_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pelayanan_keberatan t_pelayanan_keberatan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_keberatan t_pelayanan_keberatan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_keberatan t_pelayanan_keberatan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_keberatan t_pelayanan_keberatan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pelayanan_keringanan t_pelayanan_keringanan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_keringanan t_pelayanan_keringanan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_keringanan t_pelayanan_keringanan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_keringanan t_pelayanan_keringanan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pelayanan_mutasi t_pelayanan_mutasi_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_mutasi t_pelayanan_mutasi_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_mutasi t_pelayanan_mutasi_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_mutasi t_pelayanan_mutasi_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pelayanan_pembatalan t_pelayanan_pembatalan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_pembatalan t_pelayanan_pembatalan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_pembatalan t_pelayanan_pembatalan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_pembatalan t_pelayanan_pembatalan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pelayanan_penundaan t_pelayanan_penundaan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_penundaan t_pelayanan_penundaan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_penundaan t_pelayanan_penundaan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_penundaan t_pelayanan_penundaan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pembayaran_bphtb t_pembayaran_bphtb_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES public.s_status_bayar(s_id_status_bayar);


--
-- Name: t_pembayaran_bphtb t_pembayaran_bphtb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pembayaran_bphtb t_pembayaran_bphtb_t_iduser_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_t_iduser_bayar_foreign FOREIGN KEY (t_iduser_bayar) REFERENCES public.users(id);


--
-- Name: t_pembayaran_skpdkb t_pembayaran_skpdkb_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES public.s_status_bayar(s_id_status_bayar);


--
-- Name: t_pembayaran_skpdkb t_pembayaran_skpdkb_t_idskpdkb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_t_idskpdkb_foreign FOREIGN KEY (t_idskpdkb) REFERENCES public.t_skpdkb(t_idskpdkb);


--
-- Name: t_pembayaran_skpdkb t_pembayaran_skpdkb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pembayaran_skpdkb t_pembayaran_skpdkb_t_iduser_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_t_iduser_bayar_foreign FOREIGN KEY (t_iduser_bayar) REFERENCES public.users(id);


--
-- Name: t_pembayaran_skpdkbt t_pembayaran_skpdkbt_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES public.s_status_bayar(s_id_status_bayar);


--
-- Name: t_pembayaran_skpdkbt t_pembayaran_skpdkbt_t_idskpdkbt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_t_idskpdkbt_foreign FOREIGN KEY (t_idskpdkbt) REFERENCES public.t_skpdkb(t_idskpdkb);


--
-- Name: t_pembayaran_skpdkbt t_pembayaran_skpdkbt_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pembayaran_skpdkbt t_pembayaran_skpdkbt_t_iduser_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_t_iduser_bayar_foreign FOREIGN KEY (t_iduser_bayar) REFERENCES public.users(id);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES public.users(id);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idpejabat1_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat1_foreign FOREIGN KEY (t_idpejabat1) REFERENCES public.s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idpejabat2_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat2_foreign FOREIGN KEY (t_idpejabat2) REFERENCES public.s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idpejabat3_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat3_foreign FOREIGN KEY (t_idpejabat3) REFERENCES public.s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idpejabat4_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat4_foreign FOREIGN KEY (t_idpejabat4) REFERENCES public.s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idtarifbphtb_pemeriksa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idtarifbphtb_pemeriksa_foreign FOREIGN KEY (t_idtarifbphtb_pemeriksa) REFERENCES public.s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_pen_denda_ajb_notaris t_pen_denda_ajb_notaris_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES public.s_status_bayar(s_id_status_bayar);


--
-- Name: t_pen_denda_ajb_notaris t_pen_denda_ajb_notaris_t_idnotaris_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_t_idnotaris_foreign FOREIGN KEY (t_idnotaris) REFERENCES public.s_notaris(s_idnotaris);


--
-- Name: t_pen_denda_ajb_notaris t_pen_denda_ajb_notaris_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pen_denda_ajb_notaris t_pen_denda_ajb_notaris_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_pen_denda_lpor_notaris t_pen_denda_lpor_notaris_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES public.s_status_bayar(s_id_status_bayar);


--
-- Name: t_pen_denda_lpor_notaris t_pen_denda_lpor_notaris_t_idlaporbulanan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_t_idlaporbulanan_foreign FOREIGN KEY (t_idlaporbulanan) REFERENCES public.t_laporbulanan_head(t_idlaporbulanan);


--
-- Name: t_pen_denda_lpor_notaris t_pen_denda_lpor_notaris_t_idnotaris_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_t_idnotaris_foreign FOREIGN KEY (t_idnotaris) REFERENCES public.s_notaris(s_idnotaris);


--
-- Name: t_pen_denda_lpor_notaris t_pen_denda_lpor_notaris_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_skpdkb t_skpdkb_t_idjenisketetapan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idjenisketetapan_foreign FOREIGN KEY (t_idjenisketetapan) REFERENCES public.s_jenisketetapan(s_idjenisketetapan);


--
-- Name: t_skpdkb t_skpdkb_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES public.users(id);


--
-- Name: t_skpdkb t_skpdkb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_skpdkb t_skpdkb_t_idtarifbphtb_skpdkb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idtarifbphtb_skpdkb_foreign FOREIGN KEY (t_idtarifbphtb_skpdkb) REFERENCES public.s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_skpdkb t_skpdkb_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_skpdkbt t_skpdkbt_t_idjenisketetapan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idjenisketetapan_foreign FOREIGN KEY (t_idjenisketetapan) REFERENCES public.s_jenisketetapan(s_idjenisketetapan);


--
-- Name: t_skpdkbt t_skpdkbt_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES public.users(id);


--
-- Name: t_skpdkbt t_skpdkbt_t_idskpdkb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idskpdkb_foreign FOREIGN KEY (t_idskpdkb) REFERENCES public.t_skpdkb(t_idskpdkb);


--
-- Name: t_skpdkbt t_skpdkbt_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_skpdkbt t_skpdkbt_t_idtarifbphtb_skpdkbt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idtarifbphtb_skpdkbt_foreign FOREIGN KEY (t_idtarifbphtb_skpdkbt) REFERENCES public.s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_skpdkbt t_skpdkbt_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_spt t_spt_t_b_idkec_pngjwb_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_b_idkec_pngjwb_pembeli_foreign FOREIGN KEY (t_b_idkec_pngjwb_pembeli) REFERENCES public.s_kecamatan(s_idkecamatan);


--
-- Name: t_spt t_spt_t_b_idkec_pngjwb_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_b_idkec_pngjwb_penjual_foreign FOREIGN KEY (t_b_idkec_pngjwb_penjual) REFERENCES public.s_kecamatan(s_idkecamatan);


--
-- Name: t_spt t_spt_t_b_idkel_pngjwb_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_b_idkel_pngjwb_pembeli_foreign FOREIGN KEY (t_b_idkel_pngjwb_pembeli) REFERENCES public.s_kelurahan(s_idkelurahan);


--
-- Name: t_spt t_spt_t_b_idkel_pngjwb_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_b_idkel_pngjwb_penjual_foreign FOREIGN KEY (t_b_idkel_pngjwb_penjual) REFERENCES public.s_kelurahan(s_idkelurahan);


--
-- Name: t_spt t_spt_t_id_jenistanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_id_jenistanah_foreign FOREIGN KEY (t_id_jenistanah) REFERENCES public.s_jenistanah(id_jenistanah);


--
-- Name: t_spt t_spt_t_idbidang_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idbidang_penjual_foreign FOREIGN KEY (t_idbidang_penjual) REFERENCES public.s_jenis_bidangusaha(s_idbidang_usaha);


--
-- Name: t_spt t_spt_t_idbidang_usaha_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idbidang_usaha_foreign FOREIGN KEY (t_idbidang_usaha) REFERENCES public.s_jenis_bidangusaha(s_idbidang_usaha);


--
-- Name: t_spt t_spt_t_idjenisdoktanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idjenisdoktanah_foreign FOREIGN KEY (t_idjenisdoktanah) REFERENCES public.s_jenisdoktanah(s_iddoktanah);


--
-- Name: t_spt t_spt_t_idjenisfasilitas_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idjenisfasilitas_foreign FOREIGN KEY (t_idjenisfasilitas) REFERENCES public.s_jenisfasilitas(s_idjenisfasilitas);


--
-- Name: t_spt t_spt_t_idjenishaktanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idjenishaktanah_foreign FOREIGN KEY (t_idjenishaktanah) REFERENCES public.s_jenishaktanah(s_idhaktanah);


--
-- Name: t_spt t_spt_t_idjenisketetapan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idjenisketetapan_foreign FOREIGN KEY (t_idjenisketetapan) REFERENCES public.s_jenisketetapan(s_idjenisketetapan);


--
-- Name: t_spt t_spt_t_idjenistransaksi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idjenistransaksi_foreign FOREIGN KEY (t_idjenistransaksi) REFERENCES public.s_jenistransaksi(s_idjenistransaksi);


--
-- Name: t_spt t_spt_t_idkec_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idkec_pembeli_foreign FOREIGN KEY (t_idkec_pembeli) REFERENCES public.s_kecamatan(s_idkecamatan);


--
-- Name: t_spt t_spt_t_idkec_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idkec_penjual_foreign FOREIGN KEY (t_idkec_penjual) REFERENCES public.s_kecamatan(s_idkecamatan);


--
-- Name: t_spt t_spt_t_idkel_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idkel_pembeli_foreign FOREIGN KEY (t_idkel_pembeli) REFERENCES public.s_kelurahan(s_idkelurahan);


--
-- Name: t_spt t_spt_t_idkel_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idkel_penjual_foreign FOREIGN KEY (t_idkel_penjual) REFERENCES public.s_kelurahan(s_idkelurahan);


--
-- Name: t_spt t_spt_t_idnotaris_spt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idnotaris_spt_foreign FOREIGN KEY (t_idnotaris_spt) REFERENCES public.s_notaris(s_idnotaris);


--
-- Name: t_spt t_spt_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES public.users(id);


--
-- Name: t_spt t_spt_t_idpengurangan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idpengurangan_foreign FOREIGN KEY (t_idpengurangan) REFERENCES public.s_jenispengurangan(s_idpengurangan);


--
-- Name: t_spt t_spt_t_idstatus_npwpd_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idstatus_npwpd_foreign FOREIGN KEY (t_idstatus_npwpd) REFERENCES public.s_status_npwpd(s_idstatus_npwpd);


--
-- Name: t_spt t_spt_t_idtarifbphtb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idtarifbphtb_foreign FOREIGN KEY (t_idtarifbphtb) REFERENCES public.s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_spt t_spt_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_validasi_berkas t_validasi_berkas_s_id_status_berkas_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_s_id_status_berkas_foreign FOREIGN KEY (s_id_status_berkas) REFERENCES public.s_status_berkas(s_id_status_berkas);


--
-- Name: t_validasi_berkas t_validasi_berkas_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_validasi_berkas t_validasi_berkas_t_iduser_validasi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_t_iduser_validasi_foreign FOREIGN KEY (t_iduser_validasi) REFERENCES public.users(id);


--
-- Name: t_validasi_kabid t_validasi_kabid_s_id_status_kabid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_s_id_status_kabid_foreign FOREIGN KEY (s_id_status_kabid) REFERENCES public.s_status_kabid(s_id_status_kabid);


--
-- Name: t_validasi_kabid t_validasi_kabid_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_validasi_kabid t_validasi_kabid_t_iduser_validasi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_t_iduser_validasi_foreign FOREIGN KEY (t_iduser_validasi) REFERENCES public.users(id);


--
-- Name: users users_s_id_hakakses_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_s_id_hakakses_foreign FOREIGN KEY (s_id_hakakses) REFERENCES public.s_hak_akses(s_id_hakakses);


--
-- Name: users users_s_idnotaris_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_s_idnotaris_foreign FOREIGN KEY (s_idnotaris) REFERENCES public.s_notaris(s_idnotaris);


--
-- Name: users users_s_idpejabat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_s_idpejabat_foreign FOREIGN KEY (s_idpejabat) REFERENCES public.s_pejabat(s_idpejabat);


--
-- PostgreSQL database dump complete
--

