-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2021 at 04:26 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kampus`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(255) NOT NULL,
  `nama_barang` varchar(255) DEFAULT NULL,
  `harga_barang` int(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`) VALUES
('123', 'minyak', 2000),
('230', 'botol', 3000),
('412', 'mintol', 2000),
('546', 'fghj', 97);

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `id` int(32) NOT NULL,
  `kode_buku` varchar(255) DEFAULT NULL,
  `judul_buku` varchar(255) DEFAULT NULL,
  `penulis` varchar(255) DEFAULT NULL,
  `penerbit` varchar(255) DEFAULT NULL,
  `tahun_terbit` int(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id`, `kode_buku`, `judul_buku`, `penulis`, `penerbit`, `tahun_terbit`) VALUES
(1, 'buku001', 'Laskar Pewangi', 'Frennandi Ade Ilyas', 'Unpam', 2005),
(2, 'buku002', 'Udin 1990', 'Udin', 'Unpam', 2006),
(3, 'buku003', 'Samsul', 'Samsudin', 'Pamulang', 2009);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `kode_barang` int(32) NOT NULL,
  `nama_barang` varchar(255) DEFAULT NULL,
  `total_barang_tersisa` int(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`kode_barang`, `nama_barang`, `total_barang_tersisa`) VALUES
(1, 'minyak', 20),
(2, 'botol', 30),
(3, 'kartu', 50);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_siswa` int(32) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `rata` int(32) DEFAULT NULL,
  `nim` varchar(255) DEFAULT NULL,
  `nilai_1` int(32) DEFAULT NULL,
  `nilai_2` int(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_siswa`, `nama`, `rata`, `nim`, `nilai_1`, `nilai_2`) VALUES
(1, 'ade', 85, '213131', 80, 90),
(2, 'ilyas', 70, '123123213', 70, 70),
(3, 'frennandi', 79, '123123131', 68, 90);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `nama` varchar(255) DEFAULT NULL,
  `no_hp` varchar(255) DEFAULT NULL,
  `kode_member` int(32) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`nama`, `no_hp`, `kode_member`, `alamat`) VALUES
('ade', '214312312', 123, 'jalan rempoa');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_barang` int(32) NOT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `barang` varchar(255) DEFAULT NULL,
  `no_hp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_barang`, `nama_perusahaan`, `barang`, `no_hp`) VALUES
(124, 'mitra', 'minyak', '012312312'),
(125, 'mitra', 'burung bangau', '012312312');

-- --------------------------------------------------------

--
-- Table structure for table `tanah`
--

CREATE TABLE `tanah` (
  `id_tanah` int(32) NOT NULL,
  `nama_pemesan` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `tipe_rumah` varchar(255) DEFAULT NULL,
  `luas_tanah` varchar(255) DEFAULT NULL,
  `harga` varchar(255) DEFAULT NULL,
  `lama_cicilan` varchar(255) DEFAULT NULL,
  `cicilan_bulan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `nama_customer` varchar(255) DEFAULT NULL,
  `kode_transaksi` int(32) NOT NULL,
  `total_bayar` bigint(20) DEFAULT NULL,
  `jenis_barang` varchar(255) DEFAULT NULL,
  `total_barang` int(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`nama_customer`, `kode_transaksi`, `total_bayar`, `jenis_barang`, `total_barang`) VALUES
('Ade', 1, 200000, 'Minyak', 2),
('ilyas', 2, 300000, 'botol', 4),
('Frennandi', 3, 400000, 'sup', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`) USING BTREE;

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`kode_member`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `tanah`
--
ALTER TABLE `tanah`
  ADD PRIMARY KEY (`id_tanah`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`kode_transaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_siswa` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tanah`
--
ALTER TABLE `tanah`
  MODIFY `id_tanah` int(32) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
