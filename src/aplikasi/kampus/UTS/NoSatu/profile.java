/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.kampus.UTS.NoSatu;

import java.util.Scanner;

/**
 *
 * @author ade
 */
public class profile {
    public static void main(String[] args) {
        int gapok,tunjangan,lembur;
        karyawan[] data = new karyawan[2];
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < 2; i++) {
            data[i] = new karyawan();
            System.out.print("Masukan Data NIK ke " + (i+1) + " : ");
            data[i].setNik(input.nextLong());

            System.out.print("Masukan Data Nama ke " + (i+1) + " : ");
            data[i].setNama(input.next());

            System.out.print("Masukan Data Gapok ke " + (i+1) + " : ");
            gapok = input.nextInt();
            data[i].setGajiPokok(gapok);

            System.out.print("Masukan Data Jenis Kelamin ke " + (i+1) + " : ");
            data[i].setJenisKelamin(input.next());

            System.out.print("Masukan Data Tanggal Lahir ke " + (i+1) + " : ");
            data[i].setTanggalLahir(input.next());

            System.out.print("Masukan Data Tunjangan ke " + (i+1) + " : ");
            tunjangan = input.nextInt();
            data[i].setTunjangan(tunjangan);

            System.out.print("Masukan Data Lembur ke " + (i+1) + " : ");
            lembur = input.nextInt();
            data[i].setLembur(lembur);

            data[i].setBpjs(60000);
            data[i].setPajak21(97500);
            data[i].setTotalPemasukan(gapok+tunjangan+lembur);
            data[i].setTotalPengurangan(97500+60000);
            data[i].setTotalGajiBersih(data[i].getTotalPemasukan() - data[i].getTotalPengurangan());
        }

            System.out.println("==========================================================================================================================================");
            System.out.println("No. \t|\t NIK \t|\t Nama \t|\t JK \t|\t Tgl Lahir \t|\t GAPOK \t|\t Tunjangan \t|\t Lembur");
            System.out.println("==========================================================================================================================================");
            for (int i = 0; i < data.length; i++) {
                System.out.println((i+1) + "\t\t" + data[i].getNik() + " " + data[i].getNama() + "\t\t" + data[i].getJenisKelamin() + "\t\t" + data[i].getTanggalLahir() + "\t\t" + data[i].getGajiPokok() + "\t\t" + data[i].getTunjangan() + "\t\t" + data[i].getLembur());
                System.out.println("==========================================================================================================================================");
                System.out.println("Total Pemasukan : Rp." + data[i].getTotalPemasukan());
                System.out.println("Total Pengurangan : Rp." + data[i].getTotalPengurangan());
                System.out.println("Total Gaji Bersih : Rp." + data[i].getTotalGajiBersih());
                System.out.println("==========================================================================================================================================");
            }
    }
}
