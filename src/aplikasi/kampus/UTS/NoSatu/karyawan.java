/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.kampus.UTS.NoSatu;

/**
 *
 * @author ade
 */
public class karyawan {
    private Long nik;
    private String nama;
    private String jenisKelamin;
    private String tanggalLahir;
    private Integer gajiPokok;
    private Integer tunjangan;
    private Integer lembur;
    private Integer bpjs;
    private Integer pajak21;
    private Integer totalPemasukan;
    private Integer totalPengurangan;
    private Integer totalGajiBersih;

    public Integer getTotalPemasukan() {
        return totalPemasukan;
    }

    public void setTotalPemasukan(Integer totalPemasukan) {
        this.totalPemasukan = totalPemasukan;
    }

    public Integer getTotalPengurangan() {
        return totalPengurangan;
    }

    public void setTotalPengurangan(Integer totalPengurangan) {
        this.totalPengurangan = totalPengurangan;
    }

    public Integer getTotalGajiBersih() {
        return totalGajiBersih;
    }

    public void setTotalGajiBersih(Integer totalGajiBersih) {
        this.totalGajiBersih = totalGajiBersih;
    }

    public Long getNik() {
        return nik;
    }

    public void setNik(Long nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public Integer getGajiPokok() {
        return gajiPokok;
    }

    public void setGajiPokok(Integer gajiPokok) {
        this.gajiPokok = gajiPokok;
    }

    public Integer getTunjangan() {
        return tunjangan;
    }

    public void setTunjangan(Integer tunjangan) {
        this.tunjangan = tunjangan;
    }

    public Integer getLembur() {
        return lembur;
    }

    public void setLembur(Integer lembur) {
        this.lembur = lembur;
    }

    public Integer getBpjs() {
        return bpjs;
    }

    public void setBpjs(Integer bpjs) {
        this.bpjs = bpjs;
    }

    public Integer getPajak21() {
        return pajak21;
    }

    public void setPajak21(Integer pajak21) {
        this.pajak21 = pajak21;
    }


}
