/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.kampus.latihan2;

import aplikasi.kampus.latihan2.Mhs;

/**
 *
 * @author ade
 */
import java.util.Scanner;

public class latihan2 {
    public static void main(String[] args) {
        for (String b = "y"; b.equals("y") || b.equals("Y"); ) {

        Mhs[] data;
        Scanner input = new Scanner(System.in);
        float diskon, caraBayar;


            System.out.print("masukan Nama anda : ");
            String namaMhs = input.nextLine();
            System.out.print("Masukan Jumlah Barang : ");
            int jumlahInput = input.nextInt();
            data = new Mhs[jumlahInput];
            System.out.println();
            for (int i = 0; i < jumlahInput; i++) {
                data[i] = new Mhs();
                System.out.print("masukan Kode Barang ke-" + (i + 1) + " anda : ");
                data[i].setKodeBarang(input.next());

                System.out.print("masukan Nama Barang ke-" + (i + 1) + " anda : ");
                data[i].setNamaBarang(input.next());

                System.out.print("masukan Total Barang ke-" + (i + 1) + " anda : ");
                int jumlahBarang = input.nextInt();
                data[i].setTotalBarang(jumlahBarang);

                System.out.print("masukan Harga Barang ke-" + (i + 1) + " anda : ");
                int hargaBarang = input.nextInt();
                data[i].setHargaBarang(hargaBarang);

                data[i].setTotal(hargaBarang * jumlahBarang);
                System.out.println();
            }

            System.out.println("pilih system bayar : \n1. visa-master \n2.biasa");
            int systemBayar = input.nextInt();

            System.out.println("Data Penjualan Barang");
            System.out.println("PT \"Pamulang Tama\"");
            System.out.println("Nama Kasir : " + namaMhs);
            System.out.println("================================================================================");
            System.out.println("No. \t Kode Barang \t Nama Barang \t\t Harga Barang \t Jumlah Beli \t Bayar");
            System.out.println("================================================================================");
            int totalBayar = 0;
            for (int i = 0; i < data.length; i++) {
                int no = i + 1;
                System.out.println(no + " \t\t\t " + data[i].getKodeBarang() + " \t\t\t " + data[i].getNamaBarang() + " \t\t\t " + data[i].getHargaBarang() + " \t\t\t " + data[i].getTotalBarang() + " \t\t\t " + data[i].getTotal() + "");
                totalBayar = totalBayar + data[i].getTotal();
            }

            if (totalBayar >= 500000) {
                diskon = totalBayar * (float) 0.05;
            } else {
                diskon = 0;
            }

            if (systemBayar == 1) {
                caraBayar = totalBayar * (float) 0.03;
            } else {
                caraBayar = 0;
            }

            System.out.println("Jumlah Bayar \t\t: " + totalBayar);
            System.out.println("Diskon \t\t\t\t: " + (int) diskon);
            System.out.println("PPN \t\t\t\t: " + (int) (totalBayar * 0.1));
            System.out.println("Cara Bayar \t\t\t: " + totalBayar + "\tBiaya: Kartu Rp." + (int) caraBayar);
            System.out.println("Total Bayar \t\t: " + new totalHitung(diskon, caraBayar, totalBayar, (float) (totalBayar * 0.1)).getTotal());
            System.out.println("Apakah Mau mengulang ? Y/N ");
            b = input.next();

        }
    }

}

class totalHitung {
    float totalnya;

    public totalHitung(float diskon, float caraBayar, int totalBayar, float ppn) {
        totalnya = (float) totalBayar - diskon;
        totalnya = totalnya + ppn + caraBayar;
    }


    public float getTotal(){
        return this.totalnya;
    }

}
