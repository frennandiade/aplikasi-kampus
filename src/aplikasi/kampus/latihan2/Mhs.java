/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplikasi.kampus.latihan2;

/**
 *
 * @author indah
 */
public class Mhs {
    private String namaBarang,kodeBarang;
    private Integer hargaBarang,total,totalBarang;
    private float totalBayar;


    public float getTotalBayar() {
        return totalBayar;
    }

    public void setTotalBayar(float totalBayar) {
        this.totalBayar = totalBayar;
    }

    public Integer getTotalBarang() {
        return totalBarang;
    }

    public void setTotalBarang(Integer totalBarang) {
        this.totalBarang = totalBarang;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public Integer getHargaBarang() {
        return hargaBarang;
    }

    public int setHargaBarang(Integer hargaBarang) {
        this.hargaBarang = hargaBarang;
        return hargaBarang;
    }
}
